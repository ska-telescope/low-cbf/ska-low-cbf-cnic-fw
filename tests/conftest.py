import os

import pytest
from dpkt.pcap import UniversalReader, Writer
from ska_low_cbf_fpga import create_driver_map_info, mem_parse
from ska_low_cbf_sw_cnic import CnicFpga

BIG_PCAP_NAME = "three_gb.pcap"
BIG_PCAP_SIZE = 3 << 30


def create_big_pcap(filename: str, size=4 << 30):
    """Create a PCAP file of at least the given size in bytes (includes overheads)."""
    input_filename = "tests/10_packets_8306B.pcap"
    with open(filename, "wb") as output_file:
        datalink = UniversalReader(open(input_filename, "rb")).datalink()
        out_wr = Writer(output_file, linktype=datalink)
        while output_file.tell() < size:
            out_wr.writepkts(UniversalReader(open(input_filename, "rb")))


def pytest_sessionstart(session):
    """Start of pytest session hook."""
    if (
        os.path.exists(BIG_PCAP_NAME)
        and os.path.getsize(BIG_PCAP_NAME) >= BIG_PCAP_SIZE
    ):
        print(f"Yay, big PCAP {BIG_PCAP_NAME} already exists.")
    else:
        print(f"Creating a big PCAP file {BIG_PCAP_NAME} ...")
        create_big_pcap(BIG_PCAP_NAME, size=BIG_PCAP_SIZE)
        print("Done.")


def pytest_sessionfinish(session):
    """End of pytest session hook."""
    # os.remove(BIG_PCAP_NAME)


@pytest.fixture(scope="session")
def cnics():
    """Create CnicFpga (FpgaPersonality) objects."""
    target_alveo = os.environ.get("TARGET_ALVEO", "no-target-alveo").lower()
    if "u50" in target_alveo:
        # BDFs for ALVEOs in dev09, u280 = 3, u50lv = 1
        bdfs = ["0000:03:00.1", "0000:01:00.1"]
        memory = mem_parse("2048Ms:2048Ms:2048Ms:2048Ms")
        xcl_files = [
            "firmware_u280/cnic.xclbin",
            "firmware_u50lv/cnic.xclbin",
        ]  # Assuming one xcl_file per BDF
        devices = {
            bdf: create_driver_map_info(
                device=bdf, firmware_path=xcl_file, mem_config=memory
            )
            for bdf, xcl_file in zip(bdfs, xcl_files)
        }
        """BDF (str): (driver, map, info)"""

        # fpga-dev09 has no PTP
        cnics = [
            CnicFpga(driver, args_map, hw_info)
            for driver, args_map, hw_info in devices.values()
        ]
    else:
        # BDFs for ALVEOs in dev07, u50 = 7, u55c = 8
        bdfs = ["0000:08:00.1", "0000:07:00.1"]
        memories = [
            mem_parse("4095Ms:4095Ms:4095Ms:4095Ms"),
            mem_parse("2048Ms:2048Ms:2048Ms:2048Ms"),
        ]
        # u55c firmware directory has no suffix
        xcl_files = ["firmware/cnic.xclbin", "firmware_u50/cnic.xclbin"]

        devices = {
            bdf: create_driver_map_info(
                device=bdf, firmware_path=xcl_file, mem_config=memory
            )
            for bdf, xcl_file, memory in zip(bdfs, xcl_files, memories)
        }
        """BDF (str): (driver, map, info)"""

        cnics = [
            CnicFpga(driver, args_map, hw_info)
            for driver, args_map, hw_info in devices.values()
        ]
        # fpga-dev07 has PTP connected to port B of both FPGA cards
        # But... if port B is not locked, PTP time will always be zero!
        for cnic in cnics:
            if cnic.system.eth100g_b_locked:
                print("Ethernet B active, using B for PTP")
                cnic.timeslave.ptp_source_select = 1
            else:
                print("Ethernet B disconnected, using A for PTP")

    versions = [cnic.fw_version.value for cnic in cnics]
    print(f"CNIC FW Versions: {versions}")
    return cnics
