# -*- coding: utf-8 -*-
#
# This file is part of the SKA Low CBF project
#
# Copyright (c) 2023 CSIRO
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more info.
"""Check CNIC protection against accidental duplex when not in duplex mode."""


def test_duplex_guard(cnics):
    """Test the software guards against illegal duplexing."""
    fpga = cnics[0]
    print("Turning off Duplex mode")
    fpga.hbm_pktcontroller.duplex = False
    print("Starting Rx")
    fpga.receive_pcap("dummy.pcap", 1024, 100_000)

    exception_caught = False
    try:
        print("Starting Tx")
        fpga.transmit_pcap("tests/10_multi_length.pcap")
    except RuntimeError as rte:
        print("Successfully Caught Exception:")
        print(rte)
        exception_caught = True

    fpga.stop_receive()

    assert exception_caught, "Duplex guard failed to protect us!"
