# -*- coding: utf-8 -*-
#
# This file is part of the SKA Low CBF project
#
# Copyright (c) 2023 CSIRO
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more info.
"""Verify CNIC Virtual Digitiser operation."""
import time
from datetime import datetime
from time import sleep
from typing import Iterable

import numpy as np
import pytest
from dpkt.pcap import UniversalReader
from ska_low_cbf_fpga import FpgaPersonality
from ska_low_cbf_sw_cnic import (
    NULL_SOURCE,
    TIME_STR_FORMAT,
    CnicFpga,
    Source,
    StreamConfig,
    VDChannelConfig,
)

from .test_cnic import (
    SPS_HEADER_SIZE_VERSION,
    debug_register_dump,
    rx_wait_success,
    this_test_filename,
)


def ptp_working(cnics: Iterable[CnicFpga]) -> bool:
    """Is PTP Working for all CNICs?"""
    # give PTP a chance to get started
    while min_fpga_uptime(cnics) < 20:
        sleep(5)
    return all(
        [abs(time.time() - cnic.timeslave.unix_timestamp.value) < 60 for cnic in cnics]
    )


def min_fpga_uptime(fpgas: Iterable[FpgaPersonality]):
    """The minimum FPGA uptime for a given list of FPGAs."""
    return min([fpga.system.time_uptime.value for fpga in fpgas])


PTP_STARTUP_MINUTES = 6


def ptp_stable(cnics: Iterable[CnicFpga]) -> bool:
    # using timeslave_b because dev03 cards have PTP on port B
    deltas = [cnic.timeslave_b.blk1_last_delta.value for cnic in cnics]
    print(f"PTP update deltas: {deltas}")
    deltas = [abs(delta) for delta in deltas]  # for checking against threshold

    # note that register reads are not simultaneous
    unix_times = [cnic.timeslave.unix_timestamp.value for cnic in cnics]
    clock_diff = max(unix_times) - min(unix_times)
    print(f"CNIC clock difference (approximate): {clock_diff}")
    return max(deltas) < 500 and clock_diff < (0.000_5 * (len(cnics) - 1))


SECONDS_PER_YEAR = 365.25 * 86400  # not entirely accurate but close enough for us


@pytest.mark.timeout(600)  # PTP can take a while to stabilise
@pytest.mark.parametrize(
    "sps_packet_version, ska_time",
    [
        (2, None),  # SPS v2 doesn't use SKA time
        (3, 0),  # 2000-01-01 00:00
        (3, 20 * SECONDS_PER_YEAR),  # ~ 2020-01-01
        (3, 30 * SECONDS_PER_YEAR),  # ~ 2030-01-01
    ],
)
@pytest.mark.parametrize(
    "stations",
    [
        pytest.param({345, 350, 352, 355, 431, 434}, id="576streams"),
        pytest.param(set(range(18)), id="1728streams"),
    ],
)
@pytest.mark.parametrize("direction", ("forward", "reverse"))
def test_vd_burst_period(cnics, ska_time, sps_packet_version, stations, direction):
    """Verify Virtual Digitiser burst period."""
    freq_channels = list(range(100, 196))
    spead_header_length = SPS_HEADER_SIZE_VERSION[sps_packet_version]
    packet_length = 8234 + spead_header_length

    if direction == "forward":
        vd_cnic = cnics[0]
        rx_cnic = cnics[1]
    if direction == "reverse":
        vd_cnic = cnics[1]
        rx_cnic = cnics[0]

    if not bool(vd_cnic.system.datagen_1_capable):
        pytest.skip("No Virtual Digitiser Capability")

    print("")
    print("VD CNIC:")
    print(f"\t{vd_cnic.fw_version.value}")
    print(f'\t{vd_cnic.info["platform"]["static_region"]["vbnv"]}')
    print("Rx CNIC:")
    print(f"\t{rx_cnic.fw_version.value}")
    print(f'\t{rx_cnic.info["platform"]["static_region"]["vbnv"]}')
    rx_filename = this_test_filename("test_results", "pcap")
    print(f"Rx filename: '{rx_filename}'")

    for cnic in cnics:
        cnic.reset()

    if ptp_working(cnics):
        print("PTP is active")
        sleep_start = time.time()
        while not ptp_stable(cnics):
            sleep(10)
        print(f"PTP stable after {time.time() - sleep_start:.0f} s")
    else:
        # it would be better to use the marker, but I can't figure out how to get the
        # PTP working condition into the marker...
        ### pytest.xfail("PTP not active")
        print("Trying without PTP anyway lulz")

    print("VD CNIC time", vd_cnic.timeslave.time.value)
    print("Rx CNIC time", rx_cnic.timeslave.time.value)
    rx_filename = this_test_filename("test_results", "pcap")

    streams = [
        StreamConfig(
            spead_stream=VDChannelConfig(
                scan=1234,
                beam=1,
                frequency=freq,
                substation=1,
                subarray=1,
                station=stn,
            ),
            sources=[NULL_SOURCE] * 8,
        )
        for freq in freq_channels
        for stn in stations
    ]
    n_streams = len(streams)
    print(f"Channel configuration created with {len(streams)} entries")
    rx_cnic.receive_pcap(rx_filename, packet_length, n_packets=n_streams * 30)

    start_time_unix = vd_cnic.timeslave.unix_timestamp + 4
    start_time = datetime.fromtimestamp(start_time_unix)
    start_time_str = start_time.strftime(TIME_STR_FORMAT)
    print(f"Starting Virtual Digitiser at {start_time_str}")
    vd_cnic.configure_vd(
        streams, sps_packet_version=sps_packet_version, ska_time=ska_time
    )
    print("packet_counter_u", vd_cnic.vd.packet_counter_u.value)
    print("packet_counter", vd_cnic.vd.packet_counter.value)
    if n_streams > 1024:
        # very crude adjustment of packet period...
        # TODO - add as parameter to configure ?
        vd_cnic.vd.time_between_packets = 100

    # TODO - should be in CNIC-SW
    vd_cnic.vd.use_ptp_to_begin = True
    vd_cnic.timeslave.tx_start_time = start_time_str

    vd_cnic.enable_vd = True
    # start generating packets with delays applied
    vd_cnic.vd_datagen.enable_vd = True
    vd_cnic.vd_datagen_2.enable_vd = True

    # allowing 1 extra second for VD to do its business
    while vd_cnic.timeslave.unix_timestamp < (start_time_unix + 1):
        sleep(1)

    if not rx_wait_success(rx_cnic):
        debug_register_dump(vd_cnic)

    print("Stopping Virtual Digitiser")
    vd_cnic.reset()

    # Check burst period
    burst_time_nominal = vd_cnic.vd.time_between_channel_bursts.value * 1e-9
    print("Nominal burst time", burst_time_nominal)
    burst_time_margin = 0.000_500  # seconds
    print(
        "Allowable burst time range:",
        (burst_time_nominal - burst_time_margin),
        (burst_time_nominal + burst_time_margin),
    )
    packets_in_burst = None
    burst_stats = {0: (0, 0)}
    """burst number: (elapsed time, packet number)"""
    next_burst_time_nominal = burst_time_nominal
    with open(rx_filename, "rb") as vd_file:
        reader = UniversalReader(vd_file)
        for n, (ts, packet) in enumerate(reader):
            burst_number = n // n_streams
            if n == 0:
                t0 = ts
            if n % n_streams == 0 and n > 0:
                expected_time = burst_number * burst_time_nominal
                t = ts - t0
                error = expected_time - float(t)
                print(f"Packet {n} at {t:.9f} sec, error {error:.9f} sec (from t0)")

            if (ts - t0) >= (next_burst_time_nominal - burst_time_margin):
                # time delta is greater than expected burst duration,
                # so this must be the next burst
                if packets_in_burst is None:
                    packets_in_burst = n
                if burst_number not in burst_stats:
                    burst_stats[burst_number] = ((ts - t0), n)

    # enforce burst time criteria
    for burst, packet_number in burst_stats.items():
        if burst > 0:
            period = burst_stats[burst][0] - burst_stats[burst - 1][0]
            packets = burst_stats[burst][1] - burst_stats[burst - 1][1]
            print(f"Burst {burst} - duration {period} sec, {packets} packets")
            assert packets == n_streams, "Wrong number of packets in burst"
            assert (
                (burst_time_nominal - burst_time_margin)
                <= period
                <= (burst_time_nominal + burst_time_margin)
            ), "Burst period out of range"


@pytest.mark.parametrize(
    "pulsar_start, pulsar_on, pulsar_off",
    [
        (2048 * 3, 2048 // 2, 2048 * 2),
        (32, 64, 128),
        (2048 * 2, int(2048 * 1.75), int(2048 * 5.3)),
    ],
)
@pytest.mark.parametrize("sps_packet_version", [2, 3])
@pytest.mark.parametrize("direction", ("forward",))
def test_vd_pulsar_mode(
    cnics,
    pulsar_start,
    pulsar_on,
    pulsar_off,
    sps_packet_version,  # 2 means v2
    direction,
):
    """Verify Virtual Digitiser pulsar mode (amplitude gate)."""
    spead_header_length = SPS_HEADER_SIZE_VERSION[sps_packet_version]
    packet_length = 8234 + spead_header_length
    stations = [350]
    freq_channels = [100]

    if direction == "forward":
        vd_cnic = cnics[0]
        rx_cnic = cnics[1]
    if direction == "reverse":
        vd_cnic = cnics[1]
        rx_cnic = cnics[0]

    if not bool(vd_cnic.system.datagen_1_capable):
        pytest.skip("No Virtual Digitiser Capability")

    print("")
    print("VD CNIC:")
    print(f"\t{vd_cnic.fw_version.value}")
    print(f'\t{vd_cnic.info["platform"]["static_region"]["vbnv"]}')
    print("Rx CNIC:")
    print(f"\t{rx_cnic.fw_version.value}")
    print(f'\t{rx_cnic.info["platform"]["static_region"]["vbnv"]}')
    rx_filename = this_test_filename("test_results", "pcap")
    print(f"Rx filename: '{rx_filename}'")

    for cnic in cnics:
        cnic.reset()

    rx_filename = this_test_filename("test_results", "pcap")

    streams = [
        StreamConfig(
            spead_stream=VDChannelConfig(
                scan=1234,
                beam=1,
                frequency=freq,
                substation=1,
                subarray=1,
                station=stn,
            ),
            # single source, different seeds for X & Y pol.
            sources=(
                [
                    Source(
                        tone=False,
                        seed=1234,
                        scale=4000,
                        channel_frequency=freq * 781_250 * 1e-9,
                    )
                ]
                + [NULL_SOURCE] * 3
                + [
                    Source(
                        tone=False,
                        seed=5678,
                        scale=4000,
                        channel_frequency=freq * 781_250 * 1e-9,
                    )
                ]
                + [NULL_SOURCE] * 3
            ),
        )
        for freq in freq_channels
        for stn in stations
    ]
    rx_cnic.receive_pcap(rx_filename, packet_length, n_packets=100)
    vd_cnic.configure_vd(streams, sps_packet_version=sps_packet_version)
    vd_cnic.vd.configure_pulsar_mode(True, (pulsar_start, pulsar_on, pulsar_off))

    vd_cnic.enable_vd = True
    vd_cnic.vd_datagen.enable_vd = True  # Only happens in SW-CNIC with delay updates
    vd_cnic.vd_datagen_2.enable_vd = True  # Only happens in SW-CNIC with delay updates

    while not rx_cnic.finished_receive:
        sleep(1)

    vd_cnic.reset()

    # Analysis
    # aggregate a few cycles (makes us very unlikely to see zeros in the ON portion)
    iterations = 8
    total_samples = pulsar_start + iterations * (pulsar_on + pulsar_off)
    # guarantee analyse have enough packets
    n_packets = (total_samples // 2048) + 1
    assert n_packets <= 100  # we only asked for 100 packets to be captured...
    total_bytes = n_packets * 2048 * 4
    spead_payload = np.zeros(total_bytes, dtype=np.int8)
    # we will chop off first 42+72 bytes (Ethernet, IP, UDP, SPEAD headers) of packets
    headers_len = 42 + spead_header_length
    with open(rx_filename, "rb") as vd_file:
        reader = UniversalReader(vd_file)
        offset = 0
        while offset < total_bytes:
            ts, packet = next(reader)
            packet = np.frombuffer(packet, dtype=np.int8)
            end_addr = offset + len(packet) - headers_len
            spead_payload[offset:end_addr] = packet[headers_len:]
            offset = end_addr

        # pulsar_start * 4 bytes should be zero
        start_bytes = pulsar_start * 4
        start_zero_data = spead_payload[:start_bytes]
        assert (
            np.count_nonzero(start_zero_data) == 0
        ), "Non-zero data in initial OFF period"
        on_bytes = pulsar_on * 4
        off_bytes = pulsar_off * 4
        on_data = np.zeros(on_bytes, dtype=np.int64)
        off_data = np.zeros(off_bytes, dtype=np.int64)

        for i in range(iterations):
            on_start = start_bytes + i * (on_bytes + off_bytes)
            off_start = on_start + on_bytes
            on_data += np.abs(spead_payload[on_start:off_start])
            off_data += np.abs(spead_payload[off_start : off_start + off_bytes])

        assert np.count_nonzero(on_data) == on_bytes, "Zeros in ON period"
        assert np.count_nonzero(off_data) == 0, "Non-Zeros in OFF period"
