% Get interpolation filters for CNIC data generation
% Generates 16-bit filter taps (i.e. limited to the range +/-32768)
% with the same DC response for all the filters,

f1 = getInterpFilters(16,2048,1);
writeCOE = 1;

% maximum possible value that can be returned when interpolating
% 1 bit data
f1_max_abs_sum = max(sum(abs(f1),2));

% Scale factor so that the DC response is 16384
f1_scale = 16384;

% Adjust the scale of individual filters so that they all have the 
% same DC response after rounding to 9-bit values.
f_scaled = zeros(2048,16);
all_scales = zeros(2048,1);
for filt = 1:2048
    this_scale = f1_scale;
    while (sum((round(this_scale * f1(filt,:)))) > floor(f1_scale))
        this_scale = this_scale * 0.99999999; 
    end
    while (sum((round(this_scale * f1(filt,:)))) < floor(f1_scale))
        this_scale = this_scale * 1.00000001;
    end
    f_scaled(filt,:) = round(this_scale * f1(filt,:));
    all_scales(filt) = this_scale;
end

disp([' max scale = ' num2str(max(all_scales)) ', min scale = ' num2str(min(all_scales))]);
DC_response = sum(f_scaled,2);
disp([' Max DC response = ' num2str(max(DC_response)) ', min DC response = ' num2str(min(DC_response))]);
max_response = sum(abs(f_scaled),2);
disp([' Max response = ' num2str(max(max_response)) ', min max response = ' num2str(min(max_response))]);

if (writeCOE)
    fid = fopen(['interp_taps_16bit.coe'],'w');
    fprintf(fid,'memory_initialization_radix = 2;\n');
    fprintf(fid,'memory_initialization_vector = ');
    for filterSel = 1:2048
        dstr = [];
        dstr(1:256) = '0';
        for tap = 1:16
            dstr(((tap-1)*16 + 1):((tap-1)*16 + 16)) = dec2binX(f_scaled(filterSel,tap),16);
        end
        fprintf(fid,['\n' dstr]);
    end
    fprintf(fid,';\n');
    fclose(fid);
end

