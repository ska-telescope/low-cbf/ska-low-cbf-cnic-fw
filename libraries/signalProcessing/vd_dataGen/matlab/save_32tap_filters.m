% Write 32 tap filters to .coe and .txt files to be used by the firmware
% and by the python code.
% coe for the firmware only saves the first half of the filters
% i.e. for offsets from (0 to 0.5]
% Out of 2048 total filters with offsets 0, 1/2048, 2/2048,... 2047/2048
% the filters which are saved are 1 to 1024
% Filter 0 is a special case (single unity value in the filter)
% Filters 1025 to 2047 are derived in the firmware by reversing filters 
% 1023:-1:1

clear all;

fft_samples = 8192;
valid_low = 638;
valid_high = 7554;
f_taps = 32;
writeCOE = 1;
run_from_scratch = 0;
do_plots = 0;

% get f_scaled, (optimised versions of the filters)
load optimised_32tap9

% Get the interpolation filter designed by Matlab
h_ml = zeros(2048,f_taps);
for f = 0:2047
    h_ml(f+1,:) = round(16384 * designFracDelayFIR(f/2048, f_taps));
end
f_final = zeros(1025,f_taps);
error_final = zeros(1025,1);
error_ml_all = zeros(1025,1);
error_ml_phase = zeros(1025,1);
error_opt_phase = zeros(1025,1);
error_ml_abs = zeros(1025,1);
error_opt_abs = zeros(1025,1);

f_base = ((0:(fft_samples-1)) - (fft_samples/2)) * (1/fft_samples) * 4096;
% Display filters and analyse properties
% Make the DC response 16384
for f = 0:1024
    % f == 0 is the first filter, single which equals 16384
    % f == 1024 is the midpoint filter
    ideal_response = shiftdim(exp(-1i * pi * ((f_taps/2 - 1)+f/2048) * (-(fft_samples/2):(fft_samples/2 - 1))/(fft_samples/2)));
    % matlab function response
    h_pad = zeros(fft_samples,1);
    h_pad(1:f_taps) = h_ml(f+1,:);
    fr_ml = fftshift(fft(h_pad));
    % response for optimised function
    h_pad_opt = zeros(fft_samples,1);
    
    if (run_from_scratch)
        h_pad_opt(1:f_taps) = h_ml(f+1,:);
    else
        h_pad_opt(1:f_taps) = f_scaled(f+1,:);
    end
    fr_opt = fftshift(fft(h_pad_opt));
    
    % errors from ideal for the matlab and optimised versions
    error_ml = max(abs(((fr_ml(valid_low:valid_high)/16384) - (ideal_response(valid_low:valid_high)))));
    error_opt = max(abs(((fr_opt(valid_low:valid_high)/16384) - (ideal_response(valid_low:valid_high)))));
    
    h_pad_test = h_pad_opt;
    iter = 0;
    error_target = 0.00018 + f/1024 * 0.00009;
    while (error_opt > error_target)
        if (iter == 0)
            disp(['improve f = ' num2str(f) ', start error = ' num2str(error_opt) ', target error = ' num2str(error_target)])
        end
        iter = iter + 1;
        if (mod(iter,2) == 0)
            h_pad_test(1:f_taps) = h_pad_opt(1:f_taps) + round(0.25 * randn(f_taps,1));
        else
            h_pad_test(1:f_taps) = h_pad_opt(1:f_taps) + round(randn(1) * randn(f_taps,1));
        end
        fr_opt_test = fftshift(fft(h_pad_test));
        error_opt_test = max(abs(((fr_opt_test(valid_low:valid_high)/16384) - (ideal_response(valid_low:valid_high)))));
        if (error_opt_test < error_opt)
            h_pad_opt(1:f_taps) = h_pad_test(1:f_taps);
            error_opt = error_opt_test;
            disp(['f = ' num2str(f) ', iter = ' num2str(iter) ', error = ' num2str(error_opt)]);
        end
    end
    
    % Optimised version can be worse than ml version 
    % since rounding to 16 bit values was not taken into account in the 
    % precalculated optimised filters (only applies if run_from_scratch = 0)
    error_ml_all(f+1) = error_ml;
    if (error_ml < error_opt)
        f_final(f+1,:) = h_pad(1:f_taps);
        error_final(f+1) = error_ml;
    else
        f_final(f+1,:) = h_pad_opt(1:f_taps);
        error_final(f+1) = error_opt;
    end
    
    error_ml_phase(f+1)  = max(abs((180/pi)  * unwrap((angle(fr_ml(valid_low:valid_high))  - angle(ideal_response(valid_low:valid_high))))));
    error_opt_phase(f+1) = max(abs((180/pi) * unwrap((angle(fr_opt(valid_low:valid_high)) - angle(ideal_response(valid_low:valid_high)))))); 
    
    error_ml_abs(f+1) = max(abs(fr_ml(valid_low:valid_high)/16384) - 1);
    error_opt_abs(f+1) = max(abs(fr_opt(valid_low:valid_high)/16384) - 1);
    
    % plot
    if (do_plots) && (f == 807)
        figure(1);
        clf;
        subplot(3,1,1);
        hold on;
        grid on;
        plot(f_base(valid_low:valid_high),abs(fr_ml(valid_low:valid_high)/16384) - 1,'r.-');
        plot(f_base(valid_low:valid_high),abs(fr_opt(valid_low:valid_high)/16384) - 1,'g.-');
        title(['f = ' num2str(f) ', amplitude response deviation from ideal (central 27/32 only)']);
        legend('matlab','optimised');

        subplot(3,1,2);
        hold on;
        grid on;
        plot(f_base(valid_low:valid_high),(180/pi) * unwrap((angle(fr_ml(valid_low:valid_high)) - angle(ideal_response(valid_low:valid_high)))),'r.-');
        plot(f_base(valid_low:valid_high),(180/pi) * unwrap((angle(fr_opt(valid_low:valid_high)) - angle(ideal_response(valid_low:valid_high)))),'g.-');
        title('phase deviation from ideal (degrees) (central 27/32 only)');

        subplot(3,1,3);
        hold on;
        grid on;
        plot(f_base(valid_low:valid_high),abs(fr_ml(valid_low:valid_high)/16384 - ideal_response(valid_low:valid_high)),'r.-');
        plot(f_base(valid_low:valid_high),abs(fr_opt(valid_low:valid_high)/16384 - ideal_response(valid_low:valid_high)),'g.-');
        title('abs deviation from ideal (degrees) (central 27/32 only)');

        figure(2);
        clf;
        hold on;
        grid on;
        plot((-15:16),sign(h_pad(1:32)) .* log2(abs(h_pad(1:32))),'r*');
        plot((-15:16),sign(h_pad_opt(1:32)) .* log2(abs(h_pad_opt(1:32))),'go');
        title(['impulse response (log scale), max diff = ' num2str(max(abs(h_pad(1:32) - h_pad_opt(1:32))))]);
        legend('matlab','optimised');

        disp(['f = ' num2str(f) ', error_ml = ' num2str(error_ml) ', error_opt = ' num2str(error_opt)]);
        pause;
    end
    
end
disp('Maximum tap values across all filters:');
max(abs(f_final))
%bits_needed = ceil(log2(max(abs(f_final)))+0.01) + 1;
%disp(['Bits required (sum = ' num2str(sum(bits_needed)) ') : ']);
%disp(bits_needed)
figure(2);
clf;
hold on;
grid on;
plot([0:1024]/2048, error_ml_all,'r.-');
plot([0:1024]/2048, error_final,'g.-');
legend('Matlab function','optimised');
title('abs error from ideal');
xlabel('filter fractional interpolation');

error_ml_phase(807) = error_ml_phase(806);
error_ml_phase(808) = error_ml_phase(806);
figure(3);
clf;
subplot(2,1,1);
hold on;
grid on;
plot([0:1024]/2048, error_ml_phase,'r.-');
plot([0:1024]/2048, error_opt_phase,'g.-');
legend('Matlab function','optimised');
title('worst phase error, degrees, 32 tap filter');
xlabel('filter fractional interpolation');
subplot(2,1,2);
hold on;
grid on;
plot([0:1024]/2048, error_ml_abs,'r.-');
plot([0:1024]/2048, error_opt_abs,'g.-');
legend('Matlab function','optimised');
title('worst amplitude error, 32 tap filter');
xlabel('filter fractional interpolation');

% Save .coe and .txt files
if (writeCOE)
    fid = fopen(['interp_taps_16bit_32tap.coe'],'w');
    fprintf(fid,'memory_initialization_radix = 2;\n');
    fprintf(fid,'memory_initialization_vector = ');
    for filterSel = [1025 2:1024]
        % 1025 = midpoint filter = first filter listed in the memory
        % So the memory can be addressed with the low 10 bits of the 
        % fractional position.
        % e.g. fractional offset = 0 => filter is not in the memory.
        %      fractional offset = 1024 => low 10 bits = "0000000000" => choose midpoint filter at address 0 in the memory.
        % This means that position 0 has to be replaced with the unity filter,
        % based on bit 11 of the address.
        % 2 to 1025 => 1024 filters in file, skips no-shift (1), goes to the filter that interpolates to the midpoint.
        dstr = [];
        dstr(1:512) = '0';
        for tap = 1:32
            dstr(((tap-1)*16 + 1):((tap-1)*16 + 16)) = dec2binX(f_final(filterSel,tap),16);
        end
        fprintf(fid,['\n' dstr]);
    end
    fprintf(fid,';\n');
    fclose(fid);
    
    % text file is easier to read in python
    fid = fopen(['interp_taps_16bit_32tap.txt'],'w');
    for filterSel = [1:2048]
        if (filterSel > 1025)
            this_filter = f_final(2050-filterSel,32:-1:1);
        else
            this_filter = f_final(filterSel,:);
        end
        for tap = 1:32
            fprintf(fid,"%d ",this_filter(tap));
        end
        if (filterSel<2048)
            fprintf(fid,'\n');
        end
    end
    fclose(fid);
    
end

% Save .txt file





