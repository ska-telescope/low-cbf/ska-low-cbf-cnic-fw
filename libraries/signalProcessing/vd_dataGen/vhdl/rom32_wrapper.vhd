----------------------------------------------------------------------------------
-- Company: CSIRO
-- Engineer: David Humphrey (dave.humphrey@csiro.au)
-- 
-- Create Date: 07/30/2023 05:18:03 PM
-- Module Name: rom32_wrapper - Behavioral
-- Description: 
--  Wrapper for the 32-tap interpolation filter ROM.
-- 
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
library common_lib;
USE common_lib.common_pkg.ALL;

entity rom32_wrapper is
    Port( 
        clk : in std_logic;
        -- Interpolation requested
        -- Interpolation fraction is i_addr/2048
        -- 0 = left-most point, filter output is [0 0 ... 0 1 0 ... 0]
        -- 1024 = mid point filter.
        -- 2047 = right-most filter.
        i_addr : in std_logic_vector(10 downto 0);
        -- 32 filter taps, each 16 bits, 5 clock latency.
        o_data : out t_slv_16_arr(31 downto 0)
    );
end rom32_wrapper;

architecture Behavioral of rom32_wrapper is

    component datagen_interp_rom16_32tap
    port (
        clka : IN STD_LOGIC;
        addra : IN STD_LOGIC_VECTOR(9 DOWNTO 0);
        douta : OUT STD_LOGIC_VECTOR(511 DOWNTO 0));
    end component;

    signal rom_dout : std_logic_vector(511 downto 0);
    signal rom_addr : std_logic_vector(11 downto 0);
    signal addr_ext : std_logic_vector(11 downto 0);
    signal swap_order, swap_order_del1, swap_order_del2, swap_order_del3 : std_logic;
    signal addr_is_zero, addr_is_zero_del1, addr_is_zero_del2, addr_is_zero_del3 : std_logic;
    
begin
    
    addr_ext <= '0' & i_addr;
    
    process(clk)
    begin
        if rising_edge(clk) then
            -- Filters are only stored for the range of input offsets 
            -- 1/2048 to 1024/2048
            -- 1st rom entry (address 0) is for offset 1024/2048
            -- 2nd rom entry (address 1) is for offset 1/2048
            -- 3rd rom entry (address 2) is for offset 2/2048
            -- last rom entry (address 1023) is for offset 1023/2048
            --
            -- So :
            --  i_addr = 0 -> ignore rom output, select filter = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
            --  i_addr = 1 -> rom addr = 1
            --  ...
            --  i_addr = 1023 -> rom_addr = 1023
            --  i_addr = 1024 -> rom_addr = 0 = i_addr(9:0)
            --  i_addr = 1025 -> rom_addr = 1023, swap order of coeficients at the output
            --  ...
            --  i_addr = 2047 -> rom_addr = 1, swap order of coefficients 
            if i_addr(10) = '0' then
                rom_addr <= "00" & addr_ext(9 downto 0);
                swap_order <= '0';
            else
                rom_addr <= std_logic_vector(2048 - unsigned(addr_ext));
                swap_order <= '1';
            end if;
            
            if unsigned(addr_ext) = 0 then
                addr_is_zero <= '1';
            else
                addr_is_zero <= '0';
            end if; 
            
            -- 2 pipeline stages in the ROM.
            swap_order_del1 <= swap_order;
            swap_order_del2 <= swap_order_del1;
            swap_order_del3 <= swap_order_del2;
            
            addr_is_zero_del1 <= addr_is_zero;
            addr_is_zero_del2 <= addr_is_zero_del1;
            addr_is_zero_del3 <= addr_is_zero_del2;
            
            -- Select the correct output
            for i in 0 to 31 loop
                if addr_is_zero_del3 = '1' then
                    -- unity (no-shift) filter; single one at tap 16, all other taps 0.
                    if (i = 16) then
                        o_data(i) <= std_logic_vector(to_unsigned(16384,16));
                    else
                        o_data(i) <= (others => '0');
                    end if;
                else
                    if swap_order_del3 = '1' then
                        -- Reverse the filter taps.
                        o_data(i) <= rom_dout(((31-i)*16 + 15) downto ((31-i)*16));
                    else
                        o_data(i) <= rom_dout(i*16 + 15 downto i*16);
                    end if;
                end if;
            end loop;
            
        end if;
    end process;
    
    
    rom32 : datagen_interp_rom16_32tap
    port map (
        clka => clk,
        addra => rom_addr(9 downto 0),
        douta => rom_dout
    );
    
    
end Behavioral;
