----------------------------------------------------------------------------------
-- Company: CSIRO
-- Engineer: Giles Babich/David Humphrey
-- 
-- Create Date: 02/04/2023
-- Design Name: 
-- Module Name: config_mem_datagen.vhd
--  
-- Description: 
-- 64 KB of space.
-- Two ultraRAMs, 8192 deep x 8 bytes wide
--
----------------------------------------------------------------------------------

library IEEE, spead_lib, xpm, common_lib;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use xpm.vcomponents.all;
use common_lib.common_pkg.ALL;
library UNISIM;
use UNISIM.VComponents.all;

entity datagen_config_mem is
    Port ( 
        i_clk                   : in std_logic;
        i_rst                   : in std_logic;

        i_host_data             : in std_logic_vector(31 downto 0);
        i_host_addr             : in std_logic_vector(15 downto 0); -- 64kBytes of address space
        i_host_wren             : in std_logic;
        o_host_return_data      : out std_logic_vector(31 downto 0);

        -------------------------------------------------
        i_rd_addr               : in std_logic_vector(12 downto 0); -- 8192 words
        o_rd_data               : out std_logic_vector(63 downto 0)
    );
end datagen_config_mem;

architecture Behavioral of datagen_config_mem is

    constant MEMORY_INIT_FILE       : STRING := "none";
    constant g_NO_OF_ADDR_BITS      : INTEGER := 13;
    constant g_D_Q_WIDTH            : INTEGER := 64;
    constant g_BYTE_ENABLE_WIDTH    : INTEGER := 8;
    
    CONSTANT ADDR_SPACE             : INTEGER := pow2(g_NO_OF_ADDR_BITS);
    CONSTANT MEMORY_SIZE_GENERIC    : INTEGER := ADDR_SPACE * g_D_Q_WIDTH;

    signal data_a                   : std_logic_vector((g_D_Q_WIDTH-1) downto 0);
    signal addr_a                   : std_logic_vector((g_NO_OF_ADDR_BITS-1) downto 0);
    signal data_a_wr                : std_logic; 
    signal data_a_q                 : std_logic_vector((g_D_Q_WIDTH-1) downto 0);
    
    signal data_b                   : std_logic_vector((g_D_Q_WIDTH-1) downto 0);
    signal addr_b                   : std_logic_vector((g_NO_OF_ADDR_BITS-1) downto 0);
    signal data_b_wr                : std_logic; 
    signal data_b_q                 : std_logic_vector((g_D_Q_WIDTH-1) downto 0);
    
    signal data_a_wr_int            : std_logic_vector(7 downto 0);
    signal data_b_wr_int            : std_logic_vector(7 downto 0);
    
    signal SM_addr                  : unsigned(7 downto 0);
    
    signal spead_item_to_update     : std_logic_vector(31 downto 0);
    
    signal update_scratch_value     : std_logic_vector(31 downto 0);
    
    signal host_return_data_int     : std_logic_vector(31 downto 0);
    
    signal bram_addr_d1             : std_logic_vector(5 downto 0);
    signal bram_addr_d2             : std_logic_vector(5 downto 0);
    signal bram_addr_d3             : std_logic_vector(5 downto 0);

begin
    
    -------------------------------------------------------------------------
    addr_b <= i_rd_addr;
    o_rd_data <= data_b_q;
    -- only reading
    data_b_wr <= '0';
    data_b_wr_int <= ( others => '0');
    data_b <= ( others => '0');
    
    -------------------------------------------------------------------------
    -- ARGs side
    data_a  <= i_host_data(31 downto 0) & i_host_data(31 downto 0);
    addr_a  <= i_host_addr(15 downto 3);
    data_a_wr <= i_host_wren;
    
    data_a_wr_int(7 downto 0) <= 
        "00001111" when ((i_host_addr(2 downto 0) = "000") AND (data_a_wr = '1')) else 
        "11110000" when ((i_host_addr(2 downto 0) = "100") and (data_a_wr = '1')) else
        "00000000";
    
    -------------------------------------------------------------------------
    -- delay select based on address delay.
    bram_return_data_proc : process(i_clk)
    begin
        if rising_edge(i_clk) then
            bram_addr_d1    <= i_host_addr(5 downto 0);
            bram_addr_d2    <= bram_addr_d1;
            bram_addr_d3    <= bram_addr_d2;
        end if;
    end process;
    
    o_host_return_data  <= host_return_data_int;
    
    host_return_data_int <= 
        data_a_q(31 downto 0) when bram_addr_d3(2 downto 0) = "000" else 
        data_a_q(63 downto 32);

    -------------------------------------------------------------------------
    
    uram_1 : xpm_memory_tdpram
    generic map (    
        -- Common module generics
        AUTO_SLEEP_TIME         => 0,              --Do not Change
        CASCADE_HEIGHT          => 0,
        CLOCKING_MODE           => "common_clock", --string; "common_clock", "independent_clock" 
        ECC_MODE                => "no_ecc",       --string; "no_ecc", "encode_only", "decode_only" or "both_encode_and_decode" 

        MEMORY_INIT_FILE        => MEMORY_INIT_FILE,         --string; "none" or "<filename>.mem" 
        MEMORY_INIT_PARAM       => "",             --string;
        MEMORY_OPTIMIZATION     => "true",          --string; "true", "false" 
        MEMORY_PRIMITIVE        => "ultra",         --string; "auto", "distributed", "block" or "ultra" ;
        MEMORY_SIZE             => MEMORY_SIZE_GENERIC, --262144,          -- Total memory size in bits; 512 x 512 = 262144
        MESSAGE_CONTROL         => 0,              --integer; 0,1

        USE_MEM_INIT            => 0,              --integer; 0,1
        WAKEUP_TIME             => "disable_sleep",--string; "disable_sleep" or "use_sleep_pin" 
        USE_EMBEDDED_CONSTRAINT => 0,              --integer: 0,1
       
    
        RST_MODE_A              => "SYNC",   
        RST_MODE_B              => "SYNC", 
        WRITE_MODE_A            => "no_change",    --string; "write_first", "read_first", "no_change" 
        WRITE_MODE_B            => "no_change",    --string; "write_first", "read_first", "no_change" 

        -- Port A module generics ... ARGs side
        READ_DATA_WIDTH_A       => g_D_Q_WIDTH,    
        READ_LATENCY_A          => 3,              
        READ_RESET_VALUE_A      => "0",            

        WRITE_DATA_WIDTH_A      => g_D_Q_WIDTH,
        BYTE_WRITE_WIDTH_A      => g_BYTE_ENABLE_WIDTH,
        ADDR_WIDTH_A            => g_NO_OF_ADDR_BITS,
    
        -- Port B module generics
        READ_DATA_WIDTH_B       => g_D_Q_WIDTH,
        READ_LATENCY_B          => 3,           
        READ_RESET_VALUE_B      => "0",         

        WRITE_DATA_WIDTH_B      => g_D_Q_WIDTH,
        BYTE_WRITE_WIDTH_B      => g_BYTE_ENABLE_WIDTH,             
        ADDR_WIDTH_B            => g_NO_OF_ADDR_BITS
        )
    port map (
        -- Common module ports
        sleep                   => '0',
        -- Port A side
        clka                    => i_clk,  -- clock from the 100GE core; 322 MHz
        rsta                    => '0',
        ena                     => '1',
        regcea                  => '1',

        wea                     => data_a_wr_int,
        addra                   => addr_a,
        dina                    => data_a,
        douta                   => data_a_q,

        -- Port B side
        clkb                    => i_clk, 
        rstb                    => '0',
        enb                     => '1',
        regceb                  => '1',

        web                     => data_b_wr_int,
        addrb                   => addr_b,
        dinb                    => data_b,
        doutb                   => data_b_q,

        -- other features
        injectsbiterra          => '0',
        injectdbiterra          => '0',
        injectsbiterrb          => '0',
        injectdbiterrb          => '0',        
        sbiterra                => open,
        dbiterra                => open,
        sbiterrb                => open,
        dbiterrb                => open
    );

end Behavioral;
