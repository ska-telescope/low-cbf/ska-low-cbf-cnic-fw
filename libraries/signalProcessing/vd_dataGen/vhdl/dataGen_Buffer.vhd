----------------------------------------------------------------------------------
-- Company: CSIRO
-- Engineer: David Humphrey (dave.humphrey@csiro.au)
-- 
-- Create Date: 06/04/2023 11:20:16 PM
-- Module Name: dataGen_Buffer - Behavioral
-- Description: 
--  Captures data from the data generation units, buffers up blocks of 512 bytes,
--  and writes to the HBM
--
--  UltraRAM buffer : 
--     Builds 512 byte blocks to write to the HBM
--      (512 bytes) * (1024 streams) * (2 buffers) = 1 Mbyte = 32 ultraRAMs.
--     The memory is organised as :
--      (16384 deep) * (512 bits wide)
--     Buffer 0 = 8192 words, 1024 blocks of 8 words each. (8 words = 512 bytes = one HBM write)
--     Buffer 1 = second 8192 words.
--   ultraRAM buffer addressing:
--     address bits 2:0  = 8 words in a 512-word block
--             bits 12:3 = 1024 different streams
--             bit  13   = buffer select
--
--  HBM buffer:
--   Each station+channel has 512 Mbytes/(1024) = 512 kBytes of data = 64 * 8 kbytes = 64 SPS data packets (per station or channel)
--   Addressing of a 1Gbyte block of HBM is (note 29 bits = 512 Mbyte address space) :
--     bit 29 = Select 512MByte buffer
--     bits 28:19 = 1024 different streams
--     bits 18:0  = 512 kBytes of SPS data
--        - bits 18:9 = 1024 different blocks of 512 bytes for the same 
--        - bits 8:0  = 512 byte block written in a burst
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
Library xpm;
use xpm.vcomponents.all;

entity dataGen_buffer is
    generic(
        -- Number of 128-source modules instantiated.
        -- Used here to set how much stuff is written to the HBM
        g_N128_SOURCE_INSTANCES : integer;
        g_128_SAMPLE_BLOCKS     : integer range 1 to 1024 := 1024;
        g_DEBUG_ILA             : BOOLEAN := TRUE
    );
    port(
        clk : in std_logic;
        i_reset : in std_logic;
        -- control. On i_start, start writes to the next buffer.
        i_start : in std_logic;
        o_cur_buffer : out std_logic;
        -- pulse high when finished writing the last of the g_128_SAMPLE_BLOCKS
        o_buffer_done : out std_logic;
        -- data input 
        i_data_samples : in std_logic_vector(255 downto 0);
        i_data_stream  : in std_logic_vector(10 downto 0);  -- 1024 SPS streams x 2 polarisations
        i_data_block   : in std_logic; -- two blocks of 16 samples received for each stream before moving on to the next stream.
        i_data_valid   : in std_logic;
        -- HBM axi write interface
        -- aw bus
        m01_axi_awvalid  : out std_logic;
        m01_axi_awready  : in std_logic;
        m01_axi_awaddr   : out std_logic_vector(31 downto 0);
        m01_axi_awlen    : out std_logic_vector(7 downto 0);
        -- w bus - write data
        m01_axi_wvalid    : out std_logic;
        m01_axi_wready    : in std_logic;
        m01_axi_wdata     : out std_logic_vector(511 downto 0);
        m01_axi_wlast     : out std_logic;
        -- b bus - write response
        m01_axi_bvalid    : in std_logic;
        m01_axi_bresp     : in std_logic_vector(1 downto 0)
    
    );
end dataGen_buffer;

architecture Behavioral of dataGen_Buffer is

    COMPONENT ila_0
    PORT (
        clk : IN STD_LOGIC;
        probe0 : IN STD_LOGIC_VECTOR(191 DOWNTO 0));
    END COMPONENT;    

    signal ultraRAM_wrEn : std_logic_vector(63 downto 0);
    signal ultraRAM_din : std_logic_vector(511 downto 0);
    signal ultraRAM_wr_addr : std_logic_vector(13 downto 0);
    signal wr_HBM_buffer : std_logic := '1';
    signal wr_ultraRAM_word : std_logic_vector(1 downto 0);
    signal running : std_logic := '0';
    signal do_ultraRAM_rd : std_logic := '0';
    signal wr_ultraRAM_buffer : std_logic := '0';
    signal ultraRAM_buffers_written, wr_HBM_512Block : std_logic_vector(9 downto 0);
    signal rd_ultraRAM_buffer : std_logic := '0';
    signal ultraRAM_rd_addr_del1, ultraRAM_rd_addr_del2, ultraRAM_rd_addr_del3 : std_logic_vector(13 downto 0);
    signal fifo_din : std_logic_vector(512 downto 0);
    signal ultraRAM_dout : std_logic_vector(511 downto 0);
    
    signal ultraRAM_rd_del1, ultraRAM_rd_del2, ultraRAM_rd_del3 : std_logic := '0';
    signal ultraRAM_rd_addr : std_logic_vector(13 downto 0);
    --signal 
    type t_rd_data_fsm is (idle, copy_to_fifo, wait_fifo, wait_fifo_empty, set_buffer_done);
    signal rd_Data_fsm : t_rd_data_fsm := idle;
    signal fifo_wr_data_count : std_logic_vector(4 downto 0);
    type t_aw_fsm is (idle, set_aw, wait_aw, update_addr);
    signal aw_fsm : t_aw_fsm := idle;
    signal HBM_stream : std_logic_vector(9 downto 0);
    signal fifo_wrEn : std_logic;
    signal fifo_full, fifo_overflow : std_logic;
    signal fifo_data_valid : std_logic;
    signal fifo_RdEn : std_logic;
    signal fifo_dout : std_logic_vector(512 downto 0);
    signal fifo_empty : std_logic;
    signal reset_del : std_logic;
    signal end_of_buffer : std_logic := '0';

    signal rd_Data_fsm_debug    : std_logic_vector(3 downto 0);
    signal aw_fsm_debug         : std_logic_vector(3 downto 0);
    
    signal awaddr_del : std_logic_vector(31 downto 0);
    signal awvalid_del : std_logic;
    signal m01_axi_awaddr_int : std_logic_vector(31 downto 0);
    signal m01_axi_awvalid_int : std_logic := '0';
    signal awready_del : std_logic;
    signal wvalid_del : std_logic;
    signal wready_del : std_logic;
    signal wlast_del : std_logic;
    
    signal buffer_done : std_logic_vector(7 downto 0);
    
    signal start_edge   : std_logic;
    signal start_del    : std_logic;
    
begin
    
    --------------------------------------------------------------------------------------
    debug_gen : IF g_DEBUG_ILA GENERATE
        vd_ila : ila_0
            port map (
                clk                     => clk,

                probe0(9 downto 0)      => ultraRAM_buffers_written,
                probe0(13 downto 10)    => rd_Data_fsm_debug,

                probe0(17 downto 14)    => aw_fsm_debug,
                probe0(28 downto 18)    => i_data_stream,
                probe0(29)              => i_data_valid,
                probe0(30)              => do_ultraRAM_rd,
                probe0(31)              => running,
                probe0(32)              => wr_ultraRAM_buffer,
                probe0(33)              => wr_HBM_buffer,
                probe0(35 downto 34)    => wr_ultraRAM_word,
                probe0(36)              => '0',
                probe0(46 downto 37)    => wr_HBM_512Block,
                probe0(47)              => end_of_buffer,
                probe0(79 downto 48)    => awaddr_del,
                probe0(80)              => awvalid_del,
                probe0(81)              => awready_del,
                probe0(82)              => wvalid_del,
                probe0(83)              => wready_del,
                probe0(84)              => wlast_del,
                probe0(89 downto 85)    => fifo_wr_data_count,
                probe0(99 downto 90)    => HBM_stream,
                probe0(191 downto 100)   => (OTHERS => '0')
            );
            
        process(clk)
        begin
            if rising_edge(clk) then
                -- ??
                awaddr_del <= m01_axi_awaddr_int;
                awvalid_del <= m01_axi_awvalid_int;
                awready_del <= m01_axi_awready;
                wvalid_del <= fifo_data_valid;
                wready_del <= m01_axi_wready;
                wlast_del <= fifo_dout(512);
            end if;
        end process;  
    END GENERATE;
    --------------------------------------------------------------------------------------
    -- writes are 512 bytes = 8 x 512 bit words
    m01_axi_awlen <= "00000111";
    m01_axi_awaddr <= m01_axi_awaddr_int;
    m01_axi_awvalid <= m01_axi_awvalid_int;
    
    m01_axi_awaddr_int(8 downto 0) <= "000000000";
    m01_axi_awaddr_int(31 downto 30) <= "00";
    
    
    process(clk)
    begin
        if rising_edge(clk) then
            start_del   <= i_start;
            start_edge  <= start_del;
            
        
            reset_del <= i_reset;
            if reset_del = '1' then
                -- set wr_HBM_buffer to 1 here, but
                -- first buffer written to is '0', switch to 0 occurs on i_start.
                wr_HBM_buffer <= '1'; 
                wr_ultraRAM_buffer <= '0';
                wr_ultraRAM_word <= "00"; -- word within an 8 word block in the HBM 
                running <= '0';
                do_ultraRAM_rd <= '0';
                ultraRAM_buffers_written <= "0000000000";
            else
                -------------------------------------------------------------------------
                -- Write into the ultraRAM buffer
                if start_del = '1' and start_edge = '0' then
                    -- Start of capture and write to HBM of 512 MBytes of data.  
                    wr_HBM_buffer <= not wr_HBM_buffer;
                    wr_ultraRAM_buffer <= '0';
                    wr_ultraRAM_word <= "00"; -- word within an 8 word block in the HBM 
                    running <= '1';
                    do_ultraRAM_rd <= '0';
                    ultraRAM_buffers_written <= "0000000000";
                elsif running = '1' then
                    if i_data_valid = '1' then
                        if i_data_stream(0) = '0' then
                            -- Even-indexed streams are for first polarisation
                            ultraRAM_wrEn <= x"3333333333333333";
                        else
                            ultraRAM_wrEn <= x"CCCCCCCCCCCCCCCC";
                        end if;
                    else
                        ultraRAM_wrEn <= x"0000000000000000";
                    end if;
                    -- data in is 256 bits for a single polarisation. This is written to every second 
                    -- block of 16 bits in the buffer, to interleave it with the second polarisation
                    for i in 0 to 15 loop
                        ultraRAM_din(i*32 + 31 downto i*32) <= i_data_samples(i*16+15 downto i*16) & i_data_samples(i*16+15 downto i*16);
                    end loop;
                    ultraRAM_wr_addr(13) <= wr_ultraRAM_buffer;
                    ultraRAM_wr_addr(12 downto 3) <= i_data_stream(10 downto 1);
                    ultraRAM_wr_addr(2 downto 1) <= wr_ultraRAM_word;
                    ultraRAM_wr_addr(0) <= i_data_block;
                    
                    -- i_data_stream counts 256 streams for each of the g_N128_SOURCE_INSTANCES,
                    -- 256 streams => 128 SPS streams since polarisations are sent separately.
                    if ((i_data_valid = '1') and (unsigned(i_data_stream) = (255)) and (i_data_block = '1')) then
                        -- last stream has delivered data, go to the next word
                        wr_ultraRAM_word <= std_logic_vector(unsigned(wr_ultraRAM_word) + 1);
                        if (wr_ultraRAM_word = "11") then
                            -- Finished writing the ultraRAM buffer, switch to the other buffer
                            -- and write this buffer to the HBM
                            wr_ultraRAM_buffer <= not wr_ultraRAM_buffer;
                            rd_ultraRAM_buffer <= wr_ultraRAM_buffer;
                            do_ultraRAM_rd <= '1';
                            ultraRAM_buffers_written <= std_logic_vector(unsigned(ultraRAM_buffers_written) + 1);
                            wr_HBM_512Block <= ultraRAM_buffers_written;
                            if (unsigned(ultraRAM_buffers_written) = 1023) then
                                -- 512 MBytes / 1024 streams = 512 kbytes = 1024 of the ultraRAM buffers
                                running <= '0';
                            end if;
                        else
                            do_ultraRAM_rd <= '0';
                        end if;
                    else
                        do_ultraRAM_rd <= '0';
                    end if;
                else
                    do_ultraRAM_rd <= '0';
                end if;
            end if;
            
            ------------------------------------------------------------------------
            -- Generate axi transactions to write one of the ultraRAM buffers to HBM
            -- triggered by do_ultraRAM_rd to process the buffer rd_ultraRAM_buffer,
            -- and write to HBM at the location wr_HBM_512block
            if do_ultraRAM_rd = '1' then
                -- Generate g_N128_SOURCE_INSTANCES*128 HBM transactions
                rd_data_fsm <= copy_to_fifo;
                aw_fsm <= set_aw;
                ultraRAM_rd_addr(13) <= rd_ultraRAM_buffer;
                ultraRAM_rd_addr(12 downto 0) <= (others => '0');
                --     bit 29 = Select 512MByte buffer
                --     bits 28:19 = 1024 different streams
                --     bits 18:0  = 512 kBytes of SPS data
                --        - bits 18:9 = 1024 different blocks of 512 bytes for the same 
                --        - bits 8:0  = 512 byte block written in a burst
                m01_axi_awaddr_int(18 downto 9) <= wr_HBM_512block;
                HBM_stream <= "0000000000";
                m01_axi_awaddr_int(29) <= wr_HBM_buffer;
                m01_axi_awvalid_int <= '0';
                if (unsigned(wr_HBM_512block) = (g_128_SAMPLE_BLOCKS - 1)) then
                    end_of_buffer <= '1';
                else
                    end_of_buffer <= '0';
                end if;
            else
                case aw_fsm is
                    when idle =>
                        aw_fsm_debug <= x"0";
                        m01_axi_awvalid_int <= '0';
                        aw_fsm <= idle;
                    
                    when set_aw =>
                        aw_fsm_debug <= x"1";
                        m01_axi_awaddr_int(28 downto 19) <= HBM_stream;
                        m01_axi_awvalid_int <= '1';
                        aw_fsm <= wait_aw;
                    
                    when wait_aw =>
                        aw_fsm_debug <= x"2";
                        if m01_axi_awReady = '1' then
                            m01_axi_awvalid_int <= '0';
                            aw_fsm <= update_addr;
                        end if;
                    
                    when update_addr =>
                        aw_fsm_debug <= x"3";
                        m01_axi_awvalid_int <= '0';
                        HBM_stream <= std_logic_vector(unsigned(HBM_stream) + 1);
                        if unsigned(HBM_stream) = (128 * g_N128_SOURCE_INSTANCES - 1) then
                            aw_fsm <= idle;
                        else
                            aw_fsm <= set_aw;
                        end if;
                    
                    when others =>
                        aw_fsm <= idle;
                end case;
                
                -- read_data_fsm copies data from the ultraRAM buffer 
                -- into the FIFO.
                case rd_data_fsm is
                    when idle =>
                        rd_Data_fsm_debug <= x"0";
                        rd_data_fsm <= idle;
                        
                    when copy_to_fifo =>
                        rd_Data_fsm_debug <= x"1";
                        ultraRAM_rd_addr <= std_logic_vector(unsigned(ultraRAM_rd_addr) + 1);
                        if unsigned(ultraRAM_rd_addr(12 downto 0)) = (8*128 * g_N128_SOURCE_INSTANCES - 1) then
                            rd_data_fsm <= wait_fifo_empty;
                        elsif (unsigned(fifo_wr_data_count) > 8) then
                            rd_data_fsm <= wait_fifo;
                        end if;
                        
                    when wait_fifo =>
                        rd_Data_fsm_debug <= x"2";
                        if (unsigned(fifo_wr_data_count) < 8) then
                            rd_data_fsm <= copy_to_fifo;
                        end if;
                    
                    when wait_fifo_empty =>
                        rd_Data_fsm_debug <= x"3";
                        if (unsigned(fifo_wr_data_count) = 0) then
                            if end_of_buffer = '1' then
                                rd_data_fsm <= set_buffer_done;
                            else
                                rd_data_fsm <= idle;
                            end if;
                        end if;
                    
                    when set_buffer_done =>
                        rd_Data_fsm_debug <= x"4";
                        rd_data_fsm <= idle;
                       
                    when others =>
                        rd_data_fsm <= idle;
                end case;
            end if;
            
            if rd_Data_fsm = set_buffer_done then
                buffer_done <= x"FF";
            else
                buffer_done <= buffer_done(6 downto 0) & '0';
            end if;
            
            o_buffer_done   <= buffer_done(7);
            
        end if;
    end process;


    -- The memory buffer
    -- (16384 deep) * (512 bits wide)
    xpm_memory_sdpram_inst : xpm_memory_sdpram
    generic map (
        ADDR_WIDTH_A => 14,      -- DECIMAL
        ADDR_WIDTH_B => 14,      -- DECIMAL
        AUTO_SLEEP_TIME => 0,    -- DECIMAL
        BYTE_WRITE_WIDTH_A => 8, -- 8 = byte enables on the writes.
        CASCADE_HEIGHT => 0,
        CLOCKING_MODE => "common_clock",
        ECC_MODE => "no_ecc",
        MEMORY_INIT_FILE => "none",
        MEMORY_INIT_PARAM => "0",
        MEMORY_OPTIMIZATION => "true",
        MEMORY_PRIMITIVE => "ultra",
        MEMORY_SIZE => 8388608,  -- 16384 x 512 = 8388608
        MESSAGE_CONTROL => 0,
        READ_DATA_WIDTH_B => 512,
        READ_LATENCY_B => 3,
        READ_RESET_VALUE_B => "0",
        RST_MODE_A => "SYNC",            -- String
        RST_MODE_B => "SYNC",            -- String
        SIM_ASSERT_CHK => 0,             -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
        USE_EMBEDDED_CONSTRAINT => 0,    -- DECIMAL
        USE_MEM_INIT => 0,               -- DECIMAL
        USE_MEM_INIT_MMI => 0,           -- DECIMAL
        WAKEUP_TIME => "disable_sleep",  -- String
        WRITE_DATA_WIDTH_A => 512,       -- DECIMAL
        WRITE_MODE_B => "write_first",     -- String
        WRITE_PROTECT => 1               -- DECIMAL
    ) port map (
        clka => clk, 
        clkb => clk,
        -- writes
        addra => ultraRAM_wr_addr,  -- ADDR_WIDTH_A-bit input: Address for port A write operations.
        dina  => ultraRAM_din,
        wea   => ultraRAM_wrEn,    -- 64 bit byte write enable (for 512 bit wide data input)
        -- reads
        addrb => ultraRAM_rd_addr,  -- ADDR_WIDTH_B-bit input: Address for port B read operations.
        doutb => ultraRAM_dout,     -- READ_DATA_WIDTH_B-bit output: Data output for port B read operations.
        -- misc
        ena => '1',
        enb => '1',
        injectdbiterra => '0',
        injectsbiterra => '0',
        regceb => '1',
        rstb => '0',
        sleep => '0',
        dbiterrb => open,
        sbiterrb => open
    );
    
    process(clk)
    begin
        if rising_edge(clk) then
            
            ultraRAM_rd_addr_del1 <= ultraRAM_rd_addr;
            ultraRAM_rd_addr_del2 <= ultraRAM_rd_addr_del1;
            ultraRAM_rd_addr_del3 <= ultraRAM_rd_addr_del2;
            
            -- ultraRAM_rd_Addr is valid in the state "copy_to_fifo"
            if rd_data_fsm = copy_to_fifo then
                ultraRAM_rd_del1 <= '1';
            else
                ultraRAM_rd_del1 <= '0';
            end if;
            ultraRAM_rd_del2 <= ultraRAM_rd_del1;
            ultraRAM_rd_del3 <= ultraRAM_rd_del2;
            
            if ultraRAM_rd_addr_del3(2 downto 0) = "111" then
                fifo_din(512) <= '1';
            else
                fifo_din(512) <= '0';
            end if;
            fifo_din(511 downto 0) <= ultraRAM_dout;
            fifo_wrEn <= ultraRAM_rd_del3;
        end if;
    end process;
    
    -- fwft FIFO to handle the axi interface
    -- xpm_fifo_sync: Synchronous FIFO
    -- Xilinx Parameterized Macro, version 2022.2
    xpm_fifo_sync_inst : xpm_fifo_sync
    generic map (
        CASCADE_HEIGHT => 0,        -- DECIMAL
        DOUT_RESET_VALUE => "0",    -- String
        ECC_MODE => "no_ecc",       -- String
        FIFO_MEMORY_TYPE => "block", -- String
        FIFO_READ_LATENCY => 0,     -- DECIMAL
        FIFO_WRITE_DEPTH => 16,     -- DECIMAL
        FULL_RESET_VALUE => 0,      -- DECIMAL
        PROG_EMPTY_THRESH => 10,    -- DECIMAL
        PROG_FULL_THRESH => 10,     -- DECIMAL
        RD_DATA_COUNT_WIDTH => 5,   -- DECIMAL
        READ_DATA_WIDTH => 513,     -- DECIMAL
        READ_MODE => "fwft",        -- String
        SIM_ASSERT_CHK => 0,        -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
        USE_ADV_FEATURES => "1004", -- String, bit(12) = enable data valid flag, bit (2) = enable wr_data_count 
        WAKEUP_TIME => 0,           -- DECIMAL
        WRITE_DATA_WIDTH => 513,    -- DECIMAL
        WR_DATA_COUNT_WIDTH => 5    -- DECIMAL
    ) port map (
        almost_empty => open,   -- 1-bit output: Almost Empty
        almost_full => open,     -- 1-bit output
        data_valid => fifo_data_valid, -- 1-bit out; data is available on the output bus (dout).
        dbiterr => open,             -- 1-bit output: Double Bit Error
        dout => fifo_dout,         -- READ_DATA_WIDTH-bit output: Read Data
        empty => fifo_empty,       -- 1-bit output: Empty Flag
        full => fifo_full,         -- 1-bit output: Full Flag
        overflow => fifo_overflow, -- 1-bit output: Overflow
        prog_empty => open,        -- 1-bit output: Programmable Empty
        prog_full => open,         -- 1-bit output: Programmable Full:
        rd_data_count => open,
        rd_rst_busy => open,    -- 1-bit output: Read Reset Busy: 
        sbiterr => open,        -- 1-bit output: Single Bit Error: 
        underflow => open,      -- 1-bit output: Underflow
        wr_ack => open,         -- 1-bit output: Write Acknowledge
        wr_data_count => fifo_wr_data_count, -- WR_DATA_COUNT_WIDTH-bit output: Write Data Count
        wr_rst_busy => open,     -- 1-bit output: Write Reset Busy
        din => fifo_din,   -- WRITE_DATA_WIDTH-bit input: Write Data
        injectdbiterr => '0', -- 1-bit input: Double Bit Error Injection
        injectsbiterr => '0', -- 1-bit input: Single Bit Error Injection
        rd_en => fifo_rdEn,  -- 1-bit input: Read Enable
        rst => i_reset,           -- 1-bit input: Reset: Must be synchronous to wr_clk.
        sleep => '0',         -- 1-bit input: Dynamic power saving
        wr_clk => clk,        -- 1-bit input: Write clock:
        wr_en => fifo_wrEn        -- 1-bit input: Write Enable
    );
    
    fifo_rdEn <= fifo_data_valid and m01_axi_wready;
    m01_axi_wvalid <= fifo_data_valid;
    m01_axi_wdata <= fifo_dout(511 downto 0);
    m01_axi_wlast <= fifo_dout(512);

end Behavioral;
				
