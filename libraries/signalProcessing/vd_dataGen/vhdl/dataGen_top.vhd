----------------------------------------------------------------------------------
-- Company: CSIRO
-- Engineer: David Humphrey (dave.humphrey@csiro.au)
-- 
-- Create Date: 06/03/2023 11:19:13 PM
-- Module Name: dataGen_top - Behavioral
-- Description: 
--   Generate SPS-like data for the CNIC to send.
-- 
-- ---------------------------------------------------------------------
-- Control :
-- - UltraRAM configuration memory ----------------
--   Most parameters controlling the data generation are stored in ultraRAM,
--   which is read/writeable via the ARGs axi full interface.
--   See **dataGen_config_mem** section below for info on the memory contents.
--
-- - Other control ------------------------------
--   Top level signal i_start triggers writing a block of 512 Mbytes of data to the HBM
--  While running, one of the signal o_writing_buf0 or o_writing_buf1 will be high.
--  After a block of 512 Mbytes has been written, o_writing_buf0 and o_writing_buf1 will both be low. 
--  Subsequent triggers on "i_start" will alternately write to buf0 and buf1.
--
-- Config registers
--  - valid_time_buf0
--     Time in seconds since the epoch at which buffer 0 becomes valid
--  - valid_time_buf1
--     Time in seconds since the epoch at which buffer1 becomes valid.
--  - enable_vd(1:0)
--     Buffer0/1 has valid data
--  - start_time_seconds
--     Starting time in seconds since the epoch
--  - start_time_ns
--     Starting time in ns (offset from start_time_seconds)
-- Start_time_seconds and start_time_ns are only used on reset. 
--
-- Startup:
--  For data generation to function correctly, the delay polynomials need to be 
--  biased so that the delays are always positive numbers.
--  To do this, a constant value "polynomial_bias" should be added to the constant term in 
--  every polynomial. 
--  
--    Time in nanoseconds that        True time
--    generation starts               of the first 
--    = start_time_s *1e9                sample
--     + start_time_ns                 
--      |                                   |
--      |<------ polynomial_bias (ns) ----->|
--                                          |
--                                          |
--                              initial_packet_count
--                              = packet counter for the first packet
--                              (configured in packet generation registers)
--
-- inital_packet_count = (start_time_s*1e9 + start_time_ns + polynomial_bias)/(2048*1080ns)
-- But initial_packet_count must be an integer, so given start_time_s and polynomial bias,
-- calculate :
--  initial_packet_count = ceil((start_time_s*1e9 + polynomial_bias)/(2048*1080))
--   and then
--  start_time_ns = initial_packet_count * 2048*1080 - (start_time_s*1e9 + polynomial_bias)
--
-- ---------------------------------------------------------------------
-- Data Output:
--  Data is written to 512 MByte buffers in the HBM. 
--  Output data is in SPS format, i.e. 4-byte blocks with 8+8 complex data for two polarisations.
--  The output data is a continuous stream with no headers or other information included. 
--  The output consists of 1024 streams of SPS data.
--  Each station+channel has 512 Mbytes/(1024) = 512 kBytes of data = 64 * 8 kbytes = 64 SPS data packets (per station or channel)
--  Addressing of a 1Gbyte block of HBM is (note 29 bits = 512 Mbyte address space) :
--    bit 29 = Select 512MByte buffer
--    bits 28:19 = 1024 different streams
--    bits 18:0  = 512 kBytes of SPS data
--
-- ---------------------------------------------------------------------
-- Structure :
--
--  ***Overview***
--   - 4 sky sources added together to get each data stream.
--   - Each sky source has a delay polynomial, source (noise seed or sinusoid frequency), and scale factor.
--   - Separate source specification for each polarisation
--   - Polynomials are evaluated once for every 16 samples generated
--   - Data source logic cycles through 256 sky sources, calculating 16 sample bursts for a given source.
--      - Note : Effective pipelining of the double precision polynomial evaluation 
--               requires frequent switching between different sources.
--   - 4 sky sources run in parallel, and are added together to get a single data stream.
--   - Data is buffered in an ultraRAM buffer and written to HBM in blocks of 512 bytes.
--      - Total of 1024 SPS streams = about 30 Gbit/sec.
--
--
--  ***Block Diagram***
--
--   ARGs AXI full interface -->--|
--                                |
--  ------------------------------|
--  |
--  |   |-------------------------------------------------------------------------------------------------------------|
--  |   | 1: #dataGen_128# : Generate data for 128 SPS streams                                                        |
--  |   |                                                                                                             |
--  |->-|-#dataGen_config_mem#                                Interpolation_taps_BRAM                                 |
--  |   |   |                                                         |                                               |
--  |   |   |-->datagen_poly_eval-->----buf-->data_source->interp filter-> phase-> scale---+->round->16 sample buffer |
--  |   |   |                        |                        |       |  (complex  (real*  |                |         |
--  |   |   |                        |                  state_memory  |     mult) complex) |                |         |
--  |   |   |                        |                        |       |                    |                |         |
--  |   | #dataGen_config_mem#       |--buf-->data_source->interp filter-> phase-> scale--+-                |         |
--  |   |   |                                                         |                   |                 |         |
--  |   |   |-->datagen_poly_eval-->-+--buf-->data_source->interp filter-> phase-> scale-+-                 |         |
--  |   |                            |                        |       |                  |                  |         | 
--  |   |                            |                  state_memory  |                  |                  |         |
--  |   |                            |                        |       |                  |                  |         |
--  |   |                            |--buf-->data_source->interp filter-> phase-> scale-/                  |         |
--  |   |-------------------------------------------------------------------------------------------------------------|
--  |                                                                                                       | 256 bits
--  |   |-------------------------------------------------------------------------------------------------------------|
--  |->-| 2: #dataGen_128# : Generate data for 128 SPS streams                                              |         |
--  |   |-------------------------------------------------------------------------------------------------------------|
--  |                                                                                                       |          
--  |  ...                                                                                                  |          
--  |                                                                                                       |          
--  |   |-------------------------------------------------------------------------------------------------------------|
--  |->-| 8: #dataGen_128# : Generate data for 128 SPS streams                                              |         |
--      |-------------------------------------------------------------------------------------------------------------|
--                                                                                                          |          
--    |-----------------<------------------------------------------------------------------------------------
--    |
--    \---> dataGen_Buffer --> Write to HBM
--
-- MODULES:
--  **dataGen_config_mem**
--    Configuration memory specifying the delay polynomials and data source.
--    On the write side :
--      Bytes 0 to 63  : Source 1 for data stream 1, pol 0 (see read side below for specification of contents)
--      Bytes 0 to 255 : All 4 sources for data stream 1, pol 0
--      Bytes 0 to 511 : 8 sources, 4+4 for 2 polarisations, for Data stream 1, 
--      Bytes 512-1023 : Data stream 2     
--      ...
--      Bytes 524264-524287 : Data stream 1024
--      Bytes 524288-1048576 : Second buffer, identical to first buffer, to enable polynomial updates.
--
--    On the read side (as accessed by datagen_Poly_eval) each instance is:
--      (8192 deep) x (64 bits wide) 
--     The memory is split into 2x4096 buffers so that the delay polynomials can be updated.
--     Within each 4096 words, there are 512 x 8 words, with each block of 8 words specifying 
--     a single source in the sky for a particular output data stream.
--     
--      word 0 = c0,
--       ...  
--      word 5 = c5,
--               c0 to c5 are double precision floating point values for the delay polynomial :
--               c0 + c1*t + c2 * t^2 + c3 * t^3 + c4 * t^4 + c5 * t^5
--               Units for c0,.. c5 are ns/s^k for k=0,1,..5
--      word 6 = Sky frequency in GHz
--               Used to convert the delay (in ns) to a phase rotation.
--               From the Matlab code:
--                % Phase Rotation
--                %  The sampling point is (DelayOffset_all * Ts)*1e-9 ns
--                %  then multiply by the center frequency (CF) to get the number of rotations.
--                %
--                %  The number of cycles of the center frequency per output sample is not an integer due to the oversampling of the LFAA data.
--                %  For example, for coarse channel 65, the center frequency is 65 * 781250 Hz = 50781250 Hz.
--                %  50781250 Hz = a period of 1/50781250 = 19.692 ns. The sampling period for the LFAA data is 1080 ns, so 
--                %  a signal at the center of channel 65 goes through 1080/19.692 = 54.8438 cycles. 
--                %  So a delay which is an integer number of LFAA samples still requires a phase shift to be correct.
--                resampled = resampled .* exp(1i * 2*pi*DelayOffset_all * Ts * 1e-9 * CF);
--                #Note : DelayOffset_all = delay in number of samples (of period Ts)
--                #       Ts = sample period in ns (i.e. 1080 for SPS data)
--                #       CF = channel center frequency in Hz, e.g. 65 * 781250 = 50781250 for the first SPS channel
--                #       - The value [Ts * 1e-9 * CF] is the value stored here.
--                       
--  
--      word 7 = Data source specification
--                bits 14:0  = Random number generator seed. 
--                              -- OR -- 
--                             phase step for generation of sinusoids.
--                             used as the step into a 32768 deep (sin,cos) lookup table
--                             So a step of 0 = DC = center of channel
--                             step of +/-1 = (1/1080ns)*(1/32768) = +/-28.257 Hz
--                             step of 8 = (1/1080ns)*(8/32768) = 226.056 Hz (= low CBF correlator filterbank resolution)
--                             step of 4*24=96 = 5.4kHz (= standard integration bandwidth for the low CBF correlator)
--                             maximum step is +/-16383 = +/-462 kHz --> outside the used bandwidth due to 32/27 oversampling.
--                bit 15     = '0' to select pseudo-random numbers, '1' to select sinusoid 
--                bits 31:16 = Scale factor to apply to this sky source,
--                             before adding to the other 3 sky sources that contribute to a data stream.
--                             After scaling by this scale factor and summing with the other 3 sky sources, 
--                             the result is divided by 2^16 = 65536 and rounded and saturated to 8 bit values.
--                             **Sinusoids** 
--                               Generated in 2.14 format, min and max values are -16384 to 16384
--                               Standard deviation is 16384/sqrt(2) = 11585
--                               A scale factor of 1 gives a sinusoid with amplitude +/-0.25 in the SPS data.
--                               (i.e. non-existent, unless dithered by also adding noise)
--                               A scale factor of 512 gives a sinusoid with amplitude +/-128, which may result in clipping.
--                               Scale factors larger than 512 will result in clipping.
--                             **Pseudo-random Noise**
--                               Approximately Gaussian distributed, standard deviation = 209.03, range -1020 to 1020
--                               A scale factor of 65536/209.03 ~ 314 results in noise with standard deviation of 1 in SPS data
--                               A scale factor of  20*65536/209.03 ~ 6270 results in noise with standard deviation = 20
--                                 and max value of (6270/65536) * 1020 = 97
--  
--                bits 63:31 = Unused, could be used for pulsar-like windowing of noise ?
--                
--  datagen_poly_eval:
--    Calculates 5th order delay polynomials for 512 different sources.
--    Delay polynomials are computed once every 16 us, and used to generate
--     * Integer Offset into the data stream, i.e. number of 1080ns samples.
--     * Fractional Offset into the data stream, an 11 bit value to choose one of 2048 different interpolation filters
--     * Phase correction, based on the sky frequency for this channel, 16+16 bit complex value.
--
--    The polynomials are evaluated using double precision arithmetic, with
--    a pipelined implementation that builds the result starting from c0 for all 512 polynomials,
--    then calculates c0 + c1 * t for all 512 polynomials, etc
--    until the final set of mutiply-adds calculates the output of all 512 polynomials
--    in a burst of 512 clocks.
--    At the end of the polynomial evaluation, the offsets and interpolation filters
--    are output in a burst of 512 clocks.  
--   
--  buf(fer) : 
--    Data from datagen_poly_eval goes to a fifo for each sky source generation module
--    Data in the buffer is :
--      16 bit phase rotation
--      16 bit scale factor
--      15 bit Random number generator seed or sinusoid phase step
--      1  bit select random numbers ('0') or sinusoid ('1')
--      11 bits sample offset. Number of 1080ns samples to delay by
--      11 bits interpolation filter. Which interpolation filter to use.
--
--  data_source :
--    Either pseudo noise sequence or sinusoid lookup
--    Pseudo noise is constricted by a 64-bit sequence, with 8x8bit values summed to get ~ gaussian output
--    Real and imaginary parts come from a different seed.
-- 
--  Interpolation Filter :
--    Lookup table to get one of 2048 different filters.
--    Each filter is 16 taps, with 16 bit values.
--    A single table is used for all 4 data sources,
--    using 16 BRAMs. The memory is 8192 deep by 64 bits wide, with 4 reads 
--    required to get one filter out.
--    Total of 16 clocks required to read the 4 filters for the 4 different sources 
--
--  State memory :
--    Holds a history of the last 16 samples generated by the source.
--    Each data source is generating 256 different streams, with bursts of 16 samples
--    before switching.
--
--  dataGen_Buffer:
--    Builds 512 byte blocks to write to the HBM
--     (512 bytes) * (1024 streams) * (2 buffers) = 1 Mbyte = 32 ultraRAMs.
--    The memory is organised as :
--     (16384 deep) * (512 bits wide)
--    Buffer 0 = 8192 words, 1024 blocks of 8 words each. (8 words = 512 bytes = one HBM write)
--    Buffer 1 = second 8192 words.
----------------------------------------------------------------------------------
library IEEE, vd_datagen_lib, common_lib, signal_processing_common;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
library axi4_lib;
USE axi4_lib.axi4_lite_pkg.ALL;
USE axi4_lib.axi4_full_pkg.ALL;
use common_lib.common_pkg.ALL;
use vd_datagen_lib.vd_datagen_reg_pkg.ALL;

entity dataGen_top is
    generic(
        -- Number of data streams to generate as a multiple of 128
        -- Min = 1, max = 8, use less to reduce build times.
        g_N128_SOURCE_INSTANCES : integer range 1 to 8 := 8;
        -- Number of blocks of 128 SPS samples to generate per run
        -- Each block of 128 SPS samples = 512 bytes of data in the HBM = 1 HBM write /stream
        -- Maximum is 1024 => (1024 blocks)*(512 bytes/block) = 512 kBytes 
        -- (= 131072 SPS samples / stream)
        g_128_SAMPLE_BLOCKS : integer range 1 to 1024 := 1024;
        g_VD_INSTANCE_TWO       : BOOLEAN := FALSE;     -- generic name for constraints.
        g_DEBUG_ILA             : BOOLEAN := TRUE
    );
    port(
        clk             : in std_logic;         -- Axi + VD clock.
        reset           : in std_logic;         -- reset from VD

        clk_vd          : in std_logic;         -- clk for VD
        clk_vd_reset    : in std_logic;         -- power on reset for VD clock domain.

        reset_axi       : in std_logic;         -- reset from AXI clock domain.
        -- ARGs full interface
        -- Addresses two ultraRAMs for storage of delay polynomials 
        -- other signal generation controls
        i_vdgen_full_axi_mosi      : in  t_axi4_full_mosi;
        o_vdgen_full_axi_miso      : out t_axi4_full_miso;
        -- ARGs lite interface
        -- Registers for top level control and monitoring.
        i_vdgen_lite_axi_mosi      : in t_axi4_lite_mosi; 
        o_vdgen_lite_axi_miso      : out t_axi4_lite_miso;
        ----------------------------------------------------
        -- Control signals to the rest of CNIC
        -- Indicates this module is currently writing data to the first 512 MBytes of the HBM
        o_writing_buf0 : out std_logic;
        -- Indicates this module is currently writing data to the 2nd 512 MBytes of the HBM
        o_writing_buf1 : out std_logic;
        -- where we are up to in generating data for the HBM buffer.
        -- Each poly run is 16 output samples, total of 8192 per HBM buffer 
        -- 8192*16= 131072 samples = 512kBytes/stream @ 4 bytes/sample
        o_poly_runs : out std_logic_vector(15 downto 0);
        -- Pulse to start writing data (to whichever buffer we are currently up to)
        i_start : in std_logic;
        ----------------------------------------------------
        -- HBM interface, Uses 1 Gbyte of HBM. 
        -- aw bus
        m01_axi_awvalid  : out std_logic;
        m01_axi_awready  : in std_logic;
        m01_axi_awaddr   : out std_logic_vector(31 downto 0);
        m01_axi_awlen    : out std_logic_vector(7 downto 0);
        -- w bus - write data
        m01_axi_wvalid    : out std_logic;
        m01_axi_wready    : in std_logic;
        m01_axi_wdata     : out std_logic_vector(511 downto 0);
        m01_axi_wlast     : out std_logic;
        -- b bus - write response
        m01_axi_bvalid    : in std_logic;
        m01_axi_bresp     : in std_logic_vector(1 downto 0)
    );
end dataGen_top;

architecture Behavioral of dataGen_top is

    -- Number of samples calculated in a single burst. 
    -- This is also the number of samples that use a single polynomial evaluation
    constant c_SAMPLES_PER_POLY_RUN : integer := 32;
    constant c_POLY_RUNS_PER_128SPS_SAMPLES : integer := 128/c_SAMPLES_PER_POLY_RUN; -- i.e. 4 
    
    component uint64_to_double
    port (
        aclk : IN STD_LOGIC;
        s_axis_a_tvalid : IN STD_LOGIC;
        s_axis_a_tdata : IN STD_LOGIC_VECTOR(63 DOWNTO 0);
        m_axis_result_tvalid : OUT STD_LOGIC;
        m_axis_result_tdata : OUT STD_LOGIC_VECTOR(63 DOWNTO 0));
    end component;

    -- multiply by 1000000000 (=1e9), 4 cycle latency.
    component mult_x1e9
    port (
        CLK : IN STD_LOGIC;
        A   : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
        P   : OUT STD_LOGIC_VECTOR(61 DOWNTO 0));
    end component;

    COMPONENT ila_0
    PORT (
        clk : IN STD_LOGIC;
        probe0 : IN STD_LOGIC_VECTOR(191 DOWNTO 0));
    END COMPONENT;

    signal config_mem_rd_addr : std_logic_vector(12 downto 0);
    signal config_mem_rd_data : std_logic_Vector(63 downto 0);
    
    signal poly_filter : std_logic_vector(95 downto 0); -- 8x12bit filter taps
    signal poly_config : std_logic_vector(31 downto 0); -- config info, 15:0 = seed, 19:16 = scale
    signal poly_offset : std_logic_vector(31 downto 0); -- sample offset, units of 5 ns
    signal poly_count : std_logic_vector(8 downto 0);   -- which polynomial this is for
    signal poly_frames : std_logic_vector(31 downto 0);
    
    signal poly_eval_start : std_logic;
    signal poly_eval_buf : std_logic;
    signal poly_eval_running : std_logic;
    signal args_bram_wren : std_logic_vector(8 downto 0);
    signal args_bram_addr : t_slv_20_arr(8 downto 0);
    signal args_bram_wr_data : t_slv_32_arr(8 downto 0);
    signal args_bram_rd_data : t_slv_32_arr(8 downto 0);
    signal args_bram_rd_data_valid : std_logic_vector(8 downto 0);
    
    signal data_samples : t_slv_256_arr(8 downto 0);
    signal data_valid : std_logic_vector(8 downto 0);
    signal data_stream : t_slv_11_arr(8 downto 0);
    signal data_block : std_logic_vector(8 downto 0);
    signal start_512M_buffer : std_logic_vector(7 downto 0);
    
    signal reg_rw : t_vd_datagen_reg_rw;
    signal reg_ro : t_vd_datagen_reg_ro;
    
    --signal poly_space_available : std_logic_vector(7 downto 0);
    --signal poly_space_available_del1 : std_logic_vector(7 downto 0);
    --signal all_space_available : std_logic;
    signal poly_running, poly_running_del1 : std_logic_vector(7 downto 0);
    signal any_poly_running : std_logic;
    signal poly_buf0_valid, poly_buf1_valid, buf0_more_recent : std_logic;
    
    signal start_del1, start_del2 : std_logic := '0';
    signal cur_time_ns, cur_time_seconds : std_logic_vector(31 downto 0);
    signal cur_buffer : std_logic;
    signal poly_runs : std_logic_vector(15 downto 0);
    type t_running_fsm is (idle, check_buffer, check_ready, update_time_ns, update_time_seconds, calc_time_ns_start,
          calc_time_ns_run, wait_poly_start, wait_poly_done, poly_done, update_poly_runs, done_poly_run, wait_all_done);
    signal running_fsm : t_running_fsm := idle;
    signal delays_buf0_run_count, delays_buf1_run_count : std_logic_vector(15 downto 0);
    signal delays_buf0_used, delays_buf1_used : std_logic := '0';
    type t_use_delays_fsm is (idle, run_delay_buf0, run_delay_buf1, wait_a_while, wait_done, clear_used);
    signal use_delays_fsm : t_use_delays_fsm := idle;
    signal read_delays, read_delays_buffer : std_logic;
    signal wait_a_while_count : std_logic_vector(3 downto 0);
    signal read_delay_running : std_logic_vector(7 downto 0);
    
    signal all_source_rdy : t_slv_2_arr(7 downto 0);
    signal all_gen128_source_rdy, all_gen128_source_rdy_del1 : std_logic_vector(1 downto 0);
    signal wait_start_count : std_logic_vector(3 downto 0) := "1111";
    signal time_ns_fp64_hold, time_ns_fp64 : std_logic_vector(63 downto 0);
    signal calc_ns_del1, calc_ns_del2, calc_ns_del3, calc_ns_del4, calc_ns_del5, calc_ns_del6, calc_ns_del7, calc_ns_del8 : std_logic;
    signal time_ns_fp64_valid : std_logic;
    signal base_time_seconds, time_seconds_offset : std_logic_vector(31 downto 0);
    signal time_ns_offset : std_logic_vector(61 downto 0);
    signal time_ns_offset_del1, eval_time_ns, cur_time_ns_ext : std_logic_vector(63 downto 0);
    signal rdy_holdoff_block : std_logic_vector(1 downto 0);
    signal rdy_holdoff : t_slv_4_arr(1 downto 0) := (others => (others => '0'));
    signal wr_buffer_done : std_logic;
    
    signal running_fsm_debug : std_logic_vector(3 downto 0);
    signal vdgen_full_axi_miso : t_axi4_full_miso;

    signal writing_buf0_internal    : std_logic;
    signal writing_buf1_internal    : std_logic;
    signal start_internal           : std_logic;
    signal start_512M_buffer_kernel : std_logic;
    signal wr_buffer_done_kernel    : std_logic;

    signal reset_from_kernel        : std_logic;
    signal reset_vd                 : std_logic;

    constant Kernel_to_VD_clk       : integer := 4;
    signal kernel_to_vd_in          : std_logic_vector((Kernel_to_VD_clk-1) downto 0);
    signal kernel_to_vd_q           : std_logic_vector((Kernel_to_VD_clk-1) downto 0);
    
    constant VD_to_kernel_clk       : integer := 4;
    signal vd_to_kernel_in          : std_logic_vector((VD_to_kernel_clk-1) downto 0);
    signal vd_to_kernel_q           : std_logic_vector((VD_to_kernel_clk-1) downto 0);
    
    signal valid_time_buf0_vd       : std_logic_vector(31 downto 0);
    signal valid_time_buf1_vd       : std_logic_vector(31 downto 0);
    signal start_time_s_vd          : std_logic_vector(31 downto 0);
    signal start_time_ns_vd         : std_logic_vector(31 downto 0);
    signal enable_vd_vd             : std_logic_vector(1 downto 0);

    constant ARGs_Knl_to_VD_clk     : integer := 5;
    signal ARGs_Knl_to_VD_in        : t_slv_32_arr((ARGs_Knl_to_VD_clk-1) downto 0);
    signal ARGs_Knl_to_VD_q         : t_slv_32_arr((ARGs_Knl_to_VD_clk-1) downto 0);

    constant ARGs_VD_to_knl_clk     : integer := 5;
    signal ARGs_VD_to_knl_in        : t_slv_32_arr((ARGs_VD_to_knl_clk-1) downto 0);
    signal ARGs_VD_to_knl_q         : t_slv_32_arr((ARGs_VD_to_knl_clk-1) downto 0);

    signal CDC_fifo_rd              : std_logic;
    signal CDC_fifo_empty           : std_logic;

    signal CDC_fifo_data            : std_logic_vector(267 downto 0);
    signal CDC_fifo_q               : std_logic_vector(267 downto 0);

    signal debug_buffer_gen_cycles  : unsigned(31 downto 0) := (others => '0');
    signal debug_buffer_gen_cache   : unsigned(31 downto 0) := (others => '0');
    signal calc_cycles              : std_logic;
    signal calc_cycles_d            : std_logic;
    
    signal vd_gen_no_of_chans       : std_logic_vector(24 downto 0);

begin
    
    --------------------------------------------------------------------------------------
    debug_gen : IF g_DEBUG_ILA GENERATE
        vd_ila : ila_0
            port map (
                clk                     => clk,

                probe0(3 downto 0)      => running_fsm_debug,
                probe0(19 downto 4)     => poly_runs,

                probe0(20)              => cur_buffer,
                probe0(21)              => reset,
                probe0(22)              => i_start,
                probe0(23)              => wr_buffer_done,
                probe0(24)              => vdgen_full_axi_miso.rvalid,
                probe0(56 downto 25)    => vdgen_full_axi_miso.rdata(31 downto 0),
                probe0(57)              => vdgen_full_axi_miso.rlast,
                probe0(58)              => i_vdgen_full_axi_mosi.rready,
                probe0(59)              => i_vdgen_full_axi_mosi.arvalid,
                probe0(91 downto 60)    => i_vdgen_full_axi_mosi.araddr(31 downto 0),
                probe0(99 downto 92)    => i_vdgen_full_axi_mosi.arlen(7 downto 0),
                probe0(100)             => vdgen_full_axi_miso.arready,
                probe0(101)              => i_vdgen_full_axi_mosi.awvalid,
                probe0(133 downto 102)   => i_vdgen_full_axi_mosi.awaddr(31 downto 0),
                probe0(141 downto 134)  => i_vdgen_full_axi_mosi.awlen(7 downto 0),
                probe0(142)             => vdgen_full_axi_miso.awready,
                probe0(143)             => i_vdgen_full_axi_mosi.wvalid,
                probe0(175 downto 144)  => i_vdgen_full_axi_mosi.wdata(31 downto 0),
                probe0(176)             => vdgen_full_axi_miso.wready,
                probe0(177)             => args_bram_wren(0),
                probe0(183 downto 178)  => args_bram_addr(0)(5 downto 0),
                probe0(187 downto 184)  => args_bram_wr_data(0)(3 downto 0),
                probe0(191 downto 188)  => args_bram_rd_data(8)(3 downto 0)
            );

    END GENERATE;
    --------------------------------------------------------------------------------------
    -- CDC VD_GEN_CLK to the Kernel Clock
    ARGs_K_to_VD : for i in 0 to (ARGs_Knl_to_VD_clk-1) generate
        i_args_kernel_to_vd : entity signal_processing_common.sync
            generic map (
                DEST_SYNC_FF    => 2,
                WIDTH           => 32
            )
            Port Map ( 
                Clock_a     => clk,
                data_in     => ARGs_Knl_to_VD_in(i),
                
                Clock_b     => clk_vd,
                data_out    => ARGs_Knl_to_VD_q(i)
            );
    end generate;

    ARGs_Knl_to_VD_in(0)                <= reg_rw.start_time_s;
    ARGs_Knl_to_VD_in(1)                <= reg_rw.start_time_ns;
    ARGs_Knl_to_VD_in(2)                <= reg_rw.valid_time_buf0;
    ARGs_Knl_to_VD_in(3)                <= reg_rw.valid_time_buf1;
    ARGs_Knl_to_VD_in(4)(1 downto 0)    <= reg_rw.enable_vd;

    start_time_s_vd     <= ARGs_Knl_to_VD_q(0);
    start_time_ns_vd    <= ARGs_Knl_to_VD_q(1);
    valid_time_buf0_vd  <= ARGs_Knl_to_VD_q(2);
    valid_time_buf1_vd  <= ARGs_Knl_to_VD_q(3);
    enable_vd_vd        <= ARGs_Knl_to_VD_q(4)(1 downto 0);

    --------------------------------------------------------------------------------------
    -- CDC VD_GEN_CLK to the Kernel Clock
    ARGs_VD_to_K : for i in 0 to (ARGs_VD_to_knl_clk-1) generate
        i_args_kernel_to_vd : entity signal_processing_common.sync_vector
            generic map (
                --DEST_SYNC_FF    => 2,
                WIDTH           => 32
            )
            Port Map ( 
                clock_a_rst => clk_vd_reset,
                Clock_a     => clk_vd,
                data_in     => ARGs_VD_to_knl_in(i),
                
                Clock_b     => clk,
                data_out    => ARGs_VD_to_knl_q(i)
            );
    end generate;

    ARGs_VD_to_knl_in(0)                <= cur_time_seconds;
    ARGs_VD_to_knl_in(1)                <= cur_time_ns;
    ARGs_VD_to_knl_in(2)(7 downto 0)    <=  "000" &
                                            delays_buf1_used &
                                            delays_buf0_used &
                                            buf0_more_recent &
                                            poly_buf1_valid &
                                            poly_buf0_valid;
    ARGs_VD_to_knl_in(3)                <= std_logic_vector(debug_buffer_gen_cache);
    ARGs_VD_to_knl_in(4)(3 downto 0)    <= running_fsm_debug;

    reg_ro.current_time_s               <= ARGs_VD_to_knl_q(0);
    reg_ro.current_time_ns              <= ARGs_VD_to_knl_q(1);
    reg_ro.debug_delay_buffer_status    <= ARGs_VD_to_knl_q(2)(7 downto 0);
    reg_ro.debug_buffer_gen_cycles      <= ARGs_VD_to_knl_q(3);
    reg_ro.debug_running_fsm_debug      <= ARGs_VD_to_knl_q(4);
    
    vd_gen_no_of_chans                  <= std_logic_vector(to_unsigned(g_N128_SOURCE_INSTANCES,vd_gen_no_of_chans'length));
    
    reg_ro.vd_gen_no_of_chans           <= vd_gen_no_of_chans & "0000000";
    --------------------------------------------------------------------------------------    
    -- Registers
    --  
    regi : entity vd_datagen_lib.vd_datagen_reg
    port map (
        MM_CLK              => clk, -- in std_logic;
        MM_RST              => reset_axi, -- in std_logic;
        SLA_IN              => i_vdgen_lite_axi_mosi, -- in t_axi4_lite_mosi;
        SLA_OUT             => o_vdgen_lite_axi_miso, -- out t_axi4_lite_miso;
        VD_DATAGEN_REG_FIELDS_RW => reg_rw, -- out t_vd_datagen_reg_rw;
        VD_DATAGEN_REG_FIELDS_RO => reg_ro  -- in  t_vd_datagen_reg_ro;
    );
    
    -- reg_ro.current_time_s               <= cur_time_seconds;
    -- reg_ro.current_time_ns              <= cur_time_ns;

    -- reg_ro.debug_delay_buffer_status(0)             <= poly_buf0_valid;
    -- reg_ro.debug_delay_buffer_status(1)             <= poly_buf1_valid;
    -- reg_ro.debug_delay_buffer_status(2)             <= buf0_more_recent;
    -- reg_ro.debug_delay_buffer_status(3)             <= delays_buf0_used;
    -- reg_ro.debug_delay_buffer_status(4)             <= delays_buf1_used;
    -- reg_ro.debug_delay_buffer_status(7 downto 5)    <= "000";
    
    -- Note : reg_rw fields:
    --   enable_vd       : std_logic_vector(1 downto 0); 
    --                       bit 0 = buffer 0 has valid data (buffer 0 use is also subject to valid_time_buf0)
    --                       bit 1 = buffer 1 has valid data (buffer 1 use is also subject to valid_time_buf1)"
    --   valid_time_buf0 : std_logic_vector(31 downto 0);
    --   valid_time_buf1 : std_logic_vector(31 downto 0);
    --   start_time_s : 32 bits
    --   start_time_ns : 32 bits
    --
    -- reg_ro:
    --   current_time	: std_logic_vector(31 downto 0) = current unix time;
	
	-----------------------------------------------------------------------
	-- Control
	--  - On i_start, generate 512 MBytes of data 
	--    Polynomial evaluation in done once for every c_SAMPLES_PER_POLY_RUN = 32 output samples.
	--    In the HBM buffer, 
	--      Each station+channel has 512 Mbytes/(1024) = 512 kBytes of data
	--      = 64 * 8 kbytes = 64 SPS data packets (per station or channel)
	--      = 2048 samples/packet * 64 SPS packets
	--      = 131072 samples
	--      => 131072/32 = 4096 runs of the polynomial evaluation
	--      = 141.55776 ms of data
	--

    process(clk_vd)
    begin
        if rising_edge(clk_vd) then
            if start_del1 = '1' and start_del2 = '0' then
                debug_buffer_gen_cycles <= (others => '0');
                calc_cycles <= '1';
            elsif wr_buffer_done = '1' then
                calc_cycles <= '0';
            end if;

            calc_cycles_d   <= calc_cycles;

            if calc_cycles = '1' then
                debug_buffer_gen_cycles <= debug_buffer_gen_cycles + 1;
            end if;

            if calc_cycles_d = '1' AND calc_cycles = '0' then
                if debug_buffer_gen_cycles > debug_buffer_gen_cache then
                    debug_buffer_gen_cache  <= debug_buffer_gen_cycles;
                end if;

            end if;

            reset_vd <= reset_from_kernel OR clk_vd_reset;
        end if;
    end process;


    process(clk_vd)
    begin
        if rising_edge(clk_vd) then
            o_poly_runs <= poly_runs;
        
            start_del1 <= start_internal;
            start_del2 <= start_del1;
            poly_eval_start <= '0';  -- overwritten in the "check_ready" state to '1'.
            if reset_vd = '1' then
                -- time in seconds, reg_rw.valid_time_buf0 and reg_rw.valid_time_buf1
                -- are compared against this value.
                cur_time_seconds <= start_time_s_vd;
                -- Time in nanoseconds, counts 0 to 1 billion in steps of 16 SPS samples
                -- (corresponding to the amount of time generated by one polynomial evaluation cycle).
                cur_time_ns <= start_time_ns_vd;
                -- Which 512 MByte HBM buffer we are writing to
                cur_buffer <= '0';
                -- count 8192 runs of the polynomial evaluation required to fill the HBM buffer.
                poly_runs <= (others => '0');
                -- Which set of polynomials to use (also set in the state check_buffer)
                poly_eval_buf <= '0';
                start_512M_buffer <= x"00";
                running_fsm <= idle;
                running_fsm_debug <= x"F";
            else
                if start_del1 = '1' and start_del2 = '0' then
                    -- start filling a 512 MByte HBM buffer.
                    running_fsm <= check_buffer;
                    poly_runs <= (others => '0');
                    start_512M_buffer <= x"FF";
                else
                    start_512M_buffer <= start_512M_buffer(6 downto 0) & '0';
                    case running_fsm is
                        when idle =>
                            running_fsm_debug <= x"0";
                            -- wait in this state until we are told to start a new buffer.
                            running_fsm <= idle;
                        
                        when check_buffer =>
                            running_fsm_debug <= x"1";
                            -- which set of polynomials to use ?
                            if ((poly_buf0_valid = '1' and poly_buf1_valid = '0') or
                                (poly_buf0_valid = '1' and poly_buf1_valid = '1' and buf0_more_recent = '1')) then
                                poly_eval_buf <= '0';
                            elsif poly_buf1_valid = '1' then
                                poly_eval_buf <= '1';
                            end if;
                            running_fsm <= calc_time_ns_start;
                            
                        when calc_time_ns_start =>
                            running_fsm_debug <= x"2";
                            -- calculate the time in ns relative to the start time for this buffer.
                            -- eval_time_ns = 1e9 * (cur_time_seconds - reg_rw.valid_time_buf0/1) + cur_time_ns
                            -- Logic is below, this state just triggers it.
                            running_fsm <= calc_time_ns_run;
                            
                        when calc_time_ns_run =>
                            running_fsm_debug <= x"3";
                            if time_ns_fp64_valid = '1' then
                                time_ns_fp64_hold <= time_ns_fp64;
                                running_fsm <= check_ready;
                            end if;
                            
                        when check_ready =>
                            running_fsm_debug <= x"4";
                            if ((poly_runs(0) = '0' and delays_buf0_used = '0') or
                                (poly_runs(0) = '1' and delays_buf1_used = '0')) then
                                -- i.e. in the dataGen_singleSource modules, there is space available 
                                -- in the buffer at the output of the polynomial generation
                                poly_eval_start <= '1';
                                running_fsm <= update_time_ns;
                            end if;
                        
                        when update_time_ns =>
                            running_fsm_debug <= x"5";
                            -- Each polynomial evaluation updates the time by 32 samples, 1080 ns per sample.
                            cur_time_ns <= std_logic_vector(unsigned(cur_time_ns) + c_SAMPLES_PER_POLY_RUN * 1080);  -- SAMPLES_PER_BLOCK = 32 samples in block of samples created
                            running_fsm <= update_time_seconds;
                            
                        when update_time_seconds =>
                            running_fsm_debug <= x"6";
                            if (unsigned(cur_time_ns) > 999999999) then
                                cur_time_ns <= std_logic_vector(unsigned(cur_time_ns) - 1000000000);
                                cur_time_seconds <= std_logic_vector(unsigned(cur_time_seconds) + 1);
                            end if;
                            running_fsm <= wait_poly_start;
                            wait_start_count <= "1111";
                        
                        when wait_poly_start =>
                            running_fsm_debug <= x"7";
                            -- wait for polynomial evaluation to start so that any_poly_running is valid
                            wait_start_count <= std_logic_vector(unsigned(wait_start_count) - 1);
                            if unsigned(wait_start_count) = 0 then
                                running_fsm <= wait_poly_done;
                            end if; 
                        
                        when wait_poly_done =>
                            running_fsm_debug <= x"8";
                            if any_poly_running = '0' then
                                running_fsm <= poly_done;
                            end if;
                        
                        when poly_done =>
                            running_fsm_debug <= x"9";
                            running_fsm <= update_poly_runs;
                        
                        when update_poly_runs =>
                            running_fsm_debug <= x"A";
                            if unsigned(poly_runs) = (g_128_SAMPLE_BLOCKS*(c_POLY_RUNS_PER_128SPS_SAMPLES) - 1) then
                                -- Each poly run is c_SAMPLES_PER_BLOCK(=32) SPS samples, c_POLY_RUNS_PER_128SPS_SAMPLES(= 4) poly runs per 128 SPS samples,
                                -- poly_runs counts from 0, so e.g. if g_128_SAMPLE_BLOCKS = 1 then
                                -- we need 8 poly_runs, so we stop here when poly_runs = 7.
                                poly_runs <= (others => '0');
                                running_fsm <= wait_all_done;
                            else
                                poly_runs <= std_logic_vector(unsigned(poly_runs) + 1);
                                running_fsm <= done_poly_run;
                            end if;
                            
                        when done_poly_run =>
                            running_fsm_debug <= x"B";
                            running_fsm <= check_buffer;
                        
                        when wait_all_done =>
                            running_fsm_debug <= x"C";
                            -- wait until all the data has been written to the HBM buffer.
                            if wr_buffer_done = '1' then
                                running_fsm <= idle;
                                cur_buffer <= not cur_buffer;
                            end if;
                        
                        when others =>
                            running_fsm <= idle;
                        
                    end case;
                end if;
            end if;
            
            -----------------------------------------------------------------------------------
            -- calculate the time in ns relative to the start time for this buffer.
            -- eval_time_ns = 1e9 * (cur_time_seconds - reg_rw.valid_time_buf0/1) + cur_time_ns
            if poly_eval_buf = '0' then
                base_time_seconds <= valid_time_buf0_vd;
            else
                base_time_seconds <= valid_time_buf1_vd;
            end if;
            if (running_fsm = calc_time_ns_start) then
                calc_ns_del1 <= '1';
            else
                calc_ns_del1 <= '0';
            end if;
            --
            time_seconds_offset <= std_logic_vector(unsigned(cur_time_seconds) - unsigned(base_time_seconds));
            calc_ns_del2 <= calc_ns_del1;
            -- 4 cycle latency for the multiplier to get time_ns_offset from time_seconds_offset
            calc_ns_del3 <= calc_ns_del2;
            calc_ns_del4 <= calc_ns_del3;
            calc_ns_del5 <= calc_ns_del4;
            calc_ns_del6 <= calc_ns_del5;
            --
            time_ns_offset_del1 <= "00" & time_ns_offset;
            calc_ns_del7 <= calc_ns_del6;
            --
            calc_ns_del8 <= calc_ns_del7;
            eval_time_ns <= std_logic_vector(unsigned(time_ns_offset_del1) + unsigned(cur_time_ns_ext));
            
            -----------------------------------------------------------------------------------
            
            poly_running_del1 <= poly_running;
            if (poly_running_del1 /= "00000000") then
                any_poly_running <= '1';
            else
                any_poly_running <= '0';
            end if;
            
            if ((unsigned(cur_time_seconds) >= unsigned(valid_time_buf0_vd)) and enable_vd_vd(0) = '1') then
                poly_buf0_valid <= '1';
            else
                poly_buf0_valid <= '0';
            end if;
            if ((unsigned(cur_time_seconds) >= unsigned(valid_time_buf1_vd)) and enable_vd_vd(1) = '1') then
                poly_buf1_valid <= '1';
            else
                poly_buf1_valid <= '0';
            end if;
            if (unsigned(valid_time_buf0_vd) >= unsigned(valid_time_buf1_vd)) then
                buf0_more_recent <= '1';
            else
                buf0_more_recent <= '0';
            end if;
            
            if (running_fsm /= idle and cur_buffer = '0') then
                writing_buf0_internal <= '1';
            else
                writing_buf0_internal <= '0';
            end if;
            
            if (running_fsm /= idle and cur_buffer = '1') then
                writing_buf1_internal <= '1';
            else
                writing_buf1_internal <= '0';
            end if;
            
            --------------------------------------------------------------
            -- Trigger running of the data generation.
            -- About 4000 clocks after starting the polynomial generation,
            -- delay data will be available in the double buffers in the 
            -- "datagen_singleSource" modules.
            -- (of which there are up to 32 = 8 x (datagen_128) x (4 per datagen_128)
            -- 
            if running_fsm = poly_done then
                if poly_runs(0) = '0' then
                    -- Just wrote buffer 0 in datagen_singleSource modules
                    delays_buf0_used <= '1';
                    delays_buf0_run_count <= poly_runs;
                else
                    -- Just wrote buffer 1 in datagen_singleSource modules
                    delays_buf1_used <= '1';
                    delays_buf1_run_count <= poly_runs;
                end if;
            end if;
            
            if use_delays_fsm = clear_used and read_delays_buffer = '0' then
                delays_buf0_used <= '0';
            end if;
            if use_delays_fsm = clear_used and read_delays_buffer = '1' then
                delays_buf1_used <= '0';
            end if;
            
            if reset_vd = '1' then
                use_delays_fsm <= idle;
            else
                case use_delays_fsm is
                    when idle =>
                        if ((delays_buf0_used = '1' and delays_buf1_used ='0') or
                            (delays_buf0_used = '1' and delays_buf1_used = '1' and 
                             (unsigned(delays_buf0_run_count) < unsigned(delays_buf1_run_count)))) then
                            use_delays_fsm <= run_delay_buf0;
                        elsif ((delays_buf0_used = '0' and delays_buf1_used ='1') or
                               (delays_buf0_used = '1' and delays_buf1_used = '1' and 
                                (unsigned(delays_buf1_run_count) < unsigned(delays_buf0_run_count)))) then 
                            use_delays_fsm <= run_delay_buf1;
                        end if;
                        wait_a_while_count <= "1111";
                        read_delays <= '0';
                        read_delays_buffer <= '0';
                    
                    when run_delay_buf0 =>
                        read_delays <= '1';
                        read_delays_buffer <= '0';
                        use_delays_fsm <= wait_a_while;
                        wait_a_while_count <= "1111";
                    
                    when run_delay_buf1 =>
                        read_delays <= '1';
                        read_delays_buffer <= '1';
                        use_delays_fsm <= wait_a_while;
                        wait_a_while_count <= "1111";
                        
                    when wait_a_while =>
                        -- Wait a while to allow time for read_delay_running to propagate through
                        -- some pipeline stages and go high.
                        read_delays <= '0';
                        wait_a_while_count <= std_logic_vector(unsigned(wait_a_while_count) - 1);
                        if (unsigned(wait_a_while_count) = 0) then
                            use_delays_fsm <= wait_done;
                        end if; 
                        
                    when wait_done =>  -- wait until the delay buffer has been processed.
                        read_delays <= '0';
                        wait_a_while_count <= "1111";
                        if read_delay_running = "00000000" then
                            use_delays_fsm <= clear_used;
                        end if;
                    
                    when clear_used =>
                        read_delays_buffer <= '0';
                        use_delays_fsm <= idle;
                        
                    when others =>
                        use_delays_fsm <= idle;
                end case;
            end if;
            
            for i in 0 to 1 loop
                if (all_source_rdy(0)(i) = '1' and all_source_rdy(1)(i) = '1' and all_source_rdy(2)(i) = '1' and all_source_rdy(3)(i) = '1' and
                    all_source_rdy(4)(i) = '1' and all_source_rdy(5)(i) = '1' and all_source_rdy(6)(i) = '1' and all_source_rdy(7)(i) = '1' and
                    rdy_holdoff_block(1-i) = '0') then
                    all_gen128_source_rdy(i) <= '1';
                else
                    all_gen128_source_rdy(i) <= '0';
                end if;
                
                -- Prevent ready for data source 0 going high for 16 clocks after 
                -- ready for data source 1 goes high, and vice-versa. 
                all_gen128_source_rdy_del1(i) <= all_gen128_source_rdy(i);
                if all_gen128_source_rdy(i) = '1' and all_gen128_source_rdy_del1(i) = '0' then
                    rdy_holdoff(i) <= "1111";
                elsif rdy_holdoff(i) /= "0000" then
                    rdy_holdoff(i) <= std_logic_vector(unsigned(rdy_holdoff(i)) - 1);
                end if;
                
                if rdy_holdoff(i) = "0000" then
                    rdy_holdoff_block(i) <= '0';
                else
                    rdy_holdoff_block(i) <= '1';
                end if;
                
            end loop;
            
        end if;
    end process;
    
    
    -----------------------------------------------------------------------
    -- Full AXI interface - write to the ultraRAM 
    -- Full axi to bram
    vd_axi_bram_inst : entity vd_datagen_lib.vd_datagen_axi_bram_wrapper
    port map ( 
        i_clk                   => clk,
        i_rst                   => reset_axi,

        i_clk_vd                => clk_vd,
        i_clk_vd_reset          => clk_vd_reset,
        -------------------------------------------------------
        -- Block memory interface
        o_bram_wrEn             => args_bram_wren(0),   -- out std_logic;
        o_bram_addr             => args_bram_addr(0), -- out (19:0), 1 Mbyte address space, low 2 bits are always "00" since transactions are 4 bytes wide
        o_bram_wrdata           => args_bram_wr_data(0), -- out (31:0); 4-byte wide write data
        -- Assumes 11 clock read latency from bram_addr
        i_bram_rddata           => args_bram_rd_data(8), -- in (31:0); 4 byte wide read data, 
        -------------------------------------------------------
        -- ARGs axi interface
        i_vd_full_axi_mosi      => i_vdgen_full_axi_mosi, -- in  t_axi4_full_mosi
        o_vd_full_axi_miso      => vdgen_full_axi_miso  -- out t_axi4_full_miso
    );
    
    o_vdgen_full_axi_miso <= vdgen_full_axi_miso;
    
    args_bram_rd_data(0) <= (others => '0');
    args_bram_rd_data_valid(0) <= '0';
    data_samples(0) <= (others => '0');
    data_valid(0) <= '0';
    data_stream(0) <= (others => '0');
    data_block(0) <= '0';
    
    -- constant multiply by 1e9
    -- 4 cycle latency
    mult_1e9i : mult_x1e9
    port map (
        CLK => clk_vd,
        A   => time_seconds_offset, -- in(31:0)
        P   => time_ns_offset -- out (61:0);
    );
    cur_time_ns_ext <= x"00000000" & cur_time_ns;
    
    int2doublei : uint64_to_double
    port map (
        aclk => clk_vd,
        -- uint64 input
        s_axis_a_tvalid => calc_ns_del8,
        s_axis_a_tdata => eval_time_ns,
        -- double precision output, 6 clock latency
        m_axis_result_tvalid => time_ns_fp64_valid,
        m_axis_result_tdata => time_ns_fp64
    );
    
    
    gen_dataGen : for i in 0 to 7 generate
        gen_128Gen : if (i < g_N128_SOURCE_INSTANCES) generate
        
            datageni: entity vd_datagen_lib.dataGen_128
            generic map (
                -- up to 8 instances of this module, 
                g_DATAGEN_128_INSTANCE => i -- : integer range 0 to 7 := 0
            ) port map (
                clk => clk_vd,
                reset => reset_vd,
                ------------------------------------------------------------
                -- Reading/Writing from/to the configuration memory 
                i_args_wr_data => args_bram_wr_data(i), -- in (31:0);
                i_args_addr    => args_bram_addr(i),    -- in (19:0); byte address
                i_args_wren    => args_bram_wren(i),    -- in std_logic;
                i_args_rd_data => args_bram_rd_data(i), -- in (31:0);
                i_args_rd_data_valid => args_bram_rd_data_valid(i), -- in std_logic;
                -- Pipelined version of the control signals, to go to the next dataGen_128 module.
                -- If the read address is for a memory in this module, 
                -- then put i_host_return_data on o_host_return_data, 
                -- otherwise put data read from inside this module. 
                o_args_wr_data => args_bram_wr_data(i+1), -- out (31:0);
                o_args_addr    => args_bram_addr(i+1),    -- out (19:0);
                o_args_wren    => args_bram_wren(i+1),    -- out std_logic;
                o_args_rd_data => args_bram_rd_data(i+1), -- out (31:0);
                o_args_rd_data_valid => args_bram_rd_data_valid(i+1), -- out std_logic;
                ------------------------------------------------------------
                -- control - Generate a burst of 16 samples for all data streams
                i_poly_eval_start    => poly_eval_start, --  in std_logic; -- Evaluate all polynomials and then increment time by g_TIME_STEP
                -- which of the polynomial buffers to use, only read on i_start
                i_poly_eval_buf      => poly_eval_buf, --  in std_logic;
                -- Double precision time in ns, read on i_poly_eval_start
                i_poly_eval_time_ns  => time_ns_fp64_hold, --  in std_logic_vector(63 downto 0); 
                o_poly_eval_running  => poly_running(i), --  out std_logic; -- status; if '1', module is busy.
                -- trigger readout and processing of the delay data to generate samples.
                i_read_delays => read_delays,        -- in std_logic;
                i_read_buffer => read_delays_buffer, -- in std_logic; Which of the 2 buffers in the dataGen_singleSource modules to read from
                o_read_delay_running => read_delay_running(i), -- out std_logic;
                -- At the lowest level there are two pipelines for processing sources,
                -- synchronise at the top level to ensure output data goes to the HBM buffer 
                -- module at the same time. 
                o_all_source_rdy => all_source_rdy(i),     -- out (1:0);
                i_all_source_rdy => all_gen128_source_rdy, -- in  (1:0);
                -------------------------------------------------------------
                -- Data output
                -- data in from the previous dataGen_128 instance
                i_data_samples => data_samples(i), -- in (255:0);
                i_data_stream  => data_stream(i),  -- in (10:0);
                i_data_block   => data_block(i),   -- in std_logic;
                i_data_valid   => data_valid(i),   -- in std_logic;
                -- Pipelined output; either from the i_data signals or generated internally 
                o_data_samples => data_samples(i+1), -- out (255:0);
                o_data_stream  => data_stream(i+1),  -- out (10:0);
                o_data_block   => data_block(i+1),   -- out std_logic;
                o_data_valid   => data_valid(i+1)    -- out std_logic
            );
        end generate;
        
        skip_gen_128Gen : if (i >= g_N128_SOURCE_INSTANCES) generate
        
            process(clk_vd)
            begin
                if rising_edge(clk_vd) then
                    args_bram_wr_data(i+1) <= args_bram_wr_data(i);
                    args_bram_addr(i+1) <= args_bram_addr(i);
                    args_bram_wren(i+1) <= args_bram_wren(i);
                    args_bram_rd_data(i+1) <= args_bram_rd_data(i);
                    args_bram_rd_data_valid(i+1) <= args_bram_rd_data_valid(i);
                    
                    data_samples(i+1) <= data_samples(i);
                    data_stream(i+1) <= data_stream(i);
                    data_block(i+1) <= data_block(i);
                    data_valid(i+1) <= data_valid(i);
                    
                    poly_running(i) <= '0';
                    read_delay_running(i) <= '0';
                    all_source_rdy(i) <= "11";
                    
                end if;
            end process;
        
        end generate;
    end generate;


--------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------
-- CDC VD_GEN_CLK to the Kernel Clock
cdc_K_to_VD : for i in 0 to (Kernel_to_VD_clk-1) generate
    i_kernel_to_vd : entity signal_processing_common.sync
        generic map (
            DEST_SYNC_FF    => 2,
            WIDTH           => 1
        )
        Port Map ( 
            Clock_a     => clk,
            data_in(0)  => kernel_to_vd_in(i),
            
            Clock_b     => clk_vd,
            data_out(0) => kernel_to_vd_q(i)
        );
end generate;

kernel_to_vd_in(0)  <= i_start;                 -- from HBM RD circuit and holds this high while the buffer is generated, triggers on edge at dest.
kernel_to_vd_in(1)  <= wr_buffer_done_kernel;   -- from buffer write and is held high for 8 cycles.
kernel_to_vd_in(2)  <= reset;                   -- comes from VD and this is tied to a register to host.

start_internal      <= kernel_to_vd_q(0);
wr_buffer_done      <= kernel_to_vd_q(1);
reset_from_kernel   <= kernel_to_vd_q(2);

-------------------------------------------------------------
-- CDC Kernel Clock to the VD_GEN_CLK
cdc_VD_to_K : for i in 0 to (VD_to_kernel_clk-1) generate
    i_kernel_to_vd : entity signal_processing_common.sync
        generic map (
            DEST_SYNC_FF    => 2,
            WIDTH           => 1
        )
        Port Map ( 
            Clock_a     => clk_vd,
            data_in(0)  => vd_to_kernel_in(i),
            
            Clock_b     => clk,
            data_out(0) => vd_to_kernel_q(i)
        );
end generate;
vd_to_kernel_in(0)  <= writing_buf0_internal;
vd_to_kernel_in(1)  <= writing_buf1_internal;
vd_to_kernel_in(2)  <= start_512M_buffer(7);

o_writing_buf0              <= vd_to_kernel_q(0);
o_writing_buf1              <= vd_to_kernel_q(1);
start_512M_buffer_kernel    <= vd_to_kernel_q(2);


CDC_fifo : entity signal_processing_common.xpm_fifo_wrapper
    Generic map (
        FIFO_DEPTH          => 16,
        DATA_WIDTH          => 268,
        FIFO_MEMORY_TYPE    => "distributed" -- or distributed
    )
    Port Map ( 
        fifo_reset      => reset_vd,
        -- RD    
        fifo_rd_clk     => clk,
        fifo_rd         => CDC_fifo_rd,
        fifo_q          => CDC_fifo_q,
        fifo_q_valid    => open, --CDC_fifo_rd,
        fifo_empty      => CDC_fifo_empty,
        fifo_rd_count   => open,
        -- WR        
        fifo_wr_clk     => clk_vd,
        fifo_wr         => data_valid(8),
        fifo_data       => CDC_fifo_data,
        fifo_full       => open,
        fifo_wr_count   => open
    );

    CDC_fifo_data(255 downto 0)     <= data_samples(8);
    CDC_fifo_data(266 downto 256)   <= data_stream(8);
    CDC_fifo_data(267)              <= data_block(8);
    CDC_fifo_rd                     <= NOT CDC_fifo_empty;

--------------------------------------------------------------------------------------------------------

    -- dataGen_buffer holds an ultraRAM buffer to create 512-byte 
    -- data chunks that can be written to the HBM,
    dgen_bufi : entity vd_datagen_lib.dataGen_buffer
    generic map (
        g_DEBUG_ILA             => g_DEBUG_ILA,
        g_N128_SOURCE_INSTANCES => g_N128_SOURCE_INSTANCES,
        g_128_SAMPLE_BLOCKS     => g_128_SAMPLE_BLOCKS -- integer range 1 to 1024 := 1024
    ) port map (
        clk => clk, --  in std_logic;
        i_reset => reset, -- in std_logic;
        -- control. On i_start, start writes to the next buffer.
        i_start => start_512M_buffer_kernel, --  in std_logic;
        o_cur_buffer => open,         --  out std_logic;
        -- pulse high when finished writing the last of the g_128_SAMPLE_BLOCKS
        o_buffer_done => wr_buffer_done_kernel, -- out std_logic;
        -- data input 
        i_data_samples => CDC_fifo_q(255 downto 0), --data_samples(8), -- in (255:0);
        i_data_stream  => CDC_fifo_q(266 downto 256), --data_stream(8),  -- in (9:0);
        i_data_block   => CDC_fifo_q(267), --data_block(8),   -- in std_logic; -- two blocks of 16 samples received for each stream before moving on to the next stream.
        i_data_valid   => CDC_fifo_rd,   -- in std_logic;
        -- HBM axi write interface
        -- aw bus
        m01_axi_awvalid  => m01_axi_awvalid, -- out std_logic;
        m01_axi_awready  => m01_axi_awready, -- in std_logic;
        m01_axi_awaddr   => m01_axi_awaddr,  -- out (31:0);
        m01_axi_awlen    => m01_axi_awlen,   -- out (7:0);
        -- w bus - write data
        m01_axi_wvalid   => m01_axi_wvalid, -- out std_logic
        m01_axi_wready   => m01_axi_wready, -- in std_logic;
        m01_axi_wdata    => m01_axi_wdata,  -- out (511:0);
        m01_axi_wlast    => m01_axi_wlast,  -- out std_logic;
        -- b bus - write response
        m01_axi_bvalid   => m01_axi_bvalid,  -- in std_logic;
        m01_axi_bresp    => m01_axi_bresp    -- in (1:0)
    );
    
end Behavioral;
