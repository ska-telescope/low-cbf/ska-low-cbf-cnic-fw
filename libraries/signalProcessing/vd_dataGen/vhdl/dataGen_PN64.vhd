----------------------------------------------------------------------------------
-- Company: CSIRO
-- Engineer: David Humphrey (dave.humphrey@csiro.au)
-- 
-- Create Date: 06/04/2023 11:20:16 PM
-- Module Name: dataGen_dataSource - Behavioral
-- Description: 
--  Generates data, delays it and filters it
--  Data generation uses the xorshift64 algorithm :
--    uint64_t xorshift64(struct xorshift64_state *state){
--       uint64_t x = state->a;
--       x ^= x << 13;
--       x ^= x >> 7;
--       x ^= x << 17;
--	     return state->a = x;
--    }
-- 
--  8 groups of 8 bits are summed from the 64 bit state, to generate a 12 bit 
--  random value with roughly Gaussian statistics. 
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
library common_lib;
USE common_lib.common_pkg.ALL;

entity dataGen_PN64 is
    Port(
        clk : in std_logic;
        ------------------------------------------------------------------
        -- i_load should occur prior to i_run
        i_load : in std_logic;
        i_PN_state : in std_logic_vector(63 downto 0);
        i_run : in std_logic;
        -- Valid data on o_dout is signaled by o_valid, which is 2 clocks after i_run.
        -- The output data is generated from the current 64-bit state.
        -- The new state is also calculated and stored.
        o_dout : out std_logic_vector(11 downto 0);
        o_PN_state : out std_logic_vector(63 downto 0);
        o_valid : out std_logic
    );
end dataGen_PN64;

architecture Behavioral of dataGen_PN64 is

    signal cur_state : std_logic_vector(63 downto 0);
    signal data_12bit : t_slv_12_arr(7 downto 0);
    signal sum_12bit0, sum_12bit1, sum_12bit : std_logic_vector(11 downto 0);
    signal run_del1, run_del2 : std_logic;
    
begin
    
    process(clk)
        variable cur_state_v, cur_state_v2, cur_state_v3 : std_logic_vector(63 downto 0);
        variable tap : t_slv_12_arr(7 downto 0);
    begin
        if rising_edge(clk) then
            if i_load = '1' then
                cur_state <= i_PN_state;
            elsif i_run = '1' then
                cur_state_v(63 downto 13) := cur_state(50 downto 0);
                cur_state_v(12 downto 0) := "0000000000000";
                cur_state_v := cur_state_v xor cur_state;
                cur_state_v2(56 downto 0) := cur_state_v(63 downto 7);
                cur_state_v2(63 downto 57) := "0000000";
                cur_state_v := cur_state_v xor cur_state_v2;
                cur_state_v2(63 downto 17) := cur_state_v(46 downto 0);
                cur_state_v2(16 downto 0) := "00000000000000000";
                cur_state <= cur_state_v xor cur_state_v2;
            end if;
            
            run_del1 <= i_run;
            run_del2 <= run_del1;
            
            sum_12bit0 <= std_logic_vector(signed(data_12bit(0)) - signed(data_12bit(1)) + signed(data_12bit(2)) - signed(data_12bit(3)));
            sum_12bit1 <= std_logic_vector(signed(data_12bit(4)) - signed(data_12bit(5)) + signed(data_12bit(6)) - signed(data_12bit(7)));
            
            sum_12bit <= std_logic_vector(signed(sum_12bit0) + signed(sum_12bit1)); 
        end if;
    end process;
    
    b12_gen : for i in 0 to 7 generate
        data_12bit(i) <= cur_state(i*8+7) & cur_state(i*8+7) & cur_State(i*8+7) & cur_state(i*8+7) & cur_state(i*8 + 7 downto i*8);
    end generate;
    
    o_PN_state <= cur_state;
    o_dout <= sum_12bit;
    o_valid <= run_del2;
    
end Behavioral;
