create_ip -name axi_bram_ctrl -vendor xilinx.com -library ip -version 4.1 -module_name axi_bram_ctrl_vd_datagen
set_property -dict [list \
  CONFIG.SUPPORTS_NARROW_BURST {0} \
  CONFIG.SINGLE_PORT_BRAM {1} \
  CONFIG.Component_Name {axi_bram_ctrl_vd_datagen} \
  CONFIG.READ_LATENCY {11} \
  CONFIG.MEM_DEPTH {262144}] [get_ips axi_bram_ctrl_vd_datagen]
create_ip_run [get_ips axi_bram_ctrl_vd_datagen]

create_ip -name axi_clock_converter -vendor xilinx.com -library ip -version 2.1 -module_name axi_clock_converter_addr20b
set_property -dict [list \
  CONFIG.ADDR_WIDTH {20} \
  CONFIG.Component_Name {axi_clock_converter_addr20b} \
] [get_ips axi_clock_converter_addr20b]
create_ip_run [get_ips axi_clock_converter_addr20b]

create_ip -name blk_mem_gen -vendor xilinx.com -library ip -version 8.4 -module_name datagen_interp_rom16_32tap
set_property -dict [list \
  CONFIG.Coe_File "$coepath/interp_taps_16bit_32tap.coe" \
  CONFIG.Component_Name {datagen_interp_rom16_32tap} \
  CONFIG.Disable_Collision_Warnings {true} \
  CONFIG.Disable_Out_of_Range_Warnings {true} \
  CONFIG.Enable_A {Always_Enabled} \
  CONFIG.Load_Init_File {true} \
  CONFIG.Memory_Type {Single_Port_ROM} \
  CONFIG.Register_PortA_Output_of_Memory_Core {true} \
  CONFIG.Write_Depth_A {1024} \
  CONFIG.Write_Width_A {512} \
] [get_ips datagen_interp_rom16_32tap]
create_ip_run [get_ips datagen_interp_rom16_32tap]

create_ip -name floating_point -vendor xilinx.com -library ip -version 7.1 -module_name fp64_add
set_property -dict [list \
  CONFIG.A_Precision_Type {Double} \
  CONFIG.Add_Sub_Value {Add} \
  CONFIG.C_A_Exponent_Width {11} \
  CONFIG.C_A_Fraction_Width {53} \
  CONFIG.C_Accum_Input_Msb {32} \
  CONFIG.C_Accum_Lsb {-31} \
  CONFIG.C_Accum_Msb {32} \
  CONFIG.C_Latency {14} \
  CONFIG.C_Mult_Usage {Full_Usage} \
  CONFIG.C_Rate {1} \
  CONFIG.C_Result_Exponent_Width {11} \
  CONFIG.C_Result_Fraction_Width {53} \
  CONFIG.Component_Name {fp64_add} \
  CONFIG.Flow_Control {NonBlocking} \
  CONFIG.Has_RESULT_TREADY {false} \
  CONFIG.Result_Precision_Type {Double} \
] [get_ips fp64_add]
create_ip_run [get_ips fp64_add]

create_ip -name floating_point -vendor xilinx.com -library ip -version 7.1 -module_name fp64_mult
set_property -dict [list \
  CONFIG.A_Precision_Type {Double} \
  CONFIG.C_A_Exponent_Width {11} \
  CONFIG.C_A_Fraction_Width {53} \
  CONFIG.C_Accum_Input_Msb {32} \
  CONFIG.C_Accum_Lsb {-31} \
  CONFIG.C_Accum_Msb {32} \
  CONFIG.C_Latency {12} \
  CONFIG.C_Mult_Usage {Full_Usage} \
  CONFIG.C_Rate {1} \
  CONFIG.C_Result_Exponent_Width {11} \
  CONFIG.C_Result_Fraction_Width {53} \
  CONFIG.Component_Name {fp64_mult} \
  CONFIG.Flow_Control {NonBlocking} \
  CONFIG.Has_RESULT_TREADY {false} \
  CONFIG.Operation_Type {Multiply} \
  CONFIG.Result_Precision_Type {Double} \
] [get_ips fp64_mult]
create_ip_run [get_ips fp64_mult]

create_ip -name floating_point -vendor xilinx.com -library ip -version 7.1 -module_name fp64_to_int
set_property -dict [list \
  CONFIG.A_Precision_Type {Double} \
  CONFIG.C_A_Exponent_Width {11} \
  CONFIG.C_A_Fraction_Width {53} \
  CONFIG.C_Accum_Input_Msb {32} \
  CONFIG.C_Accum_Lsb {-31} \
  CONFIG.C_Accum_Msb {32} \
  CONFIG.C_Latency {6} \
  CONFIG.C_Mult_Usage {No_Usage} \
  CONFIG.C_Rate {1} \
  CONFIG.C_Result_Exponent_Width {32} \
  CONFIG.C_Result_Fraction_Width {32} \
  CONFIG.Component_Name {fp64_to_int} \
  CONFIG.Flow_Control {NonBlocking} \
  CONFIG.Has_RESULT_TREADY {false} \
  CONFIG.Operation_Type {Float_to_fixed} \
  CONFIG.Result_Precision_Type {Custom} \
] [get_ips fp64_to_int]
create_ip_run [get_ips fp64_to_int]

create_ip -name dds_compiler -vendor xilinx.com -library ip -version 6.0 -module_name sincos_lut
set_property -dict [list \
  CONFIG.Amplitude_Mode {Unit_Circle} \
  CONFIG.Component_Name {sincos_lut} \
  CONFIG.DATA_Has_TLAST {Not_Required} \
  CONFIG.Has_Phase_Out {false} \
  CONFIG.Latency {7} \
  CONFIG.M_DATA_Has_TUSER {Not_Required} \
  CONFIG.Noise_Shaping {Taylor_Series_Corrected} \
  CONFIG.Output_Frequency1 {0} \
  CONFIG.Output_Width {18} \
  CONFIG.PINC1 {0} \
  CONFIG.Parameter_Entry {Hardware_Parameters} \
  CONFIG.PartsPresent {SIN_COS_LUT_only} \
  CONFIG.Phase_Width {24} \
  CONFIG.S_PHASE_Has_TUSER {Not_Required} \
] [get_ips sincos_lut]
create_ip_run [get_ips sincos_lut]

create_ip -name cmpy -vendor xilinx.com -library ip -version 6.0 -module_name cmult_24x18
set_property -dict [list \
  CONFIG.APortWidth {24} \
  CONFIG.BPortWidth {18} \
  CONFIG.Component_Name {cmult_24x18} \
  CONFIG.MinimumLatency {4} \
  CONFIG.OptimizeGoal {Performance} \
  CONFIG.OutputWidth {43} \
] [get_ips cmult_24x18]
create_ip_run [get_ips cmult_24x18]

create_ip -name axi_protocol_checker -vendor xilinx.com -library ip -version 2.0 -module_name axi_protocol_checker_512
set_property -dict [list \
  CONFIG.Component_Name {axi_protocol_checker_512} \
  CONFIG.DATA_WIDTH {512} \
  CONFIG.MAX_CONTINUOUS_RTRANSFERS_WAITS {500} \
  CONFIG.MAX_CONTINUOUS_WTRANSFERS_WAITS {500} \
  CONFIG.MAX_RD_BURSTS {64} \
  CONFIG.MAX_WR_BURSTS {32} \
] [get_ips axi_protocol_checker_512]
create_ip_run [get_ips axi_protocol_checker_512]

create_ip -name floating_point -vendor xilinx.com -library ip -version 7.1 -module_name uint64_to_double
set_property -dict [list \
  CONFIG.A_Precision_Type {Uint64} \
  CONFIG.C_A_Exponent_Width {64} \
  CONFIG.C_A_Fraction_Width {0} \
  CONFIG.C_Accum_Input_Msb {32} \
  CONFIG.C_Accum_Lsb {-31} \
  CONFIG.C_Accum_Msb {32} \
  CONFIG.C_Latency {6} \
  CONFIG.C_Mult_Usage {No_Usage} \
  CONFIG.C_Rate {1} \
  CONFIG.C_Result_Exponent_Width {11} \
  CONFIG.C_Result_Fraction_Width {53} \
  CONFIG.Component_Name {uint64_to_double} \
  CONFIG.Flow_Control {NonBlocking} \
  CONFIG.Has_RESULT_TREADY {false} \
  CONFIG.Operation_Type {Fixed_to_float} \
  CONFIG.Result_Precision_Type {Double} \
] [get_ips uint64_to_double]
create_ip_run [get_ips uint64_to_double]

create_ip -name mult_gen -vendor xilinx.com -library ip -version 12.0 -module_name mult_x1e9
set_property -dict [list \
  CONFIG.CcmImp {Dedicated_Multiplier} \
  CONFIG.Component_Name {mult_x1e9} \
  CONFIG.ConstValue {1000000000} \
  CONFIG.MultType {Constant_Coefficient_Multiplier} \
  CONFIG.PipeStages {4} \
  CONFIG.PortAType {Unsigned} \
  CONFIG.PortAWidth {32} \
] [get_ips mult_x1e9]
create_ip_run [get_ips mult_x1e9]

create_ip -name dsp_macro -vendor xilinx.com -library ip -version 1.0 -module_name dsp_macro_pcout
set_property -dict [list \
  CONFIG.Component_Name {dsp_macro_pcout} \
  CONFIG.a_binarywidth {0} \
  CONFIG.a_width {16} \
  CONFIG.areg_3 {false} \
  CONFIG.areg_4 {false} \
  CONFIG.b_binarywidth {0} \
  CONFIG.b_width {16} \
  CONFIG.breg_3 {false} \
  CONFIG.breg_4 {false} \
  CONFIG.c_binarywidth {0} \
  CONFIG.c_width {48} \
  CONFIG.concat_binarywidth {0} \
  CONFIG.concat_width {48} \
  CONFIG.creg_3 {false} \
  CONFIG.creg_4 {false} \
  CONFIG.creg_5 {false} \
  CONFIG.d_width {18} \
  CONFIG.has_pcout {true} \
  CONFIG.instruction1 {A*B} \
  CONFIG.mreg_5 {true} \
  CONFIG.p_binarywidth {0} \
  CONFIG.p_full_width {32} \
  CONFIG.p_width {32} \
  CONFIG.pcin_binarywidth {0} \
  CONFIG.pipeline_options {Expert} \
  CONFIG.preg_6 {false} \
] [get_ips dsp_macro_pcout]
create_ip_run [get_ips dsp_macro_pcout]

create_ip -name dsp_macro -vendor xilinx.com -library ip -version 1.0 -module_name dsp_macro_pcin
set_property -dict [list \
  CONFIG.Component_Name {dsp_macro_pcin} \
  CONFIG.a_binarywidth {0} \
  CONFIG.a_width {16} \
  CONFIG.areg_3 {false} \
  CONFIG.areg_4 {false} \
  CONFIG.b_binarywidth {0} \
  CONFIG.b_width {16} \
  CONFIG.breg_3 {false} \
  CONFIG.breg_4 {false} \
  CONFIG.c_binarywidth {0} \
  CONFIG.c_width {48} \
  CONFIG.concat_binarywidth {0} \
  CONFIG.concat_width {48} \
  CONFIG.creg_3 {false} \
  CONFIG.creg_4 {false} \
  CONFIG.creg_5 {false} \
  CONFIG.d_width {18} \
  CONFIG.has_pcout {false} \
  CONFIG.instruction1 {A*B+PCIN} \
  CONFIG.p_binarywidth {0} \
  CONFIG.p_full_width {48} \
  CONFIG.p_width {48} \
  CONFIG.pcin_binarywidth {0} \
  CONFIG.pipeline_options {Expert} \
  CONFIG.preg_6 {true} \
] [get_ips dsp_macro_pcin]
create_ip_run [get_ips dsp_macro_pcin]

create_ip -name dsp_macro -vendor xilinx.com -library ip -version 1.0 -module_name dsp_multi
set_property -dict [list \
  CONFIG.Component_Name {dsp_multi} \
  CONFIG.a_binarywidth {0} \
  CONFIG.a_width {16} \
  CONFIG.areg_3 {false} \
  CONFIG.areg_4 {false} \
  CONFIG.b_binarywidth {0} \
  CONFIG.b_width {16} \
  CONFIG.breg_3 {false} \
  CONFIG.breg_4 {false} \
  CONFIG.c_binarywidth {0} \
  CONFIG.c_width {48} \
  CONFIG.concat_binarywidth {0} \
  CONFIG.concat_width {48} \
  CONFIG.creg_3 {false} \
  CONFIG.creg_4 {false} \
  CONFIG.creg_5 {false} \
  CONFIG.d_width {18} \
  CONFIG.instruction1 {A*B} \
  CONFIG.mreg_5 {false} \
  CONFIG.p_binarywidth {0} \
  CONFIG.p_full_width {32} \
  CONFIG.p_width {32} \
  CONFIG.pcin_binarywidth {0} \
  CONFIG.pipeline_options {Expert} \
  CONFIG.preg_6 {true} \
] [get_ips dsp_multi]
create_ip_run [get_ips dsp_multi]