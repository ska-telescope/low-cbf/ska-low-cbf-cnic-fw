----------------------------------------------------------------------------------
-- Company: CSIRO
-- Engineers: Giles Babich 
-- 
-- Create Date: 16.06.2023 
-- Module Name: vd_hbm_data_rd
-- Description: 
--    Get packets from data from HBM noise buffer.
--    This module also drives the generation of the noise buffer.
--
--
--  dataGen_Buffer:
--    Builds 512 byte blocks to write to the HBM
--     (512 bytes) * (1024 streams) * (2 buffers) = 1 Mbyte = 32 ultraRAMs.
--    The memory is organised as :
--     (16384 deep) * (512 bits wide)
--    Buffer 0 = 8192 words, 1024 blocks of 8 words each. (8 words = 512 bytes = one HBM write)
--    Buffer 1 = second 8192 words.
--
--    512 kbytes for a single stream means that there is 64 SPS packets per stream in the buffer.
--    64 packets ~ 141.5 ms of data. When told to run, the datagen module will fill the buffer in about 120 ms.
--    Readout for 1024 streams is about 30 Gbit/sec of data, write is faster by a factor of 141/120, so about 35 Gbit/sec when writing.
--    Because it is double buffered in 2 x 512 MByte pieces of memory, read and write should always be to different HBM controllers. So you should be able to read at 100Gb/sec if you want to.
-------------------------------------------------------------------------------

library IEEE, common_lib, xpm, signal_processing_common;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
library cnic_lib;
use cnic_lib.cnic_top_pkg.all;
USE common_lib.common_pkg.ALL;

library xpm;
use xpm.vcomponents.all;


entity vd_hbm_data_rd is
   generic (
      g_DEBUG_ILA             : BOOLEAN := FALSE
   );
   Port (
      -- (300 MHz)
      i_clk                   : in std_logic;
      i_reset                 : in std_logic;

      ------------------------------------------------------------------------------------

      o_data_from_hbm         : out  std_logic_vector(511 downto 0);
      i_data_from_hbm_rd      : in  std_logic;
      o_data_from_hbm_pkt_rdy : out std_logic;

      -----------------------------------------------------------------------
      -- Control signals to the rest of CNIC
      -- Indicates this module is currently writing data to the first 512 MBytes of the HBM
      i_writing_buf0          : in std_logic;
      -- Indicates this module is currently writing data to the 2nd 512 MBytes of the HBM
      i_writing_buf1          : in std_logic;
      -- Pulse to start writing data (to whichever buffer we are currently up to)
      o_start_noise_gen       : out std_logic;
      o_reset_noise_gen       : out std_logic;

      i_number_of_streams     : in std_logic_vector(11 downto 0); 
      i_enable_noise_gen      : in std_logic;

      o_rd_fsm_debug          : out std_logic_vector(3 downto 0);
      o_vd_gen_sm_debug       : out std_logic_vector(3 downto 0);
      o_time_gating_sm_debug  : out std_logic_vector(3 downto 0);
      o_rd_fsm_cache          : out std_logic_vector(3 downto 0);
      o_vd_gen_sm_cache       : out std_logic_vector(3 downto 0);
      o_time_gating_sm_cache  : out std_logic_vector(3 downto 0);

      o_vd_buffer_gen_status  : out std_logic_vector(3 downto 0);

      i_enable_pulsar_timing        : in std_logic;
      i_pulsar_start_sample_count   : in std_logic_vector(31 downto 0);
      i_pulsar_on_sample_count      : in std_logic_vector(31 downto 0);
      i_pulsar_off_sample_count     : in std_logic_vector(31 downto 0);

      -----------------------------------------------------------------------
      -- debug
      o_gen_while_writ_cnt    : out std_logic_vector(31 downto 0);
      o_generate_cnt          : out std_logic_vector(31 downto 0);
      o_buffer_0_cnt          : out std_logic_vector(31 downto 0);
      o_buffer_1_cnt          : out std_logic_vector(31 downto 0);

      -----------------------------------------------------------------------
      -- ar bus - read address
      vd_axi_arvalid          : out std_logic;
      vd_axi_arready          : in  std_logic;
      vd_axi_araddr           : out std_logic_vector(31 downto 0);
      vd_axi_arlen            : out std_logic_vector(7 downto 0);
      -- r bus - read data
      vd_axi_rvalid           : in  std_logic;
      vd_axi_rready           : out std_logic;
      vd_axi_rdata            : in  std_logic_vector(511 downto 0);
      vd_axi_rlast            : in  std_logic;
      vd_axi_rresp            : in  std_logic_vector(1 downto 0)

   );

end vd_hbm_data_rd;

architecture RTL of vd_hbm_data_rd is
   
constant c_addr_space_512MB        : unsigned(31 downto 0) := X"20000000";
constant c_addr_space_1024MB       : unsigned(31 downto 0) := X"40000000";

constant c_addr_space_512MB_4k_num : unsigned(21 downto 0) := "00"&X"20000";

constant c_buffer_offset           : unsigned(19 downto 0) := x"80000";

COMPONENT ila_0
PORT (
   clk : IN STD_LOGIC;
   probe0 : IN STD_LOGIC_VECTOR(191 DOWNTO 0));
END COMPONENT;

signal clock                     : std_logic;
signal reset                     : std_logic;
signal reset_chain               : std_logic;

signal fifo_reset                : std_logic;
signal trigger_fifo_reset        : std_logic;

signal enable_noise_gen          : std_logic;

signal chopped_data              : std_logic_vector(511 downto 0);
signal chopped_data_d            : std_logic_vector(511 downto 0);
signal chopped_data_wr           : std_logic;
signal chopped_data_wr_d         : std_logic;

--------------------------------------------------------
-- FIFO Cache
constant fifo_data_cache_width   : INTEGER   := 512;
constant fifo_data_cache_depth   : INTEGER   := 4096;

signal data_cache_fifo_in_reset  : std_logic;
signal data_cache_fifo_rd        : std_logic;
signal data_cache_fifo_q         : std_logic_vector((fifo_data_cache_width-1) downto 0);
signal data_cache_fifo_q_valid   : std_logic;
signal data_cache_fifo_empty     : std_logic;
signal data_cache_fifo_rd_count  : std_logic_vector(((ceil_log2(fifo_data_cache_depth))) downto 0);
-- WR        
signal data_cache_fifo_wr        : std_logic;

signal data_cache_fifo_data      : std_logic_vector((fifo_data_cache_width-1) downto 0);
signal data_cache_fifo_full      : std_logic;
signal data_cache_fifo_wr_count  : std_logic_vector(((ceil_log2(fifo_data_cache_depth))) downto 0);

signal fifo_q_4byte_swap         : std_logic_vector((fifo_data_cache_width-1) downto 0);

--------------------------------------------------------
-- Timing FIFO Cache

signal timing_fifo_in_reset      : std_logic;
signal timing_fifo_rd            : std_logic;
signal timing_fifo_q             : std_logic_vector((fifo_data_cache_width-1) downto 0);
signal timing_fifo_q_valid       : std_logic;
signal timing_fifo_empty         : std_logic;
signal timing_fifo_rd_count      : std_logic_vector(((ceil_log2(fifo_data_cache_depth))) downto 0);
-- WR        
signal timing_fifo_wr            : std_logic;

signal timing_fifo_data          : std_logic_vector((fifo_data_cache_width-1) downto 0);
signal timing_fifo_full          : std_logic;
signal timing_fifo_wr_count      : std_logic_vector(((ceil_log2(fifo_data_cache_depth))) downto 0);

--------------------------------------------------------
-- HBM AXI
signal o_axi_arvalid             :  std_logic;
signal i_axi_arready             :  std_logic;
signal o_axi_araddr              :  std_logic_vector(31 downto 0);
signal o_axi_arlen               :  std_logic_vector(7 downto 0);

signal i_axi_rvalid              :  std_logic;
signal o_axi_rready              :  std_logic;
signal i_axi_rdata               :  std_logic_vector(511 downto 0);
signal i_axi_rlast               :  std_logic;
signal i_axi_rresp               :  std_logic_vector(1 downto 0);

--------------------------------------------------------

signal spead_data_ready          : std_logic := '0';

signal axi_4k_finished           : std_logic := '0';

type rd_fsm_type is (idle, buffer_select, wait_fifo, request_lower_4k, request_upper_4k , request_stream, request_stream_position, wait_arready, fifo_drained, rd_addr_prep);
signal rd_fsm : rd_fsm_type := idle;

type vd_gen_sm_type is (idle, reset_gen, reset_fifos, init_buffer, buffer_finshing, run);
signal vd_gen_sm : vd_gen_sm_type := idle;

signal buffer_count              : unsigned(15 downto 0);

signal vd_gen_count              : unsigned(3 downto 0);
signal writing_buf0_d            : std_logic;
signal writing_buf1_d            : std_logic;
signal run_retrieval             : std_logic;
signal buffers_primed            : std_logic;

signal generate_buffer           : std_logic;

signal readaddr, readaddr_reg    : unsigned(31 downto 0);    -- 30 bits = 1GB
signal readaddr_sent             : unsigned(31 downto 0);    -- 30 bits = 1GB
signal curr_sps_buff_pos         : unsigned(31 downto 0);
signal curr_packet_in_buffer     : unsigned(7 downto 0);
signal curr_buffer_base          : unsigned(31 downto 0);
signal curr_stream_count         : unsigned(11 downto 0); 
signal req_step_tracker          : std_logic_vector(1 downto 0);
signal hbm_retrieval_trac        : unsigned(15 downto 0) := (others => '0');
signal hbm_retrieval_trac_trigger : unsigned(15 downto 0) := (others => '0');

signal hbm_request_burst         : unsigned(7 downto 0);

signal current_requests          : unsigned(15 downto 0);
   
signal total_beat_count                : unsigned(31 downto 0) := (others => '0');
signal current_axi_4k_count            : unsigned(31 downto 0);
signal wait_counter                    : unsigned(7 downto 0);
signal current_pkt_count               : unsigned(63 downto 0) := (others=>'0');
signal fpga_beat_in_burst_counter      : unsigned(31 downto 0) := (others=>'0');

signal fpga_axi_beats_per_packet       : unsigned(7 downto 0) := (others=>'0');
signal beats                           : unsigned(7 downto 0);
signal number_of_burst_beats           : unsigned(15 downto 0) := (others=>'0');
signal total_number_of_512b            : unsigned(31 downto 0) := (others=>'0');
signal beat_count                      : unsigned(31 downto 0) := (others=> '0');
signal end_of_packet                   : std_logic;
signal start_of_packet                 : std_logic;
signal start_stop_tx                   : std_logic;
signal wait_fifo_resetting             : std_logic;
signal error_fifo_stall                : std_logic;
signal bytes_in_current_packet         : std_logic_vector(13 downto 0);
signal beats_per_packet                : std_logic_vector(13 downto 0);
signal odd_beats_per_packet            : std_logic_vector(13 downto 0);


signal fpga_pkt_count_in_this_burst    : unsigned(31 downto 0);
signal burst_count                     : unsigned(31 downto 0) := (others=>'0');

signal looping                         : std_logic := '0';
signal loop_cnt                        : unsigned(31 downto 0) := (others=>'0');
signal start_next_loop                 : std_logic := '0';
signal wait_fifo_reset_cnt             : unsigned(31 downto 0) := (others=>'0');
signal rd_rst_busy ,wr_rst_busy        : std_logic := '0';
signal axi_r_num                       : unsigned(31 downto 0) := (others => '0');
signal clear_axi_r_num                 : std_logic := '0';

signal start_noise_gen_int             : std_logic;
signal last_buffer_written             : std_logic_vector(1 downto 0);

----------------------------------------------------
-- Pulsar timing signals
signal buffer_readout_start            : std_logic;

type pulsar_sm_type is (IDLE, LOAD_COUNT, WAIT_PACKET, CHECK_STREAM, CHOP_IT, NEXT_STREAM, CALC_FADE);
signal pulsar_sm : pulsar_sm_type := idle;

signal enable_pulsar_timing            : std_logic;
signal pulsar_start_sample_count       : std_logic_vector(31 downto 0);
signal pulsar_on_sample_count          : std_logic_vector(31 downto 0);
signal pulsar_off_sample_count         : std_logic_vector(31 downto 0);

signal pulsar_curr_stream              : unsigned(11 downto 0);
signal timing_cache                    : unsigned(31 downto 0);
signal samples_to_pass                 : std_logic_vector(7 downto 0);
signal timing_cache_preserve           : unsigned(31 downto 0);
signal timing_track                    : unsigned(7 downto 0);
signal timing_current_mode             : std_logic_vector(1 downto 0);
signal timing_start_mode               : std_logic_vector(1 downto 0);
signal timing_start_mode_d             : std_logic_vector(1 downto 0);
signal signal_mux_preserve             : std_logic;
signal adjustment                      : unsigned(3 downto 0);

-- debug
signal gen_while_writ_cnt              : unsigned(31 downto 0) := (others => '0');
signal generate_cnt                    : unsigned(31 downto 0) := (others => '0');
signal buffer_0_cnt                    : unsigned(31 downto 0) := (others => '0');
signal buffer_1_cnt                    : unsigned(31 downto 0) := (others => '0');

signal gen_while_writing               : std_logic;

signal time_gating_sm_debug            : std_logic_vector(3 downto 0);
signal rd_fsm_debug                    : std_logic_vector(3 downto 0);
signal vd_gen_sm_debug                 : std_logic_vector(3 downto 0);

signal time_gating_sm_debug_d          : std_logic_vector(3 downto 0);
signal rd_fsm_debug_d                  : std_logic_vector(3 downto 0);
signal vd_gen_sm_debug_d               : std_logic_vector(3 downto 0);

signal time_gating_sm_cache            : std_logic_vector(3 downto 0);
signal rd_fsm_cache                    : std_logic_vector(3 downto 0);
signal vd_gen_sm_cache                 : std_logic_vector(3 downto 0);

----------------------------------------------------

begin
---------------------------------------------------------------------------------------------------
-- MAP internal signals to ports.

clock       <= i_clk;
reset       <= i_reset;

o_rd_fsm_debug          <= rd_fsm_debug;
o_vd_gen_sm_debug       <= vd_gen_sm_debug;
o_time_gating_sm_debug  <= time_gating_sm_debug;

o_rd_fsm_cache          <= rd_fsm_cache;
o_vd_gen_sm_cache       <= vd_gen_sm_cache;
o_time_gating_sm_cache  <= time_gating_sm_cache;
   
o_start_noise_gen    <= start_noise_gen_int;

o_gen_while_writ_cnt <= std_logic_vector(gen_while_writ_cnt);
o_generate_cnt       <= std_logic_vector(generate_cnt);
o_buffer_0_cnt       <= std_logic_vector(buffer_0_cnt);
o_buffer_1_cnt       <= std_logic_vector(buffer_1_cnt);

--////////////////////////////////////////////////////////////////////////////////////////////////////
------------------------------------------------------------------------------------------------------
--HBM AXI read mappings
------------------------------------------------------------------------------------------------------
--////////////////////////////////////////////////////////////////////////////////////////////////////
   
-- ar bus - read address
vd_axi_arvalid          <= o_axi_arvalid;
i_axi_arready           <= vd_axi_arready;
vd_axi_araddr           <= o_axi_araddr;
vd_axi_arlen            <= o_axi_arlen;


-- r bus - read data
vd_axi_rready           <= o_axi_rready;

i_axi_rvalid            <= vd_axi_rvalid;
i_axi_rdata             <= vd_axi_rdata;
i_axi_rlast             <= vd_axi_rlast;  -- Used to calculate returns.
i_axi_rresp             <= vd_axi_rresp;  -- NOT NEEDED

-- Always ready to catch HBM return data.
o_axi_rready            <= '1';
-- Read in 512 bit aligned 4k words
o_axi_arlen(7 downto 0) <= x"3F"; -- Read 64 beats x 512 bits = 4096B

------------------------------------------------------------------------------------------------------

o_axi_araddr <= std_logic_vector(readaddr_sent);

read_req_in_flight_track_proc : process(clock)
begin	    
   if rising_edge(clock) then
      if (clear_axi_r_num = '1') then
         hbm_retrieval_trac <= (others => '0');
      else	    
         if (o_axi_arvalid = '1' AND i_axi_arready = '1') AND (i_axi_rvalid = '1' AND i_axi_rlast = '1') then
             hbm_retrieval_trac <= hbm_retrieval_trac;
         elsif (o_axi_arvalid = '1' AND i_axi_arready = '1') then
             hbm_retrieval_trac <= hbm_retrieval_trac + 64;
         elsif (i_axi_rvalid = '1' AND i_axi_rlast = '1') then
             hbm_retrieval_trac <= hbm_retrieval_trac - 64;
         end if;
      end if;
      
      hbm_retrieval_trac_trigger    <= hbm_retrieval_trac;
      
      enable_noise_gen              <= i_enable_noise_gen;
   end if;
end process;

------------------------------------------------------------------------------------------------------
-- Catch Error conditions.
-- Generating pulses while we are already generating.

debug_proc : process(clock)
begin
   if rising_edge(clock) then
      if (reset = '1') then
         gen_while_writ_cnt   <= (others => '0');
         generate_cnt         <= (others => '0');
         buffer_0_cnt         <= (others => '0');
         buffer_1_cnt         <= (others => '0');
      else
         gen_while_writing    <= (generate_buffer AND writing_buf0_d) OR ((generate_buffer AND writing_buf1_d));

         if gen_while_writing = '1' then
            gen_while_writ_cnt   <= gen_while_writ_cnt + 1;
         end if;

         if generate_buffer = '1' then
            generate_cnt         <= generate_cnt + 1;
         end if;

         if i_writing_buf0 = '0' AND writing_buf0_d = '1' then
            buffer_0_cnt         <= buffer_0_cnt + 1;
         end if;
         
         if i_writing_buf1 = '0' AND writing_buf1_d = '1' then
            buffer_1_cnt         <= buffer_1_cnt + 1;
         end if;
      end if;

      time_gating_sm_debug_d <= time_gating_sm_debug;

      if (time_gating_sm_debug_d /= time_gating_sm_debug) then
         time_gating_sm_cache <= time_gating_sm_debug_d;
      end if;

      rd_fsm_debug_d <= rd_fsm_debug;

      if (rd_fsm_debug_d /= rd_fsm_debug) then
         rd_fsm_cache <= rd_fsm_debug_d;
      end if;

      vd_gen_sm_debug_d <= vd_gen_sm_debug;

      if (vd_gen_sm_debug_d /= vd_gen_sm_debug) then
         vd_gen_sm_cache <= vd_gen_sm_debug_d;
      end if;         
   end if;
end process;

------------------------------------------------------------------------------------------------------
-- Initial flow will be run below.
-- This will reset and prime the noise buffers.
-- Don't trigger reset if a buffer is currently being generated, might corrupt HBM interface.
o_reset_noise_gen       <= reset_chain AND (NOT i_writing_buf0) AND (NOT i_writing_buf1);

o_vd_buffer_gen_status  <= writing_buf1_d & writing_buf0_d & last_buffer_written;

fifo_reset              <= trigger_fifo_reset OR reset_chain;

init_vd_gen_proc : process(clock)
begin
   if rising_edge(clock) then
      writing_buf0_d    <= i_writing_buf0;
      writing_buf1_d    <= i_writing_buf1;

      if (reset = '1') then
         vd_gen_sm_debug         <= x"F";
         start_noise_gen_int     <= '0';
         reset_chain             <= '1';
         trigger_fifo_reset      <= '1';
         vd_gen_sm               <= idle;
         run_retrieval           <= '0';
         buffers_primed          <= '0';
         last_buffer_written     <= "00";
      else
         case vd_gen_sm is
            when idle =>
               vd_gen_sm_debug   <= x"0";
               run_retrieval     <= '0';
               vd_gen_count            <= x"F";
               start_noise_gen_int     <= '0';  -- stop generate if back to IDLE

               -- only start if there are no transfers in flight.
               if enable_noise_gen = '1' AND (hbm_retrieval_trac_trigger = x"0000") then
                  reset_chain          <= '1';
                  vd_gen_sm            <= reset_gen;
               end if;

            when reset_gen =>
               vd_gen_sm_debug   <= x"1";
               -- hold in reset, then start
               -- reset loads the seeds into the data gen.
               vd_gen_count   <= vd_gen_count - 1;
               if vd_gen_count = x"1" then
                  reset_chain          <= '0';
                  vd_gen_sm            <= reset_fifos;
                  start_noise_gen_int  <= '1';
               end if;

            when reset_fifos =>
               vd_gen_sm_debug   <= x"5";
               -- make sure no HBM requests are in flight from previous run
               -- make sure FIFOs are empty
               if (data_cache_fifo_in_reset = '0') AND (current_requests = 0) AND (timing_fifo_in_reset = '0') then
                  vd_gen_sm            <= init_buffer;
               end if;


            when init_buffer =>
               vd_gen_sm_debug      <= x"2";
               
               buffer_count         <= x"0100";       -- over run from buffer finished to it being flushed to HBM.

               if i_writing_buf0 = '0' AND writing_buf0_d = '1' then
                  vd_gen_sm            <= buffer_finshing;
                  last_buffer_written  <= "01";
               elsif i_writing_buf1 = '0' AND writing_buf1_d = '1' then
                  vd_gen_sm            <= buffer_finshing;
                  buffers_primed       <= '1';
                  last_buffer_written  <= "10";
               end if;
               
            when buffer_finshing =>
               -- start_generation is edge detected in datagen.
               start_noise_gen_int  <= '0';
               
               vd_gen_sm_debug   <= x"3";
               buffer_count      <= buffer_count - 1;
               if buffer_count = 0 then
                  vd_gen_sm      <= run;
               end if;

            -- when entering run, 2nd buffer is set to run and the first can be used.
            -- this state just calls noise buffer generation based on a signal from the rd SM.
            -- timing will be, cue a generation run once final packet received from that buffer.
            
            when run =>
               vd_gen_sm_debug      <= x"4";
               run_retrieval        <= '1'; -- and buffers_primed;

               if generate_buffer = '1' then
                  if enable_noise_gen = '0' then
                     vd_gen_sm            <= idle;
                  else
                     vd_gen_sm            <= init_buffer;
                     start_noise_gen_int  <= '1';
                  end if;
               elsif buffers_primed = '0' then           -- buffer 0 is always first after reset, when buffer 1 comes through we are primed.
                  vd_gen_sm            <= init_buffer;
                  start_noise_gen_int  <= '1';
               end if;


            when OTHERS => vd_gen_sm <= idle;
         end case;

      end if;
   end if;
end process;

------------------------------------------------------------------------------------------------------
-- FIFO is 4096 deep.
-- a SPEAD SPS packet contains 8192 bytes of data.
-- 8192 bytes = 128 entries in a FIFO 512w.
-- FIFO can store 32 SPEAD packets.

--i_number_of_streams     : in std_logic_vector(11 downto 0);


process(clock)
begin
   if rising_edge(clock) then
      current_requests  <= (unsigned(data_cache_fifo_wr_count)) + hbm_retrieval_trac;

      if (reset_chain = '1') then
         rd_fsm               <= idle;
         rd_fsm_debug         <= x"F";
         o_axi_arvalid        <= '0';  
         axi_4k_finished      <= '0';
         req_step_tracker     <= "00";
         hbm_request_burst    <= x"00";
         readaddr             <= (others => '0');
         buffer_readout_start <= '0';
         clear_axi_r_num      <= '1';
      else
         generate_buffer      <= '0';
         clear_axi_r_num      <= '0';

         case rd_fsm is
            when idle =>
               
               rd_fsm_debug         <= x"0";
               buffer_readout_start <= '0';
               o_axi_arvalid        <= '0';

               readaddr_reg         <= c_addr_space_512MB;
               readaddr             <= (others => '0');
               
               current_axi_4k_count <= (others =>'0');
               curr_sps_buff_pos    <= (others =>'0');
               curr_stream_count    <= (others =>'0');

               if (data_cache_fifo_in_reset = '0') AND (run_retrieval = '1') then
                  rd_fsm            <= buffer_select;
                  
               end if;

            when buffer_select =>
               rd_fsm_debug         <= x"1";

               if (run_retrieval = '1') then
                  if readaddr_reg = c_addr_space_512MB then
                     readaddr             <= (others => '0');
                     readaddr_reg         <= (others => '0');
                     curr_sps_buff_pos    <= (others => '0');
                     curr_buffer_base     <= (others => '0');
                  else
                     readaddr             <= c_addr_space_512MB;
                     readaddr_reg         <= c_addr_space_512MB;
                     curr_sps_buff_pos    <= (others => '0');
                     curr_buffer_base     <= c_addr_space_512MB;
                  end if;
                  curr_packet_in_buffer   <= x"01";
                  rd_fsm                  <= wait_fifo;
                  buffer_readout_start    <= '1';
               else
                  rd_fsm                  <= idle;
               end if;

            when wait_fifo => 
               rd_fsm_debug         <= x"2";
               buffer_readout_start <= '0';

               o_axi_arvalid        <= '0';

               if (current_requests < 3584) then -- is there space for 4 streams and no requests in flight.
                  rd_fsm            <= request_lower_4k after 4us;
               end if;
               
            when request_lower_4k =>
               rd_fsm_debug         <= x"3";
               req_step_tracker     <= "01";
               rd_fsm               <= rd_addr_prep;
               readaddr             <= curr_buffer_base;

            when request_upper_4k =>
               rd_fsm_debug         <= x"4";
               req_step_tracker     <= "10";
               rd_fsm               <= rd_addr_prep;
               readaddr             <= readaddr + 4096;

            when request_stream =>
               rd_fsm_debug         <= x"5";
               -- if processed the number of streams.
               -- check to see if all 64 positions have come out then exit.
               -- else move to next position and re-read through streams.
               if std_logic_vector(curr_stream_count) = i_number_of_streams then
                  rd_fsm            <= fifo_drained;
               else
                  curr_stream_count <= curr_stream_count + 1;
                  curr_buffer_base  <= c_buffer_offset + curr_buffer_base;
                  rd_fsm            <= wait_fifo;
               end if;


            when request_stream_position =>
               rd_fsm_debug         <= x"6";
               -- move the base to 8192 along for the next data sample.
               curr_buffer_base  <= readaddr_reg + curr_sps_buff_pos;
               rd_fsm            <= wait_fifo;

            ------------------------------------
            -- register rd_addr for SLR timing assistance.
            when rd_addr_prep =>
               rd_fsm_debug         <= x"9"; 
               readaddr_sent        <= readaddr;
               o_axi_arvalid        <= '1';
               rd_fsm               <= wait_arready;
            ------------------------------------
            -- issue read and wait for
            when wait_arready =>  -- arvalid is high in this state, wait until arready is high so the transaction is complete.
               rd_fsm_debug         <= x"7";
               o_axi_arvalid        <= '1';
               if i_axi_arready = '1' then
                  o_axi_arvalid           <= '0';
                  -- requested first 4k bytes
                  -- requested second 4k bytes, go to stream to inc offset.
                  if (req_step_tracker = "01") then 
                     rd_fsm <= request_upper_4k; 
                  elsif (req_step_tracker = "10") then 
                     rd_fsm <= request_stream;
                  end if;
               end if;
      
            ------------------------------------
            -- wait for to empty, make this easier to ILA.
            when fifo_drained =>
               rd_fsm_debug         <= x"8";

               if data_cache_fifo_empty = '1' and (current_requests = 0) then
                  if curr_packet_in_buffer = 64 then     -- 8192 x 64
                     rd_fsm            <= buffer_select;
                     generate_buffer   <= '1';
                  else
                     curr_sps_buff_pos       <= curr_sps_buff_pos + 8192;
                     curr_packet_in_buffer   <= curr_packet_in_buffer + 1;
                     rd_fsm                  <= request_stream_position;
                  end if;
                  curr_stream_count       <= (others => '0');
               end if;

            when others =>
               rd_fsm <= idle;
         end case; 
      end if;
   end if;
end process;
   
--------------------------------------------------------------------------------------------
-- Capture the memory read's of the 512bit data comming back on the AXI bus
--------------------------------------------------------------------------------------------

-- data_cache_fifo_wr   <= i_axi_rvalid;
-- data_cache_fifo_data <= i_axi_rdata;

--------------------------------------------------------------------------------------------
-- Pulsar pseduo timing, applying zeros to data bus instead of passing data.
--------------------------------------------------------------------------------------------
-- Each stream is retrieved with 8192 bytes.
-- 4 bytes per sample.
-- 64 bytes per read, 16 samples per line.
-- data is written byte by byte 7-> 0 being the first.

-- 4096d x 512w
-- pull in one stream and work out what needs to be zeroed out.

timing_cache_fifo : entity signal_processing_common.xpm_sync_fifo_wrapper
    Generic map (
        FIFO_MEMORY_TYPE    => "uram",
        READ_MODE           => "fwft",
        FIFO_DEPTH          => fifo_data_cache_depth,
        DATA_WIDTH          => fifo_data_cache_width
    )
    Port map ( 
        fifo_reset          => reset_chain,
        fifo_clk            => clock,
        fifo_in_reset       => timing_fifo_in_reset,
        -- RD    
        fifo_rd             => timing_fifo_rd,
        fifo_q              => timing_fifo_q,
        fifo_q_valid        => timing_fifo_q_valid,
        fifo_empty          => timing_fifo_empty,
        fifo_rd_count       => timing_fifo_rd_count,
        -- WR        
        fifo_wr             => timing_fifo_wr,
        fifo_data           => timing_fifo_data,
        fifo_full           => timing_fifo_full,
        fifo_wr_count       => timing_fifo_wr_count
    );


process(clock)
begin
   if rising_edge(clock) then
      enable_pulsar_timing       <= i_enable_pulsar_timing;

      if enable_pulsar_timing = '0' then
         data_cache_fifo_wr   <= i_axi_rvalid;
         data_cache_fifo_data <= i_axi_rdata;
      else
         -------------------------------
         data_cache_fifo_data <= chopped_data;
         data_cache_fifo_wr   <= chopped_data_wr;
         -------------------------------
      end if;
         
      timing_fifo_wr       <= i_axi_rvalid;
      timing_fifo_data     <= i_axi_rdata;

      chopped_data_wr_d    <= timing_fifo_rd;
      chopped_data_wr      <= chopped_data_wr_d;

      timing_start_mode_d  <= timing_start_mode;

      chopped_data_d       <= timing_fifo_q;
      
      if samples_to_pass(3 downto 0) = x"0" then
         if timing_start_mode_d(0) = '1' then
            chopped_data <= chopped_data_d;
         else
            chopped_data <= (others => '0');
         end if;
      else
         chopped_data <= chopped_data_d;
         -- if only passing 15 samples from the line, then the upper 4 bytes must be zero.
         -- 14 samples = upper 8 bytes zero
         -- etc
         if samples_to_pass = x"0F" then
            chopped_data(511 downto (512-(32*1)))   <= (others => '0');
         end if;
         if samples_to_pass = x"0E" then
            chopped_data(511 downto (512-(32*2)))   <= (others => '0');
         end if;
         if samples_to_pass = x"0D" then
            chopped_data(511 downto (512-(32*3)))   <= (others => '0');
         end if;
         if samples_to_pass = x"0C" then
            chopped_data(511 downto (512-(32*4)))   <= (others => '0');
         end if;
         if samples_to_pass = x"0B" then
            chopped_data(511 downto (512-(32*5)))   <= (others => '0');
         end if;
         if samples_to_pass = x"0A" then
            chopped_data(511 downto (512-(32*6)))   <= (others => '0');
         end if;
         if samples_to_pass = x"09" then
            chopped_data(511 downto (512-(32*7)))   <= (others => '0');
         end if;
         if samples_to_pass = x"08" then
            chopped_data(511 downto (512-(32*8)))   <= (others => '0');
         end if;
         if samples_to_pass = x"07" then
            chopped_data(511 downto (512-(32*9)))   <= (others => '0');
         end if;
         if samples_to_pass = x"06" then
            chopped_data(511 downto (512-(32*10)))   <= (others => '0');
         end if;
         if samples_to_pass = x"05" then
            chopped_data(511 downto (512-(32*11)))   <= (others => '0');
         end if;
         if samples_to_pass = x"04" then
            chopped_data(511 downto (512-(32*12)))   <= (others => '0');
         end if;
         if samples_to_pass = x"03" then
            chopped_data(511 downto (512-(32*13)))   <= (others => '0');
         end if;
         if samples_to_pass = x"02" then
            chopped_data(511 downto (512-(32*14)))   <= (others => '0');
         end if;
         if samples_to_pass = x"01" then
            chopped_data(511 downto (512-(32*15)))   <= (others => '0');
         end if;

         -- When switching from zero to pass through, want to zero from LSB
         -- if only zeroing 1 sample from the line, then the lower 15 bytes must be zero.
         -- etc
         if samples_to_pass = x"81" then
            chopped_data(((512-(32*15))-1) downto 0)   <= (others => '0');
         end if;
         if samples_to_pass = x"82" then
            chopped_data(((512-(32*14))-1) downto 0)   <= (others => '0');
         end if;
         if samples_to_pass = x"83" then
            chopped_data(((512-(32*13))-1) downto 0)   <= (others => '0');
         end if;
         if samples_to_pass = x"84" then
            chopped_data(((512-(32*12))-1) downto 0)   <= (others => '0');
         end if;
         if samples_to_pass = x"85" then
            chopped_data(((512-(32*11))-1) downto 0)   <= (others => '0');
         end if;
         if samples_to_pass = x"86" then
            chopped_data(((512-(32*10))-1) downto 0)   <= (others => '0');
         end if;
         if samples_to_pass = x"87" then
            chopped_data(((512-(32*9))-1) downto 0)   <= (others => '0');
         end if;
         if samples_to_pass = x"88" then
            chopped_data(((512-(32*8))-1) downto 0)   <= (others => '0');
         end if;
         if samples_to_pass = x"89" then
            chopped_data(((512-(32*7))-1) downto 0)   <= (others => '0');
         end if;
         if samples_to_pass = x"8A" then
            chopped_data(((512-(32*6))-1) downto 0)   <= (others => '0');
         end if;
         if samples_to_pass = x"8B" then
            chopped_data(((512-(32*5))-1) downto 0)   <= (others => '0');
         end if;
         if samples_to_pass = x"8C" then
            chopped_data(((512-(32*4))-1) downto 0)   <= (others => '0');
         end if;
         if samples_to_pass = x"8D" then
            chopped_data(((512-(32*3))-1) downto 0)   <= (others => '0');
         end if;
         if samples_to_pass = x"8E" then
            chopped_data(((512-(32*2))-1) downto 0)   <= (others => '0');
         end if;
         if samples_to_pass = x"8F" then
            chopped_data(((512-(32*1))-1) downto 0)   <= (others => '0');
         end if;
      end if;


      pulsar_start_sample_count     <= i_pulsar_start_sample_count;
      pulsar_on_sample_count        <= i_pulsar_on_sample_count;
      pulsar_off_sample_count       <= i_pulsar_off_sample_count;

      if (reset_chain = '1') then
         pulsar_sm            <= IDLE;
         time_gating_sm_debug <= x"F";
         timing_fifo_rd       <= '0';
         timing_start_mode    <= (others => '0');
         timing_current_mode  <= (others => '0');
         timing_cache         <= (others => '0');
         timing_track         <= (others => '0');
         pulsar_curr_stream   <= (others => '0');
         timing_cache_preserve<= (others => '0');
         signal_mux_preserve  <= '0';
      else

         case pulsar_sm is
            when IDLE =>
               time_gating_sm_debug <= x"0";
               timing_fifo_rd       <= '0';
               timing_current_mode  <= "00";
               timing_start_mode    <= "00";
               timing_cache         <= (others => '0');
               timing_track         <= (others => '0');
               pulsar_curr_stream   <= (others => '0');
               timing_cache_preserve<= (others => '0');
               samples_to_pass      <= (others => '0');
               adjustment           <= x"0";
               signal_mux_preserve  <= '0';
               if buffer_readout_start = '1' then
                  pulsar_sm         <= LOAD_COUNT;
               end if;

            when LOAD_COUNT =>
               time_gating_sm_debug <= x"1";
               if timing_start_mode(1) = '0' then
                  timing_cache         <= unsigned(pulsar_start_sample_count);
                  timing_start_mode    <= "00"; -- reset in case pulsar on during first packet
               else
                  timing_cache         <= timing_cache_preserve;
                  timing_start_mode(0) <= signal_mux_preserve;
               end if;
               pulsar_sm         <= WAIT_PACKET;

            when WAIT_PACKET =>
               time_gating_sm_debug <= x"2";
               timing_track   <= 8D"128";
               if unsigned(timing_fifo_wr_count) >= 128 then
                  timing_fifo_rd <= '1';
                  pulsar_sm      <= CHECK_STREAM;
               end if;

            when CHECK_STREAM =>
               time_gating_sm_debug <= x"3";
               samples_to_pass      <= x"00";
               -- call out 128 lines (8192 bytes) and load counters as required.
               if timing_cache <= 16 then
                  timing_current_mode(1)  <= '1';

                  samples_to_pass   <= (NOT timing_start_mode(0)) & "000" & std_logic_vector(timing_cache(3 downto 0));
                  if timing_cache(3 downto 0) /= x"0" then
                     adjustment           <= 16 - timing_cache(3 downto 0);
                  else
                     adjustment           <= x"0";
                  end if;
                  -- IF Start count of OFF count then load ON
                  if (timing_start_mode(0) = '0') then
                     timing_cache         <= unsigned(pulsar_on_sample_count);
                     timing_start_mode(0) <= '1';
                  else
                     timing_cache         <= unsigned(pulsar_off_sample_count);
                     timing_start_mode(0) <= '0';
                  end if;

                  pulsar_sm         <= CHOP_IT;
                  timing_fifo_rd    <= '0';
               elsif timing_track = 1 then
                  pulsar_sm         <= NEXT_STREAM;
                  timing_fifo_rd    <= '0';
                  timing_cache      <= timing_cache - 16;
                  timing_track      <= timing_track - 1;
               else
                  timing_cache      <= timing_cache - 16;
                  timing_track      <= timing_track - 1;
               end if;

            when CHOP_IT =>
               time_gating_sm_debug <= x"4";
               samples_to_pass      <= x"00";
               pulsar_sm            <= CHECK_STREAM;
               timing_cache         <= timing_cache - adjustment;
               timing_fifo_rd       <= '1';
               timing_track         <= timing_track - 1;

               if timing_track = 1 then
                  pulsar_sm         <= NEXT_STREAM;
                  timing_fifo_rd    <= '0';
                  --timing_cache      <= timing_cache - 16;
                  --timing_track      <= timing_track - 1;
               end if;

            when NEXT_STREAM =>
               time_gating_sm_debug <= x"5";
               -- if all the active streams have been done, then move to next loop
               if i_number_of_streams = std_logic_vector(pulsar_curr_stream) then
                  pulsar_sm               <= LOAD_COUNT;
                  timing_start_mode(1)    <= '1';--timing_current_mode(1); -- set when break out of first count.
                  signal_mux_preserve     <= timing_start_mode(0);
                  pulsar_curr_stream      <= x"000";
                  timing_cache_preserve   <= timing_cache;
               else
                  pulsar_sm               <= LOAD_COUNT;
                  pulsar_curr_stream      <= pulsar_curr_stream + 1;
               end if;


            when others =>
               pulsar_sm <= IDLE;
         end case;
      end if;
   end if;
end process;

--------------------------------------------------------------------------------------------
-- FIFO for data from HBM for TX
--------------------------------------------------------------------------------------------
-- 4096d x 512w

inc_data_cache : entity signal_processing_common.xpm_sync_fifo_wrapper
    Generic map (
        FIFO_MEMORY_TYPE    => "uram",
        READ_MODE           => "fwft",
        FIFO_DEPTH          => fifo_data_cache_depth,
        DATA_WIDTH          => fifo_data_cache_width
    )
    Port map ( 
        fifo_reset          => reset_chain,
        fifo_clk            => clock,
        fifo_in_reset       => data_cache_fifo_in_reset,
        -- RD    
        fifo_rd             => data_cache_fifo_rd,
        fifo_q              => data_cache_fifo_q,
        fifo_q_valid        => data_cache_fifo_q_valid,
        fifo_empty          => data_cache_fifo_empty,
        fifo_rd_count       => data_cache_fifo_rd_count,
        -- WR        
        fifo_wr             => data_cache_fifo_wr,
        fifo_data           => data_cache_fifo_data,
        fifo_full           => data_cache_fifo_full,
        fifo_wr_count       => data_cache_fifo_wr_count
    );

vd_data_swap_gen : for i in 0 to 63 generate

   fifo_q_4byte_swap((511-(8*i)) downto (504-(8*i)))  <= data_cache_fifo_q(((7)+(8*i)) downto ((0)+(8*i)));

end generate;


o_data_from_hbm            <= fifo_q_4byte_swap;
o_data_from_hbm_pkt_rdy    <= spead_data_ready;

data_cache_fifo_rd         <= i_data_from_hbm_rd;

data_ready_proc : process(clock)
begin
   if rising_edge(clock) then
      if reset = '1' then
         spead_data_ready  <= '0';
      else
         if unsigned(data_cache_fifo_wr_count) >= 128 then
            spead_data_ready  <= '1';
         else
            spead_data_ready  <= '0';
         end if;
      end if;
   end if;
end process;

    
----------------------------------------------------------------------------------------------------------
-- debug

debug_gen : IF g_DEBUG_ILA GENERATE

   vd_hbm_rd_ila : ila_0
      port map (
         clk                     => clock,

         probe0(31 downto 0)     => o_axi_araddr,
         probe0(32)              => i_axi_arready,
         probe0(33)              => o_axi_arvalid,
         probe0(41 downto 34)    => o_axi_arlen,

         probe0(45 downto 42)    => rd_fsm_debug,
         probe0(49 downto 46)    => vd_gen_sm_debug,

         probe0(65 downto 50)    => std_logic_vector(hbm_retrieval_trac),

         probe0(66)              => vd_axi_rvalid,
         probe0(67)              => vd_axi_rlast,
         probe0(99 downto 68)    => vd_axi_rdata(31 downto 0),

         probe0(100)             => i_writing_buf0,
         probe0(101)             => i_writing_buf1,
         probe0(102)             => reset_chain,
         probe0(103)             => vd_axi_rlast,

         probe0(104)             => run_retrieval,
         probe0(105)             => i_enable_noise_gen,
         probe0(106)             => start_noise_gen_int,
         probe0(118 downto 107)  => i_number_of_streams,

         probe0(119)             => data_cache_fifo_wr,
         probe0(120)             => data_cache_fifo_rd,
         probe0(121)             => data_cache_fifo_empty,
         probe0(134 downto 122)  => data_cache_fifo_wr_count,
         probe0(142 downto 135)  => std_logic_vector(curr_packet_in_buffer),
         probe0(174 downto 143)  => vd_axi_rdata(63 downto 32),

         probe0(191 downto 175)  => (OTHERS => '0')
      );
  
END GENERATE;

    
end RTL;
