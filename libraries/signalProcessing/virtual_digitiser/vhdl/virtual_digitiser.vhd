----------------------------------------------------------------------------------
-- Company: CSIRO
-- Engineer: Giles Babich
-- 
-- Create Date: 08/05/2023
-- Design Name: 
-- Module Name: virtual_digitiser
--  
-- Description: 
-- 
-- Version - 0.0.1
--  This will generate a continuos stream of SPS Spead packets with zero data in the heap.
-- 
--      
-- 
----------------------------------------------------------------------------------

library IEEE, vd_lib, common_lib, signal_processing_common, Timeslave_CMAC_lib;
library axi4_lib, spead_sps_lib, technology_lib;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.NUMERIC_STD.ALL;
USE axi4_lib.axi4_lite_pkg.ALL;
USE axi4_lib.axi4_full_pkg.ALL;
USE common_lib.common_pkg.ALL;
USE vd_lib.VD_vd_reg_pkg.ALL;
use spead_sps_lib.spead_sps_pkg.ALL;
USE Timeslave_CMAC_lib.timer_pkg.ALL;

entity virtual_digitiser is
    Generic ( 
        g_DEBUG_VEC_SIZE        : INTEGER := 5;
        g_DEBUG_ILA             : BOOLEAN := TRUE
    );
    Port ( 
        -- clock used for all data input and output from this module (300 MHz)
        i_clk                   : in std_logic;
        i_rst                   : in std_logic;

        i_local_reset           : in std_logic;
        
        o_reset_packet_player   : out std_logic;
      ------------------------------------------------------------------------------------

        -- data to packet_player
        o_bytes_to_transmit     : OUT STD_LOGIC_VECTOR(13 downto 0);     
        o_data_to_player        : OUT STD_LOGIC_VECTOR(511 downto 0);
        o_data_to_player_wr     : OUT STD_LOGIC;
        i_data_to_player_rdy    : IN STD_LOGIC;

        -- Timing signals
        o_timer_fields_in       : out timer_fields_in;
        i_timer_fields_out      : in timer_fields_out;
        
        -- mux select for data path
        o_vd_enable             : out std_logic; 

        -- PTP scheduler vector
        i_schedule_action       : in std_logic_vector(7 downto 0);
        
        -- debug vector
        i_debug                 : in std_logic_vector(((g_DEBUG_VEC_SIZE)-1) downto 0);

        ------------------------------------------------------------
        -- CNIC NOISE GEN        
        -- Indicates this module is currently writing data to the first 512 MBytes of the HBM
        i_writing_buf0          : in std_logic_vector(1 downto 0);
        -- Indicates this module is currently writing data to the 2nd 512 MBytes of the HBM
        i_writing_buf1          : in std_logic_vector(1 downto 0);
        -- Pulse to start writing data (to whichever buffer we are currently up to)
        o_start_noise_gen       : out std_logic_vector(1 downto 0);
        o_reset_noise_gen       : out std_logic_vector(1 downto 0);

        -- ar bus - read address
        vd_axi_arvalid          : out std_logic;
        vd_axi_arready          : in  std_logic;
        vd_axi_araddr           : out std_logic_vector(31 downto 0);
        vd_axi_arlen            : out std_logic_vector(7 downto 0);
        -- r bus - read data
        vd_axi_rvalid           : in  std_logic;
        vd_axi_rready           : out std_logic;
        vd_axi_rdata            : in  std_logic_vector(511 downto 0);
        vd_axi_rlast            : in  std_logic;
        vd_axi_rresp            : in  std_logic_vector(1 downto 0);

        -- ar bus - read address
        vd_hbm_2_axi_arvalid    : out std_logic;
        vd_hbm_2_axi_arready    : in  std_logic;
        vd_hbm_2_axi_araddr     : out std_logic_vector(31 downto 0);
        vd_hbm_2_axi_arlen      : out std_logic_vector(7 downto 0);
        -- r bus - read data
        vd_hbm_2_axi_rvalid     : in  std_logic;
        vd_hbm_2_axi_rready     : out std_logic;
        vd_hbm_2_axi_rdata      : in  std_logic_vector(511 downto 0);
        vd_hbm_2_axi_rlast      : in  std_logic;
        vd_hbm_2_axi_rresp      : in  std_logic_vector(1 downto 0);
        -----------------------------------------------------------------------

        -- VD ARGs interface
        i_vd_lite_axi_mosi      : in t_axi4_lite_mosi_arr(1 downto 0); 
        o_vd_lite_axi_miso      : out t_axi4_lite_miso_arr(1 downto 0);
        i_vd_full_axi_mosi      : in  t_axi4_full_mosi_arr(1 downto 0);
        o_vd_full_axi_miso      : out t_axi4_full_miso_arr(1 downto 0)

    );
end virtual_digitiser;

architecture Behavioral of virtual_digitiser is

COMPONENT ila_0
PORT (
    clk : IN STD_LOGIC;
    probe0 : IN STD_LOGIC_VECTOR(191 DOWNTO 0));
END COMPONENT;

signal clk                      : STD_LOGIC;
signal reset                    : STD_LOGIC;

signal args_bram_rst            : std_logic;
signal args_bram_clk            : std_logic;
signal args_bram_en             : std_logic;
signal args_bram_we_byte        : std_logic_vector(3 DOWNTO 0);
signal args_bram_addr           : std_logic_vector(16 DOWNTO 0);
signal args_bram_wrdata         : std_logic_vector(31 DOWNTO 0);
signal args_bram_rddata         : std_logic_vector(31 DOWNTO 0);

signal config_mem_wren          : std_logic;

signal vd_reg_from_host         : t_vd_reg_rw;
signal vd_reg_to_host           : t_vd_reg_ro;

-- signals to pass into packetiser
signal spead_config_list                : spead_config_items;

signal config_rd_address                : unsigned(11 downto 0);
signal config_mem_rd_addr               : std_logic_vector(11 downto 0);

signal time_between_channel_bursts		: std_logic_vector(31 downto 0);

signal bytes_to_transmit        : STD_LOGIC_VECTOR(13 downto 0);     
signal data_to_player           : STD_LOGIC_VECTOR(511 downto 0);
signal data_to_player_wr        : STD_LOGIC;
signal data_to_player_wr_del    : STD_LOGIC;
signal stats_packet_count       : unsigned(63 downto 0) := ( others => '0' );
signal stats_curr_count         : unsigned(63 downto 0) := ( others => '0' );

signal enable_player_ready      : std_logic;
signal wait_for_pulse           : std_logic;

signal timestamp_calc           : std_logic_vector(47 downto 0);
signal enable_vd_int            : std_logic;
signal enable_vd                : std_logic;
signal packet_counter_calc      : std_logic_vector(47 downto 0);

signal ptp_enable               : std_logic;

signal time_stamp_inc           : std_logic_vector(31 downto 0);

signal noise_data               : std_logic_vector(511 downto 0);
signal noise_data_rd            : std_logic;
signal noise_data_rdy           : std_logic;

CONSTANT c_NOISE_GENERATORS       : integer := 2;

signal noise_data_from_hbm      : t_slv_512_arr((c_NOISE_GENERATORS - 1) downto 0);
signal noise_data_from_hbm_rd   : std_logic_vector((c_NOISE_GENERATORS - 1) downto 0);
signal noise_data_from_hbm_rdy  : std_logic_vector((c_NOISE_GENERATORS - 1) downto 0);

signal reset_vd_gen_logic_host  : std_logic;
signal reset_vd_gen_logic       : std_logic;

signal entries_in_gen_table     : std_logic_vector(11 downto 0);

signal entries_in_instance_1    : std_logic_vector(11 downto 0);
signal entries_in_instance_2    : std_logic_vector(11 downto 0);

signal enable_gen_2             : std_logic;
signal enable_vd_inst_2         : std_logic;

signal vd_buffer_initialised    : std_logic;

signal vd_max_bursts            : std_logic_vector(31 downto 0);
signal vd_max_bursts_cnt        : unsigned(31 downto 0);
signal enable_bursts            : std_logic;

signal mem_word_0_4b            : std_logic_vector(31 downto 0);
signal mem_word_1_2b            : std_logic_vector(15 downto 0);
signal mem_word_2_2b            : std_logic_vector(15 downto 0);
signal mem_word_3_1b            : std_logic_vector(7 downto 0);
signal mem_word_4_1b            : std_logic_vector(7 downto 0);
signal mem_word_5_2b            : std_logic_vector(15 downto 0);
signal mem_word_6_2b            : std_logic_vector(15 downto 0);

signal vd_enable_mux            : std_logic;
signal vd_enable_mux_d1         : std_logic;

begin

------------------------------------------------------------------------------
-- PORT MAPPINGS
clk                     <= i_clk;
reset                   <= i_rst;

o_bytes_to_transmit     <= bytes_to_transmit;
o_data_to_player        <= data_to_player;
o_data_to_player_wr     <= data_to_player_wr;

o_vd_enable             <= vd_enable_mux_d1;

------------------------------------------------------------------------------
-- Control register mappings.
en_vd_hbm_proc : process (clk)
begin
    if rising_edge(clk) then
        vd_enable_mux       <= vd_reg_from_host.enable_vd_hbm_path;      -- only used for MUX select to CMAC TX.
        vd_enable_mux_d1    <= vd_enable_mux;
    end if;
end process;

time_between_channel_bursts                         <= vd_reg_from_host.time_between_channel_bursts;
time_stamp_inc                                      <= vd_reg_from_host.time_stamp_inc;

enable_vd                                           <= vd_reg_from_host.enable_vd OR i_debug(3);
reset_vd_gen_logic_host                             <= vd_reg_from_host.reset_vd_data_gen_logic;

spead_config_list.channel_id                        <= vd_reg_from_host.channel_id;
spead_config_list.packet_payload_length             <= x"00000000" & vd_reg_from_host.packet_payload_length;
spead_config_list.time_between_packets              <= vd_reg_from_host.time_between_packets;
spead_config_list.unix_epoch_time                   <= vd_reg_from_host.unix_epoch_time_u & vd_reg_from_host.unix_epoch_time_l;
spead_config_list.version_select                    <= vd_reg_from_host.sps_packet_version_select;

entries_in_gen_table                                <= vd_reg_from_host.number_of_valid_lines_in_vd_ram(11 downto 0);

vd_max_bursts                                       <= vd_reg_from_host.max_number_of_VD_bursts;

vd_reg_to_host.no_of_sps_packets_l                  <= std_logic_vector(stats_packet_count(31 downto 0));
vd_reg_to_host.no_of_sps_packets_u	                <= std_logic_vector(stats_packet_count(63 downto 32));

vd_reg_to_host.no_of_packets_this_config_l          <= std_logic_vector(stats_curr_count(31 downto 0));
vd_reg_to_host.no_of_packets_this_config_u          <= std_logic_vector(stats_curr_count(63 downto 32));

vd_reg_to_host.current_packet_count                 <= packet_counter_calc(31 downto 0);
vd_reg_to_host.current_packet_count_u               <= packet_counter_calc(47 downto 32);

------------------------------------------------------------------------------
-- enable_proc
en_proc : process (clk)
begin
    if rising_edge(clk) then
        if (enable_vd = '0') then
            enable_vd_int           <= '0';
            ptp_enable              <= '0';
            vd_buffer_initialised   <= '0';
            o_reset_packet_player   <= '1';
        else
            enable_vd_int           <= ptp_enable;
        
            if (vd_reg_from_host.use_ptp_to_begin = '1')  OR (i_debug(4) = '1') then
                ptp_enable      <= i_schedule_action(1);
            else
                ptp_enable      <= '1';
            end if;

            -- If the VD is triggered without PTP, need to wait until the buffer has been generated
            -- The enable_vd signal used to start timer and VD data generate/read sequence.
            -- Once the buffer has been primed out of reset, then timer can start.
            --
            -- keep packet player in reset until VD ready to go.
            if noise_data_rdy = '1' then
                vd_buffer_initialised   <= '1';
                
                o_reset_packet_player   <= '0';
            end if;
            
        end if;
    end if;
end process;

------------------------------------------------------------------------------
-- timer config

timer_proc : process(clk)
begin
    if rising_edge(clk) then
        if reset = '1' then
            o_timer_fields_in.timer_goal_in_ns  <= ( others => '0' );
            o_timer_fields_in.timer_on          <= '0';
        else
            o_timer_fields_in.timer_goal_in_ns  <= time_between_channel_bursts;
            o_timer_fields_in.timer_on          <= enable_vd_int AND vd_buffer_initialised;

        end if;
    end if;
end process;

------------------------------------------------------------------------------
-- vd_process

spead_config_list.timestamp         <= timestamp_calc;
spead_config_list.packet_counter	<= packet_counter_calc;

vd_proc : process(clk)
begin
    if rising_edge(clk) then
        if reset = '1' then
            config_rd_address   <= ( others => '0');
            stats_packet_count  <= ( others => '0');
            stats_curr_count    <= ( others => '0');
            wait_for_pulse      <= '0';
            enable_player_ready <= '0';
            enable_bursts       <= '1';
            vd_max_bursts_cnt   <= x"00000000";
            entries_in_instance_1   <= ( others => '0');
            enable_gen_2            <= '0';
            entries_in_instance_2   <= ( others => '0');
            data_to_player_wr_del   <= '0';
        else

            if enable_vd_int = '0' then
                timestamp_calc          <= vd_reg_from_host.unix_epoch_time_u   & vd_reg_from_host.unix_epoch_time_l;
                packet_counter_calc     <= vd_reg_from_host.packet_counter_u    & vd_reg_from_host.packet_counter;
                wait_for_pulse          <= '1';
                config_rd_address       <= ( others => '0');
                stats_curr_count        <= ( others => '0');
                enable_player_ready     <= '0';
                vd_max_bursts_cnt       <= x"00000000";
                enable_bursts           <= '1';
                data_to_player_wr_del   <= '0';
            else
            
                data_to_player_wr_del   <= data_to_player_wr;
            
                -- count as packet is finished sent
                if data_to_player_wr = '0' and data_to_player_wr_del = '1' then
                    stats_packet_count  <= stats_packet_count + 1;
                    stats_curr_count    <= stats_curr_count + 1;

                    if unsigned(entries_in_gen_table) = config_rd_address then
                        config_rd_address       <= ( others => '0');
                        wait_for_pulse          <= '0';
                        timestamp_calc          <= std_logic_vector(unsigned(timestamp_calc) + unsigned(time_stamp_inc));
                        packet_counter_calc     <= std_logic_vector(unsigned(packet_counter_calc) + 1);
                        vd_max_bursts_cnt       <= vd_max_bursts_cnt + 1;
                    else
                        config_rd_address       <= config_rd_address + 1;
                    end if;
                end if;

                if i_timer_fields_out.timing_pulse = '1' then
                    wait_for_pulse      <= '1';
                end if;

                if vd_max_bursts(31) = '0' then
                    enable_bursts   <= '1';
                else
                    if vd_max_bursts(30 downto 0) = std_logic_vector(vd_max_bursts_cnt(30 downto 0)) then
                        enable_bursts   <= '0';
                    end if;
                end if;

                enable_player_ready     <= i_data_to_player_rdy AND wait_for_pulse AND enable_bursts;

            end if;
            
            -- if the number of signals to generate is above 1k then set the HBM to 1023 for instance 1
            if entries_in_gen_table(10) = '1' then
                entries_in_instance_1   <= x"3FF";
                enable_gen_2            <= '1';
                entries_in_instance_2   <= "00" & entries_in_gen_table(9 downto 0);
            else
                entries_in_instance_1   <= entries_in_gen_table;
                enable_gen_2            <= '0';
                entries_in_instance_2   <= ( others => '0');
            end if;

            config_mem_rd_addr      <= std_logic_vector(config_rd_address);

        end if;
    end if;
end process;

------------------------------------------------------------------------------
-- CONNECT PACKETISER to HBM RD

noise_data_from_hbm_rd(0)   <= noise_data_rd when config_mem_rd_addr(10) = '0' else
                                '0';

noise_data_from_hbm_rd(1)   <= noise_data_rd when config_mem_rd_addr(10) = '1' else
                                '0';

-- CONNECT HBM RD to PACKETISER

noise_data                  <= noise_data_from_hbm(0) when config_mem_rd_addr(10) = '0' else
                                noise_data_from_hbm(1);

noise_data_rdy              <= noise_data_from_hbm_rdy(0) when config_mem_rd_addr(10) = '0' else
                                noise_data_from_hbm_rdy(1);

------------------------------------------------------------------------------
-- VD noise data buffer retrieval

reset_vd_gen_logic  <= reset_vd_gen_logic_host OR reset;

i_noise_buffer_ret_1 : entity vd_lib.vd_hbm_data_rd 
    generic map ( 
        g_DEBUG_ILA             => g_DEBUG_ILA
    )
    port map (
        -- (300 MHz)
        i_clk                   => clk,
        i_reset                 => reset_vd_gen_logic,

        ------------------------------------------------------------------------------------

        o_data_from_hbm         => noise_data_from_hbm(0),
        i_data_from_hbm_rd      => noise_data_from_hbm_rd(0),
        o_data_from_hbm_pkt_rdy => noise_data_from_hbm_rdy(0),

        -----------------------------------------------------------------------
        -- Control signals to the rest of CNIC
        -- Indicates this module is currently writing data to the first 512 MBytes of the HBM
        i_writing_buf0          => i_writing_buf0(0),
        -- Indicates this module is currently writing data to the 2nd 512 MBytes of the HBM
        i_writing_buf1          => i_writing_buf1(0),
        -- Pulse to start writing data (to whichever buffer we are currently up to)
        o_start_noise_gen       => o_start_noise_gen(0),
        o_reset_noise_gen       => o_reset_noise_gen(0),

        i_number_of_streams     => entries_in_instance_1,
        i_enable_noise_gen      => enable_vd,

        o_rd_fsm_debug          => vd_reg_to_host.hbm_rd_rd_fsm_debug(3 downto 0),
        o_vd_gen_sm_debug       => vd_reg_to_host.hbm_rd_vd_gen_sm_debug(3 downto 0),

        o_time_gating_sm_debug  => vd_reg_to_host.hbm_rd_tg_sm_1_debug,
        o_rd_fsm_cache          => vd_reg_to_host.hbm_rd_rd_fsm_1_cache,
        o_vd_gen_sm_cache       => vd_reg_to_host.hbm_rd_vd_gen_fsm_1_cache,
        o_time_gating_sm_cache  => vd_reg_to_host.hbm_rd_tg_sm_1_cache,

        o_vd_buffer_gen_status  => vd_reg_to_host.vd_buffer_gen_status,

        i_enable_pulsar_timing        => vd_reg_from_host.enable_pulsar_timing,
        i_pulsar_start_sample_count   => vd_reg_from_host.pulsar_start_sample_count,
        i_pulsar_on_sample_count      => vd_reg_from_host.pulsar_on_sample_count,
        i_pulsar_off_sample_count     => vd_reg_from_host.pulsar_off_sample_count,

        -----------------------------------------------------------------------
        -- debug
        o_gen_while_writ_cnt    => vd_reg_to_host.hbm_rd_1_gen_while_writ_cnt,
        o_generate_cnt          => vd_reg_to_host.hbm_rd_1_gen_cnt,
        o_buffer_0_cnt          => vd_reg_to_host.hbm_rd_1_buf_0_cnt,
        o_buffer_1_cnt          => vd_reg_to_host.hbm_rd_1_buf_1_cnt,
        -----------------------------------------------------------------------
        -- ar bus - read address
        vd_axi_arvalid          => vd_axi_arvalid,
        vd_axi_arready          => vd_axi_arready,
        vd_axi_araddr           => vd_axi_araddr,
        vd_axi_arlen            => vd_axi_arlen,
        -- r bus - read data
        vd_axi_rvalid           => vd_axi_rvalid,
        vd_axi_rready           => vd_axi_rready,
        vd_axi_rdata            => vd_axi_rdata,
        vd_axi_rlast            => vd_axi_rlast,
        vd_axi_rresp            => vd_axi_rresp
    );

    enable_vd_inst_2    <= enable_vd AND enable_gen_2;
    
i_noise_buffer_ret_2 : entity vd_lib.vd_hbm_data_rd 
    generic map ( 
        g_DEBUG_ILA             => g_DEBUG_ILA
    )
    port map (
        -- (300 MHz)
        i_clk                   => clk,
        i_reset                 => reset_vd_gen_logic,

        ------------------------------------------------------------------------------------

        o_data_from_hbm         => noise_data_from_hbm(1),
        i_data_from_hbm_rd      => noise_data_from_hbm_rd(1),
        o_data_from_hbm_pkt_rdy => noise_data_from_hbm_rdy(1),

        -----------------------------------------------------------------------
        -- Control signals to the rest of CNIC
        -- Indicates this module is currently writing data to the first 512 MBytes of the HBM
        i_writing_buf0          => i_writing_buf0(1),
        -- Indicates this module is currently writing data to the 2nd 512 MBytes of the HBM
        i_writing_buf1          => i_writing_buf1(1),
        -- Pulse to start writing data (to whichever buffer we are currently up to)
        o_start_noise_gen       => o_start_noise_gen(1),
        o_reset_noise_gen       => o_reset_noise_gen(1),

        i_number_of_streams     => entries_in_instance_2,
        i_enable_noise_gen      => enable_vd_inst_2,

        o_rd_fsm_debug          => vd_reg_to_host.hbm_rd_rd_fsm_2_debug(3 downto 0),
        o_vd_gen_sm_debug       => vd_reg_to_host.hbm_rd_vd_gen_sm_2_debug(3 downto 0),

        o_time_gating_sm_debug  => vd_reg_to_host.hbm_rd_tg_sm_2_debug,
        o_rd_fsm_cache          => vd_reg_to_host.hbm_rd_rd_fsm_2_cache,
        o_vd_gen_sm_cache       => vd_reg_to_host.hbm_rd_vd_gen_fsm_2_cache,
        o_time_gating_sm_cache  => vd_reg_to_host.hbm_rd_tg_sm_2_cache,

        o_vd_buffer_gen_status  => vd_reg_to_host.vd_buffer_gen_2_status,

        i_enable_pulsar_timing        => vd_reg_from_host.enable_pulsar_timing,
        i_pulsar_start_sample_count   => vd_reg_from_host.pulsar_start_sample_count,
        i_pulsar_on_sample_count      => vd_reg_from_host.pulsar_on_sample_count,
        i_pulsar_off_sample_count     => vd_reg_from_host.pulsar_off_sample_count,

        -----------------------------------------------------------------------
        -- debug
        o_gen_while_writ_cnt    => vd_reg_to_host.hbm_rd_2_gen_while_writ_cnt,
        o_generate_cnt          => vd_reg_to_host.hbm_rd_2_gen_cnt,
        o_buffer_0_cnt          => vd_reg_to_host.hbm_rd_2_buf_0_cnt,
        o_buffer_1_cnt          => vd_reg_to_host.hbm_rd_2_buf_1_cnt,
        -----------------------------------------------------------------------
        -- ar bus - read address
        vd_axi_arvalid          => vd_hbm_2_axi_arvalid,
        vd_axi_arready          => vd_hbm_2_axi_arready,
        vd_axi_araddr           => vd_hbm_2_axi_araddr,
        vd_axi_arlen            => vd_hbm_2_axi_arlen,
        -- r bus - read data
        vd_axi_rvalid           => vd_hbm_2_axi_rvalid,
        vd_axi_rready           => vd_hbm_2_axi_rready,
        vd_axi_rdata            => vd_hbm_2_axi_rdata,
        vd_axi_rlast            => vd_hbm_2_axi_rlast,
        vd_axi_rresp            => vd_hbm_2_axi_rresp
    );

------------------------------------------------------------------------------
-- SPEAD Packetiser
spead_sps_packetiser : entity spead_sps_lib.sps_spead_packet
    generic map ( 
        g_DEBUG_ILA             => g_DEBUG_ILA,
        g_DEBUG_VEC_SIZE        => g_DEBUG_VEC_SIZE
    )
    port map ( 
        -- clock used for all data input and output from this module (300 MHz)
        i_clk                   => i_clk,
        i_rst                   => i_rst,

        i_local_reset           => i_local_reset,

        -- signals to pass into packetiser
        i_spead_config_list     => spead_config_list,

        -- data for SPEAD section signals
        i_data_from_hbm         => noise_data,
        o_data_from_hbm_rd      => noise_data_rd,
        i_data_from_hbm_pkt_rdy => noise_data_rdy,

        -- S_AXI aligned.
        o_bytes_to_transmit     => bytes_to_transmit,
        o_data_to_player        => data_to_player,
        o_data_to_player_wr     => data_to_player_wr,
        i_data_to_player_rdy    => enable_player_ready,

        o_packetiser_enable     => open,

        -- debug vector
        i_debug                 => i_debug,
        
        -- ARGs interface.
        i_spead_lite_axi_mosi   => i_vd_lite_axi_mosi(1),
        o_spead_lite_axi_miso   => o_vd_lite_axi_miso(1),
        i_spead_full_axi_mosi   => i_vd_full_axi_mosi(1),
        o_spead_full_axi_miso   => o_vd_full_axi_miso(1)

    );


------------------------------------------------------------------------------
-- ARGs
ARGS_register_vd : entity vd_lib.VD_vd_reg 
    PORT MAP (
        -- AXI Lite signals, 300 MHz Clock domain
        MM_CLK                  => clk,
        MM_RST                  => reset,
        
        SLA_IN                  => i_vd_lite_axi_mosi(0),
        SLA_OUT                 => o_vd_lite_axi_miso(0),

        VD_REG_FIELDS_RW        => vd_reg_from_host,
        VD_REG_FIELDS_RO        => vd_reg_to_host
        );

-- Full axi to bram
vd_axi_bram_inst : entity vd_lib.vd_axi_bram_wrapper
    port map ( 
        i_clk                   => clk,
        i_rst                   => reset,

        bram_rst                => args_bram_rst,
        bram_clk                => args_bram_clk,
        bram_en                 => args_bram_en,
        bram_we_byte            => args_bram_we_byte,
        bram_addr               => args_bram_addr,
        bram_wrdata             => args_bram_wrdata,
        bram_rddata             => args_bram_rddata,

        i_vd_full_axi_mosi      => i_vd_full_axi_mosi(0),
        o_vd_full_axi_miso      => o_vd_full_axi_miso(0)

    );

    config_mem_wren <= args_bram_we_byte(0) AND args_bram_en;

-- VD mem
vd_mem_inst : entity vd_lib.config_mem
    generic map (
        g_DEBUG_ILA             => g_DEBUG_ILA
    )
    port map ( 
        i_clk                   => clk,
        i_rst                   => reset,

        i_host_data             => args_bram_wrdata,
        i_host_addr             => args_bram_addr(16 downto 2),
        i_host_wren             => config_mem_wren,
        o_host_return_data      => args_bram_rddata,

        -------------------------------------------------
        i_rd_addr               => config_mem_rd_addr,

        o_word_0_4b             => mem_word_0_4b,
        o_word_1_2b             => mem_word_1_2b,
        o_word_2_2b             => mem_word_2_2b,
        o_word_3_1b             => mem_word_3_1b,
        o_word_4_1b             => mem_word_4_1b,
        o_word_5_2b             => mem_word_5_2b,
        o_word_6_2b             => mem_word_6_2b
    );

spead_config_list.scan_id       <= mem_word_6_2b & mem_word_0_4b;
spead_config_list.beam_id       <= mem_word_1_2b;
spead_config_list.freq_id       <= mem_word_2_2b;
spead_config_list.substation_id <= mem_word_3_1b;
spead_config_list.subarray_id   <= mem_word_4_1b;
spead_config_list.station_id    <= mem_word_5_2b;
spead_config_list.frequency_hz  <= mem_word_6_2b & mem_word_0_4b;

end Behavioral;
