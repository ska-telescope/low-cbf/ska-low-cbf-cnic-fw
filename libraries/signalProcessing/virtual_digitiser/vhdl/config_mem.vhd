----------------------------------------------------------------------------------
-- Company: CSIRO
-- Engineer: Giles Babich
-- 
-- Create Date: 02/04/2023
-- Design Name: 
-- Module Name: config_mem.vhd
--  
-- Description: 
-- 32 KB of space.
-- Each address location is 4 bytes / 32 bit
--
-- Scan_id         4B
-- Beam_id         2B
-- Frequency_id    2B
-- Substation_id   1B
-- Subarray_id     1B
-- Station_id      2B
--                 12 Bytes
-- URAM = 72 (9 bytes) x 4096 addr x 2 instances.
-- Firmware will strip the writes across the RAMs
--
-- Software address                URAM ADDR   4 byte software data interface
-- Address 0 = Scan ID for         line 0      all bytes written
-- Address 1 = Beam ID for         line 0      lowest two byte written
-- Address 2 = Freq ID for         line 0      lowest two byte written
-- Address 3 = Substation ID for   line 0      byte written
-- Address 4 = Subarray ID for     line 0      byte written
-- Address 5 = Station ID for      line 0      lowest two byte written
--
-- Address 8 = Scan ID for         line 1      all bytes written
-- Address 9 = Beam ID for         line 1      lowest two byte written
-- Address 10 = Freq ID for        line 1      lowest two byte written
-- Address 11 = Substation ID for  line 1      byte written
-- Address 12 = Subarray ID for    line 1      byte written
-- Address 12 = Station ID for     line 1      lowest two byte written
--
--
-- 2 URAMs will be instantiated at 9B x 4096 addr
-- width will be 18B. = 144 bits
----------------------------------------------------------------------------------

library IEEE, spead_lib, xpm, common_lib;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use xpm.vcomponents.all;
use common_lib.common_pkg.ALL;
library UNISIM;
use UNISIM.VComponents.all;


entity config_mem is
    Generic (
        g_DEBUG_ILA             : BOOLEAN := FALSE
    );
    Port ( 
        i_clk                   : in std_logic;
        i_rst                   : in std_logic;

        i_host_data             : in std_logic_vector(31 downto 0);
        i_host_addr             : in std_logic_vector(14 downto 0); -- 32kB of address space
        i_host_wren             : in std_logic;
        o_host_return_data      : out std_logic_vector(31 downto 0);

        -------------------------------------------------
        i_rd_addr               : in std_logic_vector(11 downto 0); -- 4096 of addr space.

        o_word_0_4b             : out std_logic_vector(31 downto 0);
        o_word_1_2b             : out std_logic_vector(15 downto 0);
        o_word_2_2b             : out std_logic_vector(15 downto 0);
        o_word_3_1b             : out std_logic_vector(7 downto 0);
        o_word_4_1b             : out std_logic_vector(7 downto 0);
        o_word_5_2b             : out std_logic_vector(15 downto 0);
        o_word_6_2b             : out std_logic_vector(15 downto 0)

    );
end config_mem;

architecture Behavioral of config_mem is

COMPONENT ila_0
    PORT (
        clk : IN STD_LOGIC;
        probe0 : IN STD_LOGIC_VECTOR(191 DOWNTO 0));
END COMPONENT;

constant MEMORY_INIT_FILE       : STRING := "config_mem_init.mem";
constant g_NO_OF_ADDR_BITS      : INTEGER := 12;
constant g_D_Q_WIDTH            : INTEGER := 144;
constant g_BYTE_ENABLE_WIDTH    : INTEGER := 8;

CONSTANT ADDR_SPACE             : INTEGER := pow2(g_NO_OF_ADDR_BITS);
CONSTANT MEMORY_SIZE_GENERIC    : INTEGER := ADDR_SPACE * g_D_Q_WIDTH;


signal clk_a                    : std_logic;
signal clk_b                    : std_logic;
signal clk                      : std_logic;
signal reset                    : std_logic;
signal reset_n                  : std_logic;

signal data_a                   : std_logic_vector((g_D_Q_WIDTH-1) downto 0);
signal addr_a                   : std_logic_vector((g_NO_OF_ADDR_BITS-1) downto 0);
signal data_a_wr                : std_logic; 
signal data_a_q                 : std_logic_vector((g_D_Q_WIDTH-1) downto 0);

signal data_b                   : std_logic_vector((g_D_Q_WIDTH-1) downto 0);
signal addr_b                   : std_logic_vector((g_NO_OF_ADDR_BITS-1) downto 0);
signal data_b_wr                : std_logic; 
signal data_b_q                 : std_logic_vector((g_D_Q_WIDTH-1) downto 0);

signal data_a_wr_int            : std_logic_vector(17 downto 0);
signal data_b_wr_int            : std_logic_vector(17 downto 0);

signal SM_addr                  : unsigned(7 downto 0);

signal spead_item_to_update     : std_logic_vector(31 downto 0);

signal update_scratch_value     : std_logic_vector(31 downto 0);

signal host_return_data_int     : std_logic_vector(31 downto 0);

signal bram_addr_d1             : std_logic_vector(5 downto 0);
signal bram_addr_d2             : std_logic_vector(5 downto 0);
signal bram_addr_d3             : std_logic_vector(5 downto 0);

begin

clk                     <= i_clk;
reset                   <= i_rst;

-------------------------------------------------------------------------

addr_b                  <= i_rd_addr;
-- only reading
data_b_wr               <= '0';
data_b_wr_int           <= ( others => '0');
data_b                  <= ( others => '0');

o_word_0_4b             <= data_b_q(31 downto 0);
o_word_1_2b             <= data_b_q(47 downto 32);
o_word_2_2b             <= data_b_q(63 downto 48);
o_word_3_1b             <= data_b_q(71 downto 64);
o_word_4_1b             <= data_b_q(79 downto 72);
o_word_5_2b             <= data_b_q(95 downto 80);
o_word_6_2b             <= data_b_q(111 downto 96);

-------------------------------------------------------------------------
-- ARGs side
data_a                  <=  x"00000000" 
                            & i_host_data(15 downto 0)
                            & i_host_data(15 downto 0)
                            & i_host_data(7 downto 0) 
                            & i_host_data(7 downto 0)
                            & i_host_data(15 downto 0)
                            & i_host_data(15 downto 0) 
                            & i_host_data(31 downto 0);

addr_a                  <= i_host_addr(14 downto 3);

data_a_wr               <= i_host_wren;

-- URAM_1                   Addr
-- Scan_id         4B       0
-- Beam_id         2B       1

-- URAM_2
-- Frequency_id    2B       2
-- Substation_id   1B       3
-- Subarray_id     1B       4
-- Station_id      2B       5
--                 2B       6
--                 4B       7


data_a_wr_int(17 downto 0)  <=  "00" & x"000F" when ((i_host_addr(2 downto 0) = "000") AND (data_a_wr = '1')) else 
                                "00" & x"0030" when ((i_host_addr(2 downto 0) = "001") AND (data_a_wr = '1')) else 
                                "00" & x"00C0" when ((i_host_addr(2 downto 0) = "010") AND (data_a_wr = '1')) else 
                                "00" & x"0100" when ((i_host_addr(2 downto 0) = "011") AND (data_a_wr = '1')) else 
                                "00" & x"0200" when ((i_host_addr(2 downto 0) = "100") AND (data_a_wr = '1')) else 
                                "00" & x"0C00" when ((i_host_addr(2 downto 0) = "101") AND (data_a_wr = '1')) else 
                                "00" & x"3000" when ((i_host_addr(2 downto 0) = "110") AND (data_a_wr = '1')) else 
                                (others => '0');

-------------------------------------------------------------------------
-- delay select based on address delay.
bram_return_data_proc : process(clk)
begin
    if rising_edge(clk) then
        bram_addr_d1    <= i_host_addr(5 downto 0);
        bram_addr_d2    <= bram_addr_d1;
        bram_addr_d3    <= bram_addr_d2;
    end if;
end process;

o_host_return_data          <= host_return_data_int;

host_return_data_int        <=              data_a_q(31 downto 0)   when bram_addr_d3(2 downto 0) = "000" else
                                x"0000" &   data_a_q(47 downto 32)  when bram_addr_d3(2 downto 0) = "001" else
                                x"0000" &   data_a_q(63 downto 48)  when bram_addr_d3(2 downto 0) = "010" else
                                x"000000" & data_a_q(71 downto 64)  when bram_addr_d3(2 downto 0) = "011" else
                                x"000000" & data_a_q(79 downto 72)  when bram_addr_d3(2 downto 0) = "100" else
                                x"0000" &   data_a_q(95 downto 80)  when bram_addr_d3(2 downto 0) = "101" else
                                x"0000" &   data_a_q(111 downto 96) when bram_addr_d3(2 downto 0) = "110" else
                                x"00C0FFEE";

-------------------------------------------------------------------------
generate_debug_ila : IF g_DEBUG_ILA GENERATE                              
    init_debug : ila_0 PORT MAP (
        clk                     => clk,
        probe0(63 downto 0)     => data_a(63 downto 0),
        probe0(95 downto 64)    => host_return_data_int,
        probe0(127 downto 96)   => i_host_data,
        probe0(139 downto 128)  => addr_a,

        probe0(154 downto 140)  => i_host_addr,
        probe0(172 downto 155)  => data_a_wr_int,

        probe0(191 downto 173)  => data_a_q(91 downto 73)
        );
END GENERATE;

-------------------------------------------------------------------------    
clk_a   <= clk;
clk_b   <= clk;



uram_1 : xpm_memory_tdpram
    generic map (    
        -- Common module generics
        AUTO_SLEEP_TIME         => 0,              --Do not Change
        CASCADE_HEIGHT          => 0,
        CLOCKING_MODE           => "common_clock", --string; "common_clock", "independent_clock" 
        ECC_MODE                => "no_ecc",       --string; "no_ecc", "encode_only", "decode_only" or "both_encode_and_decode" 

        MEMORY_INIT_FILE        => MEMORY_INIT_FILE,         --string; "none" or "<filename>.mem" 
        MEMORY_INIT_PARAM       => "",             --string;
        MEMORY_OPTIMIZATION     => "true",          --string; "true", "false" 
        MEMORY_PRIMITIVE        => "ultra",         --string; "auto", "distributed", "block" or "ultra" ;
        MEMORY_SIZE             => MEMORY_SIZE_GENERIC, --262144,          -- Total memory size in bits; 512 x 512 = 262144
        MESSAGE_CONTROL         => 0,              --integer; 0,1

        USE_MEM_INIT            => 0,              --integer; 0,1
        WAKEUP_TIME             => "disable_sleep",--string; "disable_sleep" or "use_sleep_pin" 
        USE_EMBEDDED_CONSTRAINT => 0,              --integer: 0,1
       
    
        RST_MODE_A              => "SYNC",   
        RST_MODE_B              => "SYNC", 
        WRITE_MODE_A            => "no_change",    --string; "write_first", "read_first", "no_change" 
        WRITE_MODE_B            => "no_change",    --string; "write_first", "read_first", "no_change" 

        -- Port A module generics ... ARGs side
        READ_DATA_WIDTH_A       => g_D_Q_WIDTH,    
        READ_LATENCY_A          => 3,              
        READ_RESET_VALUE_A      => "0",            

        WRITE_DATA_WIDTH_A      => g_D_Q_WIDTH,
        BYTE_WRITE_WIDTH_A      => g_BYTE_ENABLE_WIDTH,
        ADDR_WIDTH_A            => g_NO_OF_ADDR_BITS,
    
        -- Port B module generics
        READ_DATA_WIDTH_B       => g_D_Q_WIDTH,
        READ_LATENCY_B          => 3,           
        READ_RESET_VALUE_B      => "0",         

        WRITE_DATA_WIDTH_B      => g_D_Q_WIDTH,
        BYTE_WRITE_WIDTH_B      => g_BYTE_ENABLE_WIDTH,             
        ADDR_WIDTH_B            => g_NO_OF_ADDR_BITS
        )
    port map (
        -- Common module ports
        sleep                   => '0',
        -- Port A side
        clka                    => clk_a,  -- clock from the 100GE core; 322 MHz
        rsta                    => '0',
        ena                     => '1',
        regcea                  => '1',

        wea                     => data_a_wr_int,
        addra                   => addr_a,
        dina                    => data_a,
        douta                   => data_a_q,

        -- Port B side
        clkb                    => clk_b,  -- This goes to a dual clock fifo to meet the external interface clock to connect to the HBM at 300 MHz.
        rstb                    => '0',
        enb                     => '1',
        regceb                  => '1',

        web                     => data_b_wr_int,
        addrb                   => addr_b,
        dinb                    => data_b,
        doutb                   => data_b_q,

        -- other features
        injectsbiterra          => '0',
        injectdbiterra          => '0',
        injectsbiterrb          => '0',
        injectdbiterrb          => '0',        
        sbiterra                => open,
        dbiterra                => open,
        sbiterrb                => open,
        dbiterrb                => open
    );      

end Behavioral;
