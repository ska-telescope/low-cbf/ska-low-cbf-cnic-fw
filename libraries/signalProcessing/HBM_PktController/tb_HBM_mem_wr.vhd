----------------------------------------------------------------------------------
-- Company: CSIRO
-- Engineer: Giles Babich
-- 
-- 
-- Dependencies: 
-- 
-- 
----------------------------------------------------------------------------------


library IEEE,technology_lib, PSR_Packetiser_lib, signal_processing_common, HBM_PktController_lib;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use PSR_Packetiser_lib.ethernet_pkg.ALL;
use PSR_Packetiser_lib.CbfPsrHeader_pkg.ALL;


entity tb_HBM_mem_wr is
--  Port ( );
end tb_HBM_mem_wr;

architecture Behavioral of tb_HBM_mem_wr is

signal clock_300 : std_logic := '0';    -- 3.33ns
signal clock_400 : std_logic := '0';    -- 2.50ns
signal clock_322 : std_logic := '0';    -- 3.11ns

signal testCount_400        : integer   := 0;
signal testCount_300        : integer   := 0;
signal testCount_322        : integer   := 0;

signal clock_400_rst        : std_logic := '1';
signal clock_300_rst        : std_logic := '1';
signal clock_322_rst        : std_logic := '1';

signal power_up_rst_clock_400   : std_logic_vector(31 downto 0) := c_ones_dword;
signal power_up_rst_clock_300   : std_logic_vector(31 downto 0) := c_ones_dword;
signal power_up_rst_clock_322   : std_logic_vector(31 downto 0) := c_ones_dword;

signal loop_generator           : integer := 0;
signal loops                    : integer := 0;

signal streaming_data           : std_logic_vector(511 downto 0);
signal streaming_data_wren      : std_logic;

signal streaming_data_LBUS      : std_logic_vector(511 downto 0);
signal streaming_data_S_AXI     : std_logic_vector(511 downto 0);

signal streaming_data_header    : std_logic_vector(511 downto 0);

signal rx_packet_size           : std_logic_vector(13 downto 0) := "00" & x"000";   -- MODULO 64!!
signal rx_enable_capture        : std_logic := '0';

signal lfaa_bank1_addr          : std_logic_vector(31 downto 0);
signal rx_soft_reset            : std_logic := '1';
signal rx_packets_to_capture    : std_logic_vector(31 downto 0);

signal m01_axi_wvalid           : std_logic;
signal m01_axi_wready           : std_logic;
signal valid_tracker            : integer := 0;

begin

clock_300 <= not clock_300 after 3.33 ns;
clock_322 <= not clock_322 after 3.11 ns;
clock_400 <= not clock_400 after 2.50 ns;


-------------------------------------------------------------------------------------------------------------
-- powerup resets for SIM
test_runner_proc_clk300: process(clock_300)
begin
    if rising_edge(clock_300) then
        -- power up reset logic
        if power_up_rst_clock_300(31) = '1' then
            power_up_rst_clock_300(31 downto 0) <= power_up_rst_clock_300(30 downto 0) & '0';
            clock_300_rst   <= '1';
            testCount_300   <= 0;
        else
            clock_300_rst   <= '0';
            testCount_300   <= testCount_300 + 1;
        end if;
    end if;
end process;

test_runner_proc_clk400: process(clock_400)
begin
    if rising_edge(clock_400) then
        -- power up reset logic
        if power_up_rst_clock_400(31) = '1' then
            power_up_rst_clock_400(31 downto 0) <= power_up_rst_clock_400(30 downto 0) & '0';
            testCount_400   <= 0;
            clock_400_rst   <= '1';
        else
            testCount_400   <= testCount_400 + 1;
            clock_400_rst   <= '0';
        end if;
    end if;
end process;

run_proc : process(clock_300)
begin
    if rising_edge(clock_300) then
        if clock_300_rst = '1' then
            loop_generator      <= 0;
            rx_enable_capture   <= '0';
            streaming_data      <= zero_512;
            streaming_data_wren <= '0';
            lfaa_bank1_addr     <= x"00000000";
            rx_soft_reset       <= '1';
            lfaa_bank1_addr     <= x"FFEFE000";
            loops               <= 0;
            valid_tracker       <= 0;
        else
            if loop_generator = 10 then
                if loops = 1 then
                    streaming_data      <= c_hex_a_512;
                elsif loops = 2 then    
                    streaming_data      <= c_hex_b_512;
                else
                    streaming_data      <= c_ones_512;
                end if;
                    
                streaming_data_wren <= '1';
            elsif loop_generator = 141 then
                streaming_data      <= zero_512;
                streaming_data_wren <= '0';
            end if;

            if loop_generator = 150 then
                loop_generator      <= 0;
                loops               <= loops + 1;
            elsif loops < 1880064 then
                loop_generator      <= loop_generator + 1;
            end if;

            if rx_soft_reset = '1' then
                rx_soft_reset <= '0';
            end if;
            
            rx_enable_capture       <= '1';
            rx_packets_to_capture   <= x"001CB000"; -- 1880064
            rx_packet_size          <= "01100100000000"; -- 6400
            --rx_packet_size        <= "01000000000000"; -- 4096
            lfaa_bank1_addr         <= x"00000000";

            --8306 1880064 = 131

            --- Test the response if HBM de-asserts ready mid transfer.
            if m01_axi_wvalid = '1' then
                valid_tracker <= valid_tracker + 1;
            end if;
            
            if valid_tracker = 62 then
                m01_axi_wready <= '0';
            else
                m01_axi_wready <= '1';
            end if;
        
        end if;
    end if;
end process;

DUT : entity HBM_PktController_lib.HBM_memory_write 
    generic map (
        g_DEBUG_ILAs             => FALSE,
        g_HBM_bank_size          => "4095MB"
    )
    port map (
        -- shared memory interface clock (300 MHz)
        i_shared_clk                    => clock_300,
        i_shared_rst                    => clock_300_rst,

        o_module_in_reset               => open,

        ------------------------------------------------------------------------------------
        -- Data from CMAC module after CDC in shared memory clock domain
        i_data_from_cmac                => streaming_data,
        i_data_valid_from_cmac          => streaming_data_wren,

        ------------------------------------------------------------------------------------
        -- config and status registers interface
        -- rx
        i_rx_packet_size                => rx_packet_size,
        i_rx_soft_reset                 => rx_soft_reset,
        i_enable_capture                => rx_enable_capture,

        i_lfaa_bank1_addr               => lfaa_bank1_addr,
        i_lfaa_bank2_addr               => x"00000000",
        i_lfaa_bank3_addr               => x"00000000",
        i_lfaa_bank4_addr               => x"00000000",
        update_start_addr               => '0',
        i_rx_bank_enable                => "0000",

        o_1st_4GB_rx_addr               => open,
        o_2nd_4GB_rx_addr               => open,
        o_3rd_4GB_rx_addr               => open,
        o_4th_4GB_rx_addr               => open,

        o_capture_done                  => open,
        o_num_packets_received          => open,
        i_rx_packets_to_capture         => rx_packets_to_capture,

        -----------------------------------------------------------------------
        -- HBM Write interfaces.

        -- first 4GB section of AXI
        -- aw bus
        m01_axi_awvalid   => open,
        m01_axi_awready   => '1',
        m01_axi_awaddr    => open,
        m01_axi_awlen     => open,
        --w bus
        m01_axi_wvalid    => m01_axi_wvalid,
        m01_axi_wdata     => open,
        m01_axi_wstrb     => open,
        m01_axi_wlast     => open,
        m01_axi_wready    => m01_axi_wready,

        --second 4GB section of AXI
        --aw bus
        m02_axi_awvalid   => open,
        m02_axi_awready   => '1',
        m02_axi_awaddr    => open,
        m02_axi_awlen     => open,
        --w bus
        m02_axi_wvalid    => open,
        m02_axi_wdata     => open,
        m02_axi_wstrb     => open,
        m02_axi_wlast     => open,
        m02_axi_wready    => '1',

        --third 4GB section of AXI
        --aw bus
        m03_axi_awvalid   => open,
        m03_axi_awready   => '1',
        m03_axi_awaddr    => open,
        m03_axi_awlen     => open,
        --w bus
        m03_axi_wvalid    => open,
        m03_axi_wdata     => open,
        m03_axi_wstrb     => open,
        m03_axi_wlast     => open,
        m03_axi_wready    => '1',

        --fourth 4GB section of AXI
        --aw bus
        m04_axi_awvalid   => open,
        m04_axi_awready   => '1',
        m04_axi_awaddr    => open,
        m04_axi_awlen     => open,
        --w bus
        m04_axi_wvalid    => open,
        m04_axi_wdata     => open,
        m04_axi_wstrb     => open,
        m04_axi_wlast     => open,
        m04_axi_wready    => '1'
    );

end Behavioral;
