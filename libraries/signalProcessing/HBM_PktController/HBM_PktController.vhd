----------------------------------------------------------------------------------
-- Company: CSIRO
-- Engineers: Giles Babich & Jonathan Li, jonathan.li@csiro.au
-- 
-- Create Date: 13.07.2022 
-- Module Name: HBM_PktController
-- Description: 
--      Get packets from data block interface and write them to the HBM through AXI  
--      Also read the packets from HBM through AXI, packet size is in bytes and flexible, 
--      can be any value >=64bytes and <=9000bytes, it covers the whole HBM range 
--      which is 16GB
----------------------------------------------------------------------------------
--  
--
--  Distributed under the terms of the CSIRO Open Source Software Licence Agreement
--  See the file LICENSE for more info.
----------------------------------------------------------------------------------
--
--  ------------------------------------------------------------------------------
--  Flow chart for packet rx:
--  
--  parameter in: i_rx_packet_size, i_enable_capture  
--    |
--  packet coming in--------------------------------------------------------------------------------------------------<<<----------
--    |                                                                                                                           |
--  every 4096 bytes of data written into the incoming data FIFO will be flushed to HBM                                           |
--    |                                                                                                                           |
--  check address range, if can    fit into current 4GB space, then issue AXI transactions to write to HBM memory                 |
--                       if cannot fit into current 4GB space, then calculate number of bytes fit to current and next             |
--                       4GB space, and issue corresponding AXI transactions for the residual of current 4GB and the              |
--                       next 4GB space------------------------------------------------------------------------------->>>---------i
--
--  -------------------------------------------------------------------------------
--  Flow chart for packet tx:
--
--  parameter in: i_tx_packet_size, i_expected_total_number_of_4k_axi, i_expected_number_beats_per_burst, i_expected_beats_per_packet     
--                i_expected_packets_per_burst, i_expected_total_number_of_bursts   
--    | 
--  i_start_rx = 1
--    |
--  i_expected_total_number_of_4k_axi number of AXI reading transactions will be issued to HBM based on 4096 bytes length, the HBM read
--  out data will be stored in a local fifo
--    |
--  state machine will then be triggered to reading from the local fifo and write data to packetiser
--
--  For AXI write part, it's an AW and W totally separated structure, AW might be finished earlier or later than the correspoding W depnding
--  on the awready signal from HBM. Using this structure is to support the packet with large packet size with minimum 4 clock cycles gap 
--  between packets continusly
--
--  -------------------------------------------------------------------------------
--
--HBM memory layout for RX 
--    Packet stored modulo 64 bytes, ie 6330 is rounded up to 6336 and those final 6 byte are discarded at the software level.
--    Timestamp is the next address, the lower 10 bytes are a timestamp with the remaining 70 for future meta data.
--    
--    Software dumps out the HBM buffer, based on the rx_packet_size, it will demux the memory space to correct packet size and append the timestamp.
--
--HBM memory layout for TX
--    Packets from a PCAP are packed into HBM modulo 64 bytes, discarding the rest of the vector when packet size is not modulo 64.
--    This is byte swapped and this matches the data format required by the CMAC hard ip.
--    
--
-- 
-------------------------------------------------------------------------------

library IEEE, common_lib, xpm, HBM_PktController_lib, Timeslave_CMAC_lib;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
library cnic_lib;
use cnic_lib.cnic_top_pkg.all;
USE Timeslave_CMAC_lib.timer_pkg.ALL;
USE common_lib.common_pkg.ALL;

library xpm;
use xpm.vcomponents.all;

library axi4_lib;
use axi4_lib.axi4_lite_pkg.all;
use axi4_lib.axi4_full_pkg.all;


entity HBM_PktController is
   generic (
      g_DEBUG_ILAs             : BOOLEAN := FALSE;
      g_DEBUG_HBM              : BOOLEAN := FALSE;
      g_HBM_bank_size          : string  := "4095MB"
   );
   Port (
      clk_freerun                     : in std_logic;
      -- shared memory interface clock (300 MHz)
      i_shared_clk                    : in std_logic;
      i_shared_rst                    : in std_logic;

      o_reset_packet_player           : out std_logic;
      ------------------------------------------------------------------------------------
      -- Data from CMAC module after CDC in shared memory clock domain
      i_data_from_cmac                : in  std_logic_vector(511 downto 0);
      i_data_valid_from_cmac          : in  std_logic;

      ------------------------------------------------------------------------------------
      -- config and status registers interface
      -- rx
      i_rx_packet_size                : in std_logic_vector(13 downto 0);   -- MODULO 64!!
      i_rx_soft_reset                 : in std_logic;
      i_enable_capture                : in std_logic;

      i_lfaa_bank1_addr               : in std_logic_vector(31 downto 0);
      i_lfaa_bank2_addr               : in std_logic_vector(31 downto 0);
      i_lfaa_bank3_addr               : in std_logic_vector(31 downto 0);
      i_lfaa_bank4_addr               : in std_logic_vector(31 downto 0);
      update_start_addr               : in std_logic; --pulse signal to update each 4GB bank start address
      i_rx_bank_enable                : in std_logic_vector(3 downto 0);

      o_1st_4GB_rx_addr               : out std_logic_vector(31 downto 0);
      o_2nd_4GB_rx_addr               : out std_logic_vector(31 downto 0);
      o_3rd_4GB_rx_addr               : out std_logic_vector(31 downto 0);
      o_4th_4GB_rx_addr               : out std_logic_vector(31 downto 0);

      o_capture_done                  : out std_logic;
      o_num_packets_received          : out std_logic_vector(31 downto 0);
      i_rx_packets_to_capture         : in std_logic_vector(31 downto 0);
      i_rx_flush_to_hbm               : in std_logic;

      -- tx
      i_tx_packet_size                    : in std_logic_vector(13 downto 0);
      i_start_tx                          : in std_logic;
      i_reset_tx                          : in std_logic;
   
      i_loop_tx                           : in std_logic; 
      i_expected_total_number_of_4k_axi   : in std_logic_vector(31 downto 0);
      i_expected_number_beats_per_burst   : in std_logic_vector(12 downto 0);
      i_expected_beats_per_packet         : in std_logic_vector(31 downto 0);
      i_expected_packets_per_burst        : in std_logic_vector(31 downto 0);
      i_expected_total_number_of_bursts   : in std_logic_vector(31 downto 0);
      i_expected_number_of_loops          : in std_logic_vector(31 downto 0);
      i_time_between_bursts_ns            : in std_logic_vector(31 downto 0);

      i_readaddr                          : in std_logic_vector(31 downto 0); 
      i_update_readaddr                   : in std_logic;

      i_enable_duplex                     : in std_logic_vector(1 downto 0);

      o_tx_addr                           : out std_logic_vector(31 downto 0);
      o_tx_boundary_across_num            : out std_logic_vector(1  downto 0);
      o_axi_rvalid_but_fifo_full          : out std_logic;
      
      o_tx_complete                       : out std_logic;
      o_tx_packets_to_mac                 : out std_logic_vector(63 downto 0);
      o_tx_packet_count                   : out std_logic_vector(63 downto 0);

      i_legacy_rate_sel                   : in std_logic;
   ------------------------------------------------------------------------------------
      -- debug ports
      o_rd_fsm_debug                      : out std_logic_vector(3 downto 0);
      o_output_fsm_debug                  : out std_logic_vector(3 downto 0);
      o_input_fsm_debug                   : out std_logic_vector(3 downto 0);
         
------------------------------------------------------------------------------------
      -- Data output, to the packetizer
      -- Add the packetizer records here
      o_packetiser_data_in_wr          : out std_logic;
      o_packetiser_data                : out std_logic_vector(511 downto 0);
      o_packetiser_bytes_to_transmit   : out std_logic_vector(13 downto 0);
      i_packetiser_data_to_player_rdy  : in  std_logic;

      -----------------------------------------------------------------------
      i_schedule_action                : in std_logic_vector(7 downto 0);

      -- Timing signals
      o_timer_fields_in                : out timer_fields_in;
      i_timer_fields_out               : in timer_fields_out;
      
      i_current_time                   : in std_logic_vector(79 downto 0);
      i_use_timestamp_to_tx            : in std_logic;

      -----------------------------------------------------------------------

      -----------------------------------------------------------------------
      --first 4GB section of AXI
      --aw bus
      m01_axi_awvalid   : out std_logic := '0';
      m01_axi_awready   : in  std_logic;
      m01_axi_awaddr    : out std_logic_vector(31 downto 0) := (others => '0');
      m01_axi_awlen     : out std_logic_vector(7 downto 0)  := (others => '0');
      --w bus
      m01_axi_wvalid    : out std_logic := '0';
      m01_axi_wdata     : out std_logic_vector(511 downto 0) := (others => '0');
      m01_axi_wstrb     : out std_logic_vector(63  downto 0) := (others => '1');
      m01_axi_wlast     : out std_logic := '0';
      m01_axi_wready    : in  std_logic;

      -- ar bus - read address
      m01_axi_arvalid   : out std_logic;
      m01_axi_arready   : in  std_logic;
      m01_axi_araddr    : out std_logic_vector(31 downto 0);
      m01_axi_arlen     : out std_logic_vector(7 downto 0);
      -- r bus - read data
      m01_axi_rvalid    : in  std_logic;
      m01_axi_rready    : out std_logic;
      m01_axi_rdata     : in  std_logic_vector(511 downto 0);
      m01_axi_rlast     : in  std_logic;
      m01_axi_rresp     : in  std_logic_vector(1 downto 0);

      --second 4GB section of AXI
      --aw bus
      m02_axi_awvalid   : out std_logic := '0';
      m02_axi_awready   : in  std_logic;
      m02_axi_awaddr    : out std_logic_vector(31 downto 0) := (others => '0');
      m02_axi_awlen     : out std_logic_vector(7 downto 0)  := (others => '0');
      --w bus
      m02_axi_wvalid    : out std_logic := '0';
      m02_axi_wdata     : out std_logic_vector(511 downto 0) := (others => '0');
      m02_axi_wstrb     : out std_logic_vector(63  downto 0) := (others => '1');
      m02_axi_wlast     : out std_logic := '0';
      m02_axi_wready    : in  std_logic;

      -- ar bus - read address
      m02_axi_arvalid   : out std_logic;
      m02_axi_arready   : in  std_logic;
      m02_axi_araddr    : out std_logic_vector(31 downto 0);
      m02_axi_arlen     : out std_logic_vector(7 downto 0);
      -- r bus - read data
      m02_axi_rvalid    : in  std_logic;
      m02_axi_rready    : out std_logic;
      m02_axi_rdata     : in  std_logic_vector(511 downto 0);
      m02_axi_rlast     : in  std_logic;
      m02_axi_rresp     : in  std_logic_vector(1 downto 0);

      --third 4GB section of AXI
      --aw bus
      m03_axi_awvalid   : out std_logic := '0';
      m03_axi_awready   : in  std_logic;
      m03_axi_awaddr    : out std_logic_vector(31 downto 0) := (others => '0');
      m03_axi_awlen     : out std_logic_vector(7 downto 0)  := (others => '0');
      --w bus
      m03_axi_wvalid    : out std_logic := '0';
      m03_axi_wdata     : out std_logic_vector(511 downto 0) := (others => '0');
      m03_axi_wstrb     : out std_logic_vector(63  downto 0) := (others => '1');
      m03_axi_wlast     : out std_logic := '0';
      m03_axi_wready    : in  std_logic;

      -- ar bus - read address
      m03_axi_arvalid   : out std_logic;
      m03_axi_arready   : in  std_logic;
      m03_axi_araddr    : out std_logic_vector(31 downto 0);
      m03_axi_arlen     : out std_logic_vector(7 downto 0);
      -- r bus - read data
      m03_axi_rvalid    : in  std_logic;
      m03_axi_rready    : out std_logic;
      m03_axi_rdata     : in  std_logic_vector(511 downto 0);
      m03_axi_rlast     : in  std_logic;
      m03_axi_rresp     : in  std_logic_vector(1 downto 0);

      --fourth 4GB section of AXI
      --aw bus
      m04_axi_awvalid   : out  std_logic := '0';
      m04_axi_awready   : in   std_logic;
      m04_axi_awaddr    : out  std_logic_vector(31 downto 0) := (others => '0');
      m04_axi_awlen     : out  std_logic_vector(7 downto 0)  := (others => '0');
      --w bus
      m04_axi_wvalid    : out std_logic := '0';
      m04_axi_wdata     : out std_logic_vector(511 downto 0) := (others => '0');
      m04_axi_wstrb     : out std_logic_vector(63  downto 0) := (others => '1');
      m04_axi_wlast     : out std_logic := '0';
      m04_axi_wready    : in  std_logic;

      -- ar bus - read address
      m04_axi_arvalid   : out std_logic;
      m04_axi_arready   : in  std_logic;
      m04_axi_araddr    : out std_logic_vector(31 downto 0);
      m04_axi_arlen     : out std_logic_vector(7 downto 0);
      -- r bus - read data
      m04_axi_rvalid    : in  std_logic;
      m04_axi_rready    : out std_logic;
      m04_axi_rdata     : in  std_logic_vector(511 downto 0);
      m04_axi_rlast     : in  std_logic;
      m04_axi_rresp     : in  std_logic_vector(1 downto 0)
   );
    
    -- prevent optimisation 
    attribute keep_hierarchy : string;
    attribute keep_hierarchy of HBM_PktController : entity is "yes";
    
    
end HBM_PktController;

architecture RTL of HBM_PktController is
   
   constant max_space_4095MB          : unsigned(31 downto 0) := X"FFF00000";
   constant max_space_4095MB_4k_num   : unsigned(21 downto 0) := "00"&X"FFF00";

   constant max_space_2048MB          : unsigned(31 downto 0) := X"80000000";
   constant max_space_2048MB_4k_num   : unsigned(21 downto 0) := "00"&X"80000";

   COMPONENT ila_0
   PORT (
      clk : IN STD_LOGIC;
      probe0 : IN STD_LOGIC_VECTOR(191 DOWNTO 0));
   END COMPONENT;

   ---------------------------
   -- packet RX related signals
   ---------------------------

   signal rx_soft_reset_int                              : std_logic := '0';
   signal enable_capture_int                             : std_logic := '0';

   ---------------------------
   -- packet TX related signals
   ---------------------------

   -- NEEDS TO BE AT LEAST 4K deep to handle the HBM requests when there is slow playout on the 100G.
   constant SYNC_FIFO_DEPTH : integer := 4096;

   signal o_axi_arvalid :  std_logic;
   signal i_axi_arready :  std_logic;
   signal o_axi_araddr  :  std_logic_vector(31 downto 0);
   signal o_axi_arlen   :  std_logic_vector(7 downto 0);

   signal i_axi_rvalid  :  std_logic;
   signal o_axi_rready  :  std_logic;
   signal i_axi_rdata   :  std_logic_vector(511 downto 0);
   signal i_axi_rlast   :  std_logic;
   signal i_axi_rresp   :  std_logic_vector(1 downto 0);
   
   signal running         : std_logic := '0';
   signal preload         : std_logic := '0';
   signal tx_complete     : std_logic := '0';
   signal axi_4k_finished : std_logic := '0';

   type rd_fsm_type is (idle, wait_fifo_reset, wait_arready, rd_4064b, wait_fifo ,finished, loopit, wait_current_bank_finish);
   signal rd_fsm : rd_fsm_type := idle;

   type output_fsm_type is (initial_state, output_first_run0, output_first_run1 ,output_first_idle, output_idle,                                                  -- INIT
                           read_meta_data_for_playout, calc_meta_data_for_playout,
                           output_next_burst, output_next_packet, output_wait_burst_counter, read_full_packet, output_packet_finished, output_check_burst_count,  -- WORKER section
                           output_tx_complete, output_loopit, output_thats_all_folks);                                                                            -- EXIT or loop
   signal output_fsm       : output_fsm_type := initial_state;
   
   signal n_fifo_cache_in_reset : std_logic;
   signal packetizer_wr    : std_logic;
   signal packetizer_dout  :  std_logic_vector(511 downto 0);
   
   signal readaddr, readaddr_reg   : unsigned(31 downto 0);    -- 30 bits = 1GB, 33 bits = 8GB
    
   signal total_beat_count                : unsigned(31 downto 0) := (others => '0');
   signal current_axi_4k_count            : unsigned(31 downto 0);
   signal wait_counter                    : unsigned(7 downto 0);
   signal current_pkt_count               : unsigned(63 downto 0) := (others=>'0');
   signal fpga_beat_in_burst_counter      : unsigned(31 downto 0) := (others=>'0');
   
   signal fpga_axi_beats_per_packet       : unsigned(7 downto 0) := (others=>'0');
   signal beats                           : unsigned(7 downto 0);
   signal number_of_burst_beats           : unsigned(15 downto 0) := (others=>'0');
   signal total_number_of_512b            : unsigned(31 downto 0) := (others=>'0');
   signal beat_count                      : unsigned(31 downto 0) := (others=> '0');
   signal end_of_packet                   : std_logic;
   signal start_of_packet                 : std_logic;
   signal start_stop_tx                   : std_logic;
   signal wait_fifo_resetting             : std_logic;
   signal bytes_in_current_packet         : std_logic_vector(13 downto 0);
   signal beats_per_packet                : std_logic_vector(13 downto 0);
   signal odd_beats_per_packet            : std_logic_vector(13 downto 0);

   signal FIFO_dout                       : std_logic_vector(511 downto 0);     
   signal FIFO_empty                      : std_logic; 
   signal FIFO_full                       : std_logic;      
   signal FIFO_prog_full                  : std_logic;
   signal FIFO_almost_full                : std_logic;     
   signal FIFO_RdDataCount                : std_logic_vector(((ceil_log2(SYNC_FIFO_DEPTH))) downto 0);
   signal FIFO_WrDataCount                : std_logic_vector(((ceil_log2(SYNC_FIFO_DEPTH))) downto 0);
   signal FIFO_din                        : std_logic_vector(511 downto 0);      
   signal tx_FIFO_rd_en                   : std_logic;  
   signal packetiser_data_wr_int          : std_logic;
   signal FIFO_rst                        : std_logic;    
   signal tx_fifo_cache_reset             : std_logic;    
   signal tx_FIFO_wr_en                   : std_logic;     
   signal tx_reset_state                  : std_logic;

   
   signal ns_burst_timer_100Mhz           : unsigned(31 downto 0) := (others=>'0');
   signal ns_total_time_100Mhz            : unsigned(47 downto 0) := (others=>'0');

   signal target_time_100Mhz              : unsigned(47 downto 0) := (others=>'0');
   signal target_time                     : unsigned(47 downto 0) := (others=>'0');
   signal time_between_packets_100Mhz     : unsigned(47 downto 0) := (others=>'0');
   signal target_packets_100Mhz           : unsigned(63 downto 0) := (others=>'0');
   signal target_packets                  : unsigned(63 downto 0) := (others=>'0');
   signal target_packets_std_logic        : std_logic_vector(63 downto 0) := (others=>'0');

   signal ptp_target_packets              : unsigned(63 downto 0) := (others=>'0');
   signal target_packets_inuse            : unsigned(63 downto 0) := (others=>'0');

   signal time_between_bursts_ns_100Mhz   : std_logic_vector(31 downto 0) := (others=>'0');
   signal run_timing, run_timing_100Mhz   : std_logic;
   signal reset_state_100Mhz              : std_logic;
   signal start_next_burst                : std_logic;
   signal start_next_burst_ila            : std_logic;
   
   signal fpga_pkt_count_in_this_burst    : unsigned(31 downto 0);
   signal burst_count                     : unsigned(31 downto 0) := (others=>'0');
   
   signal looping                         : std_logic := '0';
   signal loop_cnt                        : unsigned(31 downto 0) := (others=>'0');
   signal start_next_loop                 : std_logic := '0';
   signal wait_fifo_reset_cnt             : unsigned(31 downto 0) := (others=>'0');
   signal rd_rst_busy ,wr_rst_busy        : std_logic := '0';
   signal axi_r_num                       : unsigned(31 downto 0) := (others => '0');
   signal clear_axi_r_num                 : std_logic := '0';

   signal rd_fsm_debug                    : std_logic_vector(3 downto 0);
   signal output_fsm_debug                : std_logic_vector(3 downto 0);

   signal total_pkts_to_mac               : unsigned(63 downto 0) := (others=>'0');
   signal reset_cnt                       : unsigned(3 downto 0);

   signal num_residual_bytes_after_4k, num_residual_bytes_after_4k_curr_4G, num_residual_bytes_after_4k_next_4G : unsigned(13 downto 0);
   signal first_time, compare_vectors, vectors_equal, vectors_not_equal, o_packetiser_data_in_wr_prev : std_logic;
   
   signal boundary_across_num             : unsigned(1 downto 0) := "00";

   signal axi_wdata_fifo_full             : std_logic;
   signal awfifo_valid_del                : std_logic := '0';
   signal awfifo_valid_rising_edge        : std_logic;

   signal awfifo_cnt                      : unsigned(2 downto 0) := (others => '0');
   signal awfifo_cnt_en                   : std_logic := '0';
   signal awfifo_wren_del1, awfifo_wren_del2, awfifo_wren_del3, awfifo_wren_del4, awfifo_wren_del5, awfifo_wren_del6, awfifo_wren_del7 : std_logic := '0';
   signal axi_wdata_fifo_empty            : std_logic;
   signal axi_wdata_fifo_empty_falling_edge : std_logic;
   signal axi_wdata_fifo_empty_reg        : std_logic := '0';
   signal axi_wlast_asserted              : std_logic := '0';
   signal size_64B                        : std_logic;
   signal first_awfifo_wren, first_awfifo_wren_del   : std_logic := '0';
   signal first_awfifo_wren_falling       : std_logic;

   signal buf_num_of_4k_current           : unsigned(21 downto 0);
   signal HBM_writer_module_in_reset      : std_logic;

   signal max_space                       : unsigned(31 downto 0) := X"FFF00000";
   signal max_space_4k_num                : unsigned(21 downto 0) := "00"&X"FFF00";

   signal hbm_tx_bank_start               : unsigned(1 downto 0) := "00";

   signal packetiser_data_to_player_rdy   : std_logic;

   signal current_time                    : std_logic_vector(79 downto 0);
   signal current_timestamp_from_HBM      : std_logic_vector(79 downto 0);
   signal last_timestamp                  : std_logic_vector(47 downto 0);
   signal play_timestamp                  : std_logic_vector(39 downto 0);
   signal time_from_HBM_firstpass         : std_logic;

   signal last_timestamp_secs             : unsigned(47 downto 0);
   signal current_timestamp_secs          : unsigned(47 downto 0);
   signal seconds_diff                    : unsigned(47 downto 0);
   signal seconds_to_offset               : unsigned(7 downto 0);

   signal timestamp_loaded                : std_logic;
   signal timestamp_play                  : std_logic;
   signal use_timestamp_to_tx             : std_logic;

   signal timestamp_trigger               : std_logic;
   type ts_fsm_type is (load_init, idle, calc_1, calc_2, calc_3, create_goal_time, wait_for_goal, trigger, complete);
   signal ts_fsm : ts_fsm_type := idle;

   signal ts_fsm_debug                    : std_logic_vector(3 downto 0);

begin
   ---------------------------------------------------------------------------------------------------
   -- MAP internal signals to ports.
   o_tx_addr                   <= std_logic_vector(readaddr);
   o_tx_boundary_across_num    <= std_logic_vector(boundary_across_num);
   o_tx_complete               <= tx_complete;
   o_tx_packets_to_mac         <= std_logic_vector(total_pkts_to_mac);
   o_tx_packet_count           <= std_logic_vector(current_pkt_count);
   
   o_rd_fsm_debug              <= rd_fsm_debug;
   o_output_fsm_debug          <= output_fsm_debug;

   ---------------------------------------------------------------------------------------------------
   --HBM AXI write transaction part, it is assumed that the recevied packet is always multiple of 64B,
   --i.e no residual AXI trans where less then 64B trans is needed, all the bits of imcoming data is 
   --    valid
   ---------------------------------------------------------------------------------------------------

   --HBM bank boundary cross condition logic, due to the fact that XRT cannot allocate 4GB size of HBM buffer, so
   --need to support the size of HBM bank to be any size, currently 4095MB is by default, and still 4096MB is second
   --choice
   g_4095MB_condition : if g_HBM_bank_size = "4095MB" generate
      max_space         <= max_space_4095MB;
      max_space_4k_num  <= max_space_4095MB_4k_num;
   end generate g_4095MB_condition;					      

   g_U50_condition : if g_HBM_bank_size = "2048MB" generate
      max_space         <= max_space_2048MB;
      max_space_4k_num  <= max_space_2048MB_4k_num;
   end generate g_U50_condition;

   ----------------------------------------------------------------------
   -- duplex HBM control
   duplex_proc : process(i_shared_clk)
   begin
      if rising_edge(i_shared_clk) then
         if i_shared_rst = '1' then
            hbm_tx_bank_start <= "00";
         else
--               0 - Card will either TX or RX for the full HBM, 
--               1 - Card will assign the first 2 buffers to RX and the last 2 to TX,
--               2 - Card will assign the first 1 buffers to RX and the last 3 to TX,
--               3 - Card will assign the first 3 buffers to RX and the last 1 to TX.
               if i_enable_duplex = "00" then
                  hbm_tx_bank_start <= "00";
               elsif i_enable_duplex = "01" then
                  hbm_tx_bank_start <= "10";
               elsif i_enable_duplex = "10" then
                  hbm_tx_bank_start <= "01";
               else
                  hbm_tx_bank_start <= "11";
               end if;
         end if;
      end if;
   end process;

   --------------------------------------------------------------------------
   -- HBM RX module
   i_HBM_writer : entity HBM_PktController_lib.HBM_memory_write 
      generic map (
          g_DEBUG_ILAs             => g_DEBUG_ILAs,
          g_HBM_bank_size          => g_HBM_bank_size
      )
      port map (
         -- shared memory interface clock (300 MHz)
         i_shared_clk                    => i_shared_clk,
         i_shared_rst                    => i_shared_rst,

         o_module_in_reset               => HBM_writer_module_in_reset,

         ------------------------------------------------------------------------------------
         -- Data from CMAC module after CDC in shared memory clock domain
         i_data_from_cmac                => i_data_from_cmac,
         i_data_valid_from_cmac          => i_data_valid_from_cmac,

         ------------------------------------------------------------------------------------
         -- config and status registers interface
         -- rx
         i_rx_packet_size                => i_rx_packet_size,
         i_rx_soft_reset                 => i_rx_soft_reset,
         i_enable_capture                => enable_capture_int, --i_enable_capture,

         i_lfaa_bank1_addr               => i_lfaa_bank1_addr,
         i_lfaa_bank2_addr               => i_lfaa_bank2_addr,
         i_lfaa_bank3_addr               => i_lfaa_bank3_addr,
         i_lfaa_bank4_addr               => i_lfaa_bank4_addr,
         update_start_addr               => update_start_addr,
         i_rx_bank_enable                => i_rx_bank_enable,

         o_1st_4GB_rx_addr               => o_1st_4GB_rx_addr,
         o_2nd_4GB_rx_addr               => o_2nd_4GB_rx_addr,
         o_3rd_4GB_rx_addr               => o_3rd_4GB_rx_addr,
         o_4th_4GB_rx_addr               => o_4th_4GB_rx_addr,

         o_capture_done                  => o_capture_done,
         o_num_packets_received          => o_num_packets_received,

         i_rx_packets_to_capture         => i_rx_packets_to_capture,

         i_rx_flush_to_hbm               => i_rx_flush_to_hbm,

         o_input_fsm_debug               => o_input_fsm_debug,

         i_enable_duplex                 => i_enable_duplex,
         -----------------------------------------------------------------------
         i_schedule_action               => i_schedule_action,
         -----------------------------------------------------------------------
         -- HBM Write interfaces.

         -- first 4GB section of AXI
         -- aw bus
         m01_axi_awvalid   => m01_axi_awvalid,
         m01_axi_awready   => m01_axi_awready,
         m01_axi_awaddr    => m01_axi_awaddr,
         m01_axi_awlen     => m01_axi_awlen,
         --w bus
         m01_axi_wvalid    => m01_axi_wvalid,
         m01_axi_wdata     => m01_axi_wdata,
         m01_axi_wstrb     => m01_axi_wstrb,
         m01_axi_wlast     => m01_axi_wlast,
         m01_axi_wready    => m01_axi_wready,

         --second 4GB section of AXI
         --aw bus
         m02_axi_awvalid   => m02_axi_awvalid,
         m02_axi_awready   => m02_axi_awready,
         m02_axi_awaddr    => m02_axi_awaddr,
         m02_axi_awlen     => m02_axi_awlen,
         --w bus
         m02_axi_wvalid    => m02_axi_wvalid,
         m02_axi_wdata     => m02_axi_wdata,
         m02_axi_wstrb     => m02_axi_wstrb,
         m02_axi_wlast     => m02_axi_wlast,
         m02_axi_wready    => m02_axi_wready,

         --third 4GB section of AXI
         --aw bus
         m03_axi_awvalid   => m03_axi_awvalid,
         m03_axi_awready   => m03_axi_awready,
         m03_axi_awaddr    => m03_axi_awaddr,
         m03_axi_awlen     => m03_axi_awlen,
         --w bus
         m03_axi_wvalid    => m03_axi_wvalid,
         m03_axi_wdata     => m03_axi_wdata,
         m03_axi_wstrb     => m03_axi_wstrb,
         m03_axi_wlast     => m03_axi_wlast,
         m03_axi_wready    => m03_axi_wready,

         --fourth 4GB section of AXI
         --aw bus
         m04_axi_awvalid   => m04_axi_awvalid,
         m04_axi_awready   => m04_axi_awready,
         m04_axi_awaddr    => m04_axi_awaddr,
         m04_axi_awlen     => m04_axi_awlen,
         --w bus
         m04_axi_wvalid    => m04_axi_wvalid,
         m04_axi_wdata     => m04_axi_wdata,
         m04_axi_wstrb     => m04_axi_wstrb,
         m04_axi_wlast     => m04_axi_wlast,
         m04_axi_wready    => m04_axi_wready
      );

   --////////////////////////////////////////////////////////////////////////////////////////////////////
   ------------------------------------------------------------------------------------------------------
   -- Reset mechanisms
   ------------------------------------------------------------------------------------------------------
   --////////////////////////////////////////////////////////////////////////////////////////////////////

   tx_fifo_cache_reset  <= tx_reset_state OR FIFO_rst;

   reset_proc : process(i_shared_clk)
   begin
      if rising_edge(i_shared_clk) then
      
      -- master TX reset mechanism
      tx_reset_state     <= i_reset_tx OR i_shared_rst; -- OR i_schedule_action(0);
      
      -- master RX reset mechanism
      rx_soft_reset_int  <= i_rx_soft_reset OR i_shared_rst; -- OR i_schedule_action(0);
         
      end if;
   end process;
    
   --////////////////////////////////////////////////////////////////////////////////////////////////////
   ------------------------------------------------------------------------------------------------------
   -- Enable mechanisms
   ------------------------------------------------------------------------------------------------------
   --////////////////////////////////////////////////////////////////////////////////////////////////////
    
    n_fifo_cache_in_reset <= '1' when (rd_rst_busy = '0' and wr_rst_busy = '0') else '0';

    process(i_shared_clk)
    begin
        if rising_edge(i_shared_clk) then
        -- TX
            start_stop_tx        <= (i_start_tx OR (i_schedule_action(1) XOR i_schedule_action(2))) AND n_fifo_cache_in_reset;
    
            running              <= start_stop_tx;   -- Used as a reset for packet timing logic.
            -- if (start_stop_tx = '1') then
            --     running <= '1';
            -- else
            --     running <= '0';
            -- end if;
            
        -- RX
            enable_capture_int   <= i_enable_capture OR (i_schedule_action(3) XOR i_schedule_action(4));
        end if;
    end process;
    
   --////////////////////////////////////////////////////////////////////////////////////////////////////
   ------------------------------------------------------------------------------------------------------
   --HBM AXI read transaction part
   ------------------------------------------------------------------------------------------------------
   --////////////////////////////////////////////////////////////////////////////////////////////////////
    
cache_sel_inc_axi_data : process(i_shared_clk)
begin
    if rising_edge(i_shared_clk) then
        i_axi_rdata <=      m01_axi_rdata   when boundary_across_num = 0 else
                            m02_axi_rdata   when boundary_across_num = 1 else
                            m03_axi_rdata   when boundary_across_num = 2 else
                            m04_axi_rdata;
                            
        i_axi_rvalid <=     (m01_axi_rvalid and o_axi_rready) when boundary_across_num = 0 else
                            (m02_axi_rvalid and o_axi_rready) when boundary_across_num = 1 else
                            (m03_axi_rvalid and o_axi_rready) when boundary_across_num = 2 else
                            (m04_axi_rvalid and o_axi_rready);
                        
    end if;
end process;

------------------------------------------------------------------------------------------------------

    i_axi_arready <=    m01_axi_arready when boundary_across_num = 0 else
                        m02_axi_arready when boundary_across_num = 1 else
                        m03_axi_arready when boundary_across_num = 2 else
                        m04_axi_arready;
-- NOT USED ----        
--        i_axi_rlast <=      m01_axi_rlast   when boundary_across_num = 0 else
--                            m02_axi_rlast   when boundary_across_num = 1 else
--                            m03_axi_rlast   when boundary_across_num = 2 else
--                            m04_axi_rlast;
        
--        i_axi_rresp <=      m01_axi_rresp   when boundary_across_num = 0 else
--                            m02_axi_rresp   when boundary_across_num = 1 else
--                            m03_axi_rresp   when boundary_across_num = 2 else
--                            m04_axi_rresp;
-- NOT USED ----
        
    

------------------------------------------------------------------------------------------------------

    -- ar bus - read address
    m01_axi_arvalid   <= o_axi_arvalid   when boundary_across_num = 0 else '0';
    m01_axi_araddr    <= o_axi_araddr;--    when boundary_across_num = 0 else (others => '0');
    m01_axi_arlen     <= o_axi_arlen;--     when boundary_across_num = 0 else (others => '0');
    -- r bus - read data
    m01_axi_rready    <= o_axi_rready;--    when boundary_across_num = 0 else '0';

    -- ar bus - read address
    m02_axi_arvalid   <= o_axi_arvalid   when boundary_across_num = 1 else '0';
    m02_axi_araddr    <= o_axi_araddr;--    when boundary_across_num = 1 else (others => '0');
    m02_axi_arlen     <= o_axi_arlen;--     when boundary_across_num = 1 else (others => '0');
    -- r bus - read data
    m02_axi_rready    <= o_axi_rready;--    when boundary_across_num = 1 else '0';

    -- ar bus - read address
    m03_axi_arvalid   <= o_axi_arvalid   when boundary_across_num = 2 else '0';
    m03_axi_araddr    <= o_axi_araddr;--    when boundary_across_num = 2 else (others => '0');
    m03_axi_arlen     <= o_axi_arlen;--     when boundary_across_num = 2 else (others => '0');
    -- r bus - read data
    m03_axi_rready    <= o_axi_rready;--    when boundary_across_num = 2 else '0';

    -- ar bus - read address
    m04_axi_arvalid   <= o_axi_arvalid   when boundary_across_num = 3 else '0';
    m04_axi_araddr    <= o_axi_araddr;--    when boundary_across_num = 3 else (others => '0');
    m04_axi_arlen     <= o_axi_arlen;--     when boundary_across_num = 3 else (others => '0');
    -- r bus - read data
    m04_axi_rready    <= o_axi_rready;--    when boundary_across_num = 3 else '0';

    o_axi_rready      <= '1';
    -- Read in 512 bit aligned 4k words
    o_axi_arlen(7 downto 0) <= x"3F"; -- Read 64 beats x 512 bits = 4096B 
    o_axi_araddr <= std_logic_vector(readaddr);

    process(i_shared_clk)
    begin	    
      if rising_edge(i_shared_clk) then
         if (clear_axi_r_num = '1') then
            axi_r_num <= (others => '0');
         else	    
            if (m01_axi_rvalid = '1' and m01_axi_rlast = '1' and m01_axi_rready = '1') or 
	            (m02_axi_rvalid = '1' and m02_axi_rlast = '1' and m02_axi_rready = '1') or
	            (m03_axi_rvalid = '1' and m03_axi_rlast = '1' and m03_axi_rready = '1') or 
	            (m04_axi_rvalid = '1' and m04_axi_rlast = '1' and m04_axi_rready = '1') then
               axi_r_num <= axi_r_num + 1;
            end if;
         end if;
      end if;
    end process;

-- SM comes out of reset and begins to retrieve data from HBM
-- Effectively preloading the FIFOs so that when a PTP trigger comes, there is an instant response.
-- This will remove a variable response which is retreive data from HBM and preload the FIFO.
   process(i_shared_clk)
   begin
      if rising_edge(i_shared_clk) then
         o_axi_arvalid <= '0';  
         axi_4k_finished <= '0';
         
         if (tx_reset_state = '1') then
            rd_fsm               <= idle;
            preload              <= '1';
            clear_axi_r_num      <= '1';
            rd_fsm_debug         <= x"F";
         else
            case rd_fsm is
               when idle =>
                  clear_axi_r_num      <= '0';
                  rd_fsm_debug         <= x"0";
                  boundary_across_num  <= hbm_tx_bank_start; --(others => '0');
                  readaddr_reg         <= (others => '0');
                  
                  if i_update_readaddr = '1' then
                     readaddr          <= unsigned(i_readaddr);
                  else
                     readaddr          <= x"00000000"; 
                  end if;		
                  
                  current_axi_4k_count <= (others =>'0');
                  rd_fsm               <= wait_fifo_reset;

               when wait_fifo_reset =>
                  rd_fsm_debug <= x"1";

                  -- buffer tracking
                  -- each buffer is 4095MB of a possible 4096MB due to XRT limitations. 
                  -- The design retrieves from HBM in lots of 4096 (4K), therefore each buffer has 0xFFF00 transactions.
                  -- This loop is only interested in retrieving in lots of 4096 up to the desired number
                  -- Which is stipulated by the software via - i_expected_total_number_of_4k_axi

                  buf_num_of_4k_current <= max_space_4k_num;
                  
                  if (start_next_loop = '1' OR (preload = '1')) then
                     o_axi_arvalid   <= '1';
                     readaddr_reg    <= readaddr;
                     rd_fsm          <= wait_arready;
                     preload         <= '0';
                  end if;

               when wait_arready =>  -- arvalid is high in this state, wait until arready is high so the transaction is complete.--o_axi_arvalid <= '0';
                  rd_fsm_debug        <= x"2";
                  o_axi_arvalid       <= '1';
                  if i_axi_arready = '1' then
                     o_axi_arvalid        <= '0';
                     readaddr             <= readaddr + 4096;
                     current_axi_4k_count <= current_axi_4k_count + 1;
                     buf_num_of_4k_current <= buf_num_of_4k_current - 1;

                     -- --when all the AXI AR 4k transactions for current 4GB has been transfered, then wait for all the pending AXi R transactions finish
                     -- three conditions to evaluate.
                     -- 1. Are we waiting for the current 4GB buffer to finish, ie have we requested the last transfer? if so go to wait_current_bank_finish
                     -- 2. Have we retrieved the total number of 4K transfers required by software?
                     -- 3. Check RX buffer FIFO has room and loop thru.   

                     if (current_axi_4k_count = (unsigned(i_expected_total_number_of_4k_axi))) then 
                        rd_fsm <= finished; 
                     elsif buf_num_of_4k_current = 1 then
                        rd_fsm <= wait_current_bank_finish;
                     else
                        rd_fsm <= wait_fifo;
                     end if;
                  end if;
         
               when wait_current_bank_finish =>
                  rd_fsm_debug <= x"6";	   
                     o_axi_arvalid  <= '0';
                  if (axi_r_num = current_axi_4k_count) then
                     rd_fsm         <= wait_arready;
                     o_axi_arvalid  <= '1';
                     if (readaddr = max_space and boundary_across_num /= 3) then
                        readaddr                <= X"00000000";
                        boundary_across_num     <= boundary_across_num + 1;
                        buf_num_of_4k_current   <= max_space_4k_num;
                     end if;
                  end if;		

               when wait_fifo => --issued read request on bus and now drive arvalid low
                  rd_fsm_debug <= x"3";
                  o_axi_arvalid <= '0';
                  if (FIFO_prog_full = '1') then  -- request in lots of 64.
                     rd_fsm <= wait_fifo;
                  else
                     rd_fsm <= wait_arready;
                     o_axi_arvalid <= '1';
                  end if;

               when finished =>
                  rd_fsm_debug <= x"4";
                  
                  if (i_loop_tx = '1') then
                     rd_fsm          <= loopit;
                  else
                     clear_axi_r_num <= '1';    
                     rd_fsm          <= finished;
                  end if;

               when loopit =>
                  -- wait for pending returns before clearing to restart.
                  if (axi_r_num = current_axi_4k_count) then
                     rd_fsm            <= idle;
                     clear_axi_r_num   <= '1';     
                  end if;
                  rd_fsm_debug      <= x"5";
                  axi_4k_finished   <= '1';

               when others =>
                  rd_fsm <= idle;
            end case; 
         end if;
      end if;
    end process;
    
    --------------------------------------------------------------------------------------------
    -- Capture the memory read's of the 512bit data comming back on the AXI bus
    --------------------------------------------------------------------------------------------
    process(i_shared_clk)
    begin
      if rising_edge(i_shared_clk) then
         tx_FIFO_wr_en <= '0';
         if (i_axi_rvalid = '1') then
            if (FIFO_full = '1') then
               o_axi_rvalid_but_fifo_full <= '1'; 
            else
               FIFO_din         <= i_axi_rdata;
               tx_FIFO_wr_en    <= '1';
            end if;
         end if;

         if (tx_reset_state = '1') then
            o_axi_rvalid_but_fifo_full <= '0'; 
         end if;
      end if;
    end process;

   --------------------------------------------------------------------------------------------
   -- State machine that reads whole packets out of the Fifo and presents to Packet_Player
   -- Input to the fifo comes from the data retrieved from the HBM via the AXI bus
   --------------------------------------------------------------------------------------------
   process(i_shared_clk)
   begin
      if rising_edge(i_shared_clk) then
         o_packetiser_data_in_wr        <= packetiser_data_wr_int;
         o_packetiser_data              <= FIFO_dout;
      
         o_packetiser_bytes_to_transmit <= bytes_in_current_packet; --i_tx_packet_size(13 downto 0);
      end if;
   end process;

   --------------------------------------------------------------------------------------------
   -- Time playout circuit
   -- Compare current time from PTP against the timestamp loaded into HBM
   --------------------------------------------------------------------------------------------
   process(i_shared_clk)
   begin
      if rising_edge(i_shared_clk) then
         current_time         <= i_current_time;
         use_timestamp_to_tx  <= i_use_timestamp_to_tx;

         -- if timestamping then wait for trigger, otherwise assume always running.
         if use_timestamp_to_tx = '1' then
            timestamp_play    <= timestamp_trigger;
         else
            timestamp_play    <= '1';
         end if;


         -- designing in some limits such as there won't be a gap of more than 1 hour between packets
         -- will use the subseconds from live time source for playout, but seconds needs to be interpreted.
         if (tx_reset_state = '1' OR start_next_loop = '1') then
            ts_fsm   <= LOAD_INIT;
         else
            timestamp_trigger <= '0';

            case ts_fsm is
               when LOAD_INIT =>
                  ts_fsm_debug               <= x"F";
                  ts_fsm                     <= IDLE;
                  time_from_HBM_firstpass    <= '1';
                  last_timestamp             <= (others => '0');

               when IDLE =>
                  ts_fsm_debug               <= x"0";
                  if time_from_HBM_firstpass = '1' then
                     last_timestamp          <= current_timestamp_from_HBM(79 downto 32);
                  end if;
                  
                  if timestamp_loaded = '1' then
                     ts_fsm                  <= CALC_1;
                  end if;
                  
               when CALC_1 =>
                  ts_fsm_debug               <= x"1";
                  ts_fsm                     <= CALC_2;

               when CALC_2 =>
                  ts_fsm_debug               <= x"2";
                  ts_fsm                     <= CALC_3;

               when CALC_3 =>
                  ts_fsm_debug               <= x"3";
                  ts_fsm                     <= CREATE_GOAL_TIME;

               when CREATE_GOAL_TIME =>
                  ts_fsm_debug               <= x"4";
                  -- starting from reset (0), will introduce 1 second of latency.
                  play_timestamp(39 downto 32) <= std_logic_vector(unsigned(current_time(39 downto 32)) + seconds_to_offset);

                  play_timestamp(31 downto 0)   <= current_timestamp_from_HBM(31 downto 0);
                  ts_fsm                        <= WAIT_FOR_GOAL;

               when WAIT_FOR_GOAL =>
                  ts_fsm_debug               <= x"5";
                  if current_time(39 downto 0)  > play_timestamp(39 downto 0) then
                     ts_fsm                     <= TRIGGER;
                  end if;

               when TRIGGER =>
                  ts_fsm_debug               <= x"6";
                  ts_fsm                     <= COMPLETE;
                  timestamp_trigger          <= '1';

               when COMPLETE =>
                  ts_fsm_debug               <= x"7";
                  ts_fsm                     <= IDLE;
                  time_from_HBM_firstpass    <= '0';
                  last_timestamp             <= current_timestamp_from_HBM(79 downto 32);

               when OTHERS => ts_fsm <= IDLE;

            end case;
         end if;
         --1
         last_timestamp_secs     <= unsigned(last_timestamp(47 downto 0));
         current_timestamp_secs  <= unsigned(current_timestamp_from_HBM(79 downto 32));
         --2
         seconds_diff            <= current_timestamp_secs - last_timestamp_secs;
         --3
         seconds_to_offset       <= seconds_diff(7 downto 0);
      end if;
   end process;
   --------------------------------------------------------------------------------------------
   -- Output FSM
   -- SM to empty FIFO to the packet player.
   --------------------------------------------------------------------------------------------

   process(i_shared_clk)
   begin
      if rising_edge(i_shared_clk) then
         packetiser_data_to_player_rdy    <= i_packetiser_data_to_player_rdy;

         if (tx_reset_state = '1') then
            o_reset_packet_player   <= '1';
            output_fsm              <= initial_state;
            run_timing              <= '0';
         else
            FIFO_rst                <= '0';

            tx_FIFO_rd_en           <= '0';
            packetiser_data_wr_int  <= '0';
            start_of_packet         <= '0';
            end_of_packet           <= '0';
            start_next_loop         <= '0';
            o_reset_packet_player   <= '0';
            tx_complete             <= '0';
            
            wait_fifo_resetting     <= '0';
            timestamp_loaded        <= '0';

            case output_fsm is
               when initial_state =>
                  output_fsm                    <= output_first_run0;
                  output_fsm_debug              <= x"0";
                  fpga_pkt_count_in_this_burst  <= (others => '0');
                  total_pkts_to_mac             <= (others=>'0');
                  loop_cnt                      <= (others =>'0');
                  run_timing                    <= '0';
                  bytes_in_current_packet       <= (others =>'0');
                  beats_per_packet              <= (others =>'0');
                  odd_beats_per_packet          <= (others =>'0');

               when output_first_run0 =>
                  output_fsm_debug <= x"1";
                  FIFO_rst <= '1';
                  o_reset_packet_player <= '1';
                  wait_fifo_reset_cnt <= (others => '0');
                  output_fsm <= output_first_run1;
                  

               when output_first_run1 =>
                  output_fsm_debug <= x"2";
                  if (wait_fifo_reset_cnt = to_unsigned(3, wait_fifo_reset_cnt'length)) then           
                     if (rd_rst_busy = '0' and wr_rst_busy = '0') then 
                        output_fsm <= output_idle;
                     end if;
                  else
                     wait_fifo_reset_cnt <= wait_fifo_reset_cnt +1;
                  end if;

               when output_idle =>
                  output_fsm_debug <= x"3";
                  current_pkt_count <=(others=>'0');
                  fpga_pkt_count_in_this_burst <= (others=>'0');
                  beat_count <= (others => '0');
                  total_beat_count <= (others => '0');
                  burst_count <= (others => '0');

                  -- This is from either software start or PTP start.
                  -- It is relying on coming out of reset about 1ms before start in order for the HBM to pre-fill the local cache.
                  -- That mechanism reduces initial latency and gives closer BW results.
                  -- Multi-size packet logic is relying on there being data in the fifo at this point, check it is not empty.
                  if (start_stop_tx = '1') AND ( FIFO_empty = '0') then
                     output_fsm <= read_meta_data_for_playout;
                  end if;

               when read_meta_data_for_playout =>
                  -- start timing circuit when coming from a reset and let it run.
                  run_timing                 <= '1';

                  output_fsm                 <= calc_meta_data_for_playout;
                  output_fsm_debug           <= x"D";
                  -- take the meta data from the leading 64 bytes, drain from FIFO and get setup to transmit that packet.
                  -- data is loaded from the host byte swapped, within the relevant meta data field
                  -- lower 10 bytes/80 bits are timestamp   - 79 downto 0
                  -- next 2 bytes are packet_size.          - 95 downto 80
                  bytes_in_current_packet    <= FIFO_dout(85 downto 80) & FIFO_dout(95 downto 88);
                  odd_beats_per_packet       <= x"000" & '0' & (OR (FIFO_dout(93 downto 88)));
                  tx_FIFO_rd_en              <= '1';

                  current_timestamp_from_HBM <= FIFO_dout(7 downto 0) & FIFO_dout(15 downto 8) & FIFO_dout(23 downto 16) & FIFO_dout(31 downto 24) &
                                                FIFO_dout(39 downto 32) & FIFO_dout(47 downto 40) & FIFO_dout(55 downto 48) & FIFO_dout(63 downto 56) &
                                                FIFO_dout(71 downto 64) & FIFO_dout(79 downto 72);
                  
                  timestamp_loaded           <= '1';

               
               when calc_meta_data_for_playout =>
                  output_fsm_debug              <= x"E";

                  -- add in timestamp playout logic
                  if timestamp_play = '1' then
                     output_fsm                 <= output_next_burst;
                  end if;

                  beats_per_packet(7 downto 0)  <= std_logic_vector(unsigned(bytes_in_current_packet(13 downto 6)) + unsigned(odd_beats_per_packet(7 downto 0)));
                  
               when output_next_burst =>  
                  output_fsm_debug              <= x"4";
                  
                  -- Check to see if data is present before streaming.
                  -- input buffer maintains a fill level of over 8 x 9k packets, this should never not be ready.
                  if FIFO_RdDataCount((ceil_log2(SYNC_FIFO_DEPTH)) downto 0) > beats_per_packet((ceil_log2(SYNC_FIFO_DEPTH)) downto 0) then
                     output_fsm <= output_next_packet;
                  end if;

               when output_next_packet =>
                  output_fsm_debug <= x"5";
                  beat_count                      <= (others => '0');
                  if (packetiser_data_to_player_rdy = '1') then
                     output_fsm                   <= read_full_packet;
                     total_pkts_to_mac            <= total_pkts_to_mac + 1;
                     start_of_packet              <= '1';
                  end if;
   
               when read_full_packet => -- start of reading
                  output_fsm_debug <= x"6";
                  -- Fifo is configured as FirstWordFallThrough, so data is immediatly available
                  -- check empty
                  tx_FIFO_rd_en                   <= '1';
                  packetiser_data_wr_int          <= '1';
                  if (beat_count = (unsigned(beats_per_packet) - 1)) then
                     end_of_packet                <= '1';
                     current_pkt_count            <= current_pkt_count + 1;
                     fpga_pkt_count_in_this_burst <= fpga_pkt_count_in_this_burst + 1;
                     output_fsm                   <= output_packet_finished;
                  else
                     beat_count                   <= beat_count + 1;
                     total_beat_count             <= total_beat_count + 1;  
                  end if;

               when output_packet_finished =>
                  output_fsm_debug <= x"7";
                  -- if number of bursts meet, reset the counter
                  if (fpga_pkt_count_in_this_burst = unsigned(i_expected_packets_per_burst)) then
                     burst_count                   <= burst_count + 1;
                     fpga_pkt_count_in_this_burst  <= (others => '0');
                     output_fsm                    <= output_check_burst_count;
                  else
                     output_fsm                    <= read_meta_data_for_playout; 
                  end if;
            
               when output_check_burst_count =>
                  output_fsm_debug <= x"C";
                  -- if total number of packets to send has been then stop SM
                  if (burst_count = unsigned(i_expected_total_number_of_bursts)) then
                     output_fsm                 <= output_tx_complete;
                  -- if timestamp playout is used loop.
                  elsif use_timestamp_to_tx = '1' then
                     output_fsm                 <= read_meta_data_for_playout;
                  -- else assume 100MHz timer being used to govern transmission BW,
                  else
                     output_fsm                 <= output_wait_burst_counter;
                  end if;		   

               when output_wait_burst_counter =>
                  output_fsm_debug <= x"8";
                  if (target_packets_inuse > total_pkts_to_mac) then
                     output_fsm                  <= read_meta_data_for_playout;
                     --burst_count               <= burst_count +1;
                  end if;

               when output_tx_complete =>
                  output_fsm_debug                <= x"9";
                  fpga_pkt_count_in_this_burst    <= (others=>'0');
                  
                  if (i_loop_tx = '1') then
                     output_fsm                  <= output_loopit;  
                     wait_fifo_reset_cnt         <= (others => '0');
                  else
                     output_fsm                  <= output_thats_all_folks;  
                  end if;
                     
               when output_loopit =>
                  output_fsm_debug <= x"A";
                  if (loop_cnt = unsigned(i_expected_number_of_loops)) then
                     output_fsm <= output_thats_all_folks;  
                  else
                     -- if request machine has clocked in all the packets in flight, then we can reset for next loop.
                     if (rd_fsm_debug = x"1") then
                        wait_fifo_reset_cnt <= wait_fifo_reset_cnt +1;
                        if (wait_fifo_reset_cnt = to_unsigned(0,wait_fifo_reset_cnt'length)) then
                           FIFO_rst <= '1';
                        elsif (wait_fifo_reset_cnt < to_unsigned(3,wait_fifo_reset_cnt'length)) then
                           wait_fifo_resetting <= '1';
                           -- give it a few clocks to clear the fifo's
                        else
                           wait_fifo_resetting <= '1';
                           if (rd_rst_busy = '0' and wr_rst_busy = '0') then 
                                 wait_fifo_resetting <= '0';
                                 output_fsm <= output_idle;
                                 looping <= '1';
                                 loop_cnt <= loop_cnt + 1;
                                 start_next_loop <= '1';
                           end if;
                        end if;
                     end if;
                  end if;

               when output_thats_all_folks =>
                  output_fsm_debug    <= x"B";
                  tx_complete         <= '1';
                  output_fsm          <= output_thats_all_folks;  

               when others =>
                  output_fsm <= output_idle;

            end case;
          
         -- if (tx_reset_state = '1') then
         --    o_reset_packet_player <= '1';
         --    output_fsm <= initial_state;
         end if;
      end if; 
   end process;

   --------------------------------------------------------------------------------------------
   -- FIFO for data from HBM for TX
   --------------------------------------------------------------------------------------------
   data_fifo : xpm_fifo_sync
      generic map (
         DOUT_RESET_VALUE     => "0", 
         ECC_MODE             => "no_ecc",
         FIFO_MEMORY_TYPE     => "ultra",
         FIFO_READ_LATENCY    => 1,  
         FIFO_WRITE_DEPTH     => SYNC_FIFO_DEPTH,
         FULL_RESET_VALUE     => 0,   
         PROG_EMPTY_THRESH    => 10,  
         PROG_FULL_THRESH     => (SYNC_FIFO_DEPTH/2), 
         RD_DATA_COUNT_WIDTH  => ((ceil_log2(SYNC_FIFO_DEPTH))+1),
         READ_DATA_WIDTH      => 512,    
         READ_MODE            => "fwft", 
         SIM_ASSERT_CHK       => 0,      
         USE_ADV_FEATURES     => "1F1F", 
         WAKEUP_TIME          => 0,      
         WRITE_DATA_WIDTH     => 512,    
         WR_DATA_COUNT_WIDTH  => ((ceil_log2(SYNC_FIFO_DEPTH))+1) 
      )
      port map (
         almost_full          => FIFO_almost_full,      -- 1-bit output: Almost Full: When asserted, this signal indicates that only one more write can be performed before the FIFO is full.
         dout                 => FIFO_dout,      -- READ_DATA_WIDTH-bit output: Read Data: The output data bus is driven when reading the FIFO.
         empty                => FIFO_empty,    -- 1-bit output: Empty Flag: When asserted, this signal indicates that- the FIFO is empty.
         full                 => FIFO_full,      -- 1-bit output: Full Flag: When asserted, this signal indicates that the FIFO is full.
         prog_full            => FIFO_prog_full,        -- 1-bit output: Programmable Full: This signal is asserted when the number of words in the FIFO is greater than or equal to the programmable full threshold value.
         rd_data_count        => FIFO_RdDataCount, -- RD_DATA_COUNT_WIDTH-bit output: Read Data Count: This bus indicates the number of words read from the FIFO.
         rd_rst_busy          => rd_rst_busy,      -- 1-bit output: Read Reset Busy: Active-High indicator that the FIFO read domain is currently in a reset state.
         wr_data_count        => FIFO_WrDataCount, -- WR_DATA_COUNT_WIDTH-bit output: Write Data Count: This bus indicates the number of words written into the FIFO.
         wr_rst_busy          => wr_rst_busy,      -- 1-bit output: Write Reset Busy: Active-High indicator that the FIFO write domain is currently in a reset state.
         din                  => FIFO_din,        -- WRITE_DATA_WIDTH-bit input: Write Data: The input data bus used when writing the FIFO.
         injectdbiterr        => '0',     -- 1-bit input: Double Bit Error Injection
         injectsbiterr        => '0',     -- 1-bit input: Single Bit Error Injection:
         rd_en                => tx_FIFO_rd_en, -- 1-bit input: Read Enable: If the FIFO is not empty, asserting this signal causes data (on dout) to be read from the FIFO.
         rst                  => tx_fifo_cache_reset,        -- 1-bit input: Reset: Must be synchronous to wr_clk.
         sleep                => '0',             -- 1-bit input: Dynamic power saving- If sleep is High, the memory/fifo block is in power saving mode.
         wr_clk               => i_shared_clk,   -- 1-bit input: Write clock: Used for write operation. wr_clk must be a free running clock.
         wr_en                => tx_FIFO_wr_en      -- 1-bit input: Write Enable:
      );

--------------------------------------------------------------------------------------------------------------------------------
-- PTP timer.
--------------------------------------

o_timer_fields_in.timer_goal_in_ns  <= i_time_between_bursts_ns;
o_timer_fields_in.timer_on          <= run_timing;

ptp_ns_counter : process(i_shared_clk)
begin
   if rising_edge(i_shared_clk) then
      if (tx_reset_state = '1') then
         ptp_target_packets   <= 64D"1";
      else
         if i_timer_fields_out.timing_pulse = '1' then
            ptp_target_packets <= ptp_target_packets + 1;
         end if;
      end if;
   end if;
end process; 


--------------------------------------
-- Mux to select timer source
--------------------------------------       

   target_packets_inuse <= target_packets when i_legacy_rate_sel = '1' else ptp_target_packets;

--------------------------------------
-- Timer running off the fixed 100Mhz
-------------------------------------- 
   ns_counter : process(clk_freerun)
   begin
      if rising_edge(clk_freerun) then
         time_between_packets_100Mhz   <= resize(unsigned(time_between_bursts_ns_100Mhz), target_time_100Mhz'length);
         
         if (reset_state_100Mhz = '1') then
            ns_burst_timer_100Mhz   <= (others => '0');
            ns_total_time_100Mhz    <= (others => '0');
            target_packets_100Mhz   <= to_unsigned(1,target_packets_100Mhz'length);
            target_time_100Mhz      <= time_between_packets_100Mhz;
            start_next_burst        <= '0';
         elsif (run_timing_100Mhz = '1') then                
            -- create psuedo 10ns counter from reset release.
            ns_total_time_100Mhz    <= ns_total_time_100Mhz + to_unsigned(10,ns_total_time_100Mhz'length);

            -- calculate how many packets we should have sent by now
            if (ns_total_time_100Mhz >= target_time_100Mhz) then
               target_time_100Mhz   <= target_time_100Mhz + time_between_packets_100Mhz;
               target_packets_100Mhz <= target_packets_100Mhz + 1;
               start_next_burst     <= '1';
            else
               start_next_burst     <= '0';
            end if;
         end if;
      end if;
   end process ns_counter;      

   current_packet_based_on_time_cdc : xpm_cdc_array_single
   generic map (
      DEST_SYNC_FF    => 4,   
      INIT_SYNC_FF    => 1,   
      SRC_INPUT_REG   => 1,   
      SIM_ASSERT_CHK  => 1, 
      WIDTH           => 64    
   )
   port map (
      dest_clk        => i_shared_clk,   
      dest_out        => target_packets_std_logic,       
      src_clk         => clk_freerun,       
      src_in          => std_logic_vector(target_packets_100Mhz)
   );
    
   target_packets <=  unsigned(target_packets_std_logic);   

   RESET_300_to_100_CDC : xpm_cdc_single
   generic map (
      DEST_SYNC_FF    => 4,   
      INIT_SYNC_FF    => 1,   
      SRC_INPUT_REG   => 1,   
      SIM_ASSERT_CHK  => 1    
   )
   port map (
      dest_clk        => clk_freerun,   
      dest_out        => reset_state_100Mhz, 
      src_clk         => i_shared_clk,    
      src_in          => tx_reset_state     
   );

   start_timer_CDC : xpm_cdc_single
   generic map (
      DEST_SYNC_FF    => 4,   
      INIT_SYNC_FF    => 1,   
      SRC_INPUT_REG   => 1,   
      SIM_ASSERT_CHK  => 1    
   )
   port map (
      dest_clk        => clk_freerun,   
      dest_out        => run_timing_100Mhz, 
      src_clk         => i_shared_clk,    
      src_in          => run_timing
   );

   start_burst_CDC : xpm_cdc_single
   generic map (
      DEST_SYNC_FF    => 4,   
      INIT_SYNC_FF    => 1,   
      SRC_INPUT_REG   => 1,   
      SIM_ASSERT_CHK  => 1    
   )
   port map (
      dest_clk        => i_shared_clk,   
      dest_out        => start_next_burst_ila, 
      src_clk         => clk_freerun,    
      src_in          => start_next_burst
   );

   time_between_packets_cdc : xpm_cdc_array_single
   generic map (
      DEST_SYNC_FF    => 4,   
      INIT_SYNC_FF    => 1,   
      SRC_INPUT_REG   => 1,   
      SIM_ASSERT_CHK  => 1, 
      WIDTH           => 32    
   )
   port map (
      dest_clk        => clk_freerun,   
      dest_out        => time_between_bursts_ns_100Mhz,       
      src_clk         => i_shared_clk,    
      src_in          => i_time_between_bursts_ns
   );
    
----------------------------------------------------------------------------------------------------------
-- debug

debug_gen : IF g_DEBUG_ILAs GENERATE

    --------------------------------------------------------------------------------------------
    -- Debug: Capture first vector sent to the MAC, so that we can compare later with the start
    -- of packet for subsequent packets and trigger on corrupt packets.
    --------------------------------------------------------------------------------------------


--hbm_capture_ila : ila_0
--  port map (
--     clk                     => i_shared_clk, 
--     probe0(28 downto 0)     => m01_axi_wdata(28 downto 0),
--     probe0(29)              => last_aw_trans,
--     probe0(30)              => m02_wr,
--     probe0(31)              => m01_wr,
      

--     probe0(63 downto 32)    => axi_wdata(31 downto 0),
--     probe0(95 downto 64)    => m02_axi_awaddr,
--     probe0(96)              => m02_axi_wvalid, 
      
--     probe0(97)              => m02_axi_awvalid,
--     probe0(98)              => m02_axi_awready,
--     probe0(99)              => m02_axi_wlast,
--     probe0(100)             => m02_axi_wready,
--     probe0(108 downto 101)  => m02_axi_awlen,
--     probe0(109)             => m02_fifo_rd_en,
--     probe0(125 downto 110)  => m02_axi_wdata(15 downto 0),
--     probe0(126)             => last_trans,
--     --probe0(126 downto 64)   => i_data_from_cmac(62 downto 0),
--     probe0(127)             => m02_axi_4G_full,
--     probe0(159 downto 128)  => m03_axi_awaddr,
--     probe0(160)             => m03_axi_wvalid, 
      
--     probe0(161)             => m03_axi_awvalid,
--     probe0(162)             => m03_axi_awready,
--     probe0(163)             => m03_axi_wlast,
--     probe0(164)             => m03_axi_wready,
--     probe0(172 downto 165)  => m03_axi_awlen,
--     probe0(179 downto 173)  => std_logic_vector(fifo_rd_counter),
--     probe0(180)             => axi_wdata_fifo_full,
--     probe0(181)             => axi_wdata_fifo_empty,
--     probe0(182)             => m03_fifo_rd_en,
--     probe0(183)             => awfifo_wren,
--     probe0(184)             => awfifo_rden,
--     probe0(185)             => awlenfifo_rden,
--     probe0(186)             => fifo_rd_en,
--     probe0(187)             => fifo_wr_en,
--     probe0(191 downto 188)  => input_fsm_state_count
--  );

   debug_HBM : IF g_DEBUG_HBM GENERATE 

      hbm_playout_ila : ila_0
         port map (
            clk                     => i_shared_clk, 
            probe0(31 downto 0)     => m01_axi_rdata(31 downto 0),
            probe0(63 downto 32)    => i_axi_rdata(31 downto 0),
            probe0(95 downto 64)    => FIFO_dout(31 downto 0),
            probe0(127 downto 96)   => FIFO_din(31 downto 0),
            
            probe0(128)             => i_axi_rvalid,
            probe0(129)             => m01_axi_rvalid,        
            probe0(130)             => tx_FIFO_wr_en,
            probe0(131)             => tx_FIFO_rd_en,
            probe0(132)             => FIFO_empty,

            probe0(146 downto 133)  => bytes_in_current_packet, --i_tx_packet_size,
            
            probe0(154 downto 147)  => beats_per_packet(7 downto 0), --( others => '0' ),
            probe0(155)             => odd_beats_per_packet(0),
            probe0(156)             =>  '0' ,
            probe0(167 downto 157)  => FIFO_WrDataCount(10 downto 0),
            probe0(171 downto 168)  => rd_fsm_debug,
            probe0(175 downto 172)  => output_fsm_debug,
            probe0(177 downto 176)  => std_logic_vector(boundary_across_num),
            probe0(191 downto 178)  => m01_axi_araddr(31 downto 18)
            
         );

      hbm_rd_ila : ila_0
         port map (
            clk                     => i_shared_clk, 
            probe0(19 downto 0)     => m01_axi_araddr(31 downto 12),
            probe0(39 downto 20)    => m02_axi_araddr(31 downto 12),
            probe0(40)              => o_axi_arvalid,
            probe0(41)              => '0',

            probe0(73 downto 42)    => std_logic_vector(readaddr),
            probe0(95 downto 74)    => std_logic_vector(buf_num_of_4k_current),
            probe0(127 downto 96)   => std_logic_vector(axi_r_num),
            
            probe0(128)             => m01_axi_arvalid,
            probe0(129)             => m02_axi_arvalid,        
            probe0(130)             => m03_axi_arvalid,
            probe0(131)             => m04_axi_arvalid,
            
            probe0(133 downto 132)  => std_logic_vector(boundary_across_num),
            
            probe0(137 downto 134)  => rd_fsm_debug,
            probe0(141 downto 138)  => output_fsm_debug,
            probe0(173 downto 142)  => std_logic_vector(current_axi_4k_count),
            probe0(174)             => '0',
            probe0(182 downto 175)  => m03_axi_araddr(31 downto 24),
            probe0(190 downto 183)  => m04_axi_araddr(31 downto 24),
            probe0(191)             => i_axi_arready
            --probe0(191 downto 190)  => ( others => '0' ) 
         );
         
   END GENERATE;

   cnic_tx_ila : ila_0
   port map (
      clk                    => i_shared_clk,
      probe0(3 downto 0)     => rd_fsm_debug,
      --probe0(35 downto 4)    => std_logic_vector(total_beat_count(31 downto 0)),
      --probe0(67 downto 36)   => std_logic_vector(beat_count(31 downto 0)),

      probe0(35 downto 4)    => std_logic_vector(target_packets(31 downto 0)),
      probe0(67 downto 36)   => std_logic_vector(ptp_target_packets(31 downto 0)),

      probe0(68)             => preload,
      probe0(69)             => FIFO_empty,
      probe0(70)             => FIFO_rst,
      probe0(71)             => i_timer_fields_out.timing_pulse,
      probe0(72)             => i_legacy_rate_sel,
      probe0(76 downto 73)   => output_fsm_debug,

      probe0(89 downto 77)   => FIFO_WrDataCount,
      probe0(90)             => tx_reset_state,
      probe0(91)             => FIFO_prog_full,
      probe0(92)             => FIFO_full,
      probe0(93)             => i_timer_fields_out.timing_pulse,
      probe0(109 downto 94)  => i_expected_total_number_of_bursts(15 downto 0), 
      probe0(122 downto 110) => FIFO_RdDataCount,
      probe0(123)            => run_timing,
      probe0(124)            => '0',
      probe0(144 downto 125) => std_logic_vector(target_packets_inuse(19 downto 0)), 
      probe0(164 downto 145) => std_logic_vector(total_pkts_to_mac(19 downto 0)), 
      probe0(184 downto 165) => std_logic_vector(burst_count(19 downto 0)),
      probe0(190 downto 185) => std_logic_vector(fpga_pkt_count_in_this_burst(5 downto 0)), 

      probe0(191) => start_next_burst_ila
   );

   cnic_tx_timestamps_ila : ila_0
   port map (
      clk                    => i_shared_clk,

      probe0(39 downto 0)    => current_timestamp_from_HBM(39 downto 0),
      probe0(79 downto 40)   => play_timestamp(39 downto 0),
      probe0(119 downto 80)  => last_timestamp(39 downto 0),
      probe0(159 downto 120) => current_time(39 downto 0),

      probe0(163 downto 160) => ts_fsm_debug,
      probe0(167 downto 164) => output_fsm_debug,
      probe0(171 downto 168) => rd_fsm_debug,

      probe0(172)            => timestamp_trigger,

      probe0(173)            => packetiser_data_wr_int,
      probe0(187 downto 174) => std_logic_vector(total_pkts_to_mac(13 downto 0)),

      probe0(188)            => use_timestamp_to_tx,

      probe0(191 downto 189) => (others => '0')

   );
  
END GENERATE;

    
end RTL;
