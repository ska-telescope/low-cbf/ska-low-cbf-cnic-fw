----------------------------------------------------------------------------------
-- Company: CSIRO
-- Engineer: Giles Babich
-- 
-- Create Date: Nov 2022
-- Design Name: 
-- Module Name: HBM_memory_write - Behavioral
--
--
--
-- Description: 
--      Takes in data bus and manages writes to HBM.
--      4 x 4095MB buffers in HBM
-- 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE, common_lib, xpm, axi4_lib, cnic_lib, signal_processing_common;

use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use cnic_lib.cnic_top_pkg.all;
use common_lib.common_pkg.ALL;
use xpm.vcomponents.all;
use axi4_lib.axi4_lite_pkg.all;
use axi4_lib.axi4_full_pkg.all;

entity HBM_memory_write is
    generic (
        g_DEBUG_ILAs             : BOOLEAN := FALSE;
        g_HBM_bank_size          : string  := "4095MB"
    );
    Port (
        -- shared memory interface clock (300 MHz)
        i_shared_clk                    : in std_logic;
        i_shared_rst                    : in std_logic;

        o_module_in_reset               : out std_logic;
        ------------------------------------------------------------------------------------
        -- Data from CMAC module after CDC in shared memory clock domain
        i_data_from_cmac                : in  std_logic_vector(511 downto 0);
        i_data_valid_from_cmac          : in  std_logic;
        ------------------------------------------------------------------------------------
        -- config and status registers interface
        -- rx
        i_rx_packet_size                : in std_logic_vector(13 downto 0);   -- MODULO 64!!
        i_rx_soft_reset                 : in std_logic;
        i_enable_capture                : in std_logic;

        i_lfaa_bank1_addr               : in std_logic_vector(31 downto 0);
        i_lfaa_bank2_addr               : in std_logic_vector(31 downto 0);
        i_lfaa_bank3_addr               : in std_logic_vector(31 downto 0);
        i_lfaa_bank4_addr               : in std_logic_vector(31 downto 0);
        update_start_addr               : in std_logic; --pulse signal to update each 4GB bank start address
        i_rx_bank_enable                : in std_logic_vector(3 downto 0);

        o_1st_4GB_rx_addr               : out std_logic_vector(31 downto 0);
        o_2nd_4GB_rx_addr               : out std_logic_vector(31 downto 0);
        o_3rd_4GB_rx_addr               : out std_logic_vector(31 downto 0);
        o_4th_4GB_rx_addr               : out std_logic_vector(31 downto 0);

        o_capture_done                  : out std_logic;
        o_num_packets_received          : out std_logic_vector(31 downto 0);
        i_rx_packets_to_capture         : in std_logic_vector(31 downto 0);
        i_rx_flush_to_hbm               : in std_logic;

        o_input_fsm_debug               : out std_logic_vector(3 downto 0);

        i_enable_duplex                 : in std_logic_vector(1 downto 0);

        -----------------------------------------------------------------------
        i_schedule_action               : in std_logic_vector(7 downto 0);
        -----------------------------------------------------------------------
        -- HBM Write interfaces.
        -- first 4GB section of AXI
        -- aw bus
        m01_axi_awvalid   : out std_logic := '0';
        m01_axi_awready   : in  std_logic;
        m01_axi_awaddr    : out std_logic_vector(31 downto 0) := (others => '0');
        m01_axi_awlen     : out std_logic_vector(7 downto 0)  := (others => '0');
        --w bus
        m01_axi_wvalid    : out std_logic := '0';
        m01_axi_wdata     : out std_logic_vector(511 downto 0) := (others => '0');
        m01_axi_wstrb     : out std_logic_vector(63  downto 0) := (others => '1');
        m01_axi_wlast     : out std_logic := '0';
        m01_axi_wready    : in  std_logic;

        --second 4GB section of AXI
        --aw bus
        m02_axi_awvalid   : out std_logic := '0';
        m02_axi_awready   : in  std_logic;
        m02_axi_awaddr    : out std_logic_vector(31 downto 0) := (others => '0');
        m02_axi_awlen     : out std_logic_vector(7 downto 0)  := (others => '0');
        --w bus
        m02_axi_wvalid    : out std_logic := '0';
        m02_axi_wdata     : out std_logic_vector(511 downto 0) := (others => '0');
        m02_axi_wstrb     : out std_logic_vector(63  downto 0) := (others => '1');
        m02_axi_wlast     : out std_logic := '0';
        m02_axi_wready    : in  std_logic;

        --third 4GB section of AXI
        --aw bus
        m03_axi_awvalid   : out std_logic := '0';
        m03_axi_awready   : in  std_logic;
        m03_axi_awaddr    : out std_logic_vector(31 downto 0) := (others => '0');
        m03_axi_awlen     : out std_logic_vector(7 downto 0)  := (others => '0');
        --w bus
        m03_axi_wvalid    : out std_logic := '0';
        m03_axi_wdata     : out std_logic_vector(511 downto 0) := (others => '0');
        m03_axi_wstrb     : out std_logic_vector(63  downto 0) := (others => '1');
        m03_axi_wlast     : out std_logic := '0';
        m03_axi_wready    : in  std_logic;

        --fourth 4GB section of AXI
        --aw bus
        m04_axi_awvalid   : out  std_logic := '0';
        m04_axi_awready   : in   std_logic;
        m04_axi_awaddr    : out  std_logic_vector(31 downto 0) := (others => '0');
        m04_axi_awlen     : out  std_logic_vector(7 downto 0)  := (others => '0');
        --w bus
        m04_axi_wvalid    : out std_logic := '0';
        m04_axi_wdata     : out std_logic_vector(511 downto 0) := (others => '0');
        m04_axi_wstrb     : out std_logic_vector(63  downto 0) := (others => '1');
        m04_axi_wlast     : out std_logic := '0';
        m04_axi_wready    : in  std_logic
    );

    -- prevent optimisation 
    attribute keep_hierarchy : string;
    attribute keep_hierarchy of HBM_memory_write : entity is "yes";

end HBM_memory_write;

architecture Behavioral of HBM_memory_write is
    COMPONENT ila_0
        PORT (
            clk         : IN STD_LOGIC;
            probe0      : IN STD_LOGIC_VECTOR(191 DOWNTO 0)
        );
    END COMPONENT;

    attribute max_fanout : integer;

    constant fifo_data_cache_width  : INTEGER   := 512;
    constant fifo_data_cache_depth  : INTEGER   := 4096;

    signal clock_int        : std_logic;
    signal reset_int        : std_logic;

    signal data_cache_fifo_in_reset : std_logic;
    signal data_cache_fifo_rd       : std_logic;
    signal data_cache_fifo_q        : std_logic_vector((fifo_data_cache_width-1) downto 0);
    signal data_cache_fifo_q_valid  : std_logic;
    signal data_cache_fifo_empty    : std_logic;
    signal data_cache_fifo_rd_count : std_logic_vector(((ceil_log2(fifo_data_cache_depth))) downto 0);
    -- WR        
    signal data_cache_fifo_wr       : std_logic;
    signal data_cache_fifo_wr_w_fl  : std_logic;

    signal data_cache_fifo_data     : std_logic_vector((fifo_data_cache_width-1) downto 0);
    signal data_cache_fifo_full     : std_logic;
    signal data_cache_fifo_wr_count : std_logic_vector(((ceil_log2(fifo_data_cache_depth))) downto 0);

    signal data_cache_fifo_wr_del   : std_logic;

    signal input_fsm_state_count    : std_logic_vector(3 downto 0);
    
    type input_fsm_type is (idle, 
                            wait_on_fifo, generate_addr, load_addr, drain_fifo,
                            complete, capture_complete);
    signal input_fsm : input_fsm_type;

    signal input_fsm_debug          : std_logic_vector(3 downto 0);

    signal num_packets_received     : std_logic_vector(31 downto 0);

    signal hbm_bank_wr_addr         : t_slv_32_arr(3 downto 0);
    signal fifo_burst_count         : unsigned(11 downto 0);
    signal fifo_rd_en               : std_logic;

    attribute max_fanout of fifo_rd_en : signal is 100;

    signal hbm_wr_addr_calc         : std_logic_vector(33 downto 0);
    signal hbm_target_interface     : std_logic_vector(1 downto 0);
    signal hbm_buffer_max           : std_logic_vector(15 downto 0);

    -- shared /combined signals for HBM interface interactions.
    signal hbm_axi_awready          : std_logic;
    signal hbm_axi_awlen            : std_logic_vector(7 downto 0);
    signal hbm_axi_awvalid          : std_logic := '0';
    signal hbm_axi_awaddr           : std_logic_vector(31 downto 0) := (others => '0');

    signal hbm_axi_wready           : std_logic;
    signal hbm_axi_wvalid           : std_logic;
    signal hbm_axi_wlast            : std_logic;

    signal hbm_axi_m01_wvalid       : std_logic;
    signal hbm_axi_m02_wvalid       : std_logic;
    signal hbm_axi_m03_wvalid       : std_logic;
    signal hbm_axi_m04_wvalid       : std_logic;

    signal flush_fifo_to_hbm        : std_logic;
    signal flush_fifo_to_hbm_local  : std_logic;
    signal flushed                  : std_logic;
    signal capture_done             : std_logic;

    signal hbm_rx_max_bank          : std_logic_vector(1 downto 0)  := "11";

    -- internals for debug
    --aw bus
    signal m01_axi_awvalid_int        : std_logic := '0';
    signal m01_axi_wvalid_int         : std_logic := '0';
    signal m01_axi_wlast_int          : std_logic := '0';
    signal m02_axi_awvalid_int        : std_logic := '0';
    signal m02_axi_wvalid_int         : std_logic := '0';
    signal m02_axi_wlast_int          : std_logic := '0'; 

begin

----------------------------------------------------------------------
-- top level mappings
clock_int               <= i_shared_clk;
reset_int               <= i_shared_rst OR i_rx_soft_reset;

o_module_in_reset       <= data_cache_fifo_in_reset;
o_num_packets_received  <= num_packets_received;
o_capture_done          <= capture_done or flush_fifo_to_hbm;

data_cache_fifo_wr      <= i_data_valid_from_cmac;
data_cache_fifo_data    <= i_data_from_cmac;
----------------------------------------------------------------------
-- Max amount of space per HBM segment based on ALVEO type.
g_U55_condition : if g_HBM_bank_size = "4095MB" generate
    hbm_buffer_max                <= x"FFF0";
end generate g_U55_condition;					      

g_U50_condition : if g_HBM_bank_size = "2048MB" generate
    hbm_buffer_max                <= x"8000";
end generate g_U50_condition;

----------------------------------------------------------------------
-- duplex HBM control
duplex_proc : process(clock_int)
begin
    if rising_edge(clock_int) then
        if reset_int = '1' then
            hbm_rx_max_bank <= "11";
        else
--           0 - Card will either TX or RX for the full HBM, 
--           1 - Card will assign the first 2 buffers to RX and the last 2 to TX,
--           2 - Card will assign the first 1 buffers to RX and the last 3 to TX,
--           3 - Card will assign the first 3 buffers to RX and the last 1 to TX.
            if i_enable_duplex = "00" then
                hbm_rx_max_bank <= "11";
                
            elsif i_enable_duplex = "01" then
                hbm_rx_max_bank <= "01";
            
            elsif i_enable_duplex = "10" then
                hbm_rx_max_bank <= "00";
            
            else
                hbm_rx_max_bank <= "10";
            end if;
        end if;
    end if;
end process;

----------------------------------------------------------------------
-- input FIFO cache... SYNC FIFO.
-- 4096d x 512w
-- Make packet size is 9000 bytes = 141 writes
-- + 64b for timestamp and other meta data.
-- 142 writes for a packet.
-- 2048 should give a nice buffer for full size packets.
inc_data_cache : entity signal_processing_common.xpm_sync_fifo_wrapper
    Generic map (
        FIFO_MEMORY_TYPE    => "uram",
        READ_MODE           => "fwft",
        FIFO_DEPTH          => fifo_data_cache_depth,
        DATA_WIDTH          => fifo_data_cache_width
    )
    Port map ( 
        fifo_reset          => reset_int,
        fifo_clk            => clock_int,
        fifo_in_reset       => data_cache_fifo_in_reset,
        -- RD    
        fifo_rd             => data_cache_fifo_rd,
        fifo_q              => data_cache_fifo_q,
        fifo_q_valid        => data_cache_fifo_q_valid,
        fifo_empty          => data_cache_fifo_empty,
        fifo_rd_count       => data_cache_fifo_rd_count,
        -- WR        
        fifo_wr             => data_cache_fifo_wr_w_fl,
        fifo_data           => data_cache_fifo_data,
        fifo_full           => data_cache_fifo_full,
        fifo_wr_count       => data_cache_fifo_wr_count
    );

    -- Stop capture after number of packets received by the URAM
    data_cache_fifo_wr_w_fl <= data_cache_fifo_wr AND (NOT flush_fifo_to_hbm_local);

    -- Read from the FIFO if the SM is allowing it and the interface is ready.
    data_cache_fifo_rd      <= hbm_axi_wready AND fifo_rd_en;

-- Packets are bursted in as a single transfer.
-- Packets captured can be counter with the falling edge of the incoming data.
p_packet_rx_proc : process(clock_int)
begin
    if rising_edge(clock_int) then
        data_cache_fifo_wr_del <= data_cache_fifo_wr;

        if reset_int = '1' then
            num_packets_received    <= x"00000000";
            flush_fifo_to_hbm       <= '0';
            flush_fifo_to_hbm_local <= '0';
        else
            -- count on the falling edge.
            if data_cache_fifo_wr = '0' AND data_cache_fifo_wr_del = '1' then
                num_packets_received <= std_logic_vector(unsigned(num_packets_received) + 1);
            end if;

            if i_rx_packets_to_capture = num_packets_received then
                flush_fifo_to_hbm_local <= '1';
            end if;

            -- Flush the remaining data to the HBM and set capture_done at the same time.
            -- if packets captured = number desired
            -- OR Manual trigger (used when manually stopping a capture)
            -- OR PTP RX stop signal is received.
            -- action vector 1 = TX start, 2 = TX stop, 3 = RX start, 4 = RX stop.
            flush_fifo_to_hbm <= flush_fifo_to_hbm_local OR i_rx_flush_to_hbm OR i_schedule_action(4);
        end if;
    end if;
end process;

----------------------------------------------------------------------
-- WR to HBM SM.
-- In the case of a U55C, there are 4095MB buffers = 0xFFF0_0000
-- AXI writes in bursts of 4096 bytes --> 1,048,320 writes per 4095MB buffer.
--
-- For U50, there are 2048MB buffers.
-- 524,160 writes of 4096 bytes.
-- 
-- The SM will be simply, for every 4096 bytes, trigger a write to the HBM.
-- update address and move to next bank at the top of the address space.
-- Packets will not be split.
-- Software will set the expected number of packets.
-- If the remainder of the packet is stuck in the ingress FIFO, it will make it to HBM
-- by math done of the number of expect packets OR a PTP stop will flush the fifo as well.

p_data_to_hbm_proc : process(clock_int)
begin
    if rising_edge(clock_int) then
        if reset_int = '1' then
            input_fsm               <= idle;
            hbm_bank_wr_addr(0)     <= i_lfaa_bank1_addr;
            hbm_bank_wr_addr(1)     <= i_lfaa_bank2_addr;
            hbm_bank_wr_addr(2)     <= i_lfaa_bank3_addr;
            hbm_bank_wr_addr(3)     <= i_lfaa_bank4_addr;
            hbm_wr_addr_calc        <= "00" & i_lfaa_bank1_addr;
            hbm_axi_awlen           <= "00000000";
            hbm_axi_awvalid         <= '0';
            fifo_rd_en              <= '0';
            hbm_axi_wlast           <= '0';
            fifo_burst_count        <= x"000";
            flushed                 <= '0';
            capture_done            <= '0';
        else
            hbm_axi_awvalid <= '0';

            case input_fsm is 
                WHEN idle =>
                    if i_enable_capture = '1' then
                        input_fsm   <= wait_on_fifo;
                    end if;

                WHEN wait_on_fifo => 
                    if (unsigned(data_cache_fifo_rd_count) >= 64) then
                        hbm_axi_awlen       <= "00111111";      -- 64 AXI transfers parameter for bus.
                        fifo_burst_count    <= x"040";          -- Burst counter, which is above + 1.
                        
                        -- Make sure AXI is ready to receive a burst and then load it.
                        if hbm_axi_awready = '1' then
                            hbm_axi_awvalid <= '1';
                            input_fsm       <= generate_addr;
                        end if;

                    elsif flush_fifo_to_hbm = '1' and flushed = '0' and (unsigned(data_cache_fifo_rd_count) /= 0) then
                        hbm_axi_awlen       <= "00111111";      -- 64 AXI transfers parameter for bus.
                        fifo_burst_count    <= x"040";          -- Burst counter, which is above + 1.
                        
                        -- Make sure AXI is ready to receive a burst and then load it.
                        if hbm_axi_awready = '1' then
                            hbm_axi_awvalid <= '1';
                            input_fsm       <= generate_addr;
                        end if;
                        flushed     <= '1';
                    end if;
                
                WHEN generate_addr => 
                    -- add 4096
                    hbm_wr_addr_calc(31 downto 12)  <= std_logic_vector(unsigned(hbm_wr_addr_calc(31 downto 12)) + 1);

                    input_fsm   <= load_addr;

                when load_addr => 
                    input_fsm   <= drain_fifo;
                    fifo_rd_en  <= '1';

                WHEN drain_fifo =>
                    -- Drain the FIFO as per the fifo_burst_count
                    if hbm_axi_wvalid = '1' then
                        hbm_axi_wlast       <= '0';
                        fifo_burst_count    <= fifo_burst_count - 1;
                        -- trigger last
                        if fifo_burst_count = x"02" then
                            hbm_axi_wlast   <= '1';
                        end if;
                        if fifo_burst_count = x"01" then
                            fifo_rd_en      <= '0';
                        end if;                        
                    end if;
                    if fifo_burst_count = x"00" then
                        input_fsm   <= complete;
                    end if;

                WHEN complete =>
                    input_fsm   <= wait_on_fifo;
                    -- if at the buffer max from last transfer, need to roll to the next.
                    if hbm_wr_addr_calc(31 downto 16) = hbm_buffer_max then
                        
                        hbm_wr_addr_calc(33 downto 32)  <= std_logic_vector(unsigned(hbm_wr_addr_calc(33 downto 32)) + 1);
                        --if at the end of the fourth buffer... FULL!
                        if hbm_wr_addr_calc(33 downto 32) = hbm_rx_max_bank then
                            input_fsm   <= capture_complete;
                        else
                            -- not circular buffer, only zero when not at the end of the 4th
                            hbm_wr_addr_calc(31 downto 0)   <= x"00000000";                        
                        end if;
                    end if;

                WHEN capture_complete =>
                    capture_done <= '1';

                WHEN OTHERS =>
                    input_fsm   <= idle;
            end case;
        end if;
    end if;
end process;

----------------------------------------------------------------------
-- HBM Address control line mappings.

hbm_target_interface    <= hbm_wr_addr_calc(33 downto 32);
hbm_axi_awaddr          <= hbm_wr_addr_calc(31 downto 0);

hbm_axi_awready         <=  m01_axi_awready when hbm_target_interface = "00" else
                            m02_axi_awready when hbm_target_interface = "01" else
                            m03_axi_awready when hbm_target_interface = "10" else
                            m04_axi_awready when hbm_target_interface = "11";

m01_axi_awvalid         <= hbm_axi_awvalid when hbm_target_interface = "00" else '0';
m01_axi_awaddr          <= hbm_axi_awaddr;
m01_axi_awlen           <= hbm_axi_awlen;

m02_axi_awvalid         <= hbm_axi_awvalid when hbm_target_interface = "01" else '0';
m02_axi_awaddr          <= hbm_axi_awaddr;
m02_axi_awlen           <= hbm_axi_awlen;

m03_axi_awvalid         <= hbm_axi_awvalid when hbm_target_interface = "10" else '0';
m03_axi_awaddr          <= hbm_axi_awaddr;
m03_axi_awlen           <= hbm_axi_awlen;

m04_axi_awvalid         <= hbm_axi_awvalid when hbm_target_interface = "11" else '0';
m04_axi_awaddr          <= hbm_axi_awaddr;
m04_axi_awlen           <= hbm_axi_awlen;

----------------------------------------------------------------------
-- HBM Data control line mappings.

hbm_axi_wready          <=  m01_axi_wready when hbm_target_interface = "00" else
                            m02_axi_wready when hbm_target_interface = "01" else
                            m03_axi_wready when hbm_target_interface = "10" else
                            m04_axi_wready when hbm_target_interface = "11";

hbm_axi_wvalid          <= (fifo_rd_en AND hbm_axi_wready);

hbm_axi_m01_wvalid      <= (fifo_rd_en AND m01_axi_wready);
hbm_axi_m02_wvalid      <= (fifo_rd_en AND m02_axi_wready);
hbm_axi_m03_wvalid      <= (fifo_rd_en AND m03_axi_wready);
hbm_axi_m04_wvalid      <= (fifo_rd_en AND m04_axi_wready);

m01_axi_wvalid          <= hbm_axi_m01_wvalid when hbm_target_interface = "00" else '0';
m01_axi_wlast           <= hbm_axi_wlast when (hbm_target_interface = "00") else '0';
m01_axi_wdata           <= data_cache_fifo_q;

m02_axi_wvalid          <= hbm_axi_m02_wvalid when hbm_target_interface = "01" else '0';
m02_axi_wlast           <= hbm_axi_wlast when (hbm_target_interface = "01") else '0';
m02_axi_wdata           <= data_cache_fifo_q;

m03_axi_wvalid          <= hbm_axi_m03_wvalid when hbm_target_interface = "10" else '0';
m03_axi_wlast           <= hbm_axi_wlast when (hbm_target_interface = "10") else '0';
m03_axi_wdata           <= data_cache_fifo_q;

m04_axi_wvalid          <= hbm_axi_m04_wvalid when hbm_target_interface = "11" else '0';
m04_axi_wlast           <= hbm_axi_wlast when (hbm_target_interface = "11") else '0';
m04_axi_wdata           <= data_cache_fifo_q;


-- internals for debug
m01_axi_awvalid_int     <= hbm_axi_awvalid when hbm_target_interface = "00" else '0';
m01_axi_wvalid_int      <= hbm_axi_m01_wvalid when hbm_target_interface = "00" else '0';
m01_axi_wlast_int       <= hbm_axi_wlast when (hbm_target_interface = "00") else '0';

m02_axi_awvalid_int     <= hbm_axi_awvalid when hbm_target_interface = "01" else '0';
m02_axi_wvalid_int      <= hbm_axi_m02_wvalid when hbm_target_interface = "01" else '0';
m02_axi_wlast_int       <= hbm_axi_wlast when (hbm_target_interface = "01") else '0';
----------------------------------------------------------------------
p_host_feedback_proc : process(clock_int)
begin
    if rising_edge(clock_int) then
        if reset_int = '1' then
            o_1st_4GB_rx_addr <= x"00000000";
            o_2nd_4GB_rx_addr <= x"00000000";
            o_3rd_4GB_rx_addr <= x"00000000";
            o_4th_4GB_rx_addr <= x"00000000";
        else
            if hbm_target_interface = "00" then
                o_1st_4GB_rx_addr   <= hbm_wr_addr_calc(31 downto 0);
            end if;
            if hbm_target_interface = "01" then
                o_2nd_4GB_rx_addr   <= hbm_wr_addr_calc(31 downto 0);
            end if;
            if hbm_target_interface = "10" then
                o_3rd_4GB_rx_addr   <= hbm_wr_addr_calc(31 downto 0);
            end if;
            if hbm_target_interface = "11" then
                o_4th_4GB_rx_addr   <= hbm_wr_addr_calc(31 downto 0);
            end if;
        end if;
    end if;
end process;

----------------------------------------------------------------------
-- debug
o_input_fsm_debug   <= input_fsm_debug;

input_fsm_debug     <=  x"0" when input_fsm = idle else 
                        x"1" when input_fsm = wait_on_fifo else
                        x"2" when input_fsm = generate_addr else
                        x"3" when input_fsm = load_addr else
                        x"4" when input_fsm = drain_fifo else
                        x"5" when input_fsm = complete else
                        x"F";
                        

debug_HBM_writes : IF g_DEBUG_ILAs GENERATE 
    hbm_playout_ila : ila_0
        port map (
        clk                     => i_shared_clk, 
        probe0(33 downto 0)     => hbm_wr_addr_calc,
        probe0(35 downto 34)    => hbm_target_interface,
        probe0(36)              => hbm_axi_wvalid,
        probe0(37)              => hbm_axi_wready,

        probe0(38)              => m01_axi_wvalid_int,
        probe0(39)              => m01_axi_wlast_int,
        probe0(40)              => m01_axi_wready,
        probe0(41)              => m02_axi_wvalid_int,
        probe0(42)              => m02_axi_wlast_int,
        probe0(43)              => m02_axi_wready,

        probe0(44)              => m01_axi_awvalid_int,
        probe0(45)              => m01_axi_awready,
        probe0(46)              => hbm_axi_awready,
        probe0(47)              => hbm_axi_awvalid,
        probe0(48)              => m02_axi_awvalid_int,
        probe0(49)              => m02_axi_awready,

        probe0(51 downto 50)    => (others => '0'),
        
        probe0(63 downto 52)    => std_logic_vector(fifo_burst_count),
        probe0(95 downto 64)    => hbm_axi_awaddr,
        probe0(127 downto 96)   => (others => '0'),

        probe0(140 downto 128)  => data_cache_fifo_rd_count,
        probe0(141)             => data_cache_fifo_wr_w_fl,
        probe0(142)             => data_cache_fifo_wr,
        probe0(143)             => flush_fifo_to_hbm_local,
        probe0(144)             => data_cache_fifo_rd,
        probe0(145)             => data_cache_fifo_empty,
        probe0(187 downto 146)  => (others => '0'),
        probe0(191 downto 188)  => input_fsm_debug
        
        );
end generate;

end Behavioral;
