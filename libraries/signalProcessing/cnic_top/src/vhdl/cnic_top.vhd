-------------------------------------------------------------------------------
--
-- File Name: cnic_top.vhd
-- Contributing Authors: Giles Babich - Giles.babich@csiro.au, Jason van Aardt, jason.vanaardt@csiro.au
-- Type: RTL
-- Created: 27 October 2021
--
-- Title: Top Level for the cnic (Traffic Generator)
--
--
--  Distributed under the terms of the CSIRO Open Source Software Licence Agreement
--  See the file LICENSE for more info.
--
-- Design updated June 2022 by Giles to include
--      * 4 x 4G HBM buffers (4G limit dictated by XRT limitations)
--      * Capture and playback from whole HBM
--      * Timestamping of incoming packets
--      * Filtering of incoming packets based on byte length
--      * TX or RX packets base on software configured byte size, only one siz at a time supported.
--      * control registers renamed and general code tidy up.
--      * PTP scheduler expanded to stop and start for rx and tx independently.
--
-------------------------------------------------------------------------------

LIBRARY IEEE, common_lib, axi4_lib, cmac_s_axi_lib, vd_lib, Timeslave_CMAC_lib, vd_datagen_lib, ethernet_lib;
library HBM_PktController_lib, cnic_lib, PSR_Packetiser_lib, signal_processing_common, spead_sps_lib;

USE cnic_lib.cnic_top_pkg.all;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.NUMERIC_STD.ALL;
USE common_lib.common_pkg.ALL;
USE common_lib.common_mem_pkg.ALL;
USE axi4_lib.axi4_lite_pkg.ALL;
USE axi4_lib.axi4_stream_pkg.ALL;
USE axi4_lib.axi4_full_pkg.ALL;

USE ethernet_lib.ethernet_pkg.ALL;
USE PSR_Packetiser_lib.CbfPsrHeader_pkg.ALL;
USE Timeslave_CMAC_lib.timer_pkg.ALL;
USE HBM_PktController_lib.HBM_PktController_hbm_pktcontroller_reg_pkg.ALL;
USE vd_datagen_lib.vd_datagen_reg_pkg.ALL;
USE spead_sps_lib.spead_sps_reg_pkg.ALL;
USE vd_lib.VD_vd_reg_pkg.ALL;

library technology_lib;
USE technology_lib.tech_mac_100g_pkg.ALL;

library xpm;
USE xpm.vcomponents.all;

-------------------------------------------------------------------------------
entity cnic_top is
    generic (
        G_NO_OF_TIMERS                  : INTEGER := 2;
        g_DEBUG_ILA                     : BOOLEAN := FALSE;
        g_VD_DEBUG_ILA                  : BOOLEAN := FALSE;
        g_CODIF_MODIFIER_HEADER_BLOCK   : BOOLEAN := FALSE;
        g_VD_data_gen                   : BOOLEAN := TRUE;
        g_LBUS_CMAC                     : BOOLEAN := FALSE;
        g_ALVEO_U55                     : BOOLEAN := FALSE;
        
        g_ALVEO_U280                    : BOOLEAN := FALSE;
        g_VD_2_instances                : integer := 8;
        
        g_VD_W_DUPLEX                   : BOOLEAN := TRUE;

        g_VD_GEN_CLK_275M               : BOOLEAN := FALSE;
        g_HBM_bank_size                 : string  := "4095MB";
        g_128_SAMPLE_BLOCKS             : integer := 1024;
        g_N128_SOURCE_INSTANCES         : integer := 8
    );
    port (
        clk_freerun : in std_logic;
        -----------------------------------------------------------------------
        -- 100G TX
        -- CMAC LBUS
        -- Received data from 100GE
        i_data_rx_sosi      : in t_lbus_sosi;
        -- Data to be transmitted on 100GE
        o_data_tx_sosi      : out t_lbus_sosi;
        i_data_tx_siso      : in t_lbus_siso;
        
        -- streaming AXI to CMAC
        i_clk_100GE         : in std_logic;
        i_eth100G_locked    : in std_logic;
        o_tx_axis_tdata     : OUT STD_LOGIC_VECTOR(511 downto 0);
        o_tx_axis_tkeep     : OUT STD_LOGIC_VECTOR(63 downto 0);
        o_tx_axis_tvalid    : OUT STD_LOGIC;
        o_tx_axis_tlast     : OUT STD_LOGIC;
        o_tx_axis_tuser     : OUT STD_LOGIC;
        i_tx_axis_tready    : in STD_LOGIC;
                
        -------
        -- 100G RX
        -- RX
        i_rx_axis_tdata     : in STD_LOGIC_VECTOR ( 511 downto 0 );
        i_rx_axis_tkeep     : in STD_LOGIC_VECTOR ( 63 downto 0 );
        i_rx_axis_tlast     : in STD_LOGIC;
        o_rx_axis_tready    : out STD_LOGIC;
        i_rx_axis_tuser     : in STD_LOGIC_VECTOR ( 79 downto 0 );
        i_rx_axis_tvalid    : in STD_LOGIC;
        
        -----------------------------------------------------------------------
        -- streaming AXI to CMAC 2nd interface.
        
        i_clk_100GE_b       : in std_logic;
        i_eth100G_locked_b  : in std_logic;
        o_tx_axis_tdata_b   : OUT STD_LOGIC_VECTOR(511 downto 0);
        o_tx_axis_tkeep_b   : OUT STD_LOGIC_VECTOR(63 downto 0);
        o_tx_axis_tvalid_b  : OUT STD_LOGIC;
        o_tx_axis_tlast_b   : OUT STD_LOGIC;
        o_tx_axis_tuser_b   : OUT STD_LOGIC;
        i_tx_axis_tready_b  : in STD_LOGIC;

        -----------------------------------------------------------------------
        -- Debug signal used in the testbench.
        o_validMemRstActive : out std_logic;  -- reset of the valid memory is in progress.
        -----------------------------------------------------------------------
        -- MACE AXI slave interfaces for modules
        -- The 300MHz MACE_clk is also used for some of the signal processing
        i_MACE_clk  : in std_logic;
        i_MACE_rst  : in std_logic;
        
        i_HBM_Pktcontroller_Lite_axi_mosi : in t_axi4_lite_mosi; 
        o_HBM_Pktcontroller_Lite_axi_miso : out t_axi4_lite_miso;
          
        -----------------------------------------------------------------------
        i_schedule_action           : in std_logic_vector(7 downto 0);
        o_timer_fields_in           : out timer_fields_in_arr((G_NO_OF_TIMERS-1) downto 0);
        i_timer_fields_out          : in timer_fields_out_arr((G_NO_OF_TIMERS-1) downto 0);
        -----------------------------------------------------------------------

        i_current_time              : in std_logic_vector(79 downto 0);

        -----------------------------------------------------------------------
        -- VD ARGs interface
        i_vd_lite_axi_mosi          : in t_axi4_lite_mosi_arr(1 downto 0); 
        o_vd_lite_axi_miso          : out t_axi4_lite_miso_arr(1 downto 0);
        i_vd_full_axi_mosi          : in  t_axi4_full_mosi_arr(1 downto 0);
        o_vd_full_axi_miso          : out t_axi4_full_miso_arr(1 downto 0);

        -- VD_Gen ARGs interface
        i_vd_gen_lite_axi_mosi      : in t_axi4_lite_mosi;
        o_vd_gen_lite_axi_miso      : out t_axi4_lite_miso;
        i_vd_gen_full_axi_mosi      : in  t_axi4_full_mosi;
        o_vd_gen_full_axi_miso      : out t_axi4_full_miso;

        -- VD_Gen ARGs interface
        i_vd_gen_2_lite_axi_mosi    : in t_axi4_lite_mosi;
        o_vd_gen_2_lite_axi_miso    : out t_axi4_lite_miso;
        i_vd_gen_2_full_axi_mosi    : in  t_axi4_full_mosi;
        o_vd_gen_2_full_axi_miso    : out t_axi4_full_miso;
	--------------------------------------------------------------------------
	-- M01
        m01_axi_awvalid  : out std_logic;
        m01_axi_awready  : in std_logic;
        m01_axi_awaddr   : out std_logic_vector(31 downto 0);
        m01_axi_awlen    : out std_logic_vector(7 downto 0);
        -- w bus - write data
        m01_axi_wvalid    : out std_logic;
        m01_axi_wready    : in std_logic;
        m01_axi_wdata     : out std_logic_vector(511 downto 0);
        m01_axi_wlast     : out std_logic;
        -- b bus - write response
        m01_axi_bvalid    : in std_logic;
        m01_axi_bresp     : in std_logic_vector(1 downto 0);
        -- ar bus - read address
        m01_axi_arvalid   : out std_logic;
        m01_axi_arready   : in std_logic;
        m01_axi_araddr    : out std_logic_vector(31 downto 0);
        m01_axi_arlen     : out std_logic_vector(7 downto 0);
        -- r bus - read data
        m01_axi_rvalid    : in std_logic;
        m01_axi_rready    : out std_logic;
        m01_axi_rdata     : in std_logic_vector(511 downto 0);
        m01_axi_rlast     : in std_logic;
        m01_axi_rresp     : in std_logic_vector(1 downto 0);

	--------------------------------------------------------------------------
	-- m02
        m02_axi_awvalid  : out std_logic;
        m02_axi_awready  : in std_logic;
        m02_axi_awaddr   : out std_logic_vector(31 downto 0);
        m02_axi_awlen    : out std_logic_vector(7 downto 0);
        -- w bus - write data
        m02_axi_wvalid    : out std_logic;
        m02_axi_wready    : in std_logic;
        m02_axi_wdata     : out std_logic_vector(511 downto 0);
        m02_axi_wlast     : out std_logic;
        -- b bus - write response
        m02_axi_bvalid    : in std_logic;
        m02_axi_bresp     : in std_logic_vector(1 downto 0);
        -- ar bus - read address
        m02_axi_arvalid   : out std_logic;
        m02_axi_arready   : in std_logic;
        m02_axi_araddr    : out std_logic_vector(31 downto 0);
        m02_axi_arlen     : out std_logic_vector(7 downto 0);
        -- r bus - read data
        m02_axi_rvalid    : in std_logic;
        m02_axi_rready    : out std_logic;
        m02_axi_rdata     : in std_logic_vector(511 downto 0);
        m02_axi_rlast     : in std_logic;
        m02_axi_rresp     : in std_logic_vector(1 downto 0);

	--------------------------------------------------------------------------
	-- m03
        m03_axi_awvalid  : out std_logic;
        m03_axi_awready  : in std_logic;
        m03_axi_awaddr   : out std_logic_vector(31 downto 0);
        m03_axi_awlen    : out std_logic_vector(7 downto 0);
        -- w bus - write data
        m03_axi_wvalid    : out std_logic;
        m03_axi_wready    : in std_logic;
        m03_axi_wdata     : out std_logic_vector(511 downto 0);
        m03_axi_wlast     : out std_logic;
        -- b bus - write response
        m03_axi_bvalid    : in std_logic;
        m03_axi_bresp     : in std_logic_vector(1 downto 0);
        -- ar bus - read address
        m03_axi_arvalid   : out std_logic;
        m03_axi_arready   : in std_logic;
        m03_axi_araddr    : out std_logic_vector(31 downto 0);
        m03_axi_arlen     : out std_logic_vector(7 downto 0);
        -- r bus - read data
        m03_axi_rvalid    : in std_logic;
        m03_axi_rready    : out std_logic;
        m03_axi_rdata     : in std_logic_vector(511 downto 0);
        m03_axi_rlast     : in std_logic;
        m03_axi_rresp     : in std_logic_vector(1 downto 0);

	--------------------------------------------------------------------------
	-- m04
        m04_axi_awvalid  : out std_logic;
        m04_axi_awready  : in std_logic;
        m04_axi_awaddr   : out std_logic_vector(31 downto 0);
        m04_axi_awlen    : out std_logic_vector(7 downto 0);
        -- w bus - write data
        m04_axi_wvalid    : out std_logic;
        m04_axi_wready    : in std_logic;
        m04_axi_wdata     : out std_logic_vector(511 downto 0);
        m04_axi_wlast     : out std_logic;
        -- b bus - write response
        m04_axi_bvalid    : in std_logic;
        m04_axi_bresp     : in std_logic_vector(1 downto 0);
        -- ar bus - read address
        m04_axi_arvalid   : out std_logic;
        m04_axi_arready   : in std_logic;
        m04_axi_araddr    : out std_logic_vector(31 downto 0);
        m04_axi_arlen     : out std_logic_vector(7 downto 0);
        -- r bus - read data
        m04_axi_rvalid    : in std_logic;
        m04_axi_rready    : out std_logic;
        m04_axi_rdata     : in std_logic_vector(511 downto 0);
        m04_axi_rlast     : in std_logic;
        m04_axi_rresp     : in std_logic_vector(1 downto 0)


    );
END cnic_top;

-------------------------------------------------------------------------------
ARCHITECTURE structure OF cnic_top IS

    COMPONENT ila_0
    PORT (
        clk : IN STD_LOGIC;
        probe0 : IN STD_LOGIC_VECTOR(191 DOWNTO 0));
    END COMPONENT;

    COMPONENT axis_data_fifo_loopback 
    Port ( 
          s_axis_aresetn : in STD_LOGIC;
          s_axis_aclk : in STD_LOGIC;
          s_axis_tvalid : in STD_LOGIC;
          s_axis_tready : out STD_LOGIC;
          s_axis_tdata : in STD_LOGIC_VECTOR ( 511 downto 0 );
          s_axis_tkeep : in STD_LOGIC_VECTOR ( 63 downto 0 );
          s_axis_tlast : in STD_LOGIC;
          m_axis_aclk : in STD_LOGIC;
          m_axis_tvalid : out STD_LOGIC;
          m_axis_tready : in STD_LOGIC;
          m_axis_tdata : out STD_LOGIC_VECTOR ( 511 downto 0 );
          m_axis_tkeep : out STD_LOGIC_VECTOR ( 63 downto 0 );
          m_axis_tlast : out STD_LOGIC
          );
    END COMPONENT;

    component clk_VD_GEN is
        Port ( 
            clk_vd : out STD_LOGIC;
            clk_in : in STD_LOGIC
        );
    end component;
    
    component clk_VD_275_GEN is
        Port ( 
            clk_275_vd : out STD_LOGIC;
            clk_in : in STD_LOGIC
        );
    end component;

    ---------------------------------------------------------------------------
    -- SIGNAL DECLARATIONS  --
    --------------------------------------------------------------------------- 
    signal clk                                  : std_logic;
    signal reset                                : std_logic;

    signal start_stop_tx : std_logic;

    signal packet_player_rdy                    : std_logic;
    signal packet_player_rdy_d                  : std_logic;

    -- HBM packets
    signal hbm_packetiser_data_in_wr            : std_logic;
    signal hbm_packetiser_data                  : std_logic_vector(511 downto 0);
    signal hbm_packetiser_bytes_to_transmit     : std_logic_vector(13 downto 0);

    signal hbm_swapped_packetiser_data          : std_logic_vector(511 downto 0);

    -- CODIF header modifier
    signal codif_hdr_mod_data_in_wr             : std_logic;
    signal codif_hdr_mod_data                   : std_logic_vector(511 downto 0);
    signal codif_hdr_mod_bytes_to_transmit      : std_logic_vector(13 downto 0);

    -- VD
    signal vd_data_in_wr                        : std_logic;
    signal vd_data                              : std_logic_vector(511 downto 0);
    signal vd_bytes_to_transmit                 : std_logic_vector(13 downto 0);
    
    -- first 100G interface packet player
    signal packet_player_1_data_in_wr           : std_logic;
    signal packet_player_1_data                 : std_logic_vector(511 downto 0);
    signal packet_player_1_bytes_to_transmit    : std_logic_vector(13 downto 0);

    signal packet_player_mux_data_in_wr         : std_logic;
    signal packet_player_mux_data               : std_logic_vector(511 downto 0);
    signal packet_player_mux_bytes_to_transmit  : std_logic_vector(13 downto 0);

    -- second 100G interface packet player
    signal packetiser_data_to_player_rdy_b      : std_logic;
    signal packetiser_data_to_player_rdy_combo  : std_logic;
       
   
    signal beamData : std_logic_vector(63 downto 0);
    signal beamPacketCount : std_logic_vector(36 downto 0);
    signal beamBeam : std_logic_vector(7 downto 0);
    signal beamFreqIndex : std_logic_vector(10 downto 0);
    signal beamValid : std_logic;
    signal cmac_ready : std_logic;
    signal i_reset_packet_player : std_logic;

    
    signal eth100G_reset : std_logic;
    signal eth100G_reset_b : std_logic;

    signal dbg_ILA_trigger, bdbg_ILA_triggerDel1, bdbg_ILA_trigger, bdbg_ILA_triggerDel2 : std_logic;
    
    signal rx_packet_size           : std_logic_vector(13 downto 0);     -- Max size is 9000.
    signal rx_reset_capture         : std_logic;
    signal rx_reset_counter         : std_logic;
    
    -- register interface
    signal config_rw                : t_config_rw;
    signal config_ro                : t_config_ro;
    
    signal data_to_hbm              : std_logic_vector(511 downto 0);
    signal data_to_hbm_wr           : std_logic;
    
    signal rx_packet_size_mod64b    : std_logic_vector(13 downto 0);
    signal packet_size_calc         : std_logic_vector(13 downto 0);
    signal packet_size_calc_b       : std_logic_vector(13 downto 0);
    signal packet_size_ceil         : std_logic_vector(5 downto 0) := "000000";

    signal rx_complete_hbm          : std_logic;
    signal s_axi_enable_capture     : std_logic;

    --------------------------------------
    signal loopback_select          : std_logic;

    signal loopback_m_axis_tvalid   : STD_LOGIC;
    signal loopback_m_axis_tdata    : STD_LOGIC_VECTOR(511 downto 0);
    signal loopback_m_axis_tkeep    : STD_LOGIC_VECTOR(63 downto 0);
    signal loopback_m_axis_tlast    : STD_LOGIC;

    signal player_m_axis_tvalid     : STD_LOGIC;
    signal player_m_axis_tdata      : STD_LOGIC_VECTOR(511 downto 0);
    signal player_m_axis_tkeep      : STD_LOGIC_VECTOR(63 downto 0);
    signal player_m_axis_tlast      : STD_LOGIC;

    --------------------------------------
    -- VD signals
    signal vd_enable                : std_logic;

    -- HBM muxing signal, from HBM controller or VD
    signal hbm_m03_axi_awvalid      : std_logic := '0';
    signal hbm_m03_axi_awaddr       : std_logic_vector(31 downto 0) := (others => '0');
    signal hbm_m03_axi_awlen        : std_logic_vector(7 downto 0)  := (others => '0');
    --w bus
    signal hbm_m03_axi_wvalid       : std_logic := '0';
    signal hbm_m03_axi_wdata        : std_logic_vector(511 downto 0) := (others => '0');
    signal hbm_m03_axi_wlast        : std_logic := '0';
    -- ar bus - read address
    signal hbm_m03_axi_arvalid      : std_logic;
    signal hbm_m03_axi_araddr       : std_logic_vector(31 downto 0);
    signal hbm_m03_axi_arlen        : std_logic_vector(7 downto 0);
    -- r bus - read data
    signal hbm_m03_axi_rready       : std_logic;
    
    signal vd_m03_axi_awvalid       : std_logic := '0';
    signal vd_m03_axi_awaddr        : std_logic_vector(31 downto 0) := (others => '0');
    signal vd_m03_axi_awlen         : std_logic_vector(7 downto 0)  := (others => '0');
    --w bus
    signal vd_m03_axi_wvalid        : std_logic := '0';
    signal vd_m03_axi_wdata         : std_logic_vector(511 downto 0) := (others => '0');
    signal vd_m03_axi_wlast         : std_logic := '0';
    -- ar bus - read address
    signal vd_m03_axi_arvalid       : std_logic;
    signal vd_m03_axi_araddr        : std_logic_vector(31 downto 0);
    signal vd_m03_axi_arlen         : std_logic_vector(7 downto 0);
    -- r bus - read data
    signal vd_m03_axi_rready        : std_logic;
    
    -- HBM muxing signal, from HBM controller or VD
    signal hbm_m04_axi_awvalid      : std_logic := '0';
    signal hbm_m04_axi_awaddr       : std_logic_vector(31 downto 0) := (others => '0');
    signal hbm_m04_axi_awlen        : std_logic_vector(7 downto 0)  := (others => '0');
    --w bus
    signal hbm_m04_axi_wvalid       : std_logic := '0';
    signal hbm_m04_axi_wdata        : std_logic_vector(511 downto 0) := (others => '0');
    signal hbm_m04_axi_wlast        : std_logic := '0';
    -- ar bus - read address
    signal hbm_m04_axi_arvalid      : std_logic;
    signal hbm_m04_axi_araddr       : std_logic_vector(31 downto 0);
    signal hbm_m04_axi_arlen        : std_logic_vector(7 downto 0);
    -- r bus - read data
    signal hbm_m04_axi_rready       : std_logic;
    
    signal vd_m04_axi_awvalid       : std_logic := '0';
    signal vd_m04_axi_awaddr        : std_logic_vector(31 downto 0) := (others => '0');
    signal vd_m04_axi_awlen         : std_logic_vector(7 downto 0)  := (others => '0');
    --w bus
    signal vd_m04_axi_wvalid        : std_logic := '0';
    signal vd_m04_axi_wdata         : std_logic_vector(511 downto 0) := (others => '0');
    signal vd_m04_axi_wlast         : std_logic := '0';
    -- ar bus - read address
    signal vd_m04_axi_arvalid       : std_logic;
    signal vd_m04_axi_araddr        : std_logic_vector(31 downto 0);
    signal vd_m04_axi_arlen         : std_logic_vector(7 downto 0);
    -- r bus - read data
    signal vd_m04_axi_rready        : std_logic;
    
    signal writing_buf0             : std_logic_vector(1 downto 0);
    signal writing_buf1             : std_logic_vector(1 downto 0);
    signal start_vd_gen             : std_logic_vector(1 downto 0);
    signal reset_vd_gen             : std_logic_vector(1 downto 0);

    signal eth100G_locked_int       : std_logic;

    -- dummy mappings for non-target alveo axi bus.
    signal spead_rw_dummy           : t_spead_ctrl_rw;
    signal spead_ro_dummy           : t_spead_ctrl_ro;

    signal vd_rw_dummy              : t_vd_reg_rw;
    signal vd_ro_dummy              : t_vd_reg_ro;

    signal vd_gen_rw_dummy          : t_vd_datagen_reg_rw;
    signal vd_gen_ro_dummy          : t_vd_datagen_reg_ro;

    signal vd_gen_rw_dummy_2        : t_vd_datagen_reg_rw;
    signal vd_gen_ro_dummy_2        : t_vd_datagen_reg_ro;

    signal clk_gen1_vd_reset        : std_logic := '1';
    signal clk_gen1_vd_reset_gen    : std_logic := '1';
    signal clk_gen1_vd_reset_cnt    : unsigned(7 downto 0) := x"00";
    
    signal clk_gen2_vd_reset        : std_logic := '1';
    signal clk_gen2_vd_reset_gen    : std_logic := '1';
    signal clk_gen2_vd_reset_cnt    : unsigned(7 downto 0) := x"00";
    
    signal clk_gen_1_vd             : std_logic;
    signal clk_gen_2_vd             : std_logic;
    
    signal codif_output_select      : std_logic;

    signal tx_enable                : std_logic;
    signal tx_reset                 : std_logic;

    signal vd_schedule_action       : std_logic_vector(7 downto 0);
    signal hbm_schedule_action      : std_logic_vector(7 downto 0);
    
    signal vd_reset_packet_player   : std_logic;
    signal hbm_reset_packet_player  : std_logic;
    
    signal enable_duplex            : std_logic_vector(1 downto 0);

begin

-------------------------------------------------------------------------------------------------------------

clk     <= i_MACE_clk;
reset   <= i_MACE_rst;

reg_inc_sig_proc : process(i_clk_100GE)
begin
    if rising_edge(i_clk_100GE) then
        eth100G_locked_int  <= i_eth100G_locked;
    
    end if;
end process;
-------------------------------------------------------------------------------------------------------------



-------------------------------------------------------------------------------------------------------------
-- Clock VD generate run on.
-- Non-scalable 300 MHz.
-- Cycles required by VD Datagen TBD.

VD_300MHz_clk : IF (NOT g_VD_GEN_CLK_275M) GENERATE
    i_clk_vdgen_1 : clk_VD_GEN
    port map ( 
        clk_vd  => clk_gen_1_vd,
        clk_in  => clk_freerun
    );
    
    i_clk_vdgen_2 : clk_VD_GEN
    port map ( 
        clk_vd  => clk_gen_2_vd,
        clk_in  => clk_freerun
    );
END GENERATE;

VD_slow : IF g_VD_GEN_CLK_275M GENERATE
    i_clk_275_vdgen_1 : clk_VD_275_GEN
    port map ( 
        clk_275_vd  => clk_gen_1_vd,
        clk_in      => clk_freerun
    );
    
    i_clk_275_vdgen_2 : clk_VD_275_GEN
    port map ( 
        clk_275_vd  => clk_gen_2_vd,
        clk_in      => clk_freerun
    );
END GENERATE;

-- clk_vdgen reset
vd_gen_1_reset : process(clk_gen_1_vd)
begin
    if rising_edge(clk_gen_1_vd) then
        if (clk_gen1_vd_reset_cnt(7) = '1') then
            clk_gen1_vd_reset_gen    <= '0';
        else
            clk_gen1_vd_reset_gen    <= '1';
            clk_gen1_vd_reset_cnt    <= clk_gen1_vd_reset_cnt + 1;
        end if;

        clk_gen1_vd_reset   <= clk_gen1_vd_reset_gen;
    end if;
end process;

vd_gen_2_reset : process(clk_gen_2_vd)
begin
    if rising_edge(clk_gen_2_vd) then
        if (clk_gen2_vd_reset_cnt(7) = '1') then
            clk_gen2_vd_reset_gen    <= '0';
        else
            clk_gen2_vd_reset_gen    <= '1';
            clk_gen2_vd_reset_cnt    <= clk_gen2_vd_reset_cnt + 1;
        end if;

        clk_gen2_vd_reset   <= clk_gen2_vd_reset_gen;
    end if;
end process;

-------------------------------------------------------------------------------------------------------------

config_ro.rx_complete   <= rx_complete_hbm;

-------------------------------------------------------------------------------------------------------------
    s_axi_enable_capture    <= config_rw.rx_enable_capture OR (i_schedule_action(3) XOR i_schedule_action(4));

    rx_s_axi : entity cmac_s_axi_lib.s_axi_packet_capture 
    Port map ( 
        --------------------------------------------------------
        -- 100G 
        i_clk_100GE             => i_clk_100GE,
        i_eth100G_locked        => eth100G_locked_int,
        
        i_clk_300               => clk,
        i_clk_300_rst           => i_MACE_rst,

        i_enable_capture        => s_axi_enable_capture,
        
        i_rx_packet_size        => config_rw.rx_packet_size(13 downto 0),
        i_rx_reset_capture      => config_rw.rx_reset_capture,
        i_reset_counter         => config_rw.rx_reset_counter,
        i_rx_packet_size_abs    => config_rw.rx_packet_size_abs,

        o_target_count          => config_ro.debug_capture_filter_target,
        o_nontarget_count       => config_ro.debug_capture_filter_non_target,

        i_rx_packets_to_capture => config_rw.rx_packets_to_capture,

        -- 100G RX S_AXI interface ~322 MHz
        i_rx_axis_tdata         => i_rx_axis_tdata,
        i_rx_axis_tkeep         => i_rx_axis_tkeep,
        i_rx_axis_tlast         => i_rx_axis_tlast,
        o_rx_axis_tready        => o_rx_axis_tready,
        i_rx_axis_tuser         => i_rx_axis_tuser,
        i_rx_axis_tvalid        => i_rx_axis_tvalid,
        
        -- Data to HBM writer - 300 MHz
        o_data_to_hbm           => data_to_hbm,
        o_data_to_hbm_wr        => data_to_hbm_wr
    
    );
    
-------------------------------------------------------------------------------------------------------------    
    ARGS_register_HBM_PktController : entity HBM_PktController_lib.HBM_PktController_hbm_pktcontroller_reg
    port map (
        MM_CLK              => clk, 
        MM_RST              => i_MACE_rst, 
        SLA_IN              => i_HBM_Pktcontroller_Lite_axi_mosi,  -- IN    t_axi4_lite_mosi;
        SLA_OUT             => o_HBM_Pktcontroller_Lite_axi_miso,  -- OUT   t_axi4_lite_miso;

        CONFIG_FIELDS_RW    => config_rw, -- OUT t_config_rw;
        CONFIG_FIELDS_RO    => config_ro  
    );


-------------------------------------------------------------------------------------------------------------



HBM_controller_proc : process(clk)
begin
    if rising_edge(clk) then
        if config_rw.rx_reset_capture = '1' then
            packet_size_ceil(0)             <= config_rw.rx_packet_size(5) OR config_rw.rx_packet_size(4) OR config_rw.rx_packet_size(3) OR config_rw.rx_packet_size(2) OR config_rw.rx_packet_size(1) OR config_rw.rx_packet_size(0); 
            packet_size_calc(13 downto 6)   <= std_logic_vector(unsigned(config_rw.rx_packet_size(13 downto 6)) + unsigned(packet_size_ceil));
            packet_size_calc(5 downto 0)    <= "000000";
            
            -- add extra 64 bytes for timestamp/metadata vector.
            packet_size_calc_b(13 downto 6) <= std_logic_vector(unsigned(packet_size_calc(13 downto 6)) + x"01");
            packet_size_calc_b(5 downto 0)  <= packet_size_calc(5 downto 0);  
             
            rx_packet_size_mod64b           <= packet_size_calc_b;        
        end if;

        tx_enable   <= config_rw.tx_enable;
        tx_reset    <= config_rw.tx_reset;

        config_ro.tx_running   <= config_rw.tx_enable;
        
        enable_duplex   <= config_rw.enable_duplex(1 downto 0);

    end if;
end process;

i_HBM_PktController : entity HBM_PktController_lib.HBM_PktController
    generic map (
        g_HBM_bank_size                 => g_HBM_bank_size
    )
    port map (
        clk_freerun                     => clk_freerun, 
        -- shared memory interface clock (300 MHz)
        i_shared_clk                    => clk, -- in std_logic;
        i_shared_rst                    => i_MACE_rst, -- in std_logic;

        o_reset_packet_player           => hbm_reset_packet_player, 

        ------------------------------------------------------------------------------------
        -- Data from CMAC module after CDC in shared memory clock domain
        i_data_from_cmac                => data_to_hbm,
        i_data_valid_from_cmac          => data_to_hbm_wr,

        ------------------------------------------------------------------------------------
        -- config and status registers interface
        -- rx
    	i_rx_packet_size                    => rx_packet_size_mod64b,--config_rw.packet_size(13 downto 0),
        i_rx_soft_reset                     => config_rw.rx_reset_capture,
        i_enable_capture                    => config_rw.rx_enable_capture,
        
        i_lfaa_bank1_addr                   => x"00000000",
        i_lfaa_bank2_addr                   => x"00000000",
        i_lfaa_bank3_addr                   => x"00000000",
        i_lfaa_bank4_addr                   => x"00000000",
        update_start_addr                   => '0',
        i_rx_bank_enable                    => config_rw.rx_bank_enable(3 downto 0),

        o_1st_4GB_rx_addr                   => config_ro.rx_hbm_1_end_addr,
        o_2nd_4GB_rx_addr                   => config_ro.rx_hbm_2_end_addr,
        o_3rd_4GB_rx_addr                   => config_ro.rx_hbm_3_end_addr,
        o_4th_4GB_rx_addr                   => config_ro.rx_hbm_4_end_addr,

        o_capture_done                      => rx_complete_hbm,
        o_num_packets_received              => config_ro.rx_packet_count_lo,
        i_rx_packets_to_capture             => config_rw.rx_packets_to_capture,
        i_rx_flush_to_hbm                   => config_rw.rx_flush_to_hbm,

        -- tx
        i_tx_packet_size                    => config_rw.tx_packet_size(13 downto 0),
        i_start_tx                          => tx_enable,
        i_reset_tx                          => tx_reset,
      
        i_loop_tx                           => config_rw.tx_loop_enable,
        i_expected_number_of_loops          => config_rw.tx_loops,
        
        i_expected_total_number_of_4k_axi   => config_rw.tx_axi_transactions,               -- total number of reads out of HBM
        i_expected_number_beats_per_burst   => config_rw.tx_beats_per_burst(12 downto 0),   -- 1 x tx_beats_per_packet
        i_expected_beats_per_packet         => config_rw.tx_beats_per_packet,               -- number of cycles per packet rd.
        
        i_expected_packets_per_burst        => config_rw.tx_packets_per_burst,              -- hard coded to 1 as we don't burst currently.
	    i_expected_total_number_of_bursts   => config_rw.tx_bursts,                         -- total bursts will be 1 x total number of packets to send.
        
        i_time_between_bursts_ns            => config_rw.tx_burst_gap,                      -- GAP between start of packets
        
        i_readaddr                          => x"00000000", 
        i_update_readaddr                   => '0',

        i_enable_duplex                     => enable_duplex,

        o_tx_addr                           => config_ro.debug_tx_current_hbm_rd_addr,
        o_tx_boundary_across_num            => config_ro.debug_tx_current_hbm_rd_buffer(1 downto 0),
        o_axi_rvalid_but_fifo_full          => open,

        o_tx_complete                       => config_ro.tx_complete,
        o_tx_packets_to_mac(63 downto 32)   => config_ro.tx_packets_to_mac_hi,
        o_tx_packets_to_mac(31 downto 0)    => config_ro.tx_packets_to_mac_lo,
        o_tx_packet_count(63 downto 32)     => config_ro.tx_packet_count_hi,
        o_tx_packet_count(31 downto 0)      => config_ro.tx_packet_count_lo,

        i_legacy_rate_sel                   => config_rw.legacy_rate_sel,
        ------------------------------------------------------------------------------------

        o_rd_fsm_debug                      => config_ro.debug_rd_fsm_debug(3 downto 0),
        o_output_fsm_debug                  => config_ro.debug_output_fsm_debug(3 downto 0),
        o_input_fsm_debug                   => config_ro.debug_input_fsm_debug(3 downto 0),
        
    ------------------------------------------------------------------------------------
        -- Data output, to the packetizer
            
        o_packetiser_data_in_wr             => hbm_packetiser_data_in_wr, 
        o_packetiser_data                   => hbm_packetiser_data, 
        o_packetiser_bytes_to_transmit      => hbm_packetiser_bytes_to_transmit, 
        i_packetiser_data_to_player_rdy     => packet_player_rdy, 
        
        -------------------------------------------------------------
        i_schedule_action                   => hbm_schedule_action,
        o_timer_fields_in                   => o_timer_fields_in(0),
        i_timer_fields_out                  => i_timer_fields_out(0),
        -------------------------------------------------------------
          
        i_current_time                      => i_current_time,   
        i_use_timestamp_to_tx               => config_rw.tx_use_packet_timestamp_to_tx,
              
        -------------------------------------------------------------
        -- AXI bus to the shared memory. 
        -- This has the aw, b, ar and r buses (the w bus is on the output of the LFAA decode module)
        -- aw bus - write address
	-----------------------------------------------------------------
	-- M01
        m01_axi_awvalid => m01_axi_awvalid, -- out std_logic;
        m01_axi_awready => m01_axi_awready, -- in std_logic;
        m01_axi_awaddr  => m01_axi_awaddr,  
        m01_axi_awlen   => m01_axi_awlen,   -- out std_logic_vector(7 downto 0);
        --w bus
        m01_axi_wvalid  => m01_axi_wvalid,
        m01_axi_wdata   => m01_axi_wdata,
        --m01_axi_wstrb   => m01_axi_wstrb,
        m01_axi_wlast   => m01_axi_wlast,
        m01_axi_wready  => m01_axi_wready,
        -- ar bus - read address
        m01_axi_arvalid => m01_axi_arvalid, -- out std_logic;
        m01_axi_arready => m01_axi_arready, -- in std_logic;
        m01_axi_araddr  => m01_axi_araddr,  
        m01_axi_arlen   => m01_axi_arlen,   -- out std_logic_vector(7 downto 0);
        -- r bus - read data
        m01_axi_rvalid  => m01_axi_rvalid,  -- in std_logic;
        m01_axi_rready  => m01_axi_rready,  -- out std_logic;
        m01_axi_rdata   => m01_axi_rdata,   -- in std_logic_vector(511 downto 0);
        m01_axi_rlast   => m01_axi_rlast,   -- in std_logic;
        m01_axi_rresp   => m01_axi_rresp,    -- in std_logic_vector(1 downto 0);

	-----------------------------------------------------------------
	-- M02
	    m02_axi_awvalid => m02_axi_awvalid, -- out std_logic;
        m02_axi_awready => m02_axi_awready, -- in std_logic;
        m02_axi_awaddr  => m02_axi_awaddr,  
        m02_axi_awlen   => m02_axi_awlen,   -- out std_logic_vector(7 downto 0);
        --w bus
        m02_axi_wvalid  => m02_axi_wvalid,
        m02_axi_wdata   => m02_axi_wdata,
        --m02_axi_wstrb   => m02_axi_wstrb,
        m02_axi_wlast   => m02_axi_wlast,
        m02_axi_wready  => m02_axi_wready,
        -- ar bus - read address
        m02_axi_arvalid => m02_axi_arvalid, -- out std_logic;
        m02_axi_arready => m02_axi_arready, -- in std_logic;
        m02_axi_araddr  => m02_axi_araddr,  
        m02_axi_arlen   => m02_axi_arlen,   -- out std_logic_vector(7 downto 0);
        -- r bus - read data
        m02_axi_rvalid  => m02_axi_rvalid,  -- in std_logic;
        m02_axi_rready  => m02_axi_rready,  -- out std_logic;
        m02_axi_rdata   => m02_axi_rdata,   -- in std_logic_vector(511 downto 0);
        m02_axi_rlast   => m02_axi_rlast,   -- in std_logic;
        m02_axi_rresp   => m02_axi_rresp,    -- in std_logic_vector(1 downto 0);

	-----------------------------------------------------------------
	-- m03
        m03_axi_awvalid => hbm_m03_axi_awvalid, -- out std_logic;
        m03_axi_awready => m03_axi_awready, -- in std_logic;
        m03_axi_awaddr  => hbm_m03_axi_awaddr,  
        m03_axi_awlen   => hbm_m03_axi_awlen,   -- out std_logic_vector(7 downto 0);
        --w bus
        m03_axi_wvalid  => hbm_m03_axi_wvalid,
        m03_axi_wdata   => hbm_m03_axi_wdata,
        --m03_axi_wstrb   => m03_axi_wstrb,
        m03_axi_wlast   => hbm_m03_axi_wlast,
        m03_axi_wready  => m03_axi_wready,
        -- ar bus - read address
        m03_axi_arvalid => hbm_m03_axi_arvalid, -- out std_logic;
        m03_axi_arready => m03_axi_arready, -- in std_logic;
        m03_axi_araddr  => hbm_m03_axi_araddr,  
        m03_axi_arlen   => hbm_m03_axi_arlen,   -- out std_logic_vector(7 downto 0);
        -- r bus - read data
        m03_axi_rvalid  => m03_axi_rvalid,  -- in std_logic;
        m03_axi_rready  => hbm_m03_axi_rready,  -- out std_logic;
        m03_axi_rdata   => m03_axi_rdata,   -- in std_logic_vector(511 downto 0);
        m03_axi_rlast   => m03_axi_rlast,   -- in std_logic;
        m03_axi_rresp   => m03_axi_rresp,    -- in std_logic_vector(1 downto 0);

	-----------------------------------------------------------------
	-- m04
        m04_axi_awvalid => hbm_m04_axi_awvalid, -- out std_logic;
        m04_axi_awready => m04_axi_awready, -- in std_logic;
        m04_axi_awaddr  => hbm_m04_axi_awaddr,  
        m04_axi_awlen   => hbm_m04_axi_awlen,   -- out std_logic_vector(7 downto 0);
        --w bus
        m04_axi_wvalid  => hbm_m04_axi_wvalid,
        m04_axi_wdata   => hbm_m04_axi_wdata,
        --m04_axi_wstrb   => m04_axi_wstrb,
        m04_axi_wlast   => hbm_m04_axi_wlast,
        m04_axi_wready  => m04_axi_wready,
        -- ar bus - read address
        m04_axi_arvalid => hbm_m04_axi_arvalid, -- out std_logic;
        m04_axi_arready => m04_axi_arready, -- in std_logic;
        m04_axi_araddr  => hbm_m04_axi_araddr,  
        m04_axi_arlen   => hbm_m04_axi_arlen,   -- out std_logic_vector(7 downto 0);
        -- r bus - read data
        m04_axi_rvalid  => m04_axi_rvalid,  -- in std_logic;
        m04_axi_rready  => hbm_m04_axi_rready,  -- out std_logic;
        m04_axi_rdata   => m04_axi_rdata,   -- in std_logic_vector(511 downto 0);
        m04_axi_rlast   => m04_axi_rlast,   -- in std_logic;
        m04_axi_rresp   => m04_axi_rresp    -- in std_logic_vector(1 downto 0);

    );

-----------------------------------------------------------------------------------------
LBUS_VECTOR_GEN : IF g_LBUS_CMAC GENERATE
    -- Swap the packetizer data because of  bizarre CMAC 512 bit vector usage 
    GEN_SWITCHER:
    for n in 0 to 3 generate
    begin
        ROO:
        for i in 0 to 15 generate
            hbm_swapped_packetiser_data((128*n + 127 - i*8) downto (128*n + 127 - i*8 -7)) <= hbm_packetiser_data((128*n + i*8+7) downto (128*n+i*8));
        end generate ROO;
    end generate GEN_SWITCHER;
    
END GENERATE;

STREAMING_AXI_VECTOR_GEN : IF (NOT g_LBUS_CMAC) GENERATE
-- byte 0 = 7->0, byte 64 = 511 -> 504, no 128 bit swaps like LBUS.
    hbm_swapped_packetiser_data <= hbm_packetiser_data;


END GENERATE;    

-----------------------------------------------------------------------------------------    
-- Intercept UDP packet and modifier logic
-----------------------------------------------------------------------------------------   
gen_CODIF_mod : if (NOT g_ALVEO_U280) GENERATE
    header_mod : entity PSR_Packetiser_lib.CODIF_header_modifier
        Port Map( 
            i_clk                               => clk,
            i_reset                             => hbm_reset_packet_player,
            
            ----------------------------
            -- Math offsets for header incremeting
            i_codif_header_packets_per_frame	=> config_rw.tx_codif_header_packets_per_frame,
            i_codif_header_packets_epoch_offset => config_rw.tx_codif_header_packets_epoch_offset,
            i_codif_header_frame_initial_value	=> config_rw.tx_codif_header_frame_initial_value,
            
            -- FROM THE HBM_packet_controller 
            i_bytes_to_transmit                 => hbm_packetiser_bytes_to_transmit,
            i_data_to_player                    => hbm_swapped_packetiser_data,
            i_data_to_player_wr                 => hbm_packetiser_data_in_wr,
            
            -- TO THE Packet_player for CMAC
            o_bytes_to_transmit                 => codif_hdr_mod_bytes_to_transmit,
            o_data_to_player                    => codif_hdr_mod_data,
            o_data_to_player_wr                 => codif_hdr_mod_data_in_wr      
        
        );
END GENERATE;


-----------------------------------------------------------------------------------------    
-- virtual digitiser, only enable for U55 ALVEO
-----------------------------------------------------------------------------------------  
VD_GEN : if (g_ALVEO_U55) AND (g_VD_data_gen) GENERATE
    vd_inst : entity vd_lib.virtual_digitiser
        generic map (
            g_DEBUG_ILA             => g_VD_DEBUG_ILA
        )
        port map ( 
            -- clock used for all data input and output from this module (300 MHz)
            i_clk                   => clk,
            i_rst                   => reset,
    
            i_local_reset           => '0',
            
            o_reset_packet_player   => vd_reset_packet_player,
    
            -- data to packet_player
            o_bytes_to_transmit     => vd_bytes_to_transmit,
            o_data_to_player        => vd_data,
            o_data_to_player_wr     => vd_data_in_wr,
            i_data_to_player_rdy    => packet_player_rdy,
    
            -- Timing signals
            o_timer_fields_in       => o_timer_fields_in(1),
            i_timer_fields_out      => i_timer_fields_out(1),
            
            -- mux select for data path
            o_vd_enable             => vd_enable,
            
            -- PTP scheduler vector
            i_schedule_action       => vd_schedule_action,
            
            -- debug vector
            i_debug                 => ( others => '0' ),
    
            ------------------------------------------------------------
            -- CNIC NOISE GEN        
            -- Indicates this module is currently writing data to the first 512 MBytes of the HBM
            i_writing_buf0          => writing_buf0,
            -- Indicates this module is currently writing data to the 2nd 512 MBytes of the HBM
            i_writing_buf1          => writing_buf1,
            -- Pulse to start writing data (to whichever buffer we are currently up to)
            o_start_noise_gen       => start_vd_gen,
            o_reset_noise_gen       => reset_vd_gen,
    
            -- ar bus - read address
            vd_axi_arvalid          => vd_m03_axi_arvalid,
            vd_axi_arready          => m03_axi_arready,
            vd_axi_araddr           => vd_m03_axi_araddr,
            vd_axi_arlen            => vd_m03_axi_arlen,
            -- r bus - read data
            vd_axi_rvalid           => m03_axi_rvalid,
            vd_axi_rready           => vd_m03_axi_rready,
            vd_axi_rdata            => m03_axi_rdata,
            vd_axi_rlast            => m03_axi_rlast,
            vd_axi_rresp            => m03_axi_rresp,
    
            -- ar bus - read address
            vd_hbm_2_axi_arvalid    => vd_m04_axi_arvalid,
            vd_hbm_2_axi_arready    => m04_axi_arready,
            vd_hbm_2_axi_araddr     => vd_m04_axi_araddr,
            vd_hbm_2_axi_arlen      => vd_m04_axi_arlen,
            -- r bus - read data
            vd_hbm_2_axi_rvalid     => m04_axi_rvalid,
            vd_hbm_2_axi_rready     => vd_m04_axi_rready,
            vd_hbm_2_axi_rdata      => m04_axi_rdata,
            vd_hbm_2_axi_rlast      => m04_axi_rlast,
            vd_hbm_2_axi_rresp      => m04_axi_rresp,
    
            -----------------------------------------------------------------------
    
            -- VD ARGs interface
            i_vd_lite_axi_mosi      => i_vd_lite_axi_mosi,
            o_vd_lite_axi_miso      => o_vd_lite_axi_miso,
            i_vd_full_axi_mosi      => i_vd_full_axi_mosi,
            o_vd_full_axi_miso      => o_vd_full_axi_miso
    
        );  

----------------------------------------------------------------------------------------------------------------------------------------------
        gen_vd_u55 : IF (g_VD_W_DUPLEX) GENERATE
            -- In VD Mode, the 3rd HBM buffer will be used for the noise generation cache.
            -- This will allow Duplex to still be used, ie capture and noise source.
            m03_axi_awvalid   <= hbm_m03_axi_awvalid    when vd_enable = '0' else vd_m03_axi_awvalid;
            m03_axi_awaddr    <= hbm_m03_axi_awaddr     when vd_enable = '0' else vd_m03_axi_awaddr;
            m03_axi_awlen     <= hbm_m03_axi_awlen      when vd_enable = '0' else vd_m03_axi_awlen;
            --w bus
            m03_axi_wvalid    <= hbm_m03_axi_wvalid     when vd_enable = '0' else vd_m03_axi_wvalid;
            m03_axi_wdata     <= hbm_m03_axi_wdata      when vd_enable = '0' else vd_m03_axi_wdata;
            m03_axi_wlast     <= hbm_m03_axi_wlast      when vd_enable = '0' else vd_m03_axi_wlast;
            -- ar bus - read address
            m03_axi_arvalid   <= hbm_m03_axi_arvalid    when vd_enable = '0' else vd_m03_axi_arvalid;
            m03_axi_araddr    <= hbm_m03_axi_araddr     when vd_enable = '0' else vd_m03_axi_araddr;
            m03_axi_arlen     <= hbm_m03_axi_arlen      when vd_enable = '0' else vd_m03_axi_arlen;
            -- r bus - read data
            m03_axi_rready    <= hbm_m03_axi_rready     when vd_enable = '0' else vd_m03_axi_rready;
            
            
            -- In VD Mode, the 4th HBM buffer will be used for the noise generation cache.
            -- This will allow Duplex to still be used, ie capture and noise source.
            m04_axi_awvalid   <= hbm_m04_axi_awvalid    when vd_enable = '0' else vd_m04_axi_awvalid;
            m04_axi_awaddr    <= hbm_m04_axi_awaddr     when vd_enable = '0' else vd_m04_axi_awaddr;
            m04_axi_awlen     <= hbm_m04_axi_awlen      when vd_enable = '0' else vd_m04_axi_awlen;
            --w bus
            m04_axi_wvalid    <= hbm_m04_axi_wvalid     when vd_enable = '0' else vd_m04_axi_wvalid;
            m04_axi_wdata     <= hbm_m04_axi_wdata      when vd_enable = '0' else vd_m04_axi_wdata;
            m04_axi_wlast     <= hbm_m04_axi_wlast      when vd_enable = '0' else vd_m04_axi_wlast;
            -- ar bus - read address
            m04_axi_arvalid   <= hbm_m04_axi_arvalid    when vd_enable = '0' else vd_m04_axi_arvalid;
            m04_axi_araddr    <= hbm_m04_axi_araddr     when vd_enable = '0' else vd_m04_axi_araddr;
            m04_axi_arlen     <= hbm_m04_axi_arlen      when vd_enable = '0' else vd_m04_axi_arlen;
            -- r bus - read data
            m04_axi_rready    <= hbm_m04_axi_rready     when vd_enable = '0' else vd_m04_axi_rready;
        
        END GENERATE;
----------------------------------------------------------------------------------------------------------------------------------------------
        gen_vd_u280 : IF (NOT g_VD_W_DUPLEX) GENERATE
            -- U280 on does VD for the moment in duplex.
            m03_axi_awvalid   <= vd_m03_axi_awvalid;
            m03_axi_awaddr    <= vd_m03_axi_awaddr;
            m03_axi_awlen     <= vd_m03_axi_awlen;
            --w bus
            m03_axi_wvalid    <= vd_m03_axi_wvalid;
            m03_axi_wdata     <= vd_m03_axi_wdata;
            m03_axi_wlast     <= vd_m03_axi_wlast;
            -- ar bus - read address
            m03_axi_arvalid   <= vd_m03_axi_arvalid;
            m03_axi_araddr    <= vd_m03_axi_araddr;
            m03_axi_arlen     <= vd_m03_axi_arlen;
            -- r bus - read data
            m03_axi_rready    <= vd_m03_axi_rready;
        
            -- U280 on does VD for the moment in duplex.
            m04_axi_awvalid   <= vd_m04_axi_awvalid;
            m04_axi_awaddr    <= vd_m04_axi_awaddr;
            m04_axi_awlen     <= vd_m04_axi_awlen;
            --w bus
            m04_axi_wvalid    <= vd_m04_axi_wvalid;
            m04_axi_wdata     <= vd_m04_axi_wdata;
            m04_axi_wlast     <= vd_m04_axi_wlast;
            -- ar bus - read address
            m04_axi_arvalid   <= vd_m04_axi_arvalid;
            m04_axi_araddr    <= vd_m04_axi_araddr;
            m04_axi_arlen     <= vd_m04_axi_arlen;
            -- r bus - read data
            m04_axi_rready    <= vd_m04_axi_rready;
        END GENERATE;
        
----------------------------------------------------------------------------------------------------------------------------------------------
        
    gen_datagen : if g_VD_data_gen GENERATE
        i_vd_data_gen_1 : entity vd_datagen_lib.dataGen_top
            generic map (
                -- Number of data streams to generate as a multiple of 128
                -- Min = 1, max = 8, use less to reduce build times.
                g_N128_SOURCE_INSTANCES     => g_N128_SOURCE_INSTANCES,
                -- TB generics
                g_128_SAMPLE_BLOCKS         => g_128_SAMPLE_BLOCKS,
                g_VD_INSTANCE_TWO           => FALSE,
                g_DEBUG_ILA                 => g_VD_DEBUG_ILA
                
            )
            port map (
                clk                         => clk,
                reset                       => reset_vd_gen(0),

                clk_vd                      => clk_gen_1_vd,
                clk_vd_reset                => clk_gen1_vd_reset,

                reset_axi                   => reset,
                -- ARGs full interface
                -- Addresses two ultraRAMs for storage of delay polynomials 
                -- other signal generation controls
                i_vdgen_lite_axi_mosi       => i_vd_gen_lite_axi_mosi,
                o_vdgen_lite_axi_miso       => o_vd_gen_lite_axi_miso,
                -- ARGs lite interface
                -- Registers for top level control and monitoring.
                i_vdgen_full_axi_mosi       => i_vd_gen_full_axi_mosi,
                o_vdgen_full_axi_miso       => o_vd_gen_full_axi_miso,
                ----------------------------------------------------
                -- Control signals to the rest of CNIC
                -- Indicates this module is currently writing data to the first 512 MBytes of the HBM
                o_writing_buf0              => writing_buf0(0),
                -- Indicates this module is currently writing data to the 2nd 512 MBytes of the HBM
                o_writing_buf1              => writing_buf1(0),
                -- Pulse to start writing data (to whichever buffer we are currently up to)
                i_start                     => start_vd_gen(0),
                ----------------------------------------------------
                -- HBM interface, Uses 1 Gbyte of HBM. 
                -- aw bus
                m01_axi_awvalid             => vd_m03_axi_awvalid,
                m01_axi_awready             => m03_axi_awready,
                m01_axi_awaddr              => vd_m03_axi_awaddr,
                m01_axi_awlen               => vd_m03_axi_awlen,
                -- w bus - write data
                m01_axi_wvalid              => vd_m03_axi_wvalid,
                m01_axi_wready              => m03_axi_wready,
                m01_axi_wdata               => vd_m03_axi_wdata,
                m01_axi_wlast               => vd_m03_axi_wlast,
                -- b bus - write response
                m01_axi_bvalid              => m03_axi_bvalid,
                m01_axi_bresp               => m03_axi_bresp
            );


        i_vd_data_gen_2 : entity vd_datagen_lib.dataGen_top
            generic map (
                -- Number of data streams to generate as a multiple of 128
                -- Min = 1, max = 8, use less to reduce build times.
                g_N128_SOURCE_INSTANCES     => g_VD_2_instances,
                -- TB generics
                g_128_SAMPLE_BLOCKS         => g_128_SAMPLE_BLOCKS,
                g_VD_INSTANCE_TWO           => TRUE,
                g_DEBUG_ILA                 => g_VD_DEBUG_ILA
                
            )
            port map (
                clk                         => clk,
                reset                       => reset_vd_gen(1),

                clk_vd                      => clk_gen_2_vd,
                clk_vd_reset                => clk_gen2_vd_reset,

                reset_axi                   => reset,
                -- ARGs full interface
                -- Addresses two ultraRAMs for storage of delay polynomials 
                -- other signal generation controls
                i_vdgen_lite_axi_mosi       => i_vd_gen_2_lite_axi_mosi,
                o_vdgen_lite_axi_miso       => o_vd_gen_2_lite_axi_miso,
                -- ARGs lite interface
                -- Registers for top level control and monitoring.
                i_vdgen_full_axi_mosi       => i_vd_gen_2_full_axi_mosi,
                o_vdgen_full_axi_miso       => o_vd_gen_2_full_axi_miso,
                ----------------------------------------------------
                -- Control signals to the rest of CNIC
                -- Indicates this module is currently writing data to the first 512 MBytes of the HBM
                o_writing_buf0              => writing_buf0(1),
                -- Indicates this module is currently writing data to the 2nd 512 MBytes of the HBM
                o_writing_buf1              => writing_buf1(1),
                -- Pulse to start writing data (to whichever buffer we are currently up to)
                i_start                     => start_vd_gen(1),
                ----------------------------------------------------
                -- HBM interface, Uses 1 Gbyte of HBM. 
                -- aw bus
                m01_axi_awvalid             => vd_m04_axi_awvalid,
                m01_axi_awready             => m04_axi_awready,
                m01_axi_awaddr              => vd_m04_axi_awaddr,
                m01_axi_awlen               => vd_m04_axi_awlen,
                -- w bus - write data
                m01_axi_wvalid              => vd_m04_axi_wvalid,
                m01_axi_wready              => m04_axi_wready,
                m01_axi_wdata               => vd_m04_axi_wdata,
                m01_axi_wlast               => vd_m04_axi_wlast,
                -- b bus - write response
                m01_axi_bvalid              => m04_axi_bvalid,
                m01_axi_bresp               => m04_axi_bresp
            );

        debug_vd : if g_VD_DEBUG_ILA GENERATE
            vd_data_gen_ila : ila_0
            port map (
               clk                     => clk,

               probe0(31 downto 0)     => vd_m03_axi_awaddr,
               probe0(32)              => vd_m03_axi_awvalid,
               probe0(33)              => m03_axi_awready,
               probe0(41 downto 34)    => vd_m03_axi_awlen,

               probe0(73 downto 42)    => vd_m03_axi_wdata(31 downto 0),
               probe0(74)              => m03_axi_wready,
               probe0(75)              => vd_m03_axi_wvalid,
               probe0(76)              => vd_m03_axi_wlast,

               probe0(77)              => m03_axi_bvalid,
               probe0(79 downto 78)    => m03_axi_bresp,

               probe0(81 downto 80)    => writing_buf0,
               probe0(83 downto 82)    => writing_buf1,
               probe0(85 downto 84)    => start_vd_gen,
               probe0(87 downto 86)    => reset_vd_gen,

               probe0(191 downto 88)   => (OTHERS => '0')
            );
        END GENERATE;
    END GENERATE;

END GENERATE;


NO_VD_GEN : if (NOT g_VD_data_gen) GENERATE

    m03_axi_awvalid   <= hbm_m03_axi_awvalid;
    m03_axi_awaddr    <= hbm_m03_axi_awaddr;
    m03_axi_awlen     <= hbm_m03_axi_awlen;
    --w bus
    m03_axi_wvalid    <= hbm_m03_axi_wvalid;
    m03_axi_wdata     <= hbm_m03_axi_wdata;
    m03_axi_wlast     <= hbm_m03_axi_wlast;
    -- ar bus - read address
    m03_axi_arvalid   <= hbm_m03_axi_arvalid;
    m03_axi_araddr    <= hbm_m03_axi_araddr;
    m03_axi_arlen     <= hbm_m03_axi_arlen;
    -- r bus - read data
    m03_axi_rready    <= hbm_m03_axi_rready;


    m04_axi_awvalid   <= hbm_m04_axi_awvalid;
    m04_axi_awaddr    <= hbm_m04_axi_awaddr;
    m04_axi_awlen     <= hbm_m04_axi_awlen;
    --w bus
    m04_axi_wvalid    <= hbm_m04_axi_wvalid;
    m04_axi_wdata     <= hbm_m04_axi_wdata;
    m04_axi_wlast     <= hbm_m04_axi_wlast;
    -- ar bus - read address
    m04_axi_arvalid   <= hbm_m04_axi_arvalid;
    m04_axi_araddr    <= hbm_m04_axi_araddr;
    m04_axi_arlen     <= hbm_m04_axi_arlen;
    -- r bus - read data
    m04_axi_rready    <= hbm_m04_axi_rready;


    vd_enable <= '0';

    ----------------------------------------------------------------------
    -- VD peripherals
    no_vd_full : entity signal_processing_common.args_axi_terminus
    port map ( 
        -- ARGS interface
        -- MACE clock is 300 MHz
        i_MACE_clk                          => clk,
        i_MACE_rst                          => reset,
                
        i_args_axi_terminus_full_axi_mosi   => i_vd_full_axi_mosi(0),
        o_args_axi_terminus_full_axi_miso   => o_vd_full_axi_miso(0)
    );

    no_vd_lite : entity vd_lib.VD_vd_reg 
        PORT MAP (
            -- AXI Lite signals, 300 MHz Clock domain
            MM_CLK                  => clk,
            MM_RST                  => reset,
            
            SLA_IN                  => i_vd_lite_axi_mosi(0),
            SLA_OUT                 => o_vd_lite_axi_miso(0),

            VD_REG_FIELDS_RW        => vd_rw_dummy,
            VD_REG_FIELDS_RO        => vd_ro_dummy
            );

    ----------------------------------------------------------------------
    -- SPEAD SPS peripherals
    no_spead_sps_full : entity signal_processing_common.args_axi_terminus
    port map ( 
        -- ARGS interface
        -- MACE clock is 300 MHz
        i_MACE_clk                          => clk,
        i_MACE_rst                          => reset,
                
        i_args_axi_terminus_full_axi_mosi   => i_vd_full_axi_mosi(1),
        o_args_axi_terminus_full_axi_miso   => o_vd_full_axi_miso(1)
    );

    no_spead_sps_lite : entity spead_sps_lib.spead_sps_reg 
        PORT MAP (
            -- AXI Lite signals, 300 MHz Clock domain
            MM_CLK                  => clk,
            MM_RST                  => reset,
            
            SLA_IN                  => i_vd_lite_axi_mosi(1),
            SLA_OUT                 => o_vd_lite_axi_miso(1),

            SPEAD_CTRL_FIELDS_RW    => spead_rw_dummy,
            SPEAD_CTRL_FIELDS_RO    => spead_ro_dummy
            
            );

    ----------------------------------------------------------------------
    -- VD_GEN 1 peripherals
    no_vd_gen_1_full : entity signal_processing_common.args_axi_terminus
    port map ( 
        -- ARGS interface
        -- MACE clock is 300 MHz
        i_MACE_clk                          => clk,
        i_MACE_rst                          => reset,
                
        i_args_axi_terminus_full_axi_mosi   => i_vd_gen_full_axi_mosi,
        o_args_axi_terminus_full_axi_miso   => o_vd_gen_full_axi_miso
    );

    no_vd_gen_1_lite : entity vd_datagen_lib.vd_datagen_reg
    port map (
        MM_CLK                              => clk,
        MM_RST                              => reset,
        SLA_IN                              => i_vd_gen_lite_axi_mosi,
        SLA_OUT                             => o_vd_gen_lite_axi_miso,
        VD_DATAGEN_REG_FIELDS_RW            => vd_gen_rw_dummy,
        VD_DATAGEN_REG_FIELDS_RO            => vd_gen_ro_dummy
    );

    ----------------------------------------------------------------------
    -- VD_GEN 2 peripherals
    no_vd_gen_2_full : entity signal_processing_common.args_axi_terminus
    port map ( 
        -- ARGS interface
        -- MACE clock is 300 MHz
        i_MACE_clk                          => clk,
        i_MACE_rst                          => reset,
                
        i_args_axi_terminus_full_axi_mosi   => i_vd_gen_2_full_axi_mosi,
        o_args_axi_terminus_full_axi_miso   => o_vd_gen_2_full_axi_miso
    );

    no_vd_gen_2_lite : entity vd_datagen_lib.vd_datagen_reg
    port map (
        MM_CLK                              => clk,
        MM_RST                              => reset,
        SLA_IN                              => i_vd_gen_2_lite_axi_mosi,
        SLA_OUT                             => o_vd_gen_2_lite_axi_miso,
        VD_DATAGEN_REG_FIELDS_RW            => vd_gen_rw_dummy_2,
        VD_DATAGEN_REG_FIELDS_RO            => vd_gen_ro_dummy_2
    );

END GENERATE;

-----------------------------------------------------------------------------------------
-- Mux
-----------------------------------------------------------------------------------------
-- in U280 no CODIF HEADER handling.

gen_packet_player_mux : if (NOT g_ALVEO_U280) GENERATE
    packet_player_mux_proc : process(clk)
    begin
        if rising_edge(clk) then
            
        
            if vd_enable = '1' then
                packet_player_mux_bytes_to_transmit     <= vd_bytes_to_transmit;
                packet_player_mux_data                  <= vd_data;
                packet_player_mux_data_in_wr            <= vd_data_in_wr;
            elsif codif_output_select = '1' then
                packet_player_mux_bytes_to_transmit     <= codif_hdr_mod_bytes_to_transmit;
                packet_player_mux_data                  <= codif_hdr_mod_data;
                packet_player_mux_data_in_wr            <= codif_hdr_mod_data_in_wr;
            else
                packet_player_mux_bytes_to_transmit     <= hbm_packetiser_bytes_to_transmit;
                packet_player_mux_data                  <= hbm_swapped_packetiser_data;
                packet_player_mux_data_in_wr            <= hbm_packetiser_data_in_wr;
            end if;
    
            packet_player_1_data_in_wr          <= packet_player_mux_data_in_wr;
            packet_player_1_data                <= packet_player_mux_data;
            packet_player_1_bytes_to_transmit   <= packet_player_mux_bytes_to_transmit;
        end if;
    end process;
END GENERATE;

gen_packet_U280_mux : if (g_ALVEO_U280) GENERATE
    packet_player_mux_proc : process(clk)
    begin
        if rising_edge(clk) then
            if vd_enable = '1' then
                packet_player_mux_bytes_to_transmit     <= vd_bytes_to_transmit;
                packet_player_mux_data                  <= vd_data;
                packet_player_mux_data_in_wr            <= vd_data_in_wr;
            else
                packet_player_mux_bytes_to_transmit     <= hbm_packetiser_bytes_to_transmit;
                packet_player_mux_data                  <= hbm_swapped_packetiser_data;
                packet_player_mux_data_in_wr            <= hbm_packetiser_data_in_wr;
            end if;
    
            packet_player_1_data_in_wr          <= packet_player_mux_data_in_wr;
            packet_player_1_data                <= packet_player_mux_data;
            packet_player_1_bytes_to_transmit   <= packet_player_mux_bytes_to_transmit;
        end if;
    end process;
END GENERATE;


    reg_proc : process(clk)
    begin
        if rising_edge(clk) then

            codif_output_select                 <= config_rw.tx_header_update(0);

            vd_schedule_action                  <= i_schedule_action;
            
            
            hbm_schedule_action(7 downto 3)     <= i_schedule_action(7 downto 3);
            -- If VD is true, turn off HBM playout trigger via PTP.
            -- Not needed for the other way as VD has master enable.
            if vd_enable = '1' then
                hbm_schedule_action(2 downto 1) <= "00";
            else
                hbm_schedule_action(2 downto 1) <= i_schedule_action(2 downto 1);
            end if;
            hbm_schedule_action(0)              <= i_schedule_action(0);
            
            if vd_enable = '1' then
                i_reset_packet_player           <= vd_reset_packet_player;
            else
                i_reset_packet_player           <= hbm_reset_packet_player;
            end if;

            packet_player_rdy   <= packet_player_rdy_d;

        end if;
    end process;

-----------------------------------------------------------------------------------------
-- Packetiser
-----------------------------------------------------------------------------------------

    eth100G_reset <= not(eth100G_locked_int);

    i_packet_player : entity PSR_Packetiser_lib.packet_player 
        Generic Map(
            g_DEBUG_ILA             => g_DEBUG_ILA,
            LBUS_TO_CMAC_INUSE      => g_LBUS_CMAC,      
            PLAYER_CDC_FIFO_DEPTH   => 512        
            -- FIFO is 512 Wide, 9KB packets = 73728 bits, 512 * 256 = 131072, 256 depth allows ~1.88 9K packets, we are target packets sizes smaller than this.
        )
        Port map ( 
            i_clk                   => clk, 
            i_clk_reset             => i_reset_packet_player,
        
            i_cmac_clk              => i_clk_100GE,
            i_cmac_clk_rst          => eth100G_reset,
            
            i_bytes_to_transmit     => packet_player_1_bytes_to_transmit,
            i_data_to_player        => packet_player_1_data,
            i_data_to_player_wr     => packet_player_1_data_in_wr,
            o_data_to_player_rdy    => packet_player_rdy_d,
            
            o_cmac_ready            => cmac_ready,
                   
            -- streaming AXI to CMAC
            o_tx_axis_tdata         => player_m_axis_tdata,
            o_tx_axis_tkeep         => player_m_axis_tkeep,
            o_tx_axis_tvalid        => player_m_axis_tvalid,
            o_tx_axis_tlast         => player_m_axis_tlast,
            o_tx_axis_tuser         => o_tx_axis_tuser,
            i_tx_axis_tready        => i_tx_axis_tready,
        
            -- LBUS to CMAC
            o_data_to_transmit      => o_data_tx_sosi,
            i_data_to_transmit_ctl  => i_data_tx_siso
        );

        
-----------------------------------------------------------------------------------------
-- Loopback - post packet player pre CMAC
-----------------------------------------------------------------------------------------        
-- DONT GENERATE U280 in Loopback

loopback_gen : if (NOT g_ALVEO_U280) GENERATE
    loopback_cdc : entity signal_processing_common.sync
    generic map (
        WIDTH           => 1,
        DEST_SYNC_FF    => 4
    )
    Port Map (
        Clock_a     => clk,
        data_in(0)  => config_rw.loopback_sel,

        Clock_b     => i_clk_100GE,
        data_out(0) => loopback_select
    );

    o_tx_axis_tdata     <= player_m_axis_tdata      when loopback_select = '0' else loopback_m_axis_tdata;
    o_tx_axis_tkeep     <= player_m_axis_tkeep      when loopback_select = '0' else loopback_m_axis_tkeep;
    o_tx_axis_tvalid    <= player_m_axis_tvalid     when loopback_select = '0' else loopback_m_axis_tvalid;
    o_tx_axis_tlast     <= player_m_axis_tlast      when loopback_select = '0' else loopback_m_axis_tlast;


    -- provide switchable loopback for CNIC to CNIC testing.
    loopback_testing_fifo : axis_data_fifo_loopback
        port map (
          s_axis_aresetn    => loopback_select,

          s_axis_aclk       => i_clk_100GE,
          s_axis_tvalid     => i_rx_axis_tvalid,
          s_axis_tready     => open,        -- driven in s_axi_capture by locked status.
          s_axis_tdata      => i_rx_axis_tdata,
          s_axis_tkeep      => i_rx_axis_tkeep,
          s_axis_tlast      => i_rx_axis_tlast,

          m_axis_aclk       => i_clk_100GE,
          m_axis_tvalid     => loopback_m_axis_tvalid,
          m_axis_tready     => i_tx_axis_tready,
          m_axis_tdata      => loopback_m_axis_tdata,
          m_axis_tkeep      => loopback_m_axis_tkeep,
          m_axis_tlast      => loopback_m_axis_tlast
        );

END GENERATE;

no_loopback_gen : if (g_ALVEO_U280) GENERATE
    o_tx_axis_tdata     <= player_m_axis_tdata;
    o_tx_axis_tkeep     <= player_m_axis_tkeep;
    o_tx_axis_tvalid    <= player_m_axis_tvalid;
    o_tx_axis_tlast     <= player_m_axis_tlast;

END GENERATE;
-----------------------------------------------------------------------------------------    

-- ZERO THIS OUT FOR THE MOMENT, until second interface is connected up.
        o_tx_axis_tdata_b   <= zero_512;
        o_tx_axis_tkeep_b   <= zero_64; 
        o_tx_axis_tvalid_b  <= '0';
        o_tx_axis_tlast_b   <= '0';
        o_tx_axis_tuser_b   <= '0';

--    eth100G_reset_b <= not(i_eth100G_locked_b);
        
--    packet_player_100G_B : entity PSR_Packetiser_lib.packet_player 
--        Generic Map(
--            LBUS_TO_CMAC_INUSE      => g_LBUS_CMAC,      -- FUTURE WORK to IMPLEMENT AXI
--            PLAYER_CDC_FIFO_DEPTH   => 512        
--            -- FIFO is 512 Wide, 9KB packets = 73728 bits, 512 * 256 = 131072, 256 depth allows ~1.88 9K packets, we are target packets sizes smaller than this.
--        )
--        Port map ( 
--            i_clk400                => clk, 
--            i_reset_400             => i_reset_packet_player,
        
--            i_cmac_clk              => i_clk_100GE_b,
--            i_cmac_clk_rst          => eth100G_reset_b,
            
--            i_bytes_to_transmit     => header_modifier_bytes_to_transmit,   --hbm_packetiser_bytes_to_transmit,    -- 
--            i_data_to_player        => header_modifier_data,                --hbm_swapped_packetiser_data, 
--            i_data_to_player_wr     => header_modifier_data_in_wr,          --packetiser_data_in_wr,
--            o_data_to_player_rdy    => packetiser_data_to_player_rdy_b,
            
--            o_cmac_ready            => open,
                   
--            -- streaming AXI to CMAC
--            o_tx_axis_tdata         => o_tx_axis_tdata_b,
--            o_tx_axis_tkeep         => o_tx_axis_tkeep_b,
--            o_tx_axis_tvalid        => o_tx_axis_tvalid_b,
--            o_tx_axis_tlast         => o_tx_axis_tlast_b,
--            o_tx_axis_tuser         => o_tx_axis_tuser_b,
--            i_tx_axis_tready        => i_tx_axis_tready_b,
        
--            -- LBUS to CMAC
--            o_data_to_transmit      => open,
--            i_data_to_transmit_ctl  => i_data_tx_siso
--        );
  
    
--    packetiser_data_to_player_rdy_combo     <= packetiser_data_to_player_rdy_b AND packet_player_rdy;
 ---------------------------------------------------------------------------------------------------------------------------------------
-- ILA for debugging

debug_gen : IF g_DEBUG_ILA GENERATE
    cnic_ila : ila_0
    port map (
        clk                     => clk, 
        probe0(13 downto 0)     => config_rw.rx_packet_size(13 downto 0),
        probe0(14)              => packet_size_ceil(0),
        probe0(28 downto 15)    => packet_size_calc,
        probe0(29)              => '0', 
        probe0(43 downto 30)    => rx_packet_size_mod64b,
        probe0(44)              => config_rw.rx_reset_capture,
        probe0(191 downto 45)   => (others => '0')
    );

END GENERATE;

END structure;
