# The GT processing clock
# get_clocks -of_objects [get_cells -hierarchical *accumulators_rx*]
# The provided processing clock
#get_clocks -of_objects [get_cells -hierarchical *system_reg*]

# Need to set processing order to LATE for these constraints to work.

set_max_delay -datapath_only -from [get_clocks -of_objects [get_cells -hierarchical *accumulators_rx*]] -to [get_clocks -of_objects [get_cells -hierarchical *system_reg*]] 10.0
set_max_delay -datapath_only -from [get_clocks -of_objects [get_cells -hierarchical *system_reg*]] -to [get_clocks -of_objects [get_cells -hierarchical *accumulators_rx*]] 10.0

########################################################################################################################
## location constraint to help with SLR timing errors.

# constrain the 100G to top SLR = SLR 2
add_cells_to_pblock pblock_dynamic_SLR2 [get_cells -hierarchical *WITH_PTP_GEN.U55_2nd_port.u_100G_port_b]
add_cells_to_pblock pblock_dynamic_SLR2 [get_cells -hierarchical *WITH_PTP_GEN.u_100G_port_a]

add_cells_to_pblock pblock_dynamic_SLR2 [get_cells -hierarchical *i_packet_player]

add_cells_to_pblock pblock_dynamic_SLR0 [get_cells -hierarchical *i_HBM_PktController]

add_cells_to_pblock pblock_dynamic_SLR0 [get_cells -hierarchical *m01_reg_slice]
add_cells_to_pblock pblock_dynamic_SLR0 [get_cells -hierarchical *m02_reg_slice]
add_cells_to_pblock pblock_dynamic_SLR0 [get_cells -hierarchical *m03_reg_slice]
add_cells_to_pblock pblock_dynamic_SLR0 [get_cells -hierarchical *m04_reg_slice]

#add_cells_to_pblock pblock_dynamic_SLR0 [get_cells -hierarchical *vd_inst]

# Heirarchy can't search across / so grouping by type, 1 at the top and 8 closest to HBM interface.
# Each of these will generate two instances.

# VD 1 - Gens 1-5  (5)
add_cells_to_pblock pblock_dynamic_SLR2 [get_cells -hier -filter {NAME =~ */VD_GEN.gen_datagen.i_vd_data_gen_1/gen_dataGen[0].gen_128Gen.datageni}]
add_cells_to_pblock pblock_dynamic_SLR2 [get_cells -hier -filter {NAME =~ */VD_GEN.gen_datagen.i_vd_data_gen_1/gen_dataGen[1].gen_128Gen.datageni}]
add_cells_to_pblock pblock_dynamic_SLR2 [get_cells -hier -filter {NAME =~ */VD_GEN.gen_datagen.i_vd_data_gen_1/gen_dataGen[2].gen_128Gen.datageni}]
add_cells_to_pblock pblock_dynamic_SLR2 [get_cells -hier -filter {NAME =~ */VD_GEN.gen_datagen.i_vd_data_gen_1/gen_dataGen[3].gen_128Gen.datageni}]
add_cells_to_pblock pblock_dynamic_SLR2 [get_cells -hier -filter {NAME =~ */VD_GEN.gen_datagen.i_vd_data_gen_1/gen_dataGen[4].gen_128Gen.datageni}]

# VD 1 - Gens 6-8, VD 2 - Gens 1-3 (6)
add_cells_to_pblock pblock_dynamic_SLR1 [get_cells -hier -filter {NAME =~ */VD_GEN.gen_datagen.i_vd_data_gen_1/gen_dataGen[5].gen_128Gen.datageni}]
add_cells_to_pblock pblock_dynamic_SLR1 [get_cells -hier -filter {NAME =~ */VD_GEN.gen_datagen.i_vd_data_gen_1/gen_dataGen[6].gen_128Gen.datageni}]
add_cells_to_pblock pblock_dynamic_SLR1 [get_cells -hier -filter {NAME =~ */VD_GEN.gen_datagen.i_vd_data_gen_1/gen_dataGen[7].gen_128Gen.datageni}]
add_cells_to_pblock pblock_dynamic_SLR1 [get_cells -hier -filter {NAME =~ */VD_GEN.gen_datagen.i_vd_data_gen_2/gen_dataGen[0].gen_128Gen.datageni}]
add_cells_to_pblock pblock_dynamic_SLR1 [get_cells -hier -filter {NAME =~ */VD_GEN.gen_datagen.i_vd_data_gen_2/gen_dataGen[1].gen_128Gen.datageni}]
add_cells_to_pblock pblock_dynamic_SLR1 [get_cells -hier -filter {NAME =~ */VD_GEN.gen_datagen.i_vd_data_gen_2/gen_dataGen[2].gen_128Gen.datageni}]

# VD 2 - Gens 4-8  (5)
add_cells_to_pblock pblock_dynamic_SLR0 [get_cells -hier -filter {NAME =~ */VD_GEN.gen_datagen.i_vd_data_gen_2/gen_dataGen[3].gen_128Gen.datageni}]
add_cells_to_pblock pblock_dynamic_SLR0 [get_cells -hier -filter {NAME =~ */VD_GEN.gen_datagen.i_vd_data_gen_2/gen_dataGen[4].gen_128Gen.datageni}]
add_cells_to_pblock pblock_dynamic_SLR0 [get_cells -hier -filter {NAME =~ */VD_GEN.gen_datagen.i_vd_data_gen_2/gen_dataGen[5].gen_128Gen.datageni}]
add_cells_to_pblock pblock_dynamic_SLR0 [get_cells -hier -filter {NAME =~ */VD_GEN.gen_datagen.i_vd_data_gen_2/gen_dataGen[6].gen_128Gen.datageni}]
add_cells_to_pblock pblock_dynamic_SLR0 [get_cells -hier -filter {NAME =~ */VD_GEN.gen_datagen.i_vd_data_gen_2/gen_dataGen[7].gen_128Gen.datageni}]

########################################################################################################################
## Time constraints if both 100G ports are in play.
## Timeslave IP constraints.. derived from reference design

set_max_delay 10.0 -datapath_only -from [get_clocks enet_refclk_p[0]] -to [get_clocks sysclk100] 
set_max_delay 3.0 -datapath_only -from [get_clocks enet_refclk_p[0]] -to [get_clocks txoutclk_out[0]] 
 
set_max_delay 3.0 -datapath_only -from [get_clocks rxoutclk_out[0]] -to [get_clocks enet_refclk_p[0]] 

set_max_delay 3.0 -datapath_only -from [get_clocks sysclk100] -to [get_clocks enet_refclk_p[0]] 
set_max_delay 3.0 -datapath_only -from [get_clocks sysclk100] -to [get_clocks txoutclk_out[0]] 
 
set_max_delay 3.0 -datapath_only -from [get_clocks txoutclk_out[0]] -to [get_clocks rxoutclk_out[0]]
set_max_delay 3.0 -datapath_only -from [get_clocks txoutclk_out[0]_1] -to [get_clocks rxoutclk_out[0]_1]

set_max_delay 3.0 -datapath_only -from [get_clocks rxoutclk_out[0]] -to [get_clocks txoutclk_out[0]] 
set_max_delay 3.0 -datapath_only -from [get_clocks rxoutclk_out[0]_1] -to [get_clocks txoutclk_out[0]_1] 

# ts - 1st instantiation of 100G and Timeslave.
set_max_delay 3.0 -datapath_only -from [get_clocks clk_300_ts_clk_wiz_0_0] -to [get_clocks rxoutclk_out[0]_1] 
set_max_delay 3.0 -datapath_only -from [get_clocks clk_300_ts_clk_wiz_0_0] -to [get_clocks txoutclk_out[0]_1] 

set_max_delay 3.0 -datapath_only -from [get_clocks rxoutclk_out[0]_1] -to [get_clocks clk_300_ts_clk_wiz_0_0] 
set_max_delay 3.0 -datapath_only -from [get_clocks txoutclk_out[0]_1] -to [get_clocks clk_300_ts_clk_wiz_0_0]

# ts_b - 2nd instantiation of 100G and Timeslave.
set_max_delay 3.0 -datapath_only -from [get_clocks clk_300_ts_b_clk_wiz_0_0] -to [get_clocks rxoutclk_out[0]] 
set_max_delay 3.0 -datapath_only -from [get_clocks clk_300_ts_b_clk_wiz_0_0] -to [get_clocks txoutclk_out[0]]

set_max_delay 3.0 -datapath_only -from [get_clocks rxoutclk_out[0]] -to [get_clocks clk_300_ts_b_clk_wiz_0_0]
set_max_delay 3.0 -datapath_only -from [get_clocks txoutclk_out[0]] -to [get_clocks clk_300_ts_b_clk_wiz_0_0]

set_max_delay 3.0 -datapath_only -from [get_clocks clk_300_ts_b_clk_wiz_0_0] -to [get_clocks rxoutclk_out[0]_1] 
set_max_delay 3.0 -datapath_only -from [get_clocks clk_300_ts_b_clk_wiz_0_0] -to [get_clocks txoutclk_out[0]_1]

set_max_delay 3.0 -datapath_only -from [get_clocks rxoutclk_out[0]_1] -to [get_clocks clk_300_ts_b_clk_wiz_0_0]
set_max_delay 3.0 -datapath_only -from [get_clocks txoutclk_out[0]_1] -to [get_clocks clk_300_ts_b_clk_wiz_0_0]


########################################################################################################################
## Time constraints if there is only 1 x 100G with TS on the top QSFP port.
## Timeslave IP constraints.. derived from reference design

set_max_delay 10.0 -datapath_only -from [get_clocks enet_refclk_p[0]] -to [get_clocks sysclk100] 
set_max_delay 3.0 -datapath_only -from [get_clocks enet_refclk_p[0]] -to [get_clocks txoutclk_out[0]] 
 
set_max_delay 3.0 -datapath_only -from [get_clocks rxoutclk_out[0]] -to [get_clocks enet_refclk_p[0]] 
set_max_delay 3.0 -datapath_only -from [get_clocks rxoutclk_out[0]] -to [get_clocks txoutclk_out[0]] 
set_max_delay 3.0 -datapath_only -from [get_clocks sysclk100] -to [get_clocks enet_refclk_p[0]] 
set_max_delay 3.0 -datapath_only -from [get_clocks sysclk100] -to [get_clocks txoutclk_out[0]] 
 
set_max_delay 3.0 -datapath_only -from [get_clocks txoutclk_out[0]] -to [get_clocks rxoutclk_out[0]]

## ts - 1st instantiation of 100G and Timeslave. ... these seem to cover CDC of PTP and PPS.
set_max_delay 3.0 -datapath_only -from [get_clocks clk_300_ts_clk_wiz_0_0] -to [get_clocks rxoutclk_out[0]] 
set_max_delay 3.0 -datapath_only -from [get_clocks clk_300_ts_clk_wiz_0_0] -to [get_clocks txoutclk_out[0]] 

set_max_delay 3.0 -datapath_only -from [get_clocks rxoutclk_out[0]] -to [get_clocks clk_300_ts_clk_wiz_0_0] 
set_max_delay 3.0 -datapath_only -from [get_clocks txoutclk_out[0]] -to [get_clocks clk_300_ts_clk_wiz_0_0]

## PTP ARGs clock
#set_max_delay 3.0 -datapath_only -from [get_clocks clk_300_ts_clk_wiz_0_0] -to [get_clocks clk_kernel_unbuffered]
#set_max_delay 3.0 -datapath_only -from [get_clocks clk_kernel_unbuffered] -to [get_clocks clk_300_ts_clk_wiz_0_0]
