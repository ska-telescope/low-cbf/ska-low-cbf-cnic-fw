create_ip -name axi_cdma -vendor xilinx.com -library ip -version 4.1 -module_name axi_cdma_0
set_property -dict [list CONFIG.C_INCLUDE_SF {1} CONFIG.C_INCLUDE_SG {0} CONFIG.C_ADDR_WIDTH {32}] [get_ips axi_cdma_0]
create_ip_run [get_ips axi_cdma_0]

create_ip -name ila -vendor xilinx.com -library ip -version 6.2 -module_name ila_0
set_property -dict [list CONFIG.C_PROBE0_WIDTH {192} CONFIG.C_DATA_DEPTH {8192}] [get_ips ila_0]
create_ip_run [get_ips ila_0]

# Generate other clocks from the 100MHz input clock - nonscalable
create_ip -name clk_wiz -vendor xilinx.com -library ip -version 6.0 -module_name clk_gen100MHz
set_property -dict [list CONFIG.Component_Name {clk_gen100MHz} CONFIG.PRIM_SOURCE {Global_buffer} CONFIG.PRIM_IN_FREQ {100.000} CONFIG.USE_LOCKED {false} CONFIG.USE_RESET {false} CONFIG.CLKIN1_JITTER_PS {33.330000000000005} CONFIG.MMCM_CLKFBOUT_MULT_F {4.000} CONFIG.MMCM_CLKIN1_PERIOD {3.333} CONFIG.MMCM_CLKIN2_PERIOD {10.0} CONFIG.CLKOUT1_JITTER {101.475} CONFIG.CLKOUT1_PHASE_ERROR {77.836}] [get_ips clk_gen100MHz]
set_property -dict [list CONFIG.CLKOUT2_USED {true} CONFIG.CLK_OUT1_PORT {clk100_out} CONFIG.CLK_OUT2_PORT {clk450_out} CONFIG.CLKOUT2_REQUESTED_OUT_FREQ {450.000} CONFIG.MMCM_CLKFBOUT_MULT_F {4.500} CONFIG.MMCM_CLKOUT0_DIVIDE_F {13.500} CONFIG.MMCM_CLKOUT1_DIVIDE {3} CONFIG.NUM_OUT_CLKS {2} CONFIG.CLKOUT1_JITTER {98.047} CONFIG.CLKOUT1_PHASE_ERROR {73.261} CONFIG.CLKOUT2_JITTER {73.020} CONFIG.CLKOUT2_PHASE_ERROR {73.261}] [get_ips clk_gen100MHz]
set_property -dict [list CONFIG.CLK_OUT2_PORT {clk250_out} CONFIG.CLKOUT2_REQUESTED_OUT_FREQ {250.000} CONFIG.MMCM_CLKFBOUT_MULT_F {12.500} CONFIG.MMCM_CLKOUT0_DIVIDE_F {12.500} CONFIG.MMCM_CLKOUT1_DIVIDE {5} CONFIG.CLKOUT1_JITTER {111.970} CONFIG.CLKOUT1_PHASE_ERROR {84.520} CONFIG.CLKOUT2_JITTER {94.797} CONFIG.CLKOUT2_PHASE_ERROR {84.520}] [get_ips clk_gen100MHz]
create_ip_run [get_ips clk_gen100MHz]

# clock for VD generate logic.
create_ip -name clk_wiz -vendor xilinx.com -library ip -version 6.0 -module_name clk_VD_GEN
set_property -dict [list \
  CONFIG.CLKOUT1_JITTER {94.862} \
  CONFIG.CLKOUT1_REQUESTED_OUT_FREQ {300.000} \
  CONFIG.CLK_OUT1_PORT {clk_vd} \
  CONFIG.Component_Name {clk_VD_GEN} \
  CONFIG.MMCM_CLKOUT0_DIVIDE_F {4.000} \
  CONFIG.OPTIMIZE_CLOCKING_STRUCTURE_EN {true} \
  CONFIG.PRIMARY_PORT {clk_in} \
  CONFIG.USE_LOCKED {false} \
  CONFIG.USE_RESET {false} \
] [get_ips clk_VD_GEN]
create_ip_run [get_ips clk_VD_GEN]

create_ip -name clk_wiz -vendor xilinx.com -library ip -version 6.0 -module_name clk_VD_275_GEN
set_property -dict [list \
  CONFIG.CLKOUT1_JITTER {154.538} \
  CONFIG.CLKOUT1_PHASE_ERROR {221.516} \
  CONFIG.CLKOUT1_REQUESTED_OUT_FREQ {275.000} \
  CONFIG.CLK_OUT1_PORT {clk_275_vd} \
  CONFIG.Component_Name {clk_VD_275_GEN} \
  CONFIG.MMCM_CLKFBOUT_MULT_F {48.125} \
  CONFIG.MMCM_CLKOUT0_DIVIDE_F {4.375} \
  CONFIG.MMCM_DIVCLK_DIVIDE {4} \
  CONFIG.OPTIMIZE_CLOCKING_STRUCTURE_EN {true} \
  CONFIG.PRIMARY_PORT {clk_in} \
  CONFIG.USE_LOCKED {false} \
  CONFIG.USE_RESET {false} \
] [get_ips clk_VD_275_GEN]
create_ip_run [get_ips clk_VD_GEN]

# 512 bit wide AXI register slice, 64 bit address
create_ip -name axi_register_slice -vendor xilinx.com -library ip -version 2.1 -module_name axi_reg_slice512_LLFFL
set_property -dict [list CONFIG.ADDR_WIDTH {64} CONFIG.DATA_WIDTH {512} CONFIG.REG_W {1} CONFIG.Component_Name {axi_reg_slice512_LLFFL}] [get_ips axi_reg_slice512_LLFFL]
set_property -dict [list CONFIG.HAS_LOCK {0} CONFIG.HAS_CACHE {0} CONFIG.HAS_REGION {0} CONFIG.HAS_QOS {0} CONFIG.HAS_PROT {0} CONFIG.REG_AW {1} CONFIG.REG_AR {1}] [get_ips axi_reg_slice512_LLFFL]
create_ip_run [get_ips axi_reg_slice512_LLFFL]

# AXI BRAM control for axi_terminus
create_ip -name axi_bram_ctrl -vendor xilinx.com -library ip -version 4.1 -module_name axi_bram_ctrl_1k
set_property -dict [list CONFIG.SUPPORTS_NARROW_BURST {0} CONFIG.SINGLE_PORT_BRAM {1} CONFIG.Component_Name {axi_bram_ctrl_1k} CONFIG.MEM_DEPTH {1024}] [get_ips axi_bram_ctrl_1k]
create_ip_run [get_ips axi_bram_ctrl_1k]

# AXI-4 STREAM Data Fifo Loopback
create_ip -name axis_data_fifo -vendor xilinx.com -library ip -version 2.0 -module_name axis_data_fifo_loopback
set_property -dict [list \
  CONFIG.Component_Name {axis_data_fifo_loopback} \
  CONFIG.HAS_TKEEP {1} \
  CONFIG.HAS_TLAST {1} \
  CONFIG.IS_ACLK_ASYNC {1} \
  CONFIG.TDATA_NUM_BYTES {64} \
] [get_ips axis_data_fifo_loopback]
create_ip_run [get_ips axis_data_fifo_loopback]

# AXI memory interface for the shared memory in the testbench
create_ip -name axi_bram_ctrl -vendor xilinx.com -library ip -version 4.1 -module_name axi_bram_RegisterSharedMem
set_property -dict [list CONFIG.SINGLE_PORT_BRAM {1} CONFIG.Component_Name {axi_bram_RegisterSharedMem} CONFIG.MEM_DEPTH {32768}] [get_ips axi_bram_RegisterSharedMem]
create_ip_run [get_ips axi_bram_RegisterSharedMem]