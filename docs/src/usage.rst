CNIC Usage
==========

This repository contains the VHDL source code to implement the CNIC.

.. image:: images/cnic_vd-block-diagram.png

Programming the CNIC
--------------------
Using the `CNIC software <https://gitlab.com/ska-telescope/low-cbf/ska-low-cbf-sw-cnic>`
The following registers will need to be programmed.
Greater register information can be found in yaml files in the CNIC FW repository.

- `CNIC-VD Packet registers - VD_yaml <https://gitlab.com/ska-telescope/low-cbf/ska-low-cbf-fw-cnic/-/blob/main/libraries/signalProcessing/virtual_digitiser/VD.peripheral.yaml>`_
- `CNIC-VD signal registers - VD_datagen_yaml <https://gitlab.com/ska-telescope/low-cbf/ska-low-cbf-fw-cnic/-/blob/main/libraries/signalProcessing/vd_dataGen/vd_datagen.peripheral.yaml>`_
- `Configureable SPEAD_SPS fields - spead_sps_yaml <https://gitlab.com/ska-telescope/low-cbf/ska-low-cbf-fw-common/-/blob/main/libraries/spead_sps/spead_sps.peripheral.yaml>`_
- Setup of TX or RX is handled in `HBM_PktController_yaml <https://gitlab.com/ska-telescope/low-cbf/ska-low-cbf-fw-cnic/-/blob/main/libraries/signalProcessing/HBM_PktController/HBM_PktController.peripheral.yaml>`_


Programming VD mode
-------------------

**LFAA SPEAD Packet - Version 1**\

.. image:: images/lfaa_spead.png

**SPS SPEAD Packet - Version 2**\

.. image:: images/sps_spead.png

**SPS SPEAD Packet - Version 3**\

.. image:: images/sps_spead_v3.png

Configure the ethernet frame
----------------------------

Use appropriate values in the registers below, these will be used for all VD generated packets.

``fpga.spead_sps.ethernet_dst_mac_u``        (Upper 2 bytes)

``fpga.spead_sps.ethernet_dst_mac_l``

``fpga.spead_sps.ethernet_src_mac_u``        (Upper 2 bytes)

``fpga.spead_sps.ethernet_src_mac_l``

``fpga.spead_sps.ipv4_src_addr``

``fpga.spead_sps.ipv4_dst_addr``

``fpga.spead_sps.udp_src_port``

``fpga.spead_sps.udp_dst_port``


Configure the channel variables
--------------------------------

Based on the two versions of SPS spead packets we support,
there are entries for 4096 entries, each entry contains the following information.

- Scan_id         4B
- Beam_id         2B
- Frequency_id    2B
- Substation_id   1B
- Subarray_id     1B
- Station_id      2B

There is 32k of address space.
Each variables has an address and will only write the byte width given above.
Channel variables span 0-7 addresses.
The address for 1st channel scan_id is 0x8, 2nd is 1x8, etc.

- Addr - 0 - Scan_id
- Addr - 1 - Beam_id
- Addr - 2 - Frequency_id
- Addr - 3 - Substation_id
- Addr - 4 - Subarray_id
- Addr - 5 - Station_id
- Addr - 6 no connect
- Addr - 7 no connect

Scan_id for channel 1 is accessed by

``fpga.vd.data[8]``

Some fields are not driven as below
- logical channel ID (Version 1)
- Frequency (Hz)     (Version 1)
- station_channel_id (Version 2)


Configure VD module
--------------------

Set number of channels, (number of channels - 1)

``fpga.vd.number_of_valid_lines_in_VD_ram``


The time between channel bursts, nominally 2211840ns

``fpga.vd.time_between_channel_bursts``


Time between packets is the number of 3.33ns cycle between packets in a burst.

``fpga.vd.time_between_packets``


Select SPS packet version, 0 = version 1, 1 = version 2

``fpga.vd.sps_packet_version_select``


Set base timestamp value (ns)

``fpga.vd.timestamp_u``

``fpga.vd.timestamp_l``


Set Unix epoch time (sec)

``fpga.vd.unix_epoch_time_u``       (Time in seconds, only applies to v1 format - upper 2 bytes of 6)

``fpga.vd.unix_epoch_time_l``


Set base value for packetcounter field
``fpga.vd.packet_counter``


Enable VD last, this switches the generator into the 100G path and loads VD module parameters set above.

``fpga.vd.enable_vd``


Running the circuit
--------------------

Enable the SPEAD packetiser to run. (1)

``fpga.spead_sps.enable_packetiser``


Pause the current configuration can be achieved by disabling the packetiser. (0)

``fpga.spead_sps.enable_packetiser``


Between runs, disable (0) and enable (1) VD to reset packetcounter and timestamps

``fpga.vd.enable_vd``

