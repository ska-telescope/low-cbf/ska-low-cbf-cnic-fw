.. vim: syntax=rst

GitLab Continuous Integration (CI) Configuration
================================================

The CI configuration inherits from both the `SKA Templates Repository
<https://gitlab.com/ska-telescope/templates-repository>`_ and
`Low CBF Firmware Common
<https://gitlab.com/ska-telescope/low-cbf/ska-low-cbf-fw-common>`_

Upload to Central Artefact Repository (CAR)
-------------------------------------------

Our Low CBF Firmware Common scripts already included a packaging step that
uploads to the GitLab package registry. 

For upload to CAR, the package_firmware.sh script is called with a variable set
so it doesn't upload, the uncompressed files to be packaged are copied into the
raw directory, and then the SKA Makefiles take care of the rest.

Running CI Tests Manually
=========================

It's often useful to execute the tests manually for troubleshooting. Here's how.

.. note:: This is not a perfect replica of the CI pipeline. For example GitLab CI
          doesn't need to download the firmware as it is already present as a job
          artefact. Refer to ``.gitlab-ci.yml`` for the true CI pipeline steps.

Optional - Use a Python virtual environment to isolate the Python modules that will be
installed from you system's default Python environment.

.. code-block:: shell

    mkdir -p venv
    python3 -m venv venv
    source venv/bin/activate

From the project root directory run the below commands.

.. code-block:: shell

    # install software ('make test' will do it later, but we want the download tool now)
    pip3 install -r test-requirements.txt --upgrade

    # download firmware - use a version string like "0.1.13-dev.a4a16c2e"
    scripts/download_u280.sh <version>
    # you may need firmware for different cards
    scripts/download_u55c.sh <version>

    # load XRT environment
    source /opt/xilinx/xrt/setup.sh

    # reset FPGAs - substitute BDFs as appropriate
    xbutil reset -d "0000:07:00.1" --force; xbutil reset -d "0000:08:00.1" --force;

    # execute test
    make test

If you want to select a subset of tests, or otherwise alter the default
`pytest <https://docs.pytest.org/en/stable/>`_
behaviour, use the ``EXTRA_PYTEST_ARGS`` environment variable.

e.g. to select a test scenario:

.. code-block:: shell

    EXTRA_PYTEST_ARGS="-k test_transmit_vd[forward-1728streams-3-0]" make test
