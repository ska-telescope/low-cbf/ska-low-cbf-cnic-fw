### Changelog
## CNIC Personality
## Changelog (Firmware version bumped immediately after release.)

* 0.1.14 -
    * To be used with CNIC SW V0.14.0+
    * Add ability to play out PCAP using timestamps instead of a desired bandwidth.
        * Looping support added.
    * Add the ability to set Duplex TX and RX buffers sizes. 
        * The ALVEO in use has the HBM divided into four sections. U55 has 16GB HBM, each section has 4GB.
        * Duplex register now takes a 2 bit value and has the following configs.
            * 0 - Card will either TX or RX for the full HBM, 
            * 1 - Card will assign the first 2 buffers to RX and the last 2 to TX,
            * 2 - Card will assign the first 1 buffers to RX and the last 3 to TX,
            * 3 - Card will assign the first 3 buffers to RX and the last 1 to TX.
* 0.1.13 - 
    * To be used with CNIC SW v0.12+
    * U280 device support added for CNIC.
        * Two 100G interfaces, with primary used for Data, second available for PTP.
        * 8GB of HBM available for TX, RX or Duplex.
        * Virtual Digitiser is supported.
        * Does not support loopback. (available through HBM_controller peripheral)
    * VD Generators added to their own clock net.
    * VD Generators available as follows (ideal):
        * U55C - 2048 channels, 2 x 1024 Generators.
        * U280 - 1792 channels, 1024 + 768 Generators.
        * There are two generators in the FPGA, each instance reports the number of channels.
* 0.1.12 - 
    * CNIC VD
        * Add second instance of VD generator to increase channels that can be generated to 2048.
        * Added debug counters to the vd peripheral to help track bufffer creation.
    * CI HIL changed to use common tests across all ALVEO builds.
* 0.1.11 - 
    * CNIC VD
        * SPS Spead v3 implemented.
* 0.1.10 - 
    * VD mode
        * Fixes for VD timing (pulsar) mode.
        * Update labels and description of SPEAD formats.
        * Power on value for SPS SPEAD version select set to v2.
    * u55 -> u55c label updates in scripts and output products.
* 0.1.9 - 
    * Additional registers added to VD generator to allow for phase corrections.
    * Signal pulsing added - signal from VD Gen can be turned off and on for all streams to simulate signal bursts.
* 0.1.8 - 
    * Commit Short Hash and build type available in fpga bitstream. Accssible via python console using CNIC SW.
    * VD now has a max number of bursts to allow for greater testing control.
* 0.1.7 - 
    * Virtual Digitiser emulator improved. A signal source producing signals in SPS SPEAD format packets, using a 32 Tap Filter.
    * Virtual Digitaiser delay logic improved.
    * HBM address management updated to handle variable buffer sizes.
    * To be used with CNIC Software version 0.6.1
* 0.1.6 - 
    * Virtual Digitiser emulator added. A signal source producing signals in SPS SPEAD format packets, using a 16 Tap Filter.
    * Virtual Digitiser is only built into ALVEO U55C kernel.
    * PTP driven TX rate option added.
    * PTP driven RX logic added to support capture without stipulating packet count.
    * Enhanced TX stats from the 100G CMAC IP block available to host application.
    * U50 platform changed to - xilinx_u50_gen3x16_xdma_201920_3
* 0.1.5 - 
    * U55C base 2 is no longer built.
    * All ALVEO kernels built in 2022.2
    * Duplex mode added, Buffers 1 and 2 assigned to RX, Buffers 3 and 4 assigned to TX.
* 0.1.4 - 
    * Support for multiple packet lengths on both RX and TX
    * U55C now also being built for Vitis 2022.2 on platform xilinx_u55c_gen3x16_xdma_3
    * RX capture filter supports equal or >= packet size.
* 0.1.3 - 
    * CNIC RX updated to eliminate corner cases stopping capture.
    * Memory layout altered so that meta data is before the packet it describes.
    * U50   - xilinx_u50_gen3x4_xdma_2_202010_1 
    * U50LV - xilinx_u50lv_gen3x4_xdma_2_202010_1
    * U55C  - xilinx_u55c_gen3x16_xdma_2_202110_1
* 0.1.2 - 
    * CNIC TX will now pre-fill the TX FIFO after reset has been released and be ready within 1us for transmit.
    * CNIC TX timer has been updated to remove burst behaviour during initial packet play out.
    * Personality register configured to ASCII value of CNIC.
    * Common VHDL source files moved to common repo.
    * U55C 
        * Second 100GbE port enabled with timeslave to allow timing of packets through network switches.
        * PTP for time stamping or scheduling can be sourced from either 100GbE port.
* 0.1.1 - 
    * Initial release